SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO';
SET time_zone = '+00:00';


DELIMITER $$

CREATE  PROCEDURE `procedure_bookingquery_v`(
	IN `act_mode` VARCHAR(50),
	IN `email` VARCHAR(50),
	IN `bookingid` VARCHAR(50)

)
BEGIN
if(act_mode='selectbookingquery') then
select * from  tbl_orderpackage as t join tbl_user b on  b.user_id=t.userid where t.ticketid=bookingid   and  b.user_emailid=email;
end if;
END$$

CREATE  PROCEDURE `procedure_contactus_v`(IN `act_mode` VARCHAR(50), IN `fname` VARCHAR(50), IN `lname` VARCHAR(50), IN `email` VARCHAR(50), IN `phone` VARCHAR(50), IN `message` VARCHAR(50))
BEGIN
if(act_mode='selectcontact')then
insert into tbl_contact (cont_fname,cont_lname,cont_email,cont_phone,cont_message,cont_status,cont_addedon)values(fname,lname,email,phone,message,'1',NOW());
end if;
END$$

CREATE  PROCEDURE `procedure_location`(IN `act_mode` VARCHAR(50), IN `locid` INT)
BEGIN
if(act_mode='s_viewactivityfrontend') then
select * from  tbl_activity as t where  t.activity_location=locid  and t.activity_status=1;
end if;

END$$

CREATE  PROCEDURE `proc_activity_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN
if(act_mode='s_addactivity') then
INSERT INTO `tbl_activity`(`activity_name`, `activity_discription`, `activity_price`, `activity_status`,activity_image,activity_location) VALUES (Param1,Param2,Param3,'1',Param4,Param5);
end if;


if(act_mode='s_addactivity_update') then
update tbl_activity as t set t.activity_name=Param1, t.activity_discription=Param2,t.activity_price=Param3,t.activity_modifiedon=NOW() where t.activity_id=Param4;
end if;


if(act_mode='s_viewactivity') then
select * from  tbl_activity as t where t.activity_status=1;
end if;


if(act_mode='delete_activity') then
update tbl_activity as t set t.activity_status='2' where activity_id=Param1;
end if;



END$$

CREATE  PROCEDURE `proc_addon`(IN `act_mode` INT, IN `branch_id` INT)
BEGIN
if(act_mode='addonscartses_package')then
select * from tbl_addon  where tbl_addon.addon_branchid=branch_id and addon_status=1 ;
end if;
if(act_mode='addonscart_package')then
select * from tbl_addon  where tbl_addon.addon_branchid=addon_id  and addon_status=1;
end if;
END$$

CREATE  PROCEDURE `proc_addon_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN
if(act_mode='addbranchAddon')then
INSERT INTO `tbl_addon`(`addon_name`, `addon_price`, `addon_status`,`addon_branchid`, `addon_description`,addon_image) VALUES (Param1,Param4,'1',Param2,Param3,Param5);
end if;

if(act_mode='s_viewaddon')then
select distinct(t.addon_name) from tbl_addon as t  where t.addon_status in (0,1);
end if;

if(act_mode='s_viewaddon_admin')then
select * from tbl_addon as t  where t.addon_status in (0,1);
end if;


if(act_mode='s_deleteaddon_admin')then
update tbl_addon as t set t.addon_status=2 where t.addon_id=Param1;
end if;

if(act_mode='s_statusaddon_admin')then
update tbl_addon as t set t.addon_status=Param2 where t.addon_id=Param1;
end if;

if(act_mode='addonviewupdate')then
select * from tbl_addon as t  where t.addon_id=Param1;
end if;


if(act_mode='addbranchAddon_update')then
update tbl_addon as t set t.addon_name=Param1,t.addon_price=Param4,t.addon_description=Param3,t.addon_modifiedon=now(),t.addon_branchid=Param5  where t.addon_id=Param2;
end if;

if(act_mode='addbranchAddon_update_pic')then
update tbl_addon as t set t.addon_name=Param1,t.addon_price=Param4,t.addon_description=Param3,t.addon_modifiedon=now(),t.addon_image=Param5,t.addon_branchid=Param6  where t.addon_id=Param2;
end if;

END$$

CREATE  PROCEDURE `proc_Adminlogin`(IN `useremail` VARCHAR(255), IN `userpassword` VARCHAR(255), IN `act_mode` VARCHAR(250), IN `row_id` INT)
begin
if(act_mode='login')then
set @coun = (select count(*) from tbl_adminlogin where `UserName`=useremail and `Password`=userpassword and EmpStatus='A');
if(@coun > 0) then
select LoginID,UserName from tbl_adminlogin where `UserName`=useremail and `Password`=userpassword and EmpStatus='A' limit 0,1;
else 
select emp_main_id as LoginID,emp_email as UserName,emp_tbl_id as EmployeeId,emp_location as LocationId,(select t.branch_id from tbl_branch_master as t where tbl_employee.emp_location = t.branch_location limit 0,1) as branchid1,(select tloc.cityid from tbl_location as tloc where tbl_employee.emp_location = tloc.locationid limit 0,1) as cityid,(select tloc.stateid from tbl_location as tloc where tbl_employee.emp_location = tloc.locationid limit 0,1) as stateid,(select tloc.countryid from tbl_location as tloc where tbl_employee.emp_location = tloc.locationid limit 0,1) as countryid, tbl_employee.emp_branch_id as branchid from tbl_employee where `emp_email`=useremail and `emp_password`=userpassword and emp_status='1' limit 0,1;
end if ;
end if;
end$$

CREATE  PROCEDURE `proc_adminpasswordcheck`(IN `act_mode` VARCHAR(100), IN `n_email` VARCHAR(100), IN `oldpassword` VARCHAR(200), IN `row_id` INT)
begin
declare adminemailcount  INT;
if(act_mode='updatepassword') then
update tbl_adminlogin set `Password`=oldpassword where tbl_adminlogin.UserName=n_email and tbl_adminlogin.LoginID=row_id;
set adminemailcount=0;
select  adminemailcount;
end if;
if(act_mode='checkpassword') then
set adminemailcount = (select count(UserName) as email   from tbl_adminlogin where tbl_adminlogin.UserName=n_email and `Password`=oldpassword and LoginID=row_id);
select  adminemailcount;
end if;

end$$

CREATE  PROCEDURE `proc_agentorder`(
	IN `act_mode` VARCHAR(50),
	IN `orderid` INT,
	IN `tracking_id` VARCHAR(50),
	IN `order_status` VARCHAR(50),
	IN `status_message` TEXT,
	IN `paymentmode` VARCHAR(50),
	IN `paymentstatus` VARCHAR(50),
	IN `ordersucesmail` VARCHAR(50),
	IN `ordermailstatus` VARCHAR(50),
	IN `agent_id` INT,
	IN `cred_debet` VARCHAR(50),
	IN `amount` BIGINT
,
	IN `bankagentcommision` VARCHAR(50)

)
BEGIN
if(act_mode='orderwalletupdatesucess') then
insert into tbl_agent_payment_history(ph_agent_id,ph_cred_debet_type,ph_amount,ph_timing,ph_orderid,ph_tracking_id,ph_order_status,ph_status_message,ph_paymentmode,ph_paymentstatus,ph_ordersucesmail,ph_ordermailstatus,ph_agentdiscount) values(agent_id,cred_debet,amount,NOW(),orderid,tracking_id,order_status,status_message,paymentmode,paymentstatus,ordersucesmail,ordermailstatus,bankagentcommision);

update tbl_agent_reg as t  set t.bank_creditamount=t.bank_creditamount-amount where t.agent_id=agent_id;

end if;

if(act_mode='selectwalletdata') then
select count(ph_agent_id) as agid from tbl_agent_payment_history;
end if;
if(act_mode='orderwalletselectagentamount') then
select *  from tbl_agent_payment_history  a join tbl_agent_reg b on a.ph_agent_id
=b.agent_id where a.ph_agent_id=agent_id ;
end if;

END$$

CREATE  PROCEDURE `proc_agent_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
    COMMENT 'agent history'
BEGIN

if(act_mode='agentamount') then
select * from tbl_agent_payment_history as t  join tbl_agent_reg as tr on t.ph_agent_id=tr.agent_id where t.ph_agent_id=Param1 order by t.ph_id desc ;
end if;

if(act_mode='agentamount_add') then
update tbl_agent_reg as th set th.bank_creditamount = (th.bank_creditamount + Param2 ) where th.agent_id=Param1;
#set @amt = ( select tr.bank_creditamount from tbl_agent_reg as tr where tr.agent_id=Param1);
#set @comission = ( select tr.bank_agentcommision from tbl_agent_reg as tr where tr.agent_id=Param1);
INSERT INTO `tbl_agent_payment_history`(`ph_agent_id`, `ph_cred_debet_type`,`ph_amount`, `ph_timing`, `ph_agentdiscount`) values (Param1,'Credit',Param2,now(),'0'); 
end if;

if(act_mode='agentamount_sub') then
update tbl_agent_reg as th set th.bank_creditamount = (th.bank_creditamount - Param2 ) where th.agent_id=Param1;
#set @amt = ( select tr.bank_creditamount from tbl_agent_reg as tr where tr.agent_id=Param1);
#set @comission = ( select tr.bank_agentcommision from tbl_agent_reg as tr where tr.agent_id=Param1);
INSERT INTO `tbl_agent_payment_history`(`ph_agent_id`, `ph_cred_debet_type`,`ph_amount`, `ph_timing`, `ph_agentdiscount`) values (Param1,'Debet',Param2,now(),'0'); 
end if;


if(act_mode='agentamount_comission') then
update tbl_agent_reg as th set th.bank_agentcommision=Param2 where th.agent_id=Param1;
end if;




END$$

CREATE  PROCEDURE `proc_assignrole`(IN `p_role` VARCHAR(512), IN `p_user` VARCHAR(512), IN `p_menu` VARCHAR(512), IN `p_submenu` VARCHAR(512), IN `act_mode` VARCHAR(512), IN `start` VARCHAR(512), IN `total_rows` VARCHAR(512))
begin 
if(act_mode='insert') then
insert into tblassignrole(role_id,emp_id,main_menu,sub_menu) values(p_role,p_user,p_menu,p_submenu);
end if;
if(act_mode='viewemployee') then
select tm.*,tr.rolename from tblemployee as tm inner join tblrole as tr on tm.Role=tr.id where tm.t_status='Active';	
end if;
if(act_mode='viewsalesemployee') then
select tm.*,tr.rolename from tblemployee as tm inner join tblrole as tr on tm.Role=tr.id where tm.t_status='Active' and tm.Role='15';	
end if;
if(act_mode='assign') then
select trole.rolename,temp.F_Name,temp.L_Name,temp.ContactNo,temp.Email,tm.menuname,tass.sub_menu from tblrole as trole inner join tblemployee as temp on trole.id=temp.Role inner join tblassignrole as tass on trole.id=tass.role_id inner join tblmenu as tm on tass.main_menu=tm.id;
end if;
if(act_mode='editemployee') then 
select tm.*,tr.rolename from tblemployee as tm inner join tblrole as tr on tm.Role=tr.id where tm.EmployeeID=p_user;	
end if;
if(act_mode='viewempmanu') then 
select tm.firstname,tm.lastname from tbl_manufacturermaster as tm where tm.id=p_user;
end if;
if(act_mode='viewexistemployee') then
select * from tbl_manufacturermaster where tbl_manufacturermaster.vendortype='retail' and tbl_manufacturermaster.rstatus!='D' and FIND_IN_SET(tbl_manufacturermaster.id, p_submenu) order by tbl_manufacturermaster.firstname;
end if;
if(act_mode='fetchempretailers') then
select * from tblemployee as e where e.EmployeeID=p_user;
end if;
if(act_mode='fetchemployeedetail') then
select e.F_Name,e.L_Name,e.Email from tblemployee as e where e.EmployeeID=p_user;
end if;
if(act_mode='fetchretailerdetail') then
select * from tbl_manufacturermaster as m where m.id=p_user;
end if;

if(act_mode='fetchtotalorderamt') then
select * from tbl_incentive_logs as l where date(l.incentive_createdon) between p_role and p_submenu and l.retailer_id=p_user;
end if;
if(act_mode='fetchincentivestatus') then
select * from tbl_incentive_logs as l where date(l.incentive_createdon) between p_role and p_submenu and l.retailer_id in (p_menu);
end if;
if(act_mode='changeincentivestatus') then
update tbl_incentive_logs as l set l.incentive_status=`start` where date(l.incentive_createdon) between p_role and p_submenu  
and find_in_set(l.retailer_id,p_menu);
end if;
if(act_mode='fetchempincentives') then
select * from tbl_incentive_slabs as s where total_rows>=s.range_from and total_rows<=s.range_to and p_submenu>=s.range_salaryFrom and p_submenu<=s.range_salaryTo and s.range_status='1';
end if;
if(act_mode='viewincentiveslabs') then
select * from tbl_incentive_slabs;
end if;
if(act_mode='insertincentiveslabs') then
insert into tbl_incentive_slabs(range_from,range_to,range_type,range_incentive,range_salaryFrom,range_salaryTo) 
values(p_user,p_menu,p_submenu,total_rows,p_role,`start`);
end if;
if(act_mode='updateincentiveslab') then
update tbl_incentive_slabs as s set s.range_from=p_user,s.range_to=p_menu,s.range_salaryFrom=p_submenu,s.range_incentive=total_rows,s.range_salaryTo=`start`
where s.range_id=p_role;
end if;
if(act_mode='deleteincentiveslab') then
delete from tbl_incentive_slabs where range_id=p_role;
end if;
if(act_mode='fetchslabdetail') then
select * from tbl_incentive_slabs where range_id = p_user;
end if;
if(act_mode='remexistretail') then
update tblemployee set e_retailid=p_menu where EmployeeID=p_user;
end if;
end$$

CREATE  PROCEDURE `proc_bookingtype_v`(IN `act_mode` VARCHAR(50), IN `countryid` VARCHAR(50), IN `stateid` VARCHAR(50), IN `cityid` INT, IN `branch_id` INT, IN `locationid` INT)
BEGIN
if(act_mode='selectbookingtype') then
select bookingtypeid,bookingname  from  tbl_bookingtypemaster  where booking_branchid=branch_id and booking_status=1;
end if;


if(act_mode='viewbookingtype_admin') then
select *  from  tbl_bookingtypemaster as t where t.booking_status in (0,1) ;
end if;


if(act_mode='addbookingtype_admin') then
insert into tbl_bookingtypemaster (bookingname,booking_branchid) values (stateid,countryid);
end if;

if(act_mode='deletebookingtype_admin') then
update tbl_bookingtypemaster as t set t.booking_status=2 where t.bookingtypeid=countryid;
end if;


END$$

CREATE  PROCEDURE `proc_branch_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN
if(act_mode='s_addbranch') then
INSERT INTO `tbl_branch_master`( `branch_name`, `branch_company`, `branch_location`, `branch_status`,branch_logo,branch_background,branch_internet_handling_charge,branch_url,branch_add)  VALUES (Param3,Param2,Param1,'1',Param4,Param5,Param6,Param7,Param8);
end if;

if(act_mode='s_viewbranch') then
select * from  tbl_branch_master as t where t.branch_status in (0,1);
end if;

if(act_mode='s_viewbranch_update') then
select * from  tbl_branch_master as t where t.branch_status in (0,1) and t.branch_id=Param1;
end if;

if(act_mode='branchstatus') then
update  tbl_branch_master as t set t.branch_status=Param2 where t.branch_id=Param1;
end if;

if(act_mode='delete_branch') then
update tbl_branch_master as t set t.branch_status='2' where t.branch_id=Param1;
end if;


if(act_mode='s_updatebranch') then
update tbl_branch_master as t set t.branch_location=Param1, t.branch_company=Param2 , t.branch_name=Param3 ,t.branch_modified=NOW() where t.branch_id=Param4;
end if;


if(act_mode='s_addbranch_update') then
update tbl_branch_master as t set t.branch_location=Param1, t.branch_company=Param2 , t.branch_name=Param3 ,t.branch_modified=NOW(),t.branch_background=Param5,t.branch_internet_handling_charge=Param6,t.branch_url=Param7,t.branch_add=Param8 where t.branch_id=Param9;
end if;

END$$

CREATE  PROCEDURE `proc_cartaddons_v`(IN `act_mode` VARCHAR(50), IN `branchid` INT, IN `addonid` INT, IN `addonqty` INT)
BEGIN
if(act_mode='addonscart_package') then
set @addqty=addonqty;
select addon_id,addon_name,addon_price,addon_image,@addqty as addonequantity from tbl_addon where addon_id=addonid ;
end if;
END$$

CREATE  PROCEDURE `proc_ccavenue_v`(IN `act_mode` VARCHAR(50), IN `branchid` VARCHAR(50))
BEGIN
if(act_mode='selectccavenue') then

select * from tbl_payment_gatway where pg_branchid=branchid and pg_status=1 order by pg_id desc limit 0,1 ;
end if;
END$$

CREATE  PROCEDURE `proc_changePassword`(IN `UserId` BIGINT(20), IN `newpassword` VARCHAR(200))
BEGIN
 
 UPDATE tbl_manufacturermaster SET `password` = newpassword WHERE id = UserId;
select 'success';	
end$$

CREATE  PROCEDURE `proc_country_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN

if(act_mode='s_addcompany') then
INSERT INTO `tbl_company_master`(`comp_name`, `comp_status`) VALUES (Param1,'1');
end if;

if(act_mode='s_updatecompany') then
update tbl_company_master as t set t.comp_name=Param1 where t.comp_id=Param2;
end if;

if(act_mode='s_viewcompany') then
select * from  tbl_company_master as t where t.comp_status=1;
end if;

if(act_mode='s_deletetcompany') then
delete from tbl_company_master where comp_id=Param1;
end if;

END$$

CREATE  PROCEDURE `proc_crone_v`(
	IN `act_mode` VARCHAR(50),
	IN `nextDate` VARCHAR(50),
	IN `starttime` VARCHAR(50)
,
	IN `branchid` VARCHAR(50)

,
	IN `dailyinventory_to` VARCHAR(50),
	IN `dailyinventory_seats` VARCHAR(50),
	IN `dailyinventory_status` VARCHAR(50),
	IN `dailyinventory_createdon` VARCHAR(50),
	IN `dailyinventory_modifiedon` VARCHAR(50),
	IN `dailyinventory_minfrom` VARCHAR(50),
	IN `dailyinventory_minto` VARCHAR(50)

)
BEGIN
if(act_mode='selectdate') then
select * from  tbl_daily_inventory as t where t.dailyinventory_date=nextDate and dailyinventory_from=starttime and dailyinventory_branchid=branchid;
end if;

if(act_mode='insertdate') then

INSERT INTO tbl_daily_inventory(dailyinventory_branchid, dailyinventory_from,dailyinventory_date,dailyinventory_to,dailyinventory_seats,dailyinventory_status,dailyinventory_createdon,dailyinventory_modifiedon,dailyinventory_minfrom,dailyinventory_minto) VALUES (branchid,starttime,nextDate,dailyinventory_to,dailyinventory_seats,dailyinventory_status,dailyinventory_createdon,dailyinventory_modifiedon,dailyinventory_minfrom,dailyinventory_minto);
end if;

if(act_mode='s_viewtimeslotf') then
select * ,(SELECT count(t1.pacorderid) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.dailyinventory_from and t1.departuredate=nextDate and t1.departuretime=starttime and t1.order_status!='Aborted') as coun from  tbl_daily_inventory as t where t.dailyinventory_date=nextDate and dailyinventory_from=starttime and dailyinventory_branchid=branchid;
end if;

END$$

CREATE  PROCEDURE `proc_dashboardview`(IN `act_mode` VARCHAR(50), IN `row_id` INT)
begin
if(act_mode='orderday') then
select *  from tblorder as torder where date(torder.OrderDate)=curdate() order by torder.OrderId desc;

end if;
if(act_mode='pricofday') then
select *  from tblorder as torder where date(torder.OrderDate)=curdate() order by torder.OrderId desc;
end if;

if(act_mode='total_ofmonth') then
select * from tblorder where MONTH(OrderDate)=MONTH(curdate()) order by OrderId desc;
end if;
if(act_mode='order_ofmonth') then
select * from tblorder where MONTH(OrderDate)=MONTH(curdate()) order by OrderId desc; 
end if;
if(act_mode='pending_order') then

select * from tblorder as tor inner join tblorderstatus as ts on tor.OrderId=ts.OrdId
where ts.ShipStatus='pending';  

end if;
if(act_mode='dispatch_order') then

select * from tblorder as tor inner join tblorderstatus as ts on tor.OrderId=ts.OrdId
where ts.ShipStatus='dispatch';   

end if;
if(act_mode='pending_paymentof') then
select * from tblorder where tblorder.PaymentStatus='unpaid' order by OrderId desc;
end if;

if(act_mode='brnad_live') then
select * from (select count(brandid) as brandcount  from tbl_productmapforlisting   group by BrandID)as t; 
end if;

if(act_mode='product_live') then

select * from tbl_product as tpro inner join tbl_productmap as tmap on tpro.proid=
tmap.proid where tmap.pstatus='A';

end if;

end$$

CREATE  PROCEDURE `proc_dashboard_data`(IN `act_mode` VARCHAR(100))
begin
if(act_mode = 'total_order_count') then 
select count(o.OrderId) as total from tblorder as o inner join tbl_manufacturermaster as man on 
man.id=o.UserId  where Date(o.OrderDate)=curdate();
end if;

if(act_mode = 'total_order_cod') then 
select count(o.OrderId) as total from tblorder as o inner join tbl_manufacturermaster as man on man.id=o.UserId  where o.PaymentMode='COD';
end if;

if(act_mode = 'total_order_cheque') then 
	select count(o.UserId) as total from tblorder as o inner join tbl_manufacturermaster as man on man.id=o.UserId  where o.PaymentMode='CHEQUE';
end if;

if(act_mode = 'total_order_online') then 
	select count(o.UserId) as total from tblorder as o inner join tbl_manufacturermaster as man on man.id=o.UserId  where o.PaymentMode='ONLINE';
end if;

if(act_mode = 'total_order_credit') then 
	select count(o.UserId) as total from tblorder as o inner join tbl_manufacturermaster as man on man.id=o.UserId  where o.PaymentMode='CREDIT';
end if;

if(act_mode = 'total_orders') then 
	select * from tblorder o order by o.OrderId desc limit 0,5;
end if;

if(act_mode = 'manu_count') then 
	select count(*) as total from tbl_manufacturermaster r where r.vendortype='manufacturer';
end if;

if(act_mode = 'manu_list') then 
	select *  from tbl_manufacturermaster r where r.vendortype='manufacturer' order by r.id desc limit 0,5;
end if;

if(act_mode = 'retail_count') then 
	select count(*) as total from tbl_manufacturermaster r where r.vendortype='retail';
end if;

if(act_mode = 'consumer_count') then 
  select count(*) as total from tbl_manufacturermaster as man inner join tbl_state as st on st.stateid=man.stateid inner join tbl_city as ct on ct.cityid=man.city where man.vendortype='consumer' and man.rstatus='A';
end if;

if(act_mode = 'brand_count') then 
	select count(*) as total from tbl_brand r;
end if;
if(act_mode='order_ofmonth') then
select count(o.OrderId) as total from tblorder as o inner join tbl_manufacturermaster as man on 
man.id=o.UserId  where MONTH(o.OrderDate) = MONTH(CURRENT_DATE) AND YEAR(o.OrderDate) = YEAR(CURRENT_DATE);
end if;

if(act_mode='total_ofmonth') then
select sum(o.TotalAmt) as total from tblorder as o inner join tbl_manufacturermaster as man on 
man.id=o.UserId  where MONTH(o.OrderDate) = MONTH(CURRENT_DATE) AND YEAR(o.OrderDate) = YEAR(CURRENT_DATE);
end if;

if(act_mode='pending_order') then
select count(distinct(tos.OrdId)) as total from tblorder as o inner join tblorderstatus as tos on 
tos.OrdId=o.OrderId inner join tbl_manufacturermaster as man on 
man.id=o.UserId where tos.ShipStatus='pending' and tos.IsActive=1;
end if;

if(act_mode='dispatch_order') then
select count(distinct(tos.OrdId)) as total from tblorder as o inner join tblorderstatus as tos on 
tos.OrdId=o.OrderId inner join tbl_manufacturermaster as man on 
man.id=o.UserId where tos.ShipStatus='dispatch' and tos.IsActive=1;
end if;

if(act_mode='pending_payment') then

select count(*) as total from tblorderstatus where tblorderstatus.ShipStatus='pending';  

end if;
if(act_mode='product_live') then

select count(t.idcount) as total from (select count(id) as idcount  
from tbl_productmapforlisting   group by SKUNO)as t; 

end if;

if(act_mode='brnad_live') then
select count(distinct(t.BrandID)) as total from tbl_productmapforlisting as t;
end if;

if(act_mode='pric_ofday') then
select sum(o.TotalAmt) as total from tblorder as o inner join tbl_manufacturermaster as man on 
man.id=o.UserId  where Date(o.OrderDate)=curdate() order by o.OrderId desc;
end if;

if(act_mode='pending_paymentof') then
select count(o.TotalAmt) as total from tblorder as o inner join tbl_manufacturermaster as man on 
man.id=o.UserId  where o.PaymentStatus='unpaid';
end if;


end$$

CREATE  PROCEDURE `proc_forgetpassword`(IN `act_mode` VARCHAR(50), IN `email` VARCHAR(50), IN `Param3` VARCHAR(50))
BEGIN
if(act_mode = 'forgetpassword') then 
select * from tbl_user where user_emailid=email and user_usertype='User';
end if;

if(act_mode = 'forgetpassword_newpass') then 
update tbl_user as t SET t.user_password=Param3 where t.user_emailid=email;
end if;


if(act_mode = 'forgetpassword_newpassagent') then 

update tbl_agent_reg as t SET t.agent_password=Param3 where t.agent_username=email;


end if;

if(act_mode = 'forgetpasswordagent') then 
select * from tbl_agent_reg where agent_username=email;
end if;

END$$

CREATE  PROCEDURE `proc_frontuser_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN

if(act_mode = 's_viewfrontuser') then 
select * from tbl_user order by user_id desc;
end if;

END$$

CREATE  PROCEDURE `proc_geographic`(IN `act_mode` VARCHAR(100), IN `row_id` INT, IN `counname` VARCHAR(100), IN `coucode` VARCHAR(50), IN `commid` INT)
begin

if(act_mode='getstate')then
select * from tbl_state as t where t.stateid=row_id and t.isdeleted!='2';
end if;

if(act_mode='getcity')then
select * from tbl_city as t where t.cityid=row_id and t.cstatus!='2';
end if;

if(act_mode='locationdelete')then
update tbl_location as t set t.lstatus='2' where t.locationid=row_id;  
end if;

if(act_mode='de_to_act')then
update tbl_location as t set t.lstatus='1' where t.locationid=row_id;  
end if;

if(act_mode='act_to_de')then
update tbl_location as t set t.lstatus='0' where t.locationid=row_id;  
end if;

if(act_mode='viewlocation')then
select * from tbl_location as t where t.lstatus in(0,1);  
end if;

if(act_mode='countryinsert')then
insert into tbl_country (name,code,counstatus,createdon)values(counname,coucode,'1',NOW());
end if;
if(act_mode='viewcountry')then
select * from tbl_country as tc where tc.counstatus in (0,1) order by tc.conid desc;
end if;
if(act_mode='viewcounid')then
select * from tbl_country where tbl_country.conid=row_id;
end if;
if(act_mode='countryupdate')then
update tbl_country set name=counname,code=coucode,modifiedon=NOW() where conid=row_id ;
end if;
if(act_mode='countrydelete')then
update tbl_country as tc set tc.counstatus='2' where tc.conid=row_id;
end if;
if(act_mode='activeandinactive') then
update tbl_country set tbl_country.counstatus='0' where tbl_country.conid=row_id;
end if;
if(act_mode='inactiveandinactive') then
update tbl_country set tbl_country.counstatus='1' where tbl_country.conid=row_id;
end if;
if(act_mode='checkcoun') then
select  count(*) as count from tbl_country where tbl_country.name=counname;
end if;
if(act_mode='countrystate')then
select * from tbl_state where tbl_state.countryid=row_id and tbl_state.isdeleted=1 order by tbl_state.statename;
end if;
if(act_mode='cityinsert')then
insert into tbl_city(stateid,cityname,cstatus,createdon) values (row_id,counname,'1',now());
end if;
if(act_mode='viewcity')then
if(commid='')then
select c.*,s.statename from tbl_city as c inner join tbl_state as s on s.stateid=c.stateid where c.cstatus in(0,1);
else
select c.*,s.statename from tbl_city as c inner join tbl_state as s on s.stateid=c.stateid where c.cstatus in(0,1) limit row_id,commid;
end if;
end if;
if(act_mode='citydelete')then
update tbl_city as t set t.cstatus='2' where t.cityid=row_id;
end if;
if(act_mode='activecity') then
update tbl_city set tbl_city.cstatus='0' where tbl_city.cityid=row_id;
end if;
if(act_mode='inactivecity') then
update tbl_city set tbl_city.cstatus='1' where tbl_city.cityid=row_id;
end if;
if(act_mode='viewcityid')then
select * from tbl_city where tbl_city.cityid=row_id;
end if;
if(act_mode='cityupdate')then
update tbl_city as t set t.stateid=coucode,t.cityname=counname,t.modifiedon=NOW() where cityid=row_id ;
end if;
if(act_mode='checkcitycoun') then
select  count(*) as count from tbl_city where tbl_city.cityname=counname;
end if;
if(act_mode='stateexist')then
select count(st.statename) as statecount from tbl_state as st where st.countryid=row_id and st.statename=counname;
end if;
if(act_mode='stateinsert')then
insert into tbl_state (countryid,statename,statecode,isdeleted,createdon) values (row_id,counname,coucode,'1',NOW());
end if;
if(act_mode='viewstate')then
select st.*,cun.name from tbl_state as st inner join tbl_country as cun on st.countryid=cun.conid where st.isdeleted in (0,1);
end if;
if(act_mode='statedelete')then
delete from tbl_state where tbl_state.stateid=row_id;
end if;
if(act_mode='stateactiveandinactive') then
update tbl_state as t set t.isdeleted='0',t.modifiedon=now() where t.stateid=row_id;
end if;
if(act_mode='stateinactiveandinactive') then
update tbl_state as t set t.isdeleted='1',t.modifiedon=now() where t.stateid=row_id;
end if;
if(act_mode='vieweditstate')then
select st.*,cun.name from tbl_state as st 
inner join tbl_country as cun on st.countryid=cun.conid where st.stateid=row_id;
end if;
if(act_mode='stateupdate') then
update tbl_state set tbl_state.statename=counname,tbl_state.statecode=coucode,tbl_state.modifiedon=now(),tbl_state.countryid=commid where tbl_state.stateid=row_id;
end if;
if(act_mode='citystate')then
select * from tbl_city where tbl_city.stateid=row_id order by tbl_city.cityname;
end if;
if(act_mode='citymasterview')then
select * from tbl_citymaster;
end if;
if(act_mode='citygview')then
select * from tbl_city where tbl_city.cstatus!='D';
end if;
if(act_mode='citygcheck')then
select count(ct.cgid) as citycount from tbl_citygroup as ct where  ct.cityidd=commid and ct.stateid=counname;
end if;
if(act_mode='cityginsert')then
insert into tbl_citygroup (cmid,cityidd,stateid,cstatus)values(row_id,commid,counname,'A');
end if;
if(act_mode='citgview')then
if(commid='')then
select cm.mastname,c.cityname,cg.cgid from tbl_citygroup as cg inner join tbl_citymaster as cm on cg.cmid=cm.cmasterid inner join tbl_city as c on cg.cityidd=c.cityid;
else
select cm.mastname,c.cityname,cg.cgid from tbl_citygroup as cg inner join tbl_citymaster as cm on cg.cmid=cm.cmasterid inner join tbl_city as c on cg.cityidd=c.cityid limit row_id, commid;
end if;
end if;
if(act_mode='cgdelete')then
delete from tbl_citygroup where tbl_citygroup.cgid=row_id;
end if;
if(act_mode='agactive')then
update tbl_citygroup set cstatus='D' where cgid=row_id;
end if;
if(act_mode='aginactive')then
update tbl_citygroup set cstatus='A' where cgid=row_id;
end if;
end$$

CREATE  PROCEDURE `proc_getAdminMenu`(IN `loginid` INT)
Begin
select tm.menuname,tm.id,tm.`position` from tbladminmenu as tm where tm.parentid=0 and tm.r_status='Active' order by amlevel;
End$$

CREATE  PROCEDURE `proc_getAssignRole`(IN `empid` INT(10))
begin
select main_menu,sub_menu from tblassignrole where emp_id=empid;
end$$

CREATE  PROCEDURE `proc_getPassword`(IN `UserId` BIGINT(20), IN `old_password` VARCHAR(100))
BEGIN
 select count(`password`) as totalmatch  from tbl_manufacturermaster WHERE id = UserId and `password`= `old_password`;
 end$$

CREATE  PROCEDURE `proc_getseo`(IN `act_mode` VARCHAR(100), IN `p_catid` INT(10), IN `p_extra` VARCHAR(100))
begin
if(act_mode= 'cat') THEN

SELECT TC.catid,TC.metatitle,TC.metadesc,TC.metakeywords  FROM tbl_categorymaster AS  TC  WHERE TC.catid = p_catid; 

END IF;

if(act_mode= 'product') THEN

SELECT TP.proid,TP.metatitle as metatitle,TP.metakeywords,TP.metadescription metadesc  FROM tbl_product AS TP WHERE TP.proid =p_catid; 

END IF;

end$$

CREATE  PROCEDURE `proc_groupbookinginsert_v`(IN `act_mode` VARCHAR(50), IN `bookingType` VARCHAR(50), IN `full_name` VARCHAR(50), IN `email_address` VARCHAR(50), IN `phone_number` INT, IN `departDate` VARCHAR(50), IN `sessionTime` VARCHAR(50), IN `number_people` INT, IN `socialGroupRadio` VARCHAR(50), IN `socialGroupName` VARCHAR(50), IN `stateid` VARCHAR(50), IN `cityid` INT, IN `branch_id` INT, IN `locationid` INT, IN `countryid` INT)
BEGIN
if(act_mode='insertgroups') then
INSERT INTO `tbl_groupbooking`(`bookingType`, `full_name`, `email_address`,`phone_number`,`departDate`, `sessionTime`, `number_people`,`socialGroupRadio`, `socialGroupName`, `addeddate`,`status`, `stateid`, `cityid`,`branch_id`, `locationid`,`countryid`) VALUES (bookingType,full_name,email_address,phone_number,departDate,sessionTime,number_people,socialGroupRadio,socialGroupName,NOW(),'1',stateid,cityid,branch_id,locationid,countryid);

select last_insert_id() as pack_id;
end if;
END$$

CREATE  PROCEDURE `proc_guestorderupdate_v`(IN `act_mode` VARCHAR(50), IN `user_id` VARCHAR(50), IN `order_id` VARCHAR(50), IN `title` VARCHAR(50), IN `billing_name` VARCHAR(50), IN `billing_email` VARCHAR(50), IN `billing_tel` VARCHAR(50), IN `billing_address` TEXT, IN `billing_city` VARCHAR(50), IN `billing_state` VARCHAR(50), IN `billing_zip` VARCHAR(50), IN `billing_country` VARCHAR(50))
BEGIN
if(act_mode = 'orderguestupdate') then
update tbl_orderpackage as t set t.userid=user_id,t.op_usertype='Guest',
t.title=title, t.billing_name=billing_name, t.billing_email=billing_email,
t.billing_tel=billing_tel, t.billing_address=billing_address,
t.billing_city=billing_city , t.billing_state=billing_state,
t.billing_zip=billing_zip ,t.billing_country=billing_country 
where t.pacorderid=order_id;

end if;

if(act_mode = 'orderuserupdate') then
update tbl_orderpackage set userid=user_id,op_usertype='User', title=title, billing_name=billing_name, billing_email=billing_email, billing_tel=billing_tel, billing_address=billing_address,  billing_city=billing_city , billing_state=billing_state, billing_zip=billing_zip ,billing_country=billing_country where pacorderid=order_id;
end if;
if(act_mode = 'orderagentupdate') then
update tbl_orderpackage set userid=user_id,op_usertype='Agent', title=title, billing_name=billing_name, billing_email=billing_email, billing_tel=billing_tel, billing_address=billing_address,  billing_city=billing_city , billing_state=billing_state, billing_zip=billing_zip ,billing_country=billing_country where pacorderid=order_id;
end if;


END$$

CREATE  PROCEDURE `proc_guestregister_v`(IN `act_mode` VARCHAR(50), IN `title` VARCHAR(50), IN `billing_name` VARCHAR(50), IN `billing_email` VARCHAR(50), IN `billing_tel` VARCHAR(50), IN `billing_address` TEXT, IN `billing_city` VARCHAR(50), IN `billing_state` VARCHAR(50), IN `billing_zip` VARCHAR(50), IN `billing_country` VARCHAR(50))
BEGIN
if(act_mode = 'guestregister') then
INSERT INTO `tbl_user`(`user_title`, `user_firstname`, `user_emailid`, `user_mobileno`, `user_Address`, `user_city`, `user_state`, `user_zip`, `user_country`,`user_usertype`) 
VALUES (title,billing_name,billing_email,billing_tel,billing_address,billing_city,billing_state,billing_zip,billing_country,'Guest');
select last_insert_id() as ls;


end if;
END$$

CREATE  PROCEDURE `proc_inventory_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
    COMMENT 'inventory '
BEGIN

if(act_mode='promo_view') then
select t.dailyinventory_seats as total_Seat,t.dailyinventory_from as tfrom from  tbl_daily_inventory as t where t.dailyinventory_branchid=Param1 and t.dailyinventory_status=1 and t.dailyinventory_date=Param2;
end if;

if(act_mode='promo_view1') then
select t.timeslot_seats as total_Seat,t.timeslot_from as tfrom from  tbl_timeslot_branch as t where t.timeslot_branchid=Param1 and t.timeslot_status=1;
end if;

if(act_mode='order_addon_view') then
select group_concat(t.packpkg) as package_qty,group_concat(t.departuretime) as package_time from tbl_orderpackage as t where t.branch_id=Param1 and t.departuredate=Param2 and order_status in ('Shipped','Success');
end if;

if(act_mode='insert_dailydate') then
update tbl_daily_inventory as t set t.dailyinventory_seats=Param2 where t.dailyinventory_id=Param1;
end if;


END$$

CREATE  PROCEDURE `proc_leftMenu`(IN `userid` INT(10), IN `moduleid` INT)
begin
select tm.menuname,tm.id,tm.url from tbladminmenu as tm where tm.parentid=moduleid and tm.r_status='Active';


end$$

CREATE  PROCEDURE `proc_location_v`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN
if(act_mode='vieweditlocation')then
select * from tbl_location where locationid=Param3;
end if;
if(act_mode='locationupdatedata') then
update tbl_location set tbl_location.locationname=Param1,tbl_location.totalseatperslot=Param2,tbl_location.modifiedon=now() where tbl_location.locationid=Param3;
end if;
if(act_mode='locationdelete')then
delete from tbl_location where tbl_location.locationid=Param3;
end if;
if(act_mode='viewlocation')then
select * from tbl_location order by locationname;
end if;
if(act_mode='viewbranch')then
select * from tbl_branch_master  where tbl_branch_master.branch_location=Param1;
end if;
if(act_mode='viewactivity_package')then
select * from tbl_package_master a join tbl_branch_package b where a.package_id=b.bp_packageid and a.package_status='1' and b.bp_branchid=Param1 ;
end if;
if(act_mode='ticketcart_package')then
select * from tbl_package_master  where tbl_package_master.package_id=Param1 ;
end if;
if(act_mode='addonscart_package')then
select * from tbl_addon  where tbl_addon.addon_branchid=Param1 ;
end if;

if(act_mode='addonscartses_package')then
select * from tbl_addon  where tbl_addon.addon_id=Param1 ;
end if;






if(act_mode='getcity')then
select * from tbl_city as t where t.cityid=Param1 and t.cstatus!='2';
end if;

if(act_mode='loc_update_modal')then
update tbl_location as t set t.cityid=Param2, t.locationname=Param3,t.modifiedon=now() where t.locationid=Param1;
end if;


if(act_mode='citylocation') then
select * from  tbl_location as t where t.cityid=Param1 and t.lstatus=1;
end if;

if(act_mode = 'insert_promo') then
INSERT INTO `tbl_promo`(`promo_id`, `p_locationid`, `p_codename`, `p_type`, `p_discount`, `p_status`, `p_createdon`, `p_modifiedon`) VALUES ('',Param1,Param2,Param3,Param4,'1','','');
end if;

if(act_mode='view_promo') then
select * from  tbl_promo as t where t.p_status=1;
end if;

if(act_mode='delete_promo') then
delete from tbl_promo where promo_id=Param1;
end if;

if(act_mode='getlocation') then
select * from tbl_location as tl where tl.locationid=Param1;
end if;

if(act_mode='getcompany') then
select * from tbl_company_master as t where t.comp_id=Param1;
end if;

if(act_mode='s_addactivity') then
INSERT INTO `tbl_activity`(`activity_name`, `activity_discription`, `activity_price`, `activity_status`) VALUES (Param1,Param2,Param3,'1');
end if;

if(act_mode='s_viewactivity') then
select * from  tbl_activity as t where t.activity_status=1;
end if;

if(act_mode='delete_activity') then
update tbl_activity as t set t.activity_status='2' where activity_id=Param1;
end if;



if(act_mode='delete_package') then
update tbl_package_master as t set t.package_status='2' where t.package_id=Param1;
end if;



if(act_mode='s_addcompany') then
INSERT INTO `tbl_company_master`(`comp_name`, `comp_status`) VALUES (Param1,'1');
end if;
if(act_mode='s_viewcompany') then
select * from  tbl_company_master as t where t.comp_status=1;
end if;
if(act_mode='s_deletetcompany') then
delete from tbl_company_master where comp_id=Param1;
end if;
if(act_mode='s_updatecompany') then
update tbl_company_master as t set t.comp_name=Param1 where t.comp_id=Param2;
end if;


if(act_mode='s_addbranch') then
INSERT INTO `tbl_branch_master`( `branch_name`, `branch_company`, `branch_location`, `branch_status`)  VALUES (Param3,Param2,Param1,'1');
end if;
if(act_mode='s_viewbranch') then
select * from  tbl_branch_master as t where t.branch_status=1;
end if;

if(act_mode='delete_branch') then
update tbl_branch_master as t set t.branch_status='2' where t.branch_id=Param1;
end if;

if(act_mode='getbranch_update') then
select *  from tbl_branch_master as t where t.branch_id=Param1;
end if;

if(act_mode='getlocation_update') then
select *  from tbl_location as t where t.lstatus=1;
end if;


if(act_mode='s_updatebranch') then
update tbl_branch_master as t set t.branch_location=Param1, t.branch_company=Param2 , t.branch_name=Param3 ,t.branch_modified=NOW() where t.branch_id=Param4;
end if;

if(act_mode='s_addactivity_update') then
update tbl_activity as t set t.activity_name=Param1, t.activity_discription=Param2,t.activity_price=Param3,t.activity_modifiedon=NOW() where t.activity_id=Param4;
end if;


if(act_mode='s_viewpackage') then
select t.*,(select group_concat(tbp.bp_branchid) from tbl_branch_package as tbp where tbp.bp_packageid=t.package_id) as bp_id,(select group_concat(tap.ap_activityid) from tbl_activity_package as tap where tap.ap_packageid=t.package_id) as ap_id from tbl_package_master as t where t.package_status=1;
end if;


if(act_mode='s_addpackage_master') then
INSERT INTO `tbl_package_master`(`package_name`, `package_description`, `package_status`,`package_price`) VALUES (Param1,Param2,'1',Param3);
select last_insert_id() as pack_id;
end if;


if(act_mode='s_addpackage_branch') then
INSERT INTO `tbl_branch_package`(`bp_packageid`, `bp_branchid`, `bp_status`) VALUES (Param1,Param2,'1');
end if;

if(act_mode='s_addpackage_activity') then
INSERT INTO `tbl_activity_package`(`ap_packageid`, `ap_activityid`, `ap_status`) VALUES (Param1,Param2,'1');
end if;

if(act_mode='activityPrice') then
select ta.activity_price  from tbl_activity as ta where ta.activity_id=Param1;
end if;

if(act_mode='getbranch')then
select * from tbl_branch_master  where tbl_branch_master.branch_id=Param1;
end if;

if(act_mode='getactivity')then
select * from tbl_activity  where tbl_activity.activity_id=Param1;
end if;

if(act_mode='addbranchAddon')then
INSERT INTO `tbl_addon`(`addon_name`, `addon_price`, `addon_status`,`addon_branchid`, `addon_description`) VALUES (Param1,Param4,'1',Param2,Param3);
end if;

if(act_mode='addactivityAddon')then
INSERT INTO `tbl_addon`(`addon_name`, `addon_price`, `addon_status`,`addon_activityid`, `addon_description`) VALUES (Param1,Param4,'1',Param2,Param3);
end if;


if(act_mode='s_viewaddon')then
select distinct(t.addon_name) from tbl_addon as t  where t.addon_status='1';
end if;



END$$

CREATE  PROCEDURE `proc_mail_s`(
	IN `act_mode` VARCHAR(50),
	IN `Param1` VARCHAR(50),
	IN `Param2` VARCHAR(50),
	IN `Param3` VARCHAR(50),
	IN `Param4` VARCHAR(50),
	IN `Param5` VARCHAR(50),
	IN `Param6` VARCHAR(50),
	IN `Param7` VARCHAR(50),
	IN `Param8` VARCHAR(50),
	IN `Param9` VARCHAR(50),
	IN `Param10` VARCHAR(50),
	IN `Param11` VARCHAR(50),
	IN `Param12` VARCHAR(50)
)
    COMMENT 'mail procedure by zzz'
BEGIN

if(act_mode = 's_viewmail') then
select * from tbl_bannerimage where bannerimage_status in (0,1);
end if;


if(act_mode = 's_addemail_details') then
INSERT INTO tbl_bannerimage ( bannerimage_branch, bannerimage_logo, bannerimage_top1, bannerimage_top2, bannerimage_top3, bannerimage_top4, bannerimage_subject, bannerimage_from, bannerimage_branch_contact, bannerimage_branch_email,internethandlingcharge) values (Param1,Param2,Param3,Param4,Param5,Param6,Param7,Param8,Param9,Param10,Param12);
end if;

if(act_mode = 's_viewmail_update') then
select * from tbl_bannerimage as t where t.bannerimage_id=Param1;
end if;

if(act_mode = 's_addemail_details_update') then
Update tbl_bannerimage set  bannerimage_branch=Param1, bannerimage_logo=Param2, bannerimage_top1=Param3, bannerimage_top2=Param4, bannerimage_top3=Param5, bannerimage_top4=Param6, bannerimage_subject=Param7, bannerimage_from=Param8, bannerimage_branch_contact=Param9, bannerimage_branch_email=Param10, internethandlingcharge=Param12 where bannerimage_id=Param11;
end if;

if(act_mode = 's_viewmail_status') then
update tbl_bannerimage as t set t.bannerimage_status=Param2 where t.bannerimage_id=Param1;
end if;

if(act_mode = 's_viewmail_delete') then
update tbl_bannerimage as t set t.bannerimage_status=2 where t.bannerimage_id=Param1;
end if;

END$$

CREATE  PROCEDURE `proc_memberlogin_v`(IN `act_mode` VARCHAR(50), IN `username` VARCHAR(50), IN `password` VARCHAR(50))
BEGIN
if(act_mode='memlogin')then
select * from tbl_user a where a.user_emailid=username and a.user_password=password and a.user_status='1';
end if ;
END$$

CREATE  PROCEDURE `proc_memberlog_v`(IN `act_mode` VARCHAR(50), IN `user_id` INT)
BEGIN
if(act_mode='memuserlog')then
insert into tbl_userlog (userid,logindate)values(user_id,NOW());
end if;

if(act_mode='memusersesiid')then
select * from tbl_user where user_id=user_id;
end if;

END$$

CREATE  PROCEDURE `proc_memberregister_v`(IN `act_mode` VARCHAR(50), IN `title` VARCHAR(50), IN `fname` VARCHAR(50), IN `lastName` VARCHAR(50), IN `mobileno` VARCHAR(50), IN `email` VARCHAR(50), IN `password` VARCHAR(50))
BEGIN
if(act_mode='memregister')then
INSERT INTO tbl_user ( `user_title`,`user_firstname`, `user_lastname`, `user_emailid`, `user_password`, `user_mobileno`, `user_status`, `user_createdon`) VALUES (title,fname, lastName, email, password, mobileno, 1, NOW());
select last_insert_id() as ls;
end if ;

END$$

CREATE  PROCEDURE `proc_memberses_v`(IN `act_mode` VARCHAR(50), IN `userid` INT)
BEGIN
if(act_mode='memusersesiid')then
select * from tbl_user as tc where tc.user_id =userid;
end if;

if(act_mode='patrnusersesiid')then
select * from tbl_agent_reg as tc where tc.	agent_id =userid;
end if;



if(act_mode='selectorderbooking')then

select *,(select group_concat(t1.addonename) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addon,(select group_concat(t1.addonqty) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonquantity,(select group_concat(t1.addoneprice) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonprice,(select group_concat(t1.addon_image) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addon_image,(select group_concat(t1.addonqty) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonqty,(select user_firstname from tbl_user as tu where t.userid=tu.user_id) as username,(select user_emailid from tbl_user as tu where t.userid=tu.user_id) as email,(select user_mobileno from tbl_user as tu where t.userid=tu.user_id ) as contact from tbl_orderpackage as t  where  t.orderstatus in (0,1) and t.op_ticket_print_status in (0,1)and t.userid=userid order by t.pacorderid desc;

end if;

if(act_mode='selectorderbookingcompses')then
select *,(select group_concat(t1.addonename) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addon,(select group_concat(t1.addonqty) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonquantity,(select group_concat(t1.addoneprice) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonprice,(select group_concat(t1.addon_image) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addon_image,(select group_concat(t1.addonqty) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonqty,(select user_firstname from tbl_user as tu where t.userid=tu.user_id) as username,(select user_emailid from tbl_user as tu where t.userid=tu.user_id) as email,(select user_mobileno from tbl_user as tu where t.userid=tu.user_id ) as contact from tbl_orderpackage as t  where  t.orderstatus in(0,1)  and t.op_ticket_print_status =2 and t.userid=userid order by t.pacorderid desc;

end if;

if(act_mode='cancelorderbooking')then
update tbl_orderpackage set op_ticket_print_status=3 where  pacorderid=userid;
end if;

if(act_mode='selectorderbookingdata')then
select * from tbl_orderpackage  where  pacorderid=userid;
end if;


END$$

CREATE  PROCEDURE `proc_memberuserchangepassword_v`(IN `act_mode` VARCHAR(50), IN `userid` VARCHAR(50), IN `currentpassword` VARCHAR(50), IN `newpassword` VARCHAR(50), IN `confirmpassword` VARCHAR(50))
BEGIN
if(act_mode='memechangepassword')then
update tbl_user  set user_password=newpassword where user_id =userid;
end if;

if(act_mode='memecheckpassword')then
select user_password,user_id from tbl_user  where user_id =userid;
end if;
END$$

CREATE  PROCEDURE `proc_memberusercheck_v`(IN `act_mode` VARCHAR(50), IN `email` VARCHAR(50))
BEGIN
if(act_mode='checkuser')then
select * from tbl_user as tc where tc.user_emailid =email and user_usertype='User';
end if;

if(act_mode='emailverified')then
select * from tbl_user as tc where tc.user_emailid =email;
end if;

if(act_mode='emailverifiedupdate')then
update  tbl_user set varified=1 where emailid =email;

end if;
END$$

CREATE  PROCEDURE `proc_memberuserupdate_v`(IN `act_mode` VARCHAR(50), IN `userid` INT, IN `firstName` VARCHAR(50), IN `lastName` VARCHAR(50), IN `myprofiledob` VARCHAR(50), IN `email` VARCHAR(50), IN `mobile` VARCHAR(50), IN `address` TEXT, IN `countryName` VARCHAR(50), IN `stateName` VARCHAR(50), IN `cityName` VARCHAR(50), IN `pincode` VARCHAR(50))
BEGIN
if(act_mode='updateuser')then
update tbl_user set user_firstname=firstName,user_lastname=lastName,user_dob=myprofiledob,user_mobileno=mobile,user_Address=address,user_country=countryName,user_state=stateName,user_city=cityName,user_pincodes=pincode where user_id=userid;
end if;

if(act_mode='selectuser')then
select * from tbl_user  where user_id=userid;
end if;
END$$

CREATE  PROCEDURE `proc_menu`(IN `id` INT)
begin

select mas.catname,mas.url,map.parentid,map.childid,map.catlevel,cp.catname as parentname,
cimg.catlogo,cimg.imgfirst,cimg.imgsecond,cimg.imgthird,cimg.imgaltname,100 as products  from tbl_categorymap as map 
left join tbl_categorymaster as mas on map.childid=mas.catid 
left join tbl_categorymaster as cp ON cp.catid=map.parentid 
left join tbl_catimage as cimg on cimg.catid=map.childid
where map.mainprent=1 and map.cstatus='A' order by map.ordersort;

end$$

CREATE  PROCEDURE `proc_orderaddone_v`(IN `act_mode` VARCHAR(50), IN `addonnameses` VARCHAR(50), IN `addonquantityses` INT, IN `addonidses` INT, IN `lastidses` INT, IN `addonpriceses` INT, IN `ccartpackageimage` VARCHAR(255))
BEGIN
if(act_mode='orderaddone_insert')then
INSERT INTO tbl_orderaddone (addonename,addonevisiter,addoneprice,addonqty,lastid,addonid,Type,addon_image) VALUES (addonnameses,addonquantityses,addonpriceses,addonquantityses,lastidses,addonidses,'Addone',ccartpackageimage);
select last_insert_id();
end if;
if(act_mode='selectaddones')then
select * from tbl_orderaddone as a where a.lastid=lastidses;
end if;
END$$

CREATE  PROCEDURE `proc_orderinsertpackage_v`(IN `act_mode` VARCHAR(50), IN `packagenameses` VARCHAR(50), IN `packagequantityses` INT, IN `packageidses` INT, IN `package_priceses` INT, IN `lastidses` INT)
if(act_mode='orderpackagedata_insert')then
 INSERT INTO tbl_orderaddone (addonename,addoneprice,lastid,addonqty,Type,addonid) VALUES (packagenameses,package_priceses,lastidses,packagequantityses,'Package',packageidses);
select last_insert_id();
end if$$

CREATE  PROCEDURE `proc_orderpackage_v`(IN `act_mode` VARCHAR(50), IN `subtotal` INT, IN `discountamount` INT, IN `total` INT, IN `countryid` INT, IN `stateid` INT, IN `cityid` INT, IN `branch_id` INT, IN `locationid` INT, IN `userid` INT, IN `txtDepartDate1` VARCHAR(50), IN `txtDepartdata` VARCHAR(50), IN `paymentmode` VARCHAR(50), IN `ticketid` VARCHAR(50), IN `packproductname` VARCHAR(50), IN `packimg` VARCHAR(50), IN `packpkg` INT, IN `packprice` INT, IN `internethandlingcharges` VARCHAR(50), IN `txtfrommin` VARCHAR(50), IN `txttohrs` VARCHAR(50), IN `txttomin` VARCHAR(50), IN `txtfromd` VARCHAR(50), IN `txttod` VARCHAR(50), IN `p_codename` VARCHAR(50), IN `p_usertype` VARCHAR(50))
BEGIN
if(act_mode='orderpackage_insert')then
 insert into tbl_orderpackage  (paymentstatus,userid,promocodeprice,total,subtotal,addedon,countryid,stateid,cityid,branch_id,locationid,departuredate,departuretime,paymentmode,ticketid,packproductname,packimg,packpkg,packprice,internethandlingcharges,frommin,tohrs,tomin,txtfromd,txttod,p_codename,op_usertype)values('0',userid,discountamount,total,subtotal,NOW(),countryid,stateid,cityid,branch_id,locationid,txtDepartDate1,txtDepartdata,paymentmode,ticketid,packproductname,packimg,packpkg,packprice,internethandlingcharges,txtfrommin,txttohrs,txttomin,txtfromd,txttod,p_codename,p_usertype);
select last_insert_id();
end if;

#booking id select 
if(act_mode='selectbooking')then
select * from tbl_orderpackage a where a.ticketid=ticketid;
end if;

if(act_mode='orderpackage_update')then
update tbl_orderpackage as t set t.paymentstatus='0',t.userid=userid,t.promocodeprice=discountamount,t.total=total,t.subtotal=subtotal,t.addedon=NOW(),t.countryid=countryid,t.stateid=stateid,t.cityid=cityid,t.branch_id=branch_id,t.locationid=locationid,t.departuredate=txtDepartDate1,t.departuretime=txtDepartdata,t.paymentmode=paymentmode,t.ticketid=ticketid,t.packproductname=packproductname,t.packimg=packimg,t.packpkg=packpkg,t.packprice=packprice,t.internethandlingcharges=internethandlingcharges,t.frommin=txtfrommin,t.tohrs=txttohrs,t.tomin=txttomin,t.txtfromd=txtfromd,t.txttod=txttod,t.p_codename=p_codename,t.op_usertype=p_usertype where t.ticketid=ticketid;
select t.pacorderid as 'last_insert_id()' from tbl_orderpackage as t where t.ticketid=ticketid;
end if;


END$$

CREATE  PROCEDURE `proc_orderpackage_vback`(IN `act_mode` VARCHAR(50), IN `subtotal` VARCHAR(50))
BEGIN
if(act_mode='orderpackage_insert')then
INSERT INTO tbl_orderpackage (subtotal) VALUES (subtotal);
select last_insert_id();
end if;
END$$

CREATE  PROCEDURE `proc_orderuser_v`(IN `act_mode` VARCHAR(50), IN `userid` INT)
BEGIN
if(act_mode='select_customer')then

select * from tbl_user where user_id=userid;
end if;

if(act_mode='select_partner')then

select * from tbl_agent_reg where agent_id=userid;
end if;
END$$

CREATE  PROCEDURE `proc_order_filter_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50), IN `Param11` VARCHAR(50), IN `Param12` VARCHAR(50), IN `Param13` VARCHAR(50), IN `Param14` VARCHAR(50), IN `Param15` VARCHAR(50), IN `Param16` VARCHAR(50), IN `Param17` VARCHAR(50), IN `Param18` VARCHAR(50), IN `Param19` VARCHAR(50))
BEGIN
if(act_mode='S_vieworder') then
if(Param7 = '-1') then
set @q3 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon asc limit 0,1);
else
set @q3 = Param7;
end if;

if(Param8 = '-1') then
set @q4 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon desc limit 0,1);
else
set @q4 = Param8;
end if;


if(Param9 = '-1') then
set @q1 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate asc limit 0,1);
else
set @q1 = Param9;
end if;

if(Param10 = '-1') then
set @q2 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate desc limit 0,1);
else
set @q2 = Param10;
end if;

if(Param12 = '-1') then
set @q5 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date asc limit 0,1);
else
set @q5 = Param12;
end if;

if(Param13 = '-1') then
set @q6 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date desc limit 0,1);
else
set @q6 = Param13;
end if;

#select Param17;

#set @aa = (select t.pacorderid from  tbl_orderpackage as t where t.order_status in (Param17));
#select @aa;

select t.*,tbm.branch_name from tbl_orderpackage as t inner join tbl_branch_master as tbm on t.branch_id=tbm.branch_id   where ((t.branch_id in (Param1)) or Param1=-1) and 
 ((t.billing_name like concat(Param2,'%')) or Param2=-1) and 
((t.billing_email like concat(Param3,'%')) or Param3=-1) and
 ((t.ticketid like concat(Param4,'%')) or Param4=-1) and 
 ((t.billing_tel like concat(Param5,'%')) or Param5=-1) and 
 ((t.paymentmode like concat(Param6,'%')) or Param6=-1) and 
 (t.addedon between (@q3) and (@q4)) and
  (t.departuredate between (@q1) and (@q2)) and 
  (( find_in_set (t.op_ticket_print_status,Param11)) or Param11=-1) and
  (t.op_printed_date between (@q5) and (@q6)) and
   ((t.departuredate=Param14 or Param14=-1 ) and (t.departuretime=Param15 or Param15=-1 ))
	 and (( find_in_set (t.departuretime,Param16)) or Param16=-1) and (( find_in_set (t.order_status,Param17)) or Param17=-1)  and t.billing_email!='NULL' order by t.pacorderid desc;
  --  ((Param14=-1 or Param15=-1) or (t.departuredate=Param14 and t.departuretime=Param15));
end if;



if(act_mode='branch_list_for_filter') then
select distinct(t.timeslot_from) from  tbl_timeslot_branch as t where t.timeslot_status=1 order by t.timeslot_from asc;
end if;

if(act_mode='status_list_for_filter') then
select distinct(order_status) from  tbl_orderpackage ;
end if;


END$$

CREATE  PROCEDURE `proc_order_filter_v`(
	IN `act_mode` VARCHAR(50),
	IN `Param1` VARCHAR(50),
	IN `Param2` VARCHAR(50),
	IN `Param3` VARCHAR(50),
	IN `Param4` VARCHAR(50),
	IN `Param5` VARCHAR(50),
	IN `Param6` VARCHAR(50),
	IN `Param7` VARCHAR(50),
	IN `Param8` VARCHAR(50),
	IN `Param9` VARCHAR(50),
	IN `Param10` VARCHAR(50),
	IN `Param11` VARCHAR(50),
	IN `Param12` VARCHAR(50),
	IN `Param13` VARCHAR(50),
	IN `Param14` VARCHAR(50),
	IN `Param15` VARCHAR(50),
	IN `Param16` VARCHAR(50),
	IN `Param17` VARCHAR(50),
	IN `Param18` VARCHAR(50),
	IN `Param19` VARCHAR(50)

)
BEGIN
if(act_mode='S_vieworder') then
if(Param7 = '-1') then
set @q3 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon asc limit 0,1);
else
set @q3 = Param7;
end if;

if(Param8 = '-1') then
set @q4 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon desc limit 0,1);
else
set @q4 = Param8;
end if;


if(Param9 = '-1') then
set @q1 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate asc limit 0,1);
else
set @q1 = Param9;
end if;

if(Param10 = '-1') then
set @q2 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate desc limit 0,1);
else
set @q2 = Param10;
end if;

if(Param12 = '-1') then
set @q5 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date asc limit 0,1);
else
set @q5 = Param12;
end if;

if(Param13 = '-1') then
set @q6 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date desc limit 0,1);
else
set @q6 = Param13;
end if;

#select Param17;

#set @aa = (select t.pacorderid from  tbl_orderpackage as t where t.order_status in (Param17));
#select @aa;

select t.*,tbm.branch_name from tbl_orderpackage as t inner join tbl_branch_master as tbm on t.branch_id=tbm.branch_id   where ((t.branch_id in (Param1)) or Param1=-1) and 
 ((t.billing_name like concat(Param2,'%')) or Param2=-1) and 
((t.billing_email like concat(Param3,'%')) or Param3=-1) and
 ((t.ticketid like concat(Param4,'%')) or Param4=-1) and 
 ((t.billing_tel like concat(Param5,'%')) or Param5=-1) and 
 ((t.paymentmode like concat(Param6,'%')) or Param6=-1) and 
 (t.addedon between (@q3) and (@q4)) and
  (t.departuredate between (@q1) and (@q2)) and 
  (( find_in_set (t.op_ticket_print_status,Param11)) or Param11=-1) and
  (t.op_printed_date between (@q5) and (@q6)) and
   ((t.departuredate=Param14 or Param14=-1 ) and (t.departuretime=Param15 or Param15=-1 ))
	 and (( find_in_set (t.departuretime,Param16)) or Param16=-1) and (( find_in_set (t.order_status,Param17)) or Param17=-1) and t.userid=Param19 and t.op_usertype='Agent'  and t.billing_email!='NULL' order by t.pacorderid desc;
  --  ((Param14=-1 or Param15=-1) or (t.departuredate=Param14 and t.departuretime=Param15));
end if;



if(act_mode='branch_list_for_filter') then
select distinct(t.timeslot_from) from  tbl_timeslot_branch as t where t.timeslot_status=1 order by t.timeslot_from asc;
end if;

if(act_mode='status_list_for_filter') then
select distinct(order_status) from  tbl_orderpackage ;
end if;


END$$

CREATE  PROCEDURE `proc_order_partner`(
	IN `act_mode` VARCHAR(50),
	IN `Param1` VARCHAR(50),
	IN `Param2` VARCHAR(50),
	IN `Param3` VARCHAR(50),
	IN `Param4` VARCHAR(50),
	IN `Param5` VARCHAR(50),
	IN `Param6` VARCHAR(50),
	IN `Param7` VARCHAR(50),
	IN `Param8` VARCHAR(50),
	IN `Param9` VARCHAR(50)


)
BEGIN
if(act_mode='S_vieworder') then
select *,
(select group_concat(t1.addonename) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addon,
(select group_concat(t1.addonqty) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonquantity,
(select group_concat(t1.addoneprice) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonprice,
(select group_concat(t1.orderaddon_print_status) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as orderaddon_print_status,
(select group_concat(t1.orderaddon_print_quantity) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as orderaddon_print_quantity,
(select user_firstname from tbl_user as tu where t.userid=tu.user_id) as username,
(select user_emailid from tbl_user as tu where t.userid=tu.user_id) as email,
(select user_mobileno from tbl_user as tu where t.userid=tu.user_id) as contact
 from tbl_orderpackage as t  where t.orderstatus in (0,1) and t.billing_email!='NULL' order by t.pacorderid desc;
end if;

if(act_mode='S_vieworder_new') then
select t.*,tbm.branch_name,tbw.ph_agentdiscount, group_concat(t1.addonename order by t1.orderaddoneid)  as addon,
 group_concat(t1.addonqty order by t1.orderaddoneid) as addonquantity,
group_concat(t1.addoneprice order by t1.orderaddoneid)  as addonprice,
 group_concat(tbw.ph_agentdiscount order by tbw.ph_id) as agentdiscount,
 
group_concat(t1.orderaddon_print_status order by t1.orderaddoneid) as orderaddon_print_status, group_concat(t1.orderaddon_print_quantity order by t1.orderaddoneid) as orderaddon_print_quantity from tbl_orderaddone as t1 right  join tbl_orderpackage as t on t.pacorderid=t1.lastid inner join tbl_branch_master as tbm on t.branch_id=tbm.branch_id  inner join tbl_agent_payment_history
 as tbw on t.pacorderid=tbw.ph_orderid  where t.orderstatus in (0,1) and t.userid=Param9 and t.op_usertype='Agent' and t.billing_email!='NULL' group by t.pacorderid order by t.pacorderid desc;

end if;



if(act_mode='op_ticket_print_status') then

if(Param2='2') then
update tbl_orderpackage as t set t.op_ticket_print_status=Param2, t.op_printed_date=now()  where t.pacorderid=Param1;
else
update tbl_orderpackage as t set t.op_ticket_print_status=Param2 where t.pacorderid=Param1;
end if;
select op_ticket_print_status as ticket_status from tbl_orderpackage where pacorderid=Param1;
end if;

if(act_mode='S_viewordergp') then
select * from tbl_groupbooking;
end if;
END$$

CREATE  PROCEDURE `proc_order_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN

if(act_mode='S_vieworder') then
select tbm.branch_name,t.*,group_concat(t1.addonename order by t1.orderaddoneid)  as addon,
 group_concat(t1.addonqty order by t1.orderaddoneid) as addonquantity,
group_concat(t1.addoneprice order by t1.orderaddoneid)  as addonprice,
group_concat(t1.orderaddon_print_status order by t1.orderaddoneid) as orderaddon_print_status, group_concat(t1.orderaddon_print_quantity order by t1.orderaddoneid) as orderaddon_print_quantity from tbl_orderaddone as t1 right  join tbl_orderpackage as t on t.pacorderid=t1.lastid inner join tbl_branch_master as tbm on t.branch_id=tbm.branch_id  where t.orderstatus in (0,1) and t.billing_email!='NULL' group by t.pacorderid order by t.pacorderid desc ;
end if;


if(act_mode='S_vieworder_new') then
select t.*,group_concat(t1.addonename order by t1.orderaddoneid)  as addon,
 group_concat(t1.addonqty order by t1.orderaddoneid) as addonquantity,
group_concat(t1.addoneprice order by t1.orderaddoneid)  as addonprice,
group_concat(t1.orderaddon_print_status order by t1.orderaddoneid) as orderaddon_print_status, group_concat(t1.orderaddon_print_quantity order by t1.orderaddoneid) as orderaddon_print_quantity from tbl_orderaddone as t1 right  join tbl_orderpackage as t on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.billing_email!='NULL' group by t.pacorderid order by t.pacorderid desc ;
end if;



if(act_mode='op_ticket_print_status') then

if(Param2='2') then
update tbl_orderpackage as t set t.op_ticket_print_status=Param2, t.op_printed_date=now()  where t.pacorderid=Param1;
else
update tbl_orderpackage as t set t.op_ticket_print_status=Param2 where t.pacorderid=Param1;
end if;
select op_ticket_print_status as ticket_status from tbl_orderpackage where pacorderid=Param1;
end if;

if(act_mode='S_viewordergp') then
select * from tbl_groupbooking;
end if;
END$$

CREATE  PROCEDURE `proc_order_v`(IN `act_mode` VARCHAR(50), IN `orderid` INT)
BEGIN
if(act_mode='select_order') then
select * from  tbl_orderpackage as t where t.pacorderid=orderid;
end if;
if(act_mode='select_package') then
select * from  tbl_orderpackage  as pack where pack.pacorderid=orderid ;
end if;
if(act_mode='select_addone') then
select * from  tbl_orderaddone as pack where pack.lastid=orderid and pack.Type='Addone';
end if;

if(act_mode='orderpaymentupdatedata') then
update  tbl_orderpackage as t set t.ordermailstatus='1' where pacorderid=orderid ;

end if;

if(act_mode='select_orderjason') then

select * from  tbl_orderpackage as t where op_ordertrackingupdate!='3'; 

end if;

if(act_mode='select_orderjasonup') then

select * from  tbl_orderpackage as t where t.op_orderjason=0; 

end if;





END$$

CREATE  PROCEDURE `proc_order_vdate`(IN `act_mode` VARCHAR(50), IN `orderid` INT, IN `order_status_date_time` VARCHAR(50))
BEGIN

if(act_mode='orderpaymentupdatejsonnew') then
update  tbl_orderpackage as t set t.order_status_date_time=order_status_date_time , t.op_orderjason=1 where pacorderid=orderid ;

end if;




END$$

CREATE  PROCEDURE `proc_order_vf`(IN `act_mode` VARCHAR(50), IN `orderid` INT, IN `tracking_id` VARCHAR(50), IN `order_status` VARCHAR(50), IN `status_message` VARCHAR(50), IN `paymentmode` VARCHAR(50), IN `paymentstatus` VARCHAR(50), IN `ordersucesmail` INT, IN `ordermailstatus` INT)
BEGIN


if(act_mode='orderpaymentupdate') then
update  tbl_orderpackage as t set t.paymentstatus=paymentstatus,t.paymentmode='CCAVENUE',t.tracking_id=tracking_id,t.order_status=order_status,t.status_message=status_message,t.paymenttype=paymentmode,t.ordersucesmail=ordersucesmail,t.ordermailstatus=ordermailstatus where t.pacorderid=orderid ;

end if;

if(act_mode='orderpaymentupdatejson') then
update  tbl_orderpackage as t set t.paymentstatus=paymentstatus,t.paymentmode='CCAVENUE',t.tracking_id=tracking_id,t.order_status=order_status,t.status_message=status_message,t.paymenttype=paymentmode,t.ordersucesmail=ordersucesmail,t.ordermailstatus=ordermailstatus, t.op_ordertrackingupdate=(t.op_ordertrackingupdate+1) where t.pacorderid=orderid ;

end if;

if(act_mode='orderpaymentupdatesucess') then
update  tbl_orderpackage as t set t.paymentstatus=paymentstatus,t.paymentmode='CCAVENUE',t.tracking_id=tracking_id,t.order_status=order_status,t.status_message=status_message,t.paymenttype=paymentmode,t.ordersucesmail=ordersucesmail,t.ordermailstatus=ordermailstatus, t.op_ordertrackingupdate=3 where t.pacorderid=orderid ;

end if;


END$$

CREATE  PROCEDURE `proc_packages_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` TEXT, IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN

if(act_mode='s_addpackage_master') then
INSERT INTO `tbl_package_master`(`package_name`, `package_description`, `package_status`,`package_price`) VALUES (Param1,Param2,'1',Param3);
select last_insert_id() as pack_id;
end if;

if(act_mode='s_addpackage_branch') then
INSERT INTO `tbl_branch_package`(`bp_packageid`, `bp_branchid`, `bp_status`) VALUES (Param1,Param2,'1');
end if;

if(act_mode='s_addpackage_activity') then
INSERT INTO `tbl_activity_package`(`ap_packageid`, `ap_activityid`, `ap_status`) VALUES (Param1,Param2,'1');
end if;

if(act_mode='s_viewbranch') then
select * from  tbl_branch_master as t where t.branch_status=1;
end if;

if(act_mode='s_viewactivity') then
select * from  tbl_activity as t where t.activity_status=1;
end if;

if(act_mode='s_viewpackage') then
select t.*,(select group_concat(tbp.bp_branchid) from tbl_branch_package as tbp where tbp.bp_packageid=t.package_id and tbp.bp_status=1) as bp_id,(select group_concat(tap.ap_activityid) from tbl_activity_package as tap where tap.ap_packageid=t.package_id) as ap_id from tbl_package_master as t where t.package_status=1;
end if;

if(act_mode='activityPrice') then
select ta.activity_price  from tbl_activity as ta where ta.activity_id=Param1;
end if;

if(act_mode='delete_package') then
update tbl_package_master as t set t.package_status='2' where t.package_id=Param1;
end if;

if(act_mode='update_package_data') then
select *,(select group_concat(tbp.bp_branchid) from tbl_branch_package as tbp where tbp.bp_packageid=t.package_id and tbp.bp_status=1) as bp_id from  tbl_package_master as t where t.package_id=Param1;
end if;


if(act_mode='s_addpackage_master_update') then
update tbl_package_master as t set t.package_name=Param1, t.package_description=Param2,t.package_modifiedon=now(),t.package_price=Param3 where t.package_id=Param4;
update tbl_branch_package as t set t.bp_status=2 where t.bp_packageid=Param4;
end if;

if(act_mode='s_addpackage_branch_update') then
INSERT INTO `tbl_branch_package`(`bp_packageid`, `bp_branchid`, `bp_status`) VALUES (Param1,Param2,'1');
end if;


END$$

CREATE  PROCEDURE `proc_packages_v`(IN `act_mode` VARCHAR(50), IN `branchid` INT, IN `packageid` INT, IN `packageqty` INT)
BEGIN
if(act_mode='view_package')then
set @pkgqty=packageqty;
select package_id,package_name,package_image,package_description,package_price,@pkgqty as pkg from tbl_package_master a join tbl_branch_package b on b.bp_packageid=a.package_id where a.package_id=packageid  and a.package_status =1 order by package_name;

end if;

if(act_mode='select_package')then
set @branch_id_new = branchid;

select a.package_image,a.package_id,a.package_name,a.package_description,a.package_price from tbl_package_master a join tbl_branch_package b on b.bp_packageid=a.package_id where b.bp_branchid=branchid and a.package_status =1 and b.bp_status=1 order by a.package_id asc limit 0,1;
end if;

if(act_mode='select_packagepartner')then
set @branch_id_new = branchid;

select a.package_image,a.package_id,a.package_name,a.package_description,a.package_price from tbl_package_master a join tbl_branch_package b on b.bp_packageid=a.package_id where b.bp_branchid=branchid and a.package_status =1  and a.package_for='1' and b.bp_status=1 order by a.package_id desc ;
end if;

if(act_mode='select_packageuserguest')then
set @branch_id_new = branchid;

select a.package_image,a.package_id,a.package_name,a.package_description,a.package_price from tbl_package_master a join tbl_branch_package b on b.bp_packageid=a.package_id where b.bp_branchid=branchid and a.package_status =1   and b.bp_status=1 order by a.package_id desc ;
end if;


if(act_mode='ticketcart_package')then
select * from tbl_package_master  where tbl_package_master.package_id=packageid ;
end if;
END$$

CREATE  PROCEDURE `proc_partnerlogin_v`(IN `act_mode` VARCHAR(50), IN `pusername` VARCHAR(50), IN `ppassword` VARCHAR(50))
BEGIN
if(act_mode='partnerlogincorporate')then
select * from tbl_agent_reg where agent_username=pusername and agent_status='1' and logintype='1' ;
end if;
 
if(act_mode='partnerloginofficer')then

select * from tbl_agent_reg where agent_username=pusername and agent_status='1' and  logintype='2';
end if;

END$$

CREATE  PROCEDURE `proc_partnerregister_v`(
	IN `act_mode` VARCHAR(50),
	IN `title` VARCHAR(50),
	IN `firstname` VARCHAR(50),
	IN `lastname` VARCHAR(50),
	IN `row_id` INT,
	IN `logintype` VARCHAR(50),
	IN `agencyname` VARCHAR(50),
	IN `office_address` TEXT,
	IN `city` VARCHAR(50),
	IN `state` VARCHAR(50),
	IN `pincode` VARCHAR(50),
	IN `telephone` VARCHAR(50),
	IN `mobile` VARCHAR(50),
	IN `fax` VARCHAR(50),
	IN `website` VARCHAR(50),
	IN `email` VARCHAR(50),
	IN `primarycontact_name` VARCHAR(50),
	IN `primarycontact_email_mobile` VARCHAR(50),
	IN `primarycontact_telephone` INT,
	IN `secondrycontact_name` VARCHAR(50),
	IN `secondrycontact_email_phone` VARCHAR(50),
	IN `secondrycontact_telephone` INT,
	IN `management_executives_name` VARCHAR(50),
	IN `management_executives_email_phone` VARCHAR(50),
	IN `management_executives_telephone` INT,
	IN `branches` VARCHAR(50),
	IN `yearofestablism` VARCHAR(50),
	IN `organisationtype` VARCHAR(50),
	IN `lstno` VARCHAR(50),
	IN `cstno` VARCHAR(50),
	IN `registrationno` VARCHAR(50),
	IN `vatno` VARCHAR(50),
	IN `panno` VARCHAR(50),
	IN `servicetaxno` VARCHAR(50),
	IN `tanno` VARCHAR(50),
	IN `pfno` VARCHAR(50),
	IN `esisno` VARCHAR(50),
	IN `officeregistrationno` VARCHAR(50),
	IN `exceptiontax` VARCHAR(50),
	IN `otherexceptiontax` VARCHAR(50),
	IN `bank_benificialname` VARCHAR(50),
	IN `bank_benificialaccno` VARCHAR(50),
	IN `bank_benificialbankname` VARCHAR(50),
	IN `bank_benificialbranchname` VARCHAR(50),
	IN `bank_benificialaddress` TEXT,
	IN `bank_benificialifsc` VARCHAR(50),
	IN `bank_benificialswiftcode` VARCHAR(50),
	IN `bank_benificialibanno` VARCHAR(50),
	IN `intermidiatebankname` VARCHAR(50),
	IN `intermidiatebankaddress` TEXT,
	IN `intermidiatebankswiftcode` VARCHAR(50),
	IN `bank_ecs` VARCHAR(50),
	IN `copypancart` VARCHAR(50),
	IN `copyservicetax` VARCHAR(50),
	IN `copytan` VARCHAR(50),
	IN `copyaddressproof` VARCHAR(50),
	IN `emailstatus` VARCHAR(50),
	IN `emailcurrency` VARCHAR(50),
	IN `emailcountry` VARCHAR(50),
	IN `username` VARCHAR(50),
	IN `password` VARCHAR(50),
	IN `countryid` INT,
	IN `stateid` INT,
	IN `cityid` INT,
	IN `branch_id` INT,
	IN `locationid` INT,
	IN `bank_creditamount` VARCHAR(50),
	IN `bank_agentcommision` VARCHAR(50)








)
BEGIN
if(act_mode='insertpartner')then
#select * from tbl_agent_reg;
INSERT INTO tbl_agent_reg (`agent_agencyname`, `agent_office_address`,`agent_city`, `agent_state`, `agent_pincode` , `agent_telephone`, `agent_mobile`, `agent_fax`, `agent_website`, `agent_email`, `agent_primarycontact_name`, `agent_primarycontact_email_mobile`, `agent_primarycontact_telephone`, `agent_secondrycontact_name`, `agent_secondrycontact_email_phone`, `agent_secondrycontact_telephone`, `agent_management_executives_name`, `agent_management_executives_email_phone`, `agent_management_executives_telephone`, `agent_branches`, `agent_yearofestablism`, `agent_organisationtype`, `agent_lstno`, `agent_cstno`, `agent_registrationno`, `agent_vatno`,`agent_panno`, `agent_servicetaxno`, `agent_tanno`, `agent_pfno`, `agent_esisno`, `agent_officeregistrationno`, `agent_exceptiontax`, `agent_otherexceptiontax`, `agent_bank_benificialname`,agent_bank_benificialaccno,agent_bank_benificialbankname, bank_benificialaddress, bank_benificialifsc, bank_benificialswiftcode, bank_benificialibanno,`agent_intermidiatebankname`,`agent_intermidiatebankaddress`,`intermidiatebankswiftcode`,`agent_bank_ecs`,`agent_copypancart`, `agent_copyservicetax`,`agent_copytan`, `agent_copyaddressproof`,  `agent_emailstatus`,`agent_emailcurrency`, `agent_emailcountry`, `agent_username`, `agent_password`, `agent_createdon`, `agent_status`,`logintype`,`countryid`,`stateid`,`cityid`,`branch_id`,`locationid`)
VALUES (agencyname, office_address, city, state, pincode, telephone, mobile, fax, website, email, primarycontact_name, primarycontact_email_mobile, primarycontact_telephone, secondrycontact_name, secondrycontact_email_phone, secondrycontact_telephone, management_executives_name, management_executives_email_phone, management_executives_telephone, branches, yearofestablism, organisationtype, lstno, cstno, registrationno, vatno,panno, servicetaxno, tanno, pfno, esisno
, officeregistrationno, exceptiontax, otherexceptiontax,bank_benificialname, bank_benificialaccno, bank_benificialbankname, bank_benificialaddress, bank_benificialifsc, bank_benificialswiftcode, bank_benificialibanno, intermidiatebankname, intermidiatebankaddress, intermidiatebankswiftcode, bank_ecs, copypancart, copyservicetax,copytan,copyaddressproof,emailstatus,emailcurrency,emailcountry,username,password,NOW(),'0',logintype,countryid,stateid,cityid,branch_id,locationid);
end if;


if(act_mode='insertpartnerstartstage')then

INSERT INTO tbl_agent_reg (`agent_title`,`agent_firstname`,`agent_lastname`, `agent_mobile`, `agent_password`, `agent_createdon`, `agent_status`,`countryid`,`agent_email`,`stateid`,`cityid`,`branch_id`,`locationid`,`agent_username`)VALUES (title,firstname, lastname, mobile, password, NOW(),'0',countryid,username,stateid,cityid,branch_id,locationid,username);
select last_insert_id() as lid;
end if;


if(act_mode='selectpartneremail')then

select agent_id from  tbl_agent_reg where agent_username=username;
end if;


if(act_mode='selectpartner')then

select * from  tbl_agent_reg where agent_status in (0,1);
end if;

if(act_mode='agentdelete')then

update  tbl_agent_reg set 	agent_status=2 where agent_id=row_id;
end if;



if(act_mode='activeandinactive') then
update tbl_agent_reg set tbl_agent_reg.agent_status=0 where tbl_agent_reg.agent_id=row_id;
end if;
if(act_mode='inactiveandinactive') then
update tbl_agent_reg set tbl_agent_reg.agent_status=1 where tbl_agent_reg.agent_id=row_id;
end if;

if(act_mode='inactiveandinactive') then
update tbl_agent_reg set tbl_agent_reg.agent_status=1 where tbl_agent_reg.agent_id=row_id;
end if;

if(act_mode='selectpartnerdata') then
select * from tbl_agent_reg where tbl_agent_reg.agent_id=row_id;
end if;



if(act_mode='updatepartner')then
#select * from tbl_agent_reg;
update  tbl_agent_reg as t set t.agent_agencyname=agencyname,t.agent_office_address=office_address, t.agent_state=state, t.agent_pincode=pincode , t.agent_telephone=telephone, t.agent_mobile=mobile, t.agent_fax=fax, t.agent_website=website, t.agent_email=email, t.agent_primarycontact_name=primarycontact_name, t.agent_primarycontact_email_mobile=primarycontact_email_mobile, t.agent_primarycontact_telephone=primarycontact_telephone, t.agent_secondrycontact_name=secondrycontact_name, t.agent_secondrycontact_email_phone=secondrycontact_email_phone, t.agent_secondrycontact_telephone=secondrycontact_telephone, t.agent_management_executives_name=management_executives_name, t.agent_management_executives_email_phone=management_executives_email_phone, t.agent_management_executives_telephone=management_executives_telephone, t.agent_branches=branches, t.agent_yearofestablism=yearofestablism, t.agent_organisationtype=organisationtype, t.agent_lstno=lstno, t.agent_cstno=cstno, t.agent_registrationno=registrationno, t.agent_vatno=vatno,t.agent_panno=panno, t.agent_servicetaxno=servicetaxno, t.agent_tanno=tanno, t.agent_pfno=pfno, t.agent_esisno=esisno, t.agent_officeregistrationno=officeregistrationno, t.agent_exceptiontax=exceptiontax, t.agent_otherexceptiontax=otherexceptiontax, t.agent_bank_benificialname=bank_benificialname,t.agent_bank_benificialaccno=bank_benificialaccno,t.agent_bank_benificialbankname=bank_benificialbankname, t.bank_benificialaddress=bank_benificialaddress, t.bank_benificialifsc=bank_benificialifsc, t.bank_benificialswiftcode=bank_benificialswiftcode, t.bank_benificialibanno=bank_benificialibanno,t.agent_intermidiatebankname=intermidiatebankname,t.agent_intermidiatebankaddress=intermidiatebankaddress,t.intermidiatebankswiftcode=intermidiatebankswiftcode,t.agent_bank_ecs=bank_ecs,t.agent_copypancart=copypancart, t.agent_copyservicetax=copyservicetax,t.agent_copytan=copytan, t.agent_copyaddressproof=copyaddressproof,  t.agent_emailstatus=emailstatus,t.agent_emailcurrency=emailcurrency, t.agent_emailcountry=emailcountry, t.agent_username=username, t.logintype=logintype
=city,t.agent_state=state,t.agent_pincode=pincode,t.agent_telephone=telephone,t.agent_mobile=mobile,t.agent_fax=fax,t.agent_website=website,t.agent_email=email,t.agent_primarycontact_name=primarycontact_name,t.agent_primarycontact_email_mobile=primarycontact_email_mobile,t.agent_bank_benificialbranchname=bank_benificialbranchname,t.bank_creditamount=t.bank_creditamount+bank_creditamount,t.bank_agentcommision=bank_agentcommision where agent_id=row_id;


end if;
if(act_mode='insertpartnerstep1')then

update  tbl_agent_reg set 	logintype=logintype,agent_emailcountry=emailcountry,agent_agencyname=agencyname,agent_office_address=office_address,	agent_city=	city, agent_mobile=mobile,agent_state=state,agent_pincode=pincode,agent_telephone=telephone,agent_website=website,agent_fax=fax,	agent_emailstatus=emailstatus,	agent_emailcurrency=emailcurrency where agent_id=row_id and agent_username=username;


end if;

if(act_mode='insertpartnerstep2')then
update  tbl_agent_reg set agent_primarycontact_name= primarycontact_name,agent_primarycontact_email_mobile=primarycontact_email_mobile,agent_primarycontact_telephone=primarycontact_telephone,agent_secondrycontact_name=secondrycontact_name,	agent_secondrycontact_email_phone=secondrycontact_email_phone,agent_secondrycontact_telephone=secondrycontact_telephone,agent_management_executives_name=management_executives_name,agent_management_executives_email_phone=management_executives_email_phone,	agent_branches=branches,agent_yearofestablism=yearofestablism,	agent_organisationtype=organisationtype  where agent_id=row_id and agent_username=username;
end if;

if(act_mode='insertpartnerstep4')then
update  tbl_agent_reg set  agent_lstno=lstno,agent_cstno=cstno,agent_registrationno=registrationno,agent_vatno=vatno,agent_panno=panno,agent_servicetaxno=servicetaxno,agent_tanno=tanno,agent_pfno=pfno,agent_esisno=esisno,agent_officeregistrationno=officeregistrationno,	agent_exceptiontax=exceptiontax,agent_otherexceptiontax=otherexceptiontax where agent_id=row_id and agent_username=username;
end if;

if(act_mode='insertpartnerstep5')then

update  tbl_agent_reg set agent_bank_benificialname=bank_benificialname ,agent_bank_benificialaccno=bank_benificialaccno,agent_bank_benificialbankname=bank_benificialbankname,agent_bank_benificialbranchname=bank_benificialbranchname,bank_benificialaddress=bank_benificialaddress ,bank_benificialifsc=bank_benificialifsc,bank_benificialswiftcode=bank_benificialswiftcode,bank_benificialibanno=bank_benificialibanno ,agent_intermidiatebankname=intermidiatebankname,agent_intermidiatebankaddress=intermidiatebankaddress,intermidiatebankswiftcode=intermidiatebankswiftcode ,agent_bank_ecs=bank_ecs where agent_id=row_id and agent_username=username;
end if;

if(act_mode='insertpartnerstep6')then
update  tbl_agent_reg set  agent_copytan=copytan,agentprofilecomplete=1,agent_copyaddressproof=copyaddressproof,agent_copypancart=copypancart,agent_copyservicetax=copyservicetax where agent_id=row_id and agent_username=username;
end if;


END$$

CREATE  PROCEDURE `proc_payment_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50))
BEGIN


if(act_mode='paymentinsert') then
INSERT INTO `tbl_payment_gatway`(`pg_branchid`, `pg_merchant_id`, `pg_access_key`, `pg_working_key`, `pg_sucess_link`, `pg_fail_link`, `pg_currency`, `pg_language`,pg_prefix) VALUES (Param1,Param2,Param8,Param3,Param4,Param5,Param6,Param7,Param9);
end if;

if(act_mode='paymentview') then
select p.*,b.branch_name from tbl_payment_gatway as p left join tbl_branch_master as b on b.branch_id=p.pg_branchid where p.pg_status in (0,1); 
end if;


if(act_mode='paymentdelete') then
update tbl_payment_gatway as p set p.pg_status=2 where p.pg_id=Param1;
end if;

if(act_mode='paymentstatus') then
update  tbl_payment_gatway as p set  p.pg_status=Param2 where p.pg_id=Param1;
end if;

if(act_mode='paymentup') then
select * from  tbl_payment_gatway as p  where p.pg_id=Param1;
end if;


if(act_mode='paymentupdate') then
update  tbl_payment_gatway as p set p.pg_branchid=Param1, p.pg_merchant_id=Param2, p.pg_access_key=Param8,p.pg_working_key=Param3,p.pg_sucess_link=Param4,p.pg_fail_link=Param5,p.pg_currency=Param6,p.pg_language=Param7,p.pg_prefix=Param9,p.pg_updated=now() where p.pg_id=Param10;

end if;


END$$

CREATE  PROCEDURE `proc_promocode_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN

if(act_mode = 'insert_promo') then
INSERT INTO `tbl_promo`(`p_branchid`, `p_codename`, `p_type`, `p_discount`, `p_status`, `p_expiry_date`, `p_allowed_per_user`, `p_start_date`, `p_allowed_times`,p_logintype) VALUES (Param1,Param2,Param3,Param4,'1',Param8,Param5,Param7,Param6,Param9);
end if;

if(act_mode='view_promo') then
select * from  tbl_promo as t where t.p_status in (1,0);
end if;

if(act_mode='delete_promo') then
update tbl_promo as t set t.p_status='2' where promo_id=Param1;
end if;

if(act_mode='status_promo') then
update tbl_promo as t set t.p_status=Param2 where promo_id=Param1;
end if;

END$$

CREATE  PROCEDURE `proc_promo_v`(IN `act_mode` VARCHAR(50), IN `promovalue` VARCHAR(50), IN `locationid` INT, IN `branch_id` INT, IN `userid` INT)
BEGIN
if(act_mode= 'promoselect_package') THEN
SELECT *  FROM tbl_promo AS  a  WHERE  a.p_branchid = branch_id and p_codename=promovalue and p_status=1; 
END IF;

if(act_mode= 'promoselectuser_package') THEN
SELECT count(pacorderid) as num  FROM tbl_orderpackage AS  a  WHERE  a.userid = userid and a.p_codename=promovalue and a.paymentstatus=1 ; 
END IF;
END$$

CREATE  PROCEDURE `proc_rolemanage_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN

if(act_mode='addrole') then
INSERT INTO `tblrole`(`role_name`) VALUES (Param1);
end if;

if(act_mode='addemp') then
INSERT INTO `tbl_employee`(`emp_name`,`emp_email`, `emp_password`, `emp_contact`, `emp_branch_id`) VALUES (Param1,Param3,Param4,Param5,Param6);
select last_insert_id() as dd;
end if;

if(act_mode='addemp_role') then
set @aa='';
set @aa = (select t.parentid from tbladminmenu as t where t.id=Param1);
#select @aa;
INSERT INTO tblassignrole  (role_id,emp_id,main_menu,sub_menu) values ('100',Param2,@aa,Param1);
select * from tblassignrole as t where t.assign_id=last_insert_id();

end if;

if(act_mode='viewemp') then
select *,(select group_concat((select group_concat(menu.menuname) from tbladminmenu as menu where menu.id=dd.sub_menu)) from tblassignrole as dd where dd.emp_id=t.emp_tbl_id limit 0,1) as submenuu from tbl_employee as t where t.emp_status in(0,1);
end if;

if(act_mode='viewemp_update') then
select * from tbl_employee as t where t.emp_tbl_id=Param1;
end if;


if(act_mode='delemp') then
update tbl_employee as t set t.emp_status=2 where t.emp_tbl_id=Param1;
end if;

if(act_mode='viewrole') then
select * from tblrole as t where t.role_status=1;
end if;

if(act_mode='viewmenu') then
select * from tbladminmenu as t where t.parentid=0 and t.r_status='Active';
end if;

if(act_mode='deleterole') then
update tblrole as t set t.role_status=2,t.role_modifiedon=now() where t.role_id=Param1;
end if;



if(act_mode='getsubmenu') then
select * from tbladminmenu as t where find_in_set(parentid,Param1) and t.r_status='Active';
end if;


if(act_mode='getmainmenu') then
select parentid as pid from tbladminmenu as t where t.id=Param1;
end if;

if(act_mode='insertadminrole') then
set @a = (select count(*) from tblassignrole where role_id=Param1 and emp_id=Param2 and main_menu=Param3 and sub_menu=Param4);
if(@a = 0) then
INSERT INTO `tblassignrole`(`role_id`, `emp_id`, `main_menu`, `sub_menu`) VALUES (Param1,Param2,Param3,Param4);
end if;
end if;


if(act_mode='addasignrole_DELETE') then
DELETE from tblassignrole where role_id=Param1 and emp_id=Param2;
end if;

if(act_mode='get_menu_submenu') then
select (group_concat(distinct(t.main_menu))) as mainmenu , (group_concat(distinct(t.sub_menu))) as submenu   from tblassignrole as t  where t.role_id=Param1 and t.emp_id=Param2;
end if;


if(act_mode='upd_emp') then
update tbl_employee as t set t.emp_branch_id=Param6,t.emp_name=Param1,t.emp_email=Param3,t.emp_password=Param4,t.emp_contact=Param5 where t.emp_tbl_id=Param7;
end if;

END$$

CREATE  PROCEDURE `proc_select_banner_v`(
	IN `act_mode` VARCHAR(50),
	IN `branchid` VARCHAR(50)
)
BEGIN
if(act_mode='selectbannerimages') then
select * from tbl_bannerimage  where bannerimage_branch=branchid and bannerimage_status=1;
end if;
END$$

CREATE  PROCEDURE `proc_select_branch_v`(IN `act_mode` VARCHAR(50), IN `weburl` VARCHAR(50))
BEGIN
if(act_mode='selectbranch') then
select a.branch_url,a.branch_add,a.branch_location,a.branch_background,a.branch_logo,a.branch_internet_handling_charge,a.branch_id,b.cityid,c.stateid,d.countryid from tbl_branch_master a join tbl_location b on a.branch_location=b.locationid join tbl_city c on c.cityid=b.cityid join tbl_state d on d.stateid=c.stateid  where a.branch_status=1 and  a.branch_url=weburl;
end if;
END$$

CREATE  PROCEDURE `proc_select_timeslot_v`(IN `act_mode` VARCHAR(50), IN `branchid` INT, IN `destinationType` INT)
BEGIN

if(act_mode='selecttimeslot') then
select *  from tbl_timeslot_branch   where timeslot_branchid=branchid and timeslot_status=1;
end if;
if(act_mode='selectsestimeslot') then
select *  from tbl_timeslot_branch   where timeslot_branchid=branchid and timeslot_status=1 and timeslot_from=destinationType;
end if;
END$$

CREATE  PROCEDURE `proc_siteconfig`(IN `act_mode` VARCHAR(50), IN `row_id` INT)
begin

if(act_mode='bizzgainconfig')then
select * from tbl_bizzgainconfig as tbc where tbc.name='Email'; 
end if;



if(act_mode='shopotoxcofigph')then
select * from tbl_bizzgainconfig as tbc where tbc.name='Contact Us'; 
end if;
if(act_mode='servicetax')then
select * from tbl_bizzgainconfig as tbc where tbc.name='Service Tax'; 
end if;
if(act_mode='accconfig')then
select * from tbl_bizzgainconfig as tbc where tbc.name='account change'; 
end if;
if(act_mode='Offersconfig')then
select * from tbl_bizzgainconfig as tbc where tbc.name='Offers'; 
end if;
if(act_mode='complainconfig')then
select * from tbl_bizzgainconfig as tbc where tbc.name='complain'; 
end if;
if(act_mode='adminconfig')then
select * from tbl_bizzgainconfig as tbc where tbc.name='admin mail'; 
end if;
if(act_mode='vendorconfig')then
select * from tbl_bizzgainconfig as tbc where tbs.name='vendor mail'; 
end if;
if(act_mode='viewsiteconfig')then
select * from tbl_bizzgainconfig limit 8; 
end if;

if(act_mode='viewupdateconfig')then
select * from tbl_bizzgainconfig where tbl_bizzgainconfig.id=row_id; 
end if;

if(act_mode='viewseotags')then
select * from tbl_seoskiindia where tbl_seoskiindia.seo_id=row_id; 
end if;

end$$

CREATE  PROCEDURE `proc_tempaddoneadd_v`(IN `act_mode` VARCHAR(50), IN `categories1` VARCHAR(50), IN `addonid` VARCHAR(50), IN `addonprice` VARCHAR(50), IN `addonqty` VARCHAR(50), IN `addonimage` VARCHAR(50), IN `addonname` VARCHAR(50), IN `uniqueid` VARCHAR(50))
BEGIN


if(act_mode='insertaddone') then
INSERT INTO `tempaddoncarttable`(`uniqid`,`addonid`,`addonprice`,`addonqty`,`addonimage`,`addonname`) VALUES (uniqueid,addonid,addonprice,addonqty,addonimage,addonname);
select last_insert_id() as ls;
end if;
if(act_mode='aaddonedisplay') then

select * from tempaddoncarttable where uniqid=uniqueid;
end if;
if(act_mode='selectaddone') then

select * from tempaddoncarttable as t where t.uniqid=uniqueid and t.addonid=addonid;
end if;


if(act_mode='deleteaddone') then

delete  from tempaddoncarttable where tempaddon=addonid;
end if;

if(act_mode='updateaddone111') then
update tempaddoncarttable as t set t.addonprice = addonprice , t.addonqty=addonqty where t.addonid=addonid and t.uniqid=uniqueid;
end if;

if(act_mode='deleteaddones') then

delete  from tempaddoncarttable where addonid=addonid and uniqid=uniqueid;
end if;
END$$

CREATE  PROCEDURE `proc_timeslot_s`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN

if(act_mode = 's_addtimeslot') then
INSERT INTO `tbl_timeslot_branch`(`timeslot_branchid`, `timeslot_from`, `timeslot_to`, `timeslot_status`,timeslot_seats,timeslot_minfrom,timeslot_minto) VALUES (Param1,Param2,Param3,'1',Param4,Param5,Param6);
end if;

if(act_mode = 's_checktimeslot') then
select count(*) as cou  from tbl_timeslot_branch as t where t.timeslot_branchid=Param1 and t.timeslot_from=Param2 and t.timeslot_to=Param3 and t.timeslot_status=1;
end if;


if(act_mode = 's_viewtimeslot') then
select *,(select group_concat(timeslot_to) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslotto,(select group_concat(timeslot_from) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslotfrom,(select group_concat(timeslot_seats) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslotseats,(select group_concat(timeslot_minfrom) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslot_minfrom,(select group_concat(timeslot_minto) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslot_minto from tbl_branch_master as t where t.branch_status=1;
end if;

if(act_mode = 'deletesingletime') then
update tbl_timeslot_branch as t set t.timeslot_status=2 where t.timeslot_branchid=Param1 and t.timeslot_from=Param2 and t.timeslot_to=Param3;
end if;

END$$

CREATE  PROCEDURE `proc_viewtimeslot_v`(IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))
BEGIN
if(act_mode = 's_viewtimeslot') then
select *,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.timeslot_from and t1.departuredate=Param2 and t1.order_status in ('Shipped','Success') ) as coun from `tbl_timeslot_branch` as t where t.timeslot_branchid=Param1 and t.timeslot_status=1;
end if;
if(act_mode = 's_viewtimeslotdata') then
SELECT * from `tbl_orderpackage` WHERE departuretime = Param3;
end if;

if(act_mode = 's_viewtimeslotf') then
select *,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.timeslot_from and t1.departuredate=Param2 and t1.departuretime=Param3) as coun from `tbl_timeslot_branch` as t where t.timeslot_branchid=Param1 and t.timeslot_status=1 and t.timeslot_from=Param3;
end if;


if(act_mode = 's_viewtimeslotpackage') then
select *,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.timeslot_from and t1.departuredate=Param2  ) as coun ,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.timeslot_from and t1.departuredate=Param2 and t1.order_status='Aborted'  ) as Aborted from `tbl_timeslot_branch` as t where t.timeslot_branchid=Param1 and t.timeslot_status=1;
end if;

if(act_mode = 's_viewtimeslotaddones') then
select *,(SELECT count(t1.addonqty) from `tbl_orderaddone` as t1 join `tbl_orderpackage` as t2 on t2.pacorderid=t1.lastid    WHERE t1.addonid = t.addon_id and t2.departuredate=Param2  ) as coun,(SELECT count(t1.addonqty) from `tbl_orderaddone` as t1 join `tbl_orderpackage` as t2 on t2.pacorderid=t1.lastid    WHERE t1.addonid = t.addon_id and t2.departuredate=Param2  and t2.order_status='Aborted' ) as Aborted from `tbl_addon` as t where t.addon_branchid=Param1 and t.addon_status=1;
end if;


if(act_mode = 's_viewtimeslottimepackage') then
select *,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.dailyinventory_from and t1.departuredate=Param2 and t1.paymentstatus=1  ) as coun ,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.dailyinventory_from and t1.departuredate=Param2 and t1.order_status not in ('Shipped','Success')  ) as Aborted from `tbl_daily_inventory` as t where t.dailyinventory_branchid=Param1 and t.dailyinventory_status=1 and t.dailyinventory_date=Param2;
end if;

if(act_mode = 's_viewtimeslottimeaddones') then
select *,(SELECT count(t1.addonqty) from `tbl_orderaddone` as t1 join `tbl_orderpackage` as t2 on t2.pacorderid=t1.lastid    WHERE t1.addonid = t.addon_id and t2.departuredate=Param2  ) as coun,(SELECT count(t1.addonqty) from `tbl_orderaddone` as t1 join `tbl_orderpackage` as t2 on t2.pacorderid=t1.lastid    WHERE t1.addonid = t.addon_id and t2.departuredate=Param2  and t2.order_status='Aborted' ) as Aborted from `tbl_addon` as t where t.addon_branchid=Param1 and t.addon_status=1;
end if;

END$$

DELIMITER ;


CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(250) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tbladminmenu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `menuname` varchar(255) DEFAULT NULL,
  `parentid` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `position` varchar(500) DEFAULT NULL,
  `amlevel` int(11) DEFAULT NULL,
  `r_status` enum('Active','Inactive','Delete') DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tblassignmenu` (
  `assign_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `emp_id` int(11) NOT NULL DEFAULT '0',
  `main_menu` varchar(250) NOT NULL,
  `sub_menu` varchar(250) NOT NULL,
  PRIMARY KEY (`assign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tblassignrole` (
  `assign_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `emp_id` int(11) NOT NULL DEFAULT '0',
  `main_menu` int(11) NOT NULL,
  `sub_menu` varchar(100) NOT NULL,
  PRIMARY KEY (`assign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tblconfigdetails` (
  `configurationid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `insertdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  PRIMARY KEY (`configurationid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tblrole` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `role_status` tinyint(1) NOT NULL DEFAULT '1',
  `role_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `role_modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(50) DEFAULT NULL,
  `activity_discription` varchar(100) DEFAULT NULL,
  `activity_price` int(11) DEFAULT NULL,
  `activity_image` varchar(500) DEFAULT NULL,
  `activity_location` int(11) DEFAULT NULL,
  `activity_status` tinyint(1) DEFAULT NULL COMMENT '0,1,2',
  `activity_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `activity_modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_activity_package` (
  `ap_id` int(11) NOT NULL AUTO_INCREMENT,
  `ap_packageid` int(11) DEFAULT NULL,
  `ap_activityid` varchar(255) DEFAULT NULL,
  `ap_status` tinyint(1) DEFAULT NULL COMMENT '0,1,2',
  `ap_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `ap_modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`ap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_addon` (
  `addon_id` int(11) NOT NULL AUTO_INCREMENT,
  `addon_name` varchar(255) DEFAULT NULL,
  `addon_price` int(11) DEFAULT NULL,
  `addon_activityid` int(11) DEFAULT NULL,
  `addon_status` tinyint(4) DEFAULT NULL COMMENT '0,1,2',
  `addon_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `addon_modifiedon` datetime DEFAULT NULL,
  `addon_branchid` int(11) NOT NULL,
  `addon_description` text NOT NULL,
  `addon_image` varchar(255) NOT NULL,
  PRIMARY KEY (`addon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_adminlogin` (
  `LoginID` smallint(6) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(100) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `EmployeeId` smallint(6) DEFAULT NULL,
  `Role` smallint(6) DEFAULT NULL,
  `EmpStatus` char(1) DEFAULT 'A',
  `Modified_By` smallint(6) DEFAULT NULL,
  `Modified_On` datetime DEFAULT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`LoginID`),
  UNIQUE KEY `UNIQUE KEY` (`UserName`),
  KEY `FK_tbl_emplogin_tbl_employee` (`EmployeeId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;



CREATE TABLE IF NOT EXISTS `tbl_agent_payment_history` (
  `ph_id` int(11) NOT NULL AUTO_INCREMENT,
  `ph_agent_id` int(11) DEFAULT NULL,
  `ph_cred_debet_type` varchar(50) DEFAULT NULL,
  `ph_total_amount_atm` int(11) NOT NULL,
  `ph_amount` int(11) DEFAULT NULL,
  `ph_timing` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ph_orderid` int(11) NOT NULL,
  `ph_tracking_id` int(11) NOT NULL,
  `ph_order_status` varchar(255) NOT NULL,
  `ph_status_message` text NOT NULL,
  `ph_paymentmode` varchar(255) NOT NULL,
  `ph_paymentstatus` int(11) NOT NULL,
  `ph_ordersucesmail` int(11) NOT NULL,
  `ph_ordermailstatus` int(11) NOT NULL,
  `ph_agentdiscount` int(11) NOT NULL,
  `ph_commpaidunpaid` varchar(20) NOT NULL DEFAULT 'Unpaid',
  PRIMARY KEY (`ph_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_agent_profile` (
  `agent_id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_name` int(11) NOT NULL,
  `agent_address` int(11) NOT NULL,
  `agent_city` int(11) NOT NULL,
  `agent_state` int(11) NOT NULL,
  `agent_pincode` int(11) NOT NULL,
  `agent_telephone` int(11) NOT NULL,
  `agent_mobile` int(11) NOT NULL,
  `agent_fax` int(11) NOT NULL,
  `agent_website` int(11) NOT NULL,
  `agent_email` int(11) NOT NULL,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_agent_reg` (
  `agent_id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_agencyname` varchar(255) NOT NULL,
  `agent_office_address` text NOT NULL,
  `agent_city` varchar(255) NOT NULL,
  `agent_state` varchar(255) NOT NULL,
  `agent_pincode` int(255) NOT NULL,
  `agent_telephone` bigint(20) NOT NULL,
  `agent_mobile` bigint(20) NOT NULL,
  `agent_fax` int(255) NOT NULL,
  `agent_website` varchar(255) NOT NULL,
  `agent_email` varchar(255) NOT NULL,
  `agent_primarycontact_name` varchar(255) NOT NULL,
  `agent_primarycontact_email_mobile` varchar(255) NOT NULL,
  `agent_primarycontact_telephone` bigint(20) NOT NULL,
  `agent_secondrycontact_name` varchar(255) NOT NULL,
  `agent_secondrycontact_email_phone` varchar(255) NOT NULL,
  `agent_secondrycontact_telephone` bigint(20) NOT NULL,
  `agent_management_executives_name` varchar(255) NOT NULL,
  `agent_modified` date NOT NULL,
  `agent_s` int(11) NOT NULL,
  `agent_copytan` varchar(255) NOT NULL,
  `agent_copyaddressproof` varchar(255) NOT NULL,
  `agent_emailcurrency` varchar(255) NOT NULL,
  `agent_emailcountry` varchar(255) NOT NULL,
  `agent_username` varchar(255) NOT NULL,
  `agent_password` varchar(255) NOT NULL,
  `agent_createdon` date NOT NULL,
  `agent_status` int(11) NOT NULL DEFAULT '0',
  `agent_management_executives_email_phone` varchar(255) NOT NULL,
  `agent_management_executives_telephone` varchar(255) NOT NULL,
  `agent_branches` varchar(255) NOT NULL,
  `agent_yearofestablism` varchar(255) NOT NULL,
  `agent_organisationtype` varchar(255) NOT NULL,
  `agent_lstno` varchar(255) NOT NULL,
  `agent_cstno` varchar(255) NOT NULL,
  `agent_registrationno` varchar(255) NOT NULL,
  `agent_vatno` varchar(255) NOT NULL,
  `agent_servicetaxno` varchar(255) NOT NULL,
  `agent_tanno` varchar(255) NOT NULL,
  `agent_pfno` varchar(255) NOT NULL,
  `agent_esisno` varchar(255) NOT NULL,
  `agent_officeregistrationno` varchar(255) NOT NULL,
  `agent_exceptiontax` varchar(255) NOT NULL,
  `agent_otherexceptiontax` varchar(255) NOT NULL,
  `agent_bank_benificialname` varchar(255) NOT NULL,
  `agent_intermidiatebankname` varchar(255) NOT NULL,
  `agent_bank_ecs` varchar(255) NOT NULL,
  `agent_copypancart` varchar(255) NOT NULL,
  `agent_copyservicetax` varchar(255) NOT NULL,
  `logintype` int(11) NOT NULL DEFAULT '1',
  `agent_panno` varchar(255) NOT NULL,
  `agent_bank_benificialaccno` varchar(255) DEFAULT NULL,
  `agent_bank_benificialbankname` varchar(255) DEFAULT NULL,
  `bank_benificialaddress` text NOT NULL,
  `bank_benificialifsc` varchar(255) NOT NULL,
  `bank_benificialswiftcode` varchar(255) NOT NULL,
  `bank_benificialibanno` varchar(255) NOT NULL,
  `intermidiatebankswiftcode` varchar(255) NOT NULL,
  `agent_emailstatus` varchar(255) NOT NULL,
  `agent_intermidiatebankaddress` text NOT NULL,
  `countryid` int(11) DEFAULT NULL,
  `stateid` int(11) DEFAULT NULL,
  `cityid` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `locationid` int(11) DEFAULT NULL,
  `bank_creditamount` int(11) NOT NULL,
  `bank_agentcommision` int(11) NOT NULL,
  `agent_bank_benificialbranchname` varchar(255) NOT NULL,
  `agent_title` varchar(255) NOT NULL,
  `agent_firstname` varchar(255) NOT NULL,
  `agent_lastname` varchar(255) NOT NULL,
  `agentprofilecomplete` int(11) NOT NULL,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_bannerimage` (
  `bannerimage_id` int(11) NOT NULL AUTO_INCREMENT,
  `bannerimage_top` varchar(255) DEFAULT 'background.jpg',
  `bannerimage_country` int(11) DEFAULT NULL,
  `bannerimage_state` int(11) DEFAULT NULL,
  `bannerimage_city` int(11) DEFAULT NULL,
  `bannerimage_location` int(11) DEFAULT NULL,
  `bannerimage_branch` int(11) DEFAULT NULL,
  `bannerimage_logo` varchar(255) NOT NULL,
  `bannerimage_index` varchar(255) NOT NULL,
  `bannerimage_top1` varchar(255) NOT NULL,
  `bannerimage_top2` varchar(255) NOT NULL,
  `bannerimage_top3` varchar(255) NOT NULL,
  `bannerimage_top4` varchar(255) NOT NULL,
  `bannerimage_mailtype` varchar(255) DEFAULT NULL,
  `bannerimage_subject` varchar(255) DEFAULT NULL,
  `bannerimage_from` varchar(255) DEFAULT NULL,
  `bannerimage_branch_contact` varchar(255) DEFAULT NULL,
  `bannerimage_branch_email` varchar(255) DEFAULT NULL,
  `bannerimage_status` tinyint(4) NOT NULL DEFAULT '1',
  `internethandlingcharge` int(11) NOT NULL,
  PRIMARY KEY (`bannerimage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_bookingtypemaster` (
  `bookingtypeid` int(11) NOT NULL AUTO_INCREMENT,
  `bookingname` varchar(255) DEFAULT NULL,
  `booking_addedon` datetime DEFAULT NULL,
  `booking_modifiedon` datetime DEFAULT NULL,
  `booking_status` int(11) DEFAULT '1',
  `booking_country` int(11) DEFAULT NULL,
  `booking_state` int(11) DEFAULT NULL,
  `booking_city` int(11) DEFAULT NULL,
  `booking_location` int(11) DEFAULT NULL,
  `booking_branchid` int(11) NOT NULL,
  PRIMARY KEY (`bookingtypeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_branch_master` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(255) NOT NULL,
  `branch_company` int(255) NOT NULL,
  `branch_location` int(255) NOT NULL,
  `branch_url` varchar(50) DEFAULT 'http://115.124.98.243/~skiindia/',
  `branch_background` varchar(255) DEFAULT NULL,
  `branch_logo` varchar(255) DEFAULT NULL,
  `branch_internet_handling_charge` int(5) DEFAULT NULL,
  `branch_status` tinyint(1) NOT NULL COMMENT '0=inactive,1=active,2=deleted',
  `branch_add` varchar(255) DEFAULT NULL,
  `branch_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `branch_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_branch_package` (
  `bp_id` int(11) NOT NULL AUTO_INCREMENT,
  `bp_packageid` int(11) DEFAULT NULL,
  `bp_branchid` varchar(255) DEFAULT NULL,
  `bp_status` tinyint(1) DEFAULT NULL COMMENT '0,1,2',
  `bp_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `bp_modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`bp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_city` (
  `cityid` bigint(20) NOT NULL AUTO_INCREMENT,
  `stateid` bigint(20) NOT NULL,
  `cityname` varchar(100) DEFAULT NULL,
  `cstatus` varchar(50) DEFAULT '0',
  `createdon` datetime DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `countryid` int(11) NOT NULL,
  PRIMARY KEY (`cityid`),
  KEY `fk_State_idx` (`stateid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_company_master` (
  `comp_id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_name` varchar(255) NOT NULL,
  `comp_status` tinyint(1) NOT NULL COMMENT '0=inactive,1=active,2=deleted',
  `comp_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comp_modified` datetime NOT NULL,
  PRIMARY KEY (`comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `cont_id` int(11) NOT NULL AUTO_INCREMENT,
  `cont_fname` varchar(255) DEFAULT NULL,
  `cont_lname` varchar(255) DEFAULT NULL,
  `cont_email` varchar(255) DEFAULT NULL,
  `cont_phone` int(11) DEFAULT NULL,
  `cont_message` text,
  `cont_status` int(11) DEFAULT NULL,
  `cont_addedon` datetime DEFAULT NULL,
  PRIMARY KEY (`cont_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_country` (
  `conid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(50) NOT NULL,
  `counstatus` varchar(100) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`conid`),
  KEY `ttaxm_conid` (`conid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `custid` int(11) NOT NULL AUTO_INCREMENT,
  `custtype` enum('Corporate','Officer') NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedon` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_daily_inventory` (
  `dailyinventory_id` int(11) NOT NULL AUTO_INCREMENT,
  `dailyinventory_branchid` int(11) DEFAULT NULL,
  `dailyinventory_from` varchar(11) DEFAULT NULL,
  `dailyinventory_to` varchar(11) DEFAULT NULL,
  `dailyinventory_seats` int(11) DEFAULT NULL,
  `dailyinventory_status` tinyint(1) DEFAULT NULL,
  `dailyinventory_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `dailyinventory_modifiedon` datetime DEFAULT NULL,
  `dailyinventory_minfrom` varchar(11) NOT NULL,
  `dailyinventory_minto` varchar(11) NOT NULL,
  `dailyinventory_date` varchar(11) NOT NULL,
  PRIMARY KEY (`dailyinventory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_employee` (
  `emp_tbl_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_branch_id` int(11) NOT NULL,
  `emp_name` varchar(50) DEFAULT NULL,
  `emp_main_id` varchar(50) DEFAULT '100',
  `emp_email` varchar(50) DEFAULT NULL,
  `emp_password` varchar(50) DEFAULT NULL,
  `emp_contact` bigint(20) DEFAULT NULL,
  `emp_country` int(11) DEFAULT NULL,
  `emp_state` int(11) DEFAULT NULL,
  `emp_city` int(11) DEFAULT NULL,
  `emp_location` int(11) DEFAULT NULL,
  `emp_status` tinyint(1) DEFAULT '1' COMMENT '0,1,2',
  `emp_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `emp_modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`emp_tbl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_groupbooking` (
  `bookingid` int(11) NOT NULL AUTO_INCREMENT,
  `bookingType` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `departDate` varchar(20) DEFAULT NULL,
  `sessionTime` varchar(20) DEFAULT NULL,
  `number_people` int(11) DEFAULT NULL,
  `socialGroupRadio` varchar(255) DEFAULT NULL,
  `socialGroupName` varchar(255) DEFAULT NULL,
  `addeddate` datetime DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `stateid` int(11) NOT NULL,
  `cityid` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `locationid` int(11) NOT NULL,
  `countryid` int(11) NOT NULL,
  PRIMARY KEY (`bookingid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_location` (
  `locationid` int(11) NOT NULL AUTO_INCREMENT,
  `cityid` int(11) NOT NULL,
  `locationname` varchar(255) NOT NULL,
  `lstatus` varchar(20) NOT NULL COMMENT '0,1,2',
  `createdon` datetime NOT NULL,
  `modifiedon` datetime NOT NULL,
  `countryid` int(11) NOT NULL,
  `stateid` int(11) NOT NULL,
  PRIMARY KEY (`locationid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_orderaddone` (
  `orderaddoneid` int(11) NOT NULL AUTO_INCREMENT,
  `addonename` varchar(255) NOT NULL,
  `addonevisiter` int(11) NOT NULL,
  `addoneprice` float NOT NULL,
  `addonqty` int(11) NOT NULL,
  `lastid` int(11) NOT NULL,
  `addonid` int(11) NOT NULL,
  `Type` enum('Package','Addone') NOT NULL,
  `orderaddon_print_status` tinyint(4) NOT NULL DEFAULT '0',
  `orderaddon_print_quantity` int(11) NOT NULL DEFAULT '0',
  `addon_image` varchar(255) NOT NULL,
  PRIMARY KEY (`orderaddoneid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `tbl_orderpackage` (
  `pacorderid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `promocodeprice` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `addedon` datetime DEFAULT NULL,
  `countryid` int(11) DEFAULT NULL,
  `stateid` int(11) DEFAULT NULL,
  `cityid` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `locationid` int(11) DEFAULT NULL,
  `departuredate` varchar(200) DEFAULT NULL,
  `paymentstatus` int(11) DEFAULT '0',
  `paymentmode` varchar(255) DEFAULT 'Pending',
  `ticketid` varchar(255) DEFAULT NULL,
  `tracking_id` varchar(50) DEFAULT NULL,
  `packproductname` varchar(255) DEFAULT NULL,
  `packimg` varchar(255) DEFAULT NULL,
  `packpkg` int(11) DEFAULT NULL,
  `packprice` int(11) DEFAULT NULL,
  `orderstatus` int(11) DEFAULT '1',
  `op_ticket_print_status` tinyint(4) DEFAULT '0',
  `op_printed_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `internethandlingcharges` int(11) DEFAULT NULL,
  `op_usertype` varchar(255) DEFAULT 'User',
  `order_status` varchar(50) DEFAULT NULL,
  `status_message` varchar(50) DEFAULT NULL,
  `paymenttype` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `billing_name` varchar(255) DEFAULT NULL,
  `billing_email` varchar(255) DEFAULT NULL,
  `billing_tel` varchar(255) DEFAULT NULL,
  `billing_address` text,
  `billing_city` varchar(255) DEFAULT NULL,
  `billing_state` varchar(255) DEFAULT NULL,
  `billing_zip` varchar(255) DEFAULT NULL,
  `billing_country` varchar(255) DEFAULT NULL,
  `billing_pdf` varchar(255) DEFAULT NULL,
  `ordersucesmail` int(11) DEFAULT NULL,
  `ordermailstatus` int(11) DEFAULT NULL,
  `departuretime` varchar(200) DEFAULT NULL,
  `frommin` varchar(20) DEFAULT NULL,
  `tohrs` varchar(20) DEFAULT NULL,
  `tomin` varchar(20) DEFAULT NULL,
  `txtfromd` varchar(20) DEFAULT NULL,
  `txttod` varchar(20) DEFAULT NULL,
  `p_codename` varchar(255) NOT NULL,
  `op_ordertrackingupdate` int(11) NOT NULL,
  `order_status_date_time` varchar(255) NOT NULL,
  `op_orderjason` int(11) NOT NULL,
  PRIMARY KEY (`pacorderid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_packagevalue` (
  `packagevalueid` int(11) NOT NULL AUTO_INCREMENT,
  `packagenameses` varchar(255) NOT NULL,
  `package_priceses` int(11) NOT NULL,
  `lastidses` int(11) NOT NULL,
  PRIMARY KEY (`packagevalueid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_package_master` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(50) DEFAULT NULL,
  `package_description` text,
  `package_status` tinyint(1) DEFAULT NULL,
  `package_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `package_modifiedon` datetime DEFAULT NULL,
  `package_price` float NOT NULL,
  `package_image` varchar(255) NOT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_partner` (
  `part_id` int(11) NOT NULL AUTO_INCREMENT,
  `emaiid` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `type` enum('Corporate Login','Officer Login') DEFAULT NULL,
  PRIMARY KEY (`part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_payment_gatway` (
  `pg_id` int(11) NOT NULL AUTO_INCREMENT,
  `pg_branchid` int(11) NOT NULL,
  `pg_merchant_id` varchar(50) NOT NULL,
  `pg_access_key` varchar(200) NOT NULL,
  `pg_working_key` varchar(50) NOT NULL,
  `pg_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pg_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pg_status` tinyint(4) NOT NULL DEFAULT '1',
  `pg_sucess_link` varchar(100) NOT NULL,
  `pg_fail_link` varchar(100) NOT NULL,
  `pg_currency` varchar(50) NOT NULL,
  `pg_language` varchar(50) NOT NULL,
  `pg_prefix` varchar(20) NOT NULL,
  PRIMARY KEY (`pg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_promo` (
  `promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_locationid` int(11) DEFAULT NULL,
  `p_branchid` int(11) DEFAULT NULL,
  `p_codename` varchar(255) DEFAULT NULL,
  `p_type` text,
  `p_discount` int(255) DEFAULT NULL,
  `p_status` tinyint(1) DEFAULT NULL,
  `p_expiry_date` varchar(50) DEFAULT NULL,
  `p_allowed_per_user` int(11) DEFAULT NULL,
  `p_start_date` varchar(50) DEFAULT NULL,
  `p_allowed_times` int(11) DEFAULT NULL,
  `p_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `p_modifiedon` datetime DEFAULT NULL,
  `p_logintype` int(11) DEFAULT '0',
  PRIMARY KEY (`promo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_seoskiindia` (
  `seo_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `seo_pagename` varchar(500) DEFAULT NULL,
  `seo_meta_keyword` text,
  `seo_meta_title` text,
  `seo_meta_desc` text,
  `seo_meta_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`seo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_state` (
  `stateid` bigint(20) NOT NULL AUTO_INCREMENT,
  `countryid` int(11) NOT NULL,
  `statename` varchar(50) DEFAULT NULL,
  `statecode` varchar(50) NOT NULL,
  `isdeleted` tinyint(4) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`stateid`),
  KEY `ttaxm_stateid` (`stateid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_timeslot_branch` (
  `timeslot_id` int(11) NOT NULL AUTO_INCREMENT,
  `timeslot_branchid` int(11) DEFAULT NULL,
  `timeslot_from` varchar(11) DEFAULT NULL,
  `timeslot_to` varchar(11) DEFAULT NULL,
  `timeslot_seats` int(11) DEFAULT NULL,
  `timeslot_status` tinyint(1) DEFAULT NULL,
  `timeslot_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `timeslot_modifiedon` datetime DEFAULT NULL,
  `timeslot_minfrom` varchar(11) NOT NULL,
  `timeslot_minto` varchar(11) NOT NULL,
  PRIMARY KEY (`timeslot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(255) DEFAULT NULL,
  `user_lastname` varchar(255) DEFAULT NULL,
  `user_emailid` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_mobileno` varchar(255) DEFAULT NULL,
  `user_status` int(11) DEFAULT NULL,
  `user_createdon` datetime DEFAULT NULL,
  `user_modifiedon` datetime DEFAULT NULL,
  `user_Address` text NOT NULL,
  `user_varified` int(11) NOT NULL DEFAULT '0',
  `user_town` varchar(255) NOT NULL,
  `user_zip` varchar(255) NOT NULL,
  `user_country` varchar(255) NOT NULL,
  `user_title` varchar(20) NOT NULL,
  `user_dob` varchar(20) NOT NULL,
  `user_state` varchar(200) NOT NULL,
  `user_city` varchar(200) NOT NULL,
  `user_pincodes` varchar(200) NOT NULL,
  `user_usertype` varchar(255) NOT NULL DEFAULT 'User',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_userlog` (
  `userlogid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `logindate` datetime DEFAULT NULL,
  PRIMARY KEY (`userlogid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tbl_usertoken` (
  `userlogid` int(11) NOT NULL AUTO_INCREMENT,
  `createddate` datetime DEFAULT NULL,
  `expirydate` datetime DEFAULT NULL,
  `tokenid` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`userlogid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tempaddoncarttable` (
  `tempaddon` int(11) NOT NULL AUTO_INCREMENT,
  `uniqid` varchar(255) NOT NULL,
  `addonid` varchar(11) NOT NULL,
  `addonprice` varchar(11) NOT NULL,
  `addonqty` varchar(11) NOT NULL,
  `addonimage` varchar(255) NOT NULL,
  `addonname` varchar(255) NOT NULL,
  PRIMARY KEY (`tempaddon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
