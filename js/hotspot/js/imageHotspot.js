/**
 * Image Hotspot Slider version 1.0.0
 * 
 * Copyright 2014, Madmonkey Studios.
 * All rights reserved.
 */

;( function( window ) {

   var mobile = false,

   // Class definition / constructor
    imageHotspot = function imageHotspot($el, options) {
      // Initialization
      this.$el = $($el);
      this.backupOptions = options ? options : {};
      this.init();
   };

   imageHotspot.defaults = {
        setCurrent : 0,
        width : '100%',
        height : 400,
        ratio: 0.75,
        autoheight: false,
        imagePercent : 100,
        mainAxis: 'x',
        navigationArrows: true,
        navigationDots: true,
        thumbnailDots: false,
        animationTime: '600',
        animationEasing: 'ease',
        zoom: 1.5,
        enableFilters: true,
        enableBlendingModes: true,
        filterOpacity: 1,
        maskRadius: '15%',
        maskAnimateEaseOut: 'Quad.easeInOut',
        maskAnimateEaseIn: 'Quad.easeInOut',
        maskAnimateDurationIn: 0.5,
        maskAnimateDurationOut: 0.5,
        tooltipInsideMask: true,
        strokeWidth: 10,
        imageAlign: 'center',
        showCaptions: true,
        zoomDelay: 0,
        labelDistanceX: 0,
        labelDistanceY: 0,
        zoomOnHover: false,
        hideFilterAfterAnim: true,
        pointerZoom: true,
        blendFile: 'blend.php',
        strokeWidth: 10,
        strokeColor: '#ffffff',
        strokeOpacity: 1
   };

    // Instance methods
    imageHotspot.prototype = {

        init : function () {

            this.filterOptions();

            this.oneTimeConfig();

            this.setDimensions();

            this.iterateSlides();

            this.addNavigation();

            this.initEvents();

            this.$li = this.$ul.children;

        }, // end of init

        filterOptions : function (options) {

            var w_w = window.innerWidth;

            this.options = options ? copyObj(options) : copyObj(this.backupOptions);

            for (var opt in imageHotspot.defaults) {
                if (imageHotspot.defaults.hasOwnProperty(opt) && !this.options.hasOwnProperty(opt)) {
                    this.options[opt] = imageHotspot.defaults[opt];
                }    
            }

            if ( this.options.breakpoints ) {
                  for (var key in this.options.breakpoints) {
                    var obj = this.options.breakpoints[key],
                        max_w = obj['maxWidth'];
                   
                        if(w_w <= max_w) {  
                           for (var key in obj) {
                                this.options[key] = obj[key]; 
                            }
                        }    
                  }
            }
        

        }, // end of filterOptions

        oneTimeConfig : function () {

            var self = this;

            this.$el.classList.add('ihs-container');

            this.$slides = $$('.ihs-slide', this.$el);

            this.$slideContainer = document.createElement('div');

            this.$ul = document.createElement('ul');

            this.$ul.classList.add('ihs-list');

            this.$slideContainer.classList.add('ihs-slideContainer');

            this.$slideContainer.appendChild(this.$ul);

            this.$el.appendChild(this.$slideContainer);

            this.$content = document.createElement("div");

            this.$content.classList.add('ihs-content');

            this.$el.appendChild(this.$content);

            this.$captions = document.createDocumentFragment();

            this.captionTimeout;

            this.navArrows = false;

            this.navDots = false;

            this.firstTime = true;

            this.currentMarker;

            this.markerOutHandler = function(e) {
                self.markerOut();
            };

            this.pointerZoomHandler = function(e) {
                var cursor_x =  e.clientX - self.$el.offsetLeft;

                console.log(cursor_x);
            };

            this.animating = false;

            this.current = this.options.setCurrent;

            this.old = this.current;

            var svgNs = 'http://www.w3.org/2000/svg';

            if ( this.options.enableFilters ) {

                this.$filters = document.createDocumentFragment();

                var $gaussianBlur = document.createElementNS(svgNs,'feGaussianBlur'),
                    $morphologyDilate = document.createElementNS(svgNs,'feMorphology'),
                    $ColorMatrixSaturate = document.createElementNS(svgNs,'feColorMatrix'),
                    $ColorMatrixHuerotate = document.createElementNS(svgNs,'feColorMatrix'),
                    $ColorMatrixSepia = document.createElementNS(svgNs,'feColorMatrix'),
                    $blendMultiply = document.createElementNS(svgNs,'feBlend'),
                    $feImage = document.createElementNS(svgNs,'feImage'),
                    result = randomString(10);

                $gaussianBlur.setAttribute('stdDeviation', 0);
                $morphologyDilate.setAttribute('operator', 'dilate');
                $morphologyDilate.setAttribute('radius', 0);
                $ColorMatrixSaturate.setAttribute('type', 'saturate');
                $ColorMatrixSaturate.setAttribute('values', 1);
                $ColorMatrixHuerotate.setAttribute('type', 'hueRotate');
                $ColorMatrixHuerotate.setAttribute('values', 0);
                $ColorMatrixSepia.setAttribute('type', 'matrix');
                $ColorMatrixSepia.setAttribute('values', '.343 .669 .119 0 0 .249 .626 .130 0 0 .172 .334 .111 0 0 .000 .000 .000 1 0');
                $blendMultiply.setAttribute('in', 'SourceGraphic');
                $blendMultiply.setAttribute('in2', result);
                $feImage.setAttribute('x', '-50%');
                $feImage.setAttribute('y', '-50%');
                $feImage.setAttribute('width', '200%');
                $feImage.setAttribute('height', '200%');
                $feImage.setAttribute('result', result);


                this.$filters.appendChild($gaussianBlur);
                this.$filters.appendChild($morphologyDilate);
                this.$filters.appendChild($ColorMatrixSaturate);
                this.$filters.appendChild($ColorMatrixHuerotate);
                this.$filters.appendChild($ColorMatrixSepia);
                this.$filters.appendChild($feImage);
                this.$filters.appendChild($blendMultiply);

                this.$globalFilters = this.$filters.childNodes;

            }
   

        }, // end of oneTimeConfig

        setDimensions : function (update) {

            this.$el.style.maxWidth = this.options.width;

            this.$ul.style.width = (this.$slides.length *2) * this.$el.offsetWidth + 'px';

            PrefixedTransform(this.$ul, 'Transition', 'all ' + this.options.animationTime  + 'ms ' + this.options.animationEasing);
            

        }, // end of setDimensions

        initEvents : function () {

           var self = this,
               timer;

            function removeOld(e) {

                 if(e.target.classList){

                    // to do: replace classlist to make it crossbrowser
                    if (e.target.classList.contains('ihs-list')) {
                        self.toggleStatesAfter();
                    }

                 }   
            }
           
            PrefixedEvent(this.$ul, "transitionEnd", removeOld); 

            // update on resize window
            window.addEventListener('resize', function(event){
                   timer && clearTimeout(timer);
                   timer = setTimeout(function(){
                      self.update();
                   }, 500);
            });

        }, // end of initEvents

        iterateSlides : function () {

            var self = this;
            var i = 0; 

            var order = { };

            // store node elements in memory and remove it from the DOM  
           Array.prototype.forEach.call(this.$slides, function(each) {

                var thisSlide = each,
                    thisImg = getFirstchild(thisSlide),
                    firstSlide = i == this.current ? true : false,
                    $caption = $('.ihs-caption', each),
                    $hotspots = $('.ihs-hotspots', each),
                    $mask = document.createElement('li'),

                slideOptions = {
                   zoomImg : thisSlide.dataset.largeimg,
                   imageAlign : thisSlide.dataset.imageAlign,
                   zoom : thisSlide.dataset.zoom
                };    

                if ($caption) {    
                    $caption.dataset.order = i;  
                    self.$captions.appendChild($caption);
                } 

                self.$ul.appendChild($mask);

                    var tmpImg = new Image() ;
                        tmpImg.src = thisImg.src;

                    loadImage(thisImg, firstSlide, slideOptions, $hotspots, false, i);

                    function loadImage (a, b, c, d, e, f) {

                        tmpImg.onload = function () {

                             self.setSlideDimensions(a, b, c, d, e, f); 
                             thisSlide.remove();

                             if ( f == self.options.setCurrent ) {
                                self.navigate(self.options.setCurrent);  
                             }
                        } 

                    }

                i++;
               
            });

           

        }, // end of parseMarkup


      setSlideDimensions : function ($img, firstSlide, slideOptions, hotspots, $slideToUpdate, slideOrder) {

         var self = this,
             $originalImg = $img,
             mainAxis = this.options.mainAxis == 'x' ? true : false,   
             img_w = $originalImg.width,
             img_h = $originalImg.height,
             ratio = mainAxis ? img_h / img_w : img_w / img_h,
             mainContainerLarge = mainAxis ? this.$el.offsetWidth : this.options.height,
             mainAxisLarge = mainAxis ? img_w : img_h,
             canvas_w = mainAxis ? mainContainerLarge * (this.options.imagePercent/100) : ( mainContainerLarge * (this.options.imagePercent/100) ) * ratio,
             canvas_h = mainAxis ? ( mainContainerLarge * (this.options.imagePercent/100) ) * ratio : mainContainerLarge * (this.options.imagePercent/100),
             imgSrc = $originalImg.src,
             svgNs = 'http://www.w3.org/2000/svg',

         $mask = $slideToUpdate ? $slideToUpdate : $$('li', this.$ul)[slideOrder],
         $canvas = $slideToUpdate ? getFirstchild($slideToUpdate) : document.createElement('div'),     
         $svg = $slideToUpdate ? $('svg', $slideToUpdate) : document.createElementNS(svgNs,'svg'),
         $image = $slideToUpdate ? $('.mainImg', $slideToUpdate) : document.createElementNS(svgNs,'image');

         if ( this.options.autoheight ) {
            var slide_h = canvas_h;
         } else {
            var slide_h = this.options.ratio ? this.$el.offsetWidth * this.options.ratio : this.options.height;
         }   
       
         $mask.style.width = this.$el.offsetWidth +'px';
         $mask.style.height = slide_h + 'px';
         $canvas.style.width = canvas_w+'px';
         $canvas.style.height = canvas_h+'px';
         
         if ( firstSlide && this.options.autoheight ) {
            this.$slideContainer.style.height = canvas_h+'px';
         }

         if ( !$slideToUpdate ) {

             $canvas.classList.add('ihs-canvas');
             $canvas.style.opacity = 0;

             var imageAlign = slideOptions.imageAlign || this.options.imageAlign;

             switch(imageAlign) {

                case 'left':
                     var canvas_x = 0;
                break;

                case 'right':
                     var canvas_x = 100-this.options.imagePercent;
                break;

                case 'center':
                     var canvas_x = (100-this.options.imagePercent)/2;
                break;

             }     

             $canvas.style.left = canvas_x+'%';   

             $mask.classList.add('ihs-slide');
             $image.setAttributeNS(null, 'x', 0);
             $image.setAttributeNS(null, 'y', 0);
             $image.setAttribute('class', 'mainImg');
             $image.setAttributeNS('http://www.w3.org/1999/xlink', 'href', imgSrc);

             // var $rectFlood = document.createElementNS(svgNs,'rect');

             // $rectFlood.setAttribute('x', 0);
             // $rectFlood.setAttribute('y', 0);
             // $rectFlood.setAttribute('width', '100%');
             // $rectFlood.setAttribute('height', '100%');
             // $rectFlood.setAttribute('fill', '#FF0000');
             // $rectFlood.setAttribute('id', randomString(10));
             // $rectFlood.setAttribute('class', 'rectFlood');

             if ( this.options.enableFilters ) {

                 var $filter = document.createElementNS(svgNs,'filter'),
                     $defs = document.createElementNS(svgNs,'defs'),
                     id = randomString(10),
                     $imageFilter = $image.cloneNode(),
                     $gaussianBlur = document.createElementNS(svgNs,'feGaussianBlur');

                 $gaussianBlur.setAttribute('stdDeviation', 0);    
                 $filter.setAttribute('id', id);
                 $filter.setAttribute('x', 0);
                 $filter.setAttribute('y', 0);
                 $filter.setAttribute('width', '100%');
                 $filter.setAttribute('height', '100%');
                 $filter.appendChild($gaussianBlur);
                 $filter.setAttribute('class', 'mainFilters');
                 //$defs.appendChild($rectFlood);
                 $defs.appendChild($filter);
                 $imageFilter.setAttribute('filter', 'url(#'+ id +')');
                 $imageFilter.setAttribute('class', 'filterImg');
                 $svg.appendChild($defs); 
                 $svg.appendChild($imageFilter);

             }   

             $svg.appendChild($image);
             $svg.appendChild($imageFilter);
            
             $canvas.appendChild($svg);
             $mask.appendChild($canvas);

         } else {
             var $imageFilter = $('.filterImg', $slideToUpdate);
         }

         if( slideOptions) {
           if ( slideOptions.zoomImg ) {

              var $zoomImg =  document.createElementNS(svgNs,'image'),
                  $zoomMask = document.createElementNS(svgNs,'mask'),
                  $maskCircle = document.createElementNS(svgNs,'circle'),
                  $borderCircle = document.createElementNS(svgNs,'circle');
                  maskID = randomString(10);
                 

              $zoomMask.setAttribute('x', 0);
              $zoomMask.setAttribute('y', 0);
              $zoomMask.setAttribute('width', '100%');
              $zoomMask.setAttribute('height', '100%');
              $zoomMask.setAttribute('id', maskID);

              $maskCircle.setAttribute('fill', '#ffffff');

              $zoomMask.appendChild($maskCircle);  
              $defs.appendChild($zoomMask);
              
              $zoomImg.setAttributeNS(null, 'x', 0);
              $zoomImg.setAttributeNS(null, 'y', 0);
              $zoomImg.setAttributeNS('http://www.w3.org/1999/xlink', 'href', slideOptions.zoomImg);
              $zoomImg.setAttributeNS(null, 'mask', 'url(#'+ maskID +')');
              $zoomImg.setAttribute('class', 'zoomImg');
              $maskCircle.setAttribute('class', 'maskCircle');
              $borderCircle.setAttribute('class', 'borderCircle');
              $borderCircle.setAttribute('fill', '#ffffff');
              $borderCircle.setAttribute('fill-opacity', 0);
              $svg.appendChild($zoomImg);
              $svg.appendChild($borderCircle);
           }  
         }  

         if( $slideToUpdate ) {
            $zoomImg = $('.zoomImg', $slideToUpdate);

            var currentDraggYPercent = $slideToUpdate.dataset.currentDragY,
                currentDragY = (currentDraggYPercent / 100) * $canvas.offsetHeight;

             $canvas.style['transform'] = 'translate3d(0px, -' + currentDragY + 'px, 0px)';
         }

         $image.setAttribute('width', canvas_w);
         $image.setAttribute('height', canvas_h);

         $imageFilter.setAttribute('width', canvas_w);
         $imageFilter.setAttribute('height', canvas_h);

         if ( hotspots ) {
            var $hotspotContainer = hotspots.cloneNode(true),
                $markers = document.createElement('div'),
                $hotspots = $hotspotContainer.children;

            for(var k in $hotspotContainer.dataset) {  
                $markers.dataset[k] = $hotspotContainer.dataset[k];
            }  

            $markers.classList.add('ihs-markers');    
            
            for (var i = 0; i <= $hotspots.length - 1; i++) {

                 var thisHotspot = $hotspots[i],
                     $marker = thisHotspot.cloneNode(),
                     left = thisHotspot.dataset.x + '%',
                     top = thisHotspot.dataset.y + '%';

                 $marker.style.left = left;
                 $marker.style.top = top;
                 $marker.addEventListener(self.options.zoomOnHover ? "mouseover" : 'click', function(e) {
                      self.markerIn(e);
                 });

                 $marker.innerHTML = '<a href="#">More</a>';
                 $markers.appendChild($marker);

            }
             if( this.options.tooltipInsideMask ) {
                $canvas.appendChild($hotspotContainer);
            } else {
                $mask.appendChild($hotspotContainer);
            }

            
            $canvas.appendChild($markers);
         }

         if( slideOptions) {

             if ( $canvas.offsetWidthh > $mask.offsetWidth || $canvas.offsetHeight > $mask.offsetHeight ) {
                  Draggable.create($canvas, {type:"x,y", edgeResistance:1, bounds: $mask, throwProps:true});
             }

            $mask.style.opacity = 1;
            $canvas.style.opacity = 1;

            $imageFilter.addEventListener("mousemove", self.markerOutHandler);

            if ( this.options.pointerZoom ) {
                 $image.addEventListener("mouseenter", self.pointerZoomHandler);
            }        
          
        }

         


      }, // end of setSlideDimensions


      addNavigation : function () {

            var self = this;
          
            this.$renderedSlides = this.$ul.childNodes;

            if ( this.$slides.length > 1 ) {
          
                if( this.options.navigationArrows && !this.navArrows ) {

                    // add navigation arrows:
                    this.$navArrows = document.createElement("div");
                    this.$navPrev = document.createElement("span");
                    this.$navNext = document.createElement("span");

                    this.$navArrows.appendChild(this.$navPrev);
                    this.$navArrows.appendChild(this.$navNext);

                    this.$navPrev.classList.add('ihs-prev');
                    this.$navNext.classList.add('ihs-next');

                    this.$el.appendChild(this.$navArrows);
                    this.$navArrows.classList.add('active');

                    this.navArrows = true;

                    // next arrow - move forward
                    this.$navNext.addEventListener("click", function() {
                        self.navigate('next');
                    });

                    // prev arrow - move backwards
                    this.$navPrev.addEventListener("click", function() {
                        self.navigate('prev');
                    });

                } else if ( this.options.navigationArrows ) {
                    this.$navArrows.classList.add('active');
                } else if ( !this.options.navigationArrows ) {
                    if(this.$navArrows) {
                       this.$navArrows.classList.remove('active');
                    }   
                }



                if ( this.options.navigationDots && !this.navDots ) {

                // add navigation dots
                this.$navDots = document.createElement("div"); 
                for( var i = 0; i < this.$slides.length; ++i ) {
                  // current dot will have the class skw-current
                  var dot = this.options.thumbnailDots ? document.createElement("div") : document.createElement("span");

                  if ( this.options.thumbnailDots ) {
                      
                     var src = $('img', this.$slides[i]).src,
                          $thumb = document.createElement("img");

                      $thumb.setAttribute('src', src); 
                      dot.appendChild($thumb);
                  }   

                  this.$navDots.appendChild(dot);
                }

                
                this.$el.appendChild(this.$navDots);
                this.$navDots.classList.add('ihs-nav');
                //this.$navDots.style.marginLeft = '-' + this.$navDots.offsetWidth/2 +'px';

                if ( this.options.thumbnailDots ) {
                   this.$navDots.classList.add('ihs-navThumbs');
                }

                this.$navDots.classList.add('active');

                this.navDots = true;

                 // add an event listener to each nav dot
                 for (var i = this.$navDots.childNodes.length - 1; i >= 0; i--) {
                      this.$navDots.childNodes[i].addEventListener("click", function(e) {
                            if(self.isUpdating) {
                                return false;
                            }

                            self.navigate(index(this));
                       });
                  } 

            } else if ( this.options.navigationDots ) {
                this.$navDots.classList.add('active');
            } else if ( !this.options.navigationDots ) {
                if(this.$navDots) {
                   this.$navDots.classList.remove('active');
                }   
            }
        }    

          
          

      }, // end of addNavigation

      navigate : function (pos) {
       
          this.old = this.current;
          this.currentMarker = 999999;
          this.firstMarker = true;

          if ( this.current == pos && !this.firstTime ) {
             return false;
          }

          this.firstTime = false;

          if ( pos == 'next') {
              this.current++;
          } else if ( pos == 'prev' ) {
              this.current--;
          } else if ( typeof pos == 'number') {
              this.current = pos;
          }

          if ( this.current > this.$slides.length -1 ) {
              this.current = 0;
          } else if ( this.current < 0 ) {
              this.current = this.$slides.length -1
          }

          this.$thisSlide = this.$li[this.current];
          this.$filterImg = $('.filterImg', this.$thisSlide);
          this.$mainImg = $('.mainImg', this.$thisSlide);
          this.$filter = $('.mainFilters', this.$thisSlide);
          this.$maskCircle = $('.maskCircle', this.$thisSlide);
          this.$borderCircle = $('.borderCircle', this.$thisSlide);
          this.$zoomImg = $('.zoomImg', this.$thisSlide);
          this.$markers = $('.ihs-hotspots', this.$thisSlide);

          this.toggleStates();
          this.transitionSlideBasic();
          
      }, // end of navigate

      toggleStates : function () {

        if ( this.$slides.length > 1 ) {

             if ( this.options.navigationDots || this.navDots ) {

                   this.$navDots.childNodes[this.old].className = '';
                   this.$navDots.childNodes[this.current].className = 'ihs-current';

               } 

        }       

         if ( this.options.autoheight ) {
              var $slides = this.$ul.children;
              this.$slideContainer.style.height = $slides[this.current].style.height;
         }   

         // hide the html content
         this.$content.classList.add('out');
         this.$content.classList.remove('active');    
          

      }, // end of toggleStates

      toggleStatesAfter : function () {
        
            this.loadCaption();
        
     
      }, // end of toggleStatesAfter

      markerIn : function (e) {

         if ( this.animating ) return false;

          var $target = e.target,
              markerIndex = index(e.target),
              sameMarker = markerIndex == this.currentMarker ? true : false,
              self = this;

          this.currentMarker = markerIndex; 
          this.animating = true;

          var $thisSlide = $target.parentNode.parentNode,
              localVars = $target.parentNode.dataset.localSettings, 
              dataset = !localVars ? $target.parentNode.dataset : $target.dataset,
              markerDataset = $target.dataset,
              $filter = this.$filter,
              $filters = this.$globalFilters,
              $borderCircle = this.$borderCircle,
              $zoomImg = this.$zoomImg,
              $filterImg = this.$filterImg,
              $maskCircle = this.$maskCircle,
              filterOpacity = dataset.filterOpacity || 1;

          this.$el.classList.add('ihs-masked'); 

          if ( !sameMarker && localVars || this.firstMarker ) {

             empty_element($filter);

             if (dataset.blendingMode) {
                var $feImage = $filter.appendChild($filters[5].cloneNode()),
                    $blendMultiply = $filter.appendChild($filters[6].cloneNode()),
                    blendSrc = this.options.blendFile+ '?hex='+ dataset.blendingColor.replace('#', '');
                    // $rectFlood = $('.rectFlood', $thisSlide),
                    // $id = $rectFlood.getAttribute('id');    
                
                $blendMultiply.setAttribute('mode', dataset.blendingMode);
                $feImage.setAttributeNS('http://www.w3.org/1999/xlink', 'href', blendSrc);
                $feImage.setAttributeNS(null, 'preserveAspectRatio', 'none');

                var tmpImg = new Image() ;
                tmpImg.src = blendSrc;

                tmpImg.onload = function () {
                    $filterImg.style.opacity = dataset.filterOpacity || self.options.filterOpacity;
                }
             } else {

                  setTimeout(function(){
                      $filterImg.style.opacity = dataset.filterOpacity || self.options.filterOpacity;
                  }, (self.options.zoomDelay*1000)/4);

             }      

             if (dataset.filterBlur) {
                var blur = $filter.appendChild($filters[0].cloneNode()); 
                blur.setAttribute('stdDeviation', dataset.filterBlur);
             } 

             if (dataset.filterDilate) {
                var dilate = $filter.appendChild($filters[1].cloneNode()); 
                dilate.setAttribute('radius', dataset.filterDilate);
             } 

             if (dataset.filterSaturate) {
                var saturate = $filter.appendChild($filters[2].cloneNode()); 
                saturate.setAttribute('values', dataset.filterSaturate);
             }

             if (dataset.filterHuerotate) {
                var hueRotate = $filter.appendChild($filters[3].cloneNode()); 
                hueRotate.setAttribute('values', dataset.filterHuerotate);
             }

             if (dataset.filterSepia) {
                $filter.appendChild($filters[4].cloneNode()); 
             } 

             this.firstMarker = false;
            

         } else {


                  setTimeout(function(){
                      $filterImg.style.opacity = dataset.filterOpacity || self.options.filterOpacity;
                  }, (self.options.zoomDelay*1000)/4);


         }

         if ( !sameMarker || this.firstFromUpdate ) {

             var markerX = (markerDataset.x*1),
                 markerY = (markerDataset.y*1);  

             if ( markerDataset.xOffset ) {
                markerX += (markerDataset.xOffset*1);
             }

             if ( markerDataset.yOffset ) {
                markerY += (markerDataset.yOffset*1);
             }     

             $borderCircle.setAttribute('cx', markerX +'%');
             $borderCircle.setAttribute('cy', markerY+'%');
             $borderCircle.setAttribute('stroke-width', dataset.strokeWidth || 0);
             $borderCircle.setAttribute('stroke', dataset.strokeColor);
             $borderCircle.setAttribute('stroke-opacity', dataset.strokeOpacity || 1);
             $borderCircle.setAttribute('r', 0);
             if ( dataset.strokeDash ) {
                 $borderCircle.setAttribute('stroke-dasharray', dataset.strokeDash);
             }
          

            var mainImg_w = this.$mainImg.getAttribute('width'),
                mainImg_h = this.$mainImg.getAttribute('height'),  
                zoom = markerDataset.zoom || dataset.zoom || this.options.zoom,
                xOffset = (mainImg_w * (zoom-1)) * ( markerX / 100 );
                yOffset = (mainImg_h * (zoom-1)) * ( markerY / 100 );


             $zoomImg.setAttribute('width', mainImg_w*zoom);
             $zoomImg.setAttribute('height', mainImg_h*zoom);
             $zoomImg.setAttribute('x', -xOffset);
             $zoomImg.setAttribute('y', -yOffset);

             $maskCircle.setAttribute('cx', markerX+'%');
             $maskCircle.setAttribute('cy', markerY+'%');
             $maskCircle.setAttribute('r', 0);

              this.firstFromUpdate = false;

         }

          var objGhost = {
                    r: 0
                  },
              maskRadius = markerDataset.maskRadius || dataset.maskRadius || this.options.maskRadius;  

         
          if ( typeof maskRadius == 'string' ) {  
              if ( maskRadius.indexOf('%') > -1 ) {
                  maskRadius = (maskRadius.replace("%", "") * $thisSlide.offsetWidth)/100;
              } 
          }       

          TweenLite.to(objGhost, this.options.maskAnimateDurationIn, {
                r: maskRadius,
                onUpdate: onUpdate,
                ease: this.options.maskAnimateEaseIn,
                onComplete: onComplete,
                delay: self.options.zoomDelay
          });

          var radius = dataset.maskRadius || this.options.maskRadius,
              scale = (radius * 2) / e.target.offsetWidth,
              strokeWidth = dataset.strokeWidth || this.options.strokeWidth;

          function onUpdate () {
             $maskCircle.setAttribute('r', objGhost.r);
             $borderCircle.setAttribute('r', objGhost.r + (strokeWidth/2));  
          }

          function onComplete () {

               if ( !self.animating ) return false;
               var $captions = $('.ihs-hotspots', $thisSlide.parentNode).children,
                   captionAlign = markerDataset.labelAlign,
                   markerIndex = index(e.target);
 
               if ( captionAlign ) {

                     var caption_h = $captions[markerIndex].offsetHeight,
                         slide_h = $thisSlide.offsetHeight,
                         y_fix = getPercent(caption_h, slide_h) / 2,
                         caption_w = $captions[markerIndex].offsetWidth,
                         slide_w = $thisSlide.offsetWidth,
                         x_fix = getPercent(caption_w, slide_w) / 2,
                         maskPercentW = getPercent(maskRadius, slide_w),
                         maskPercentH = getPercent(maskRadius, slide_h),
                         $currentCaption = $captions[markerIndex],

                     capTop = (markerDataset.y - y_fix),
                     capLeft = (markerDataset.x - x_fix),
                     capBottom = (Math.abs(markerDataset.y - 100) - y_fix),
                     capRight = (Math.abs(markerDataset.x - 100) - x_fix),
                     labelDistanceX = markerDataset.labelDistanceX*1 || self.options.labelDistanceX,
                     labelDistanceY = markerDataset.labelDistanceY*1 || self.options.labelDistanceY;  

                    

                     switch(captionAlign) {

                        case 'right':
                             capLeft += maskPercentW + x_fix;
                             capLeft += labelDistanceX;
                             capTop += labelDistanceY; 

                             $currentCaption.style.top = capTop + '%';
                             $currentCaption.style.left = capLeft +'%';
                        break;

                        case 'left':
                             capRight += maskPercentW + x_fix;
                             capRight += labelDistanceX;
                             capTop += labelDistanceY; 

                             $currentCaption.style.top = capTop + '%';
                             $currentCaption.style.right = capRight +'%';
                        break;

                        case 'bottom':
                             capTop += maskPercentH + y_fix;
                             capTop += labelDistanceY;
                             capLeft += labelDistanceX; 

                             $currentCaption.style.top = capTop + '%';
                             $currentCaption.style.left = capLeft +'%';
                        break;

                        case 'top':
                             capBottom += maskPercentH + y_fix;
                             capBottom += labelDistanceY;
                             capLeft += labelDistanceX; 

                             $currentCaption.style.bottom = capBottom + '%';
                             $currentCaption.style.left = capLeft +'%';
                        break;
                       
                     }     

                     

               }

               $captions[markerIndex].classList.add('active');
               self.$el.classList.add('ihs-maskedComplete');

               //Draggable.create($maskCircle, {type:"x,y", edgeResistance:1, throwProps:true});
          }
          

      }, // end of markerIn

      markerOut : function () {

          this.$el.classList.remove('ihs-maskedComplete');

          var self = this,
              $markers = this.$markers.children;
              $target = $markers[self.currentMarker],
              $thisSlide = $target.parentNode.parentNode,
              $filterImg = this.$filterImg,
              $maskCircle = this.$maskCircle,
              $borderCircle = this.$borderCircle,
              $zoomImg = this.$zoomImg,
              localVars = $target.parentNode.dataset.localSettings, 
              dataset = !localVars ? $target.parentNode.dataset : $target.dataset,
              markerDataset = $target.dataset;
    
          this.animating = false; 

          var maskRadius = markerDataset.maskRadius || dataset.maskRadius || this.options.maskRadius;  

         if ( typeof maskRadius == 'string' ) {
             if ( maskRadius.indexOf('%') > -1 ) {
                  maskRadius = (maskRadius.replace("%", "") * $thisSlide.offsetWidth)/100;
              }  
          }        

          var objGhost = {
                    r: maskRadius
                  };

          TweenLite.to(objGhost, this.options.maskAnimateDurationOut, {
                r: 0,
                onUpdate: onUpdate,
                ease: this.options.maskAnimateEaseOut,
                onComplete : onComplete
          });

          function onUpdate () {
              $maskCircle.setAttribute('r', objGhost.r);
              $borderCircle.setAttribute('r', objGhost.r);  
          }

         function onComplete () {
            self.$el.classList.remove('ihs-masked');
            if ( self.options.hideFilterAfterAnim ) {
              self.$filterImg.style.opacity = 0;
            }
         }

         if ( !self.options.hideFilterAfterAnim ) {
              self.$filterImg.style.opacity = 0;
         }

          var $captions = $('.ihs-hotspots', $thisSlide.parentNode).children;

          $captions[self.currentMarker].classList.remove('active'); 


         

      }, // end of markerOut

      loadCaption : function () {

         if ( !this.options.showCaptions ) { return false; }

         var self = this;
      
         // html content to load 
         self.$content.innerHTML = '';

         var captionPos = false,
             $captions = self.$captions.childNodes;  
         
         for (var i = $captions.length - 1; i >= 0; i--) {
            var thisCaption = $captions[i].dataset.order;

            if(thisCaption == self.current) {
               captionPos = i;
            }
         }
          if ( captionPos || captionPos == 0 ) {

            var currentCaption =  self.$captions.childNodes[captionPos];

            if ( currentCaption ) {

                self.$content.appendChild(currentCaption.cloneNode(true));
                self.$content.classList.add('ihs-noTransition');
                self.$content.classList.remove('out');

                // add active class to html container
                setTimeout(function(){
                    self.$content.classList.remove('ihs-noTransition');
                    self.$content.classList.add('active');
                }, 50);

            } 

         }

      }, // loadCaption

      update : function () {

          this.filterOptions();

          //this.currentMarker = 999999;

          this.setDimensions(true);

          var $slides = this.$renderedSlides,
              $currentSlide = $slides[this.current];
  
          for (var i = 0; i < $slides.length; i++) {

              var $canvas = getFirstchild( $slides[i] ),
                  currentDraggY = getTransformMatrix( $canvas ),
                  currentDraggYPercent = Math.abs( getPercent( currentDraggY[5], $canvas.offsetHeight ) );
              
              $slides[i].dataset.currentDragY = currentDraggYPercent;    

              this.setSlideDimensions(getFirstchild(this.$slides[i]), false, null, false, $slides[i]);
          }

          var currentSlide_h = getFirstchild($currentSlide).offsetHeight;

          if ( this.options.autoheight ) {
              this.$slideContainer.style.height = $slides[this.current].style.height;
          } else {

              var slide_h = this.options.ratio ? this.$el.offsetWidth * this.options.ratio : this.options.height;
          
              if ( currentSlide_h < slide_h ) {
                 this.$slideContainer.style.height = currentSlide_h + 'px';
              } else {
                 this.$slideContainer.style.height = slide_h + 'px';
              }

          }

          this.firstFromUpdate = true;

          if( this.$el.classList.contains('ihs-masked') ) {
             this.markerOut();
          }   

          this.transitionSlideBasic();  

      }, // end of update

      transitionSlideBasic : function () {

          var pxToMove = this.$el.offsetWidth;
          PrefixedTransform(this.$ul, 'Transform', 'translate3D('+ -((pxToMove * this.current)) +'px,0,0)');

      }, // end of transitionSlideBasic



    };

    window.imageHotspot = imageHotspot;

    // Helper Functions

    Element.prototype.remove = function() {
      this.parentElement.removeChild(this);
    };
    NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
        for(var i = 0, len = this.length; i < len; i++) {
            if(this[i] && this[i].parentElement) {
                this[i].parentElement.removeChild(this[i]);
            }
        }
    };

    var empty_element = function (element) {

      var node = element;
      while (element.hasChildNodes()) {              // selected elem has children

          if (node.hasChildNodes()) {                // current node has children
              node = node.lastChild;                 // set current node to child
          }
          else {                                     // last child found
              node = node.parentNode;                // set node to parent
              node.removeChild(node.lastChild);      // remove last node
          }
       }
    };

    // get the fisrst child
    function getFirstchild(n) {
        x=n.firstChild;
        while (x.nodeType!=1)
          {
          x=x.nextSibling;
          }
        return x;
    }

     // get index position
    function index(node)
    {
        var i = 0;
        while (node = node.previousSibling) {
            if (node.nodeType === 1) { ++i }
        }
        return i;
    }

    imageLoaded = function(node) {
       var w = 'undefined' != typeof node.clientWidth ? node.clientWidth : node.offsetWidth;
       var h = 'undefined' != typeof node.clientHeight ? node.clientHeight : node.offsetHeight;
       return w+h > 0 ? true : false;
    };

     // angles to radians
    function toRadians (angle) {
        return angle * (Math.PI / 180);
    } 
    
     // get percent 
     function getPercent (val, val2) {
        return (val * 100)/val2;
     } 

    // returns node by unique selector
    function $ (selector, el) {
         if (!el) {el = document;}
         return el.querySelector(selector);
    }

    // returns a nodelist matching the selector
    function $$ (selector, el) {
        if (!el) {el = document;}
        return Array.prototype.slice.call(el.querySelectorAll(selector));
    }

    // add attributes to nodes
    function addAttr (el, attrObj) {
        for (var key in attrObj) {
           el.setAttribute(key, attrObj[key]);
        }
    }

    // get index position
    function index(node)
    {
        var i = 0;
        while (node = node.previousSibling) {
            if (node.nodeType === 1) { ++i }
        }
        return i;
    }

    // get the transform matrix
    function getTransformMatrix (el) {
         var computedStyle = getComputedStyle(el, null),
             transformString = computedStyle.transform || computedStyle.webkitTransform || computedStyle.mozTransform;

         return transformString.substr(7, transformString.length - 8).split(', ');
    }

    // add prefixed event listeners
    var pfx = ["webkit", "moz", "MS", "o", ""];
    function PrefixedEvent(element, type, callback) {
       for (var p = 0; p < pfx.length; p++) {
          if (!pfx[p]) type = type.toLowerCase();
          element.addEventListener(pfx[p]+type, callback, false);
       }
    }

     // remove prefixed event listeners
     function removePrefixedEvent(element, type, listener) {
       for (var p = 0; p < pfx.length; p++) {
          if (!pfx[p]) type = type.toLowerCase();
          element.removeEventListener(pfx[p]+type, listener, false);
       }
    }

    // add prefixed transforms
    function PrefixedTransform(element, type, value) {
       for (var p = 0; p < pfx.length; p++) {
          if (!pfx[p]) type = type.toLowerCase();
             element.style[pfx[p]+type] = value;
       }
    }

    function randomString(howmanyChars)
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < howmanyChars; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    function copyObj(o) {
        var copy = Object.create(o);
        for (prop in o) {
          if (o.hasOwnProperty(prop)) {
            copy[prop] = o[prop];
          }
        }
        return copy;
      }

      function logCache(source, storage) {
          if (storage.cachedElements.indexOf(source, 0) < 0) {
              if (storage.cachedElements != "") 
                  storage.cachedElements += ";";
              storage.cachedElements += source;

             return false;
          } else {
             return true;
          }
      }

      function cached(source, storage) {
          return (storage.cachedElements.indexOf(source, 0) >= 0);
      }
    
} )( window );