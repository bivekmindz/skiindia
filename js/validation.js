
var DOMHomeCache=(function() {
        var expose = {};
        expose.refreshStaticElements = function() {
            expose.isSmallScreen=$("#small-screen").is(':visible');

        };
        expose.setDefaultForm= function(){
        	expose.tripDate=$('#datepicker1');
        	expose.inputVisitor=$('#inputVisitor1');
        	expose.sessionType=$('#sessionType1');
        	expose.bookNow1=$('#bookNow1');
        	expose.sessionTypeError=$('#sessionTypeError1');
        	expose.inputVisitorError=$('#inputVisitorError1');
        };

        expose.setModifyForm=function(){
        	expose.tripDate=$('#tripDate');
        	expose.inputVisitor=$('#inputVisitor');
        	expose.sessionType=$('#destinationType');
        	expose.sessionTypeError=$('#sessionTypeError');
        	expose.inputVisitorError=$('#inputVisitorError');
        };
        expose.refreshDynamicElements = function() {
            //No dynamic elements on this page yet!!! --- ( o_o) ---
        };

        return expose;
    })();

$( document ).ready(function() {
    DOMHomeCache.refreshStaticElements();
    DOMHomeCache.setDefaultForm();
});

function getFormattedDate(inDate) {
    if (inDate instanceof Date) {
        return ("0"+inDate.getDate()).slice(-2) + '/' + ("0"+(inDate.getMonth() + 1)).slice(-2)  + '/' + inDate.getFullYear();
    }
    return "";
}

function validateModifySearch(){
    var isValid=true;
    DOMHomeCache.sessionTypeError.text("");
    DOMHomeCache.inputVisitorError.text("");

    var visitDate= DOMHomeCache.tripDate.datepicker( "getDate" );
    var today =new Date();
    var todayStartTime=new Date(today.getFullYear,today.getMonth, today.getDate());
	alert();
    if(visitDate==null){
        isValid=false;
        window.alert("Please select visit date ");
    }else if(visitDate < todayStartTime){
        isValid=false;
        window.alert("Please select visit date ");
    }

    else if(DOMHomeCache.sessionType.val()==="" || DOMHomeCache.sessionType.val()==null){
        isValid=false;
        window.alert("Please select session type");
    }

    else if(DOMHomeCache.inputVisitor.val()===""){
        isValid=false;
        window.alert("Please enter visitor count");
    }else if(DOMHomeCache.inputVisitor.val() < 1){
        isValid=false;
        window.alert("Please enter valid visitor count");
    }else if(!DOMHomeCache.inputVisitor.val().trim().match(/^[0-9]+$/)){
        isValid=false;
        window.alert("Please enter numeric visitor count");
    }/*else if(DOMHomeCache.inputVisitor.val()>=25 && DOMHomeCache.inputVisitor.val()<=225){
        isValid=false;
        window.alert("Please refer group booking section for 25 or more visitors");
    }*/else if(DOMHomeCache.inputVisitor.val()>225){
        isValid=false;
        window.alert("Contact Sagar Pai @ 9022112422");
    }

    if(isValid){    
        var timeDiffMin =  checkSessionTime(visitDate,getSession(DOMHomeCache.sessionType.val()));
        if(timeDiffMin<180){
            window.alert("Sorry, Online booking is allowed only 3 hours prior to the session time, kindly contact 02261801591 / 92 to check availability.");
            isValid=false;
        }
    }
    return isValid;
}

function checkSessionTime(visitDate,sessionTime){
  var today = new Date();
  var timeArr= sessionTime.match(/(\d+)(?::(\d\d))?\s*(AM|PM|am|pm)/);
  var visitDateTime=visitDate;
  visitDateTime.setHours( parseInt(timeArr[1]) + ((timeArr[3].toUpperCase()==="PM" && parseInt(timeArr[1])!=12) ? 12 : 0) );
  visitDateTime.setMinutes(parseInt(timeArr[2]));
  var diffMinutes = Math.floor(((visitDateTime-today)/1000)/60);
  return diffMinutes;
}

function getSession(destinationType){
    var sessionType;
    switch(destinationType) {
    case '0':
        sessionType="11:00 AM";
        break;
    case '1':
        sessionType="12:15 PM";
        break;
    case '2':
        sessionType="01:30 PM";
        break;
    case '3':
        sessionType="03:00 PM";
        break;
    case '4':
        sessionType="04:30 PM";
        break;
    case '5':
        sessionType="06:00 PM";
        break;
    case '6':
        sessionType="07:30 PM";
        break;
    case '7':
        sessionType="09:00 PM";
        break;
    default:
        sessionType="00:00 AM";     
    }
    return sessionType;
}
