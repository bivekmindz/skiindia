<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Graphmanagement extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
     $this->load->library('session');
    
  }
  
//............. DEFAULT FUNCTION ............... //

public function toptenorderbyagent()
{
//pend($_POST('startdate'));

    $paramater = array(
            'act_mode' => 'count_top10_order_by_agent',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => ''
        );
        //pend($paramater);
        $data['result'] = $this->supper_admin->call_procedure('proc_order_s', $paramater);

        $p = json_encode($data['result']);

        echo $p;


}

public function toptenorderbydate()
{

    //pend($this->uri->segment(2));

    $paramater = array(
        'act_mode'=>'count_top10_order_by_agent',
        'Param1'=>$this->input->post('datepicker1'),
        'Param2'=>$this->input->post('datepicker2'),
        'Param3'=>'',
        'Param4'=>'',
        'Param5'=>'',
        'Param6'=>'',
        'Param7'=>'',
        'Param8'=>'',
        'Param9'=>''
    );
    //pend($param);
    $data['result'] = $this->supper_admin->call_procedure('proc_order_s',$paramater);

    $p=json_encode($data['result'] );
    echo $p;

}


  public function agentgrapg()
  {
      if($this->input->post('search')=='Search')
      {
          $paramater = array(
              'act_mode'=>'count_top10_order_by_date',
              'Param1'=>$this->input->post('datepicker1'),
              'Param2'=>$this->input->post('datepicker2'),
              'Param3'=>'',
              'Param4'=>'',
              'Param5'=>'',
              'Param6'=>'',
              'Param7'=>'',
              'Param8'=>'',
              'Param9'=>''
          );
         // pend($paramater);
          $data['result'] = $this->supper_admin->call_procedure('proc_order_s',$paramater);

      }
      else
      {
          $paramater = array(
              'act_mode'=>'count_top10_order_by_agent',
              'Param1'=>'',
              'Param2'=>'',
              'Param3'=>'',
              'Param4'=>'',
              'Param5'=>'',
              'Param6'=>'',
              'Param7'=>'',
              'Param8'=>'',
              'Param9'=>''
          );
         // pend($paramater);
          $data['result'] = $this->supper_admin->call_procedure('proc_order_s',$paramater);

      }


      $paramater = array(
          'act_mode'=>'count_total_order',
          'Param1'=>'',
          'Param2'=>'',
          'Param3'=>'',
          'Param4'=>'',
          'Param5'=>'',
          'Param6'=>'',
          'Param7'=>'',
          'Param8'=>'',
          'Param9'=>''
      );
      //pend($param);
      $data['countorder'] = $this->supper_admin->call_procedureRow('proc_order_s',$paramater);

      $paramater = array(
          'act_mode'=>'count_total_agent',
          'Param1'=>'',
          'Param2'=>'',
          'Param3'=>'',
          'Param4'=>'',
          'Param5'=>'',
          'Param6'=>'',
          'Param7'=>'',
          'Param8'=>'',
          'Param9'=>''
      );
      //pend($param);
      $data['countagent'] = $this->supper_admin->call_procedureRow('proc_order_s',$paramater);


      //pend($data['toptenorder']);

      $param=array(
            'act_mode'=>'getorderbyagentlist',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );

        $creditamount= $this->supper_admin->call_procedure('proc_order_s',$param); 
        $j = 1;
        $dataList =array();
        foreach($creditamount as $value)
        {
         $list['y'] = $value->totalamount;
         $list['name'] = $value->billing_name;
         if($j == 1){ 
           $list['exploded'] =true;   
         }else{
          unset($list['exploded'] );
         }
         $dataList[] = $list;
         $j++; 
        }
        $data['dataPoints'] = $dataList;
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('agentgraph/graph', $data);

  }

}//end class
?>