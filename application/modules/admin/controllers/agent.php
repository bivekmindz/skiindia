<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 15/12/17
 * Time: 12:45 PM
 */

class Agent extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        //$this->load->library('session');
        $this->load->helper('adminmenu_helper');
    }



    public function order_sess()
    {

        if($_POST['Clear']=='Show All Records')
        {
            $this->session->unset_userdata('order_filter');
        }


        $a = explode(":",$this->input->post('filter_date_session'));
        $b = explode(" ",$this->input->post('filter_date_session'));

        if($b['1'] == 'PM'){
            $session_time_param = $a['0'] + 12;
        }
        else{
            $session_time_param = $a['0'];
        }

        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');

        $branchids = implode(',',$a);
        $filterstatus = implode(',',$b);
        $filtertime = implode(',',$c);
        $filtersta = implode(',',$d);
        $array = array('branchids' =>$branchids,
            'filter_name' => $this->input->post('filter_name'),
            'filter_email' => $this->input->post('filter_email'),
            'filter_ticket' => $this->input->post('filter_ticket'),
            'filter_mobile' => $this->input->post('filter_mobile'),
            'filter_payment' => $this->input->post('filter_payment'),
            'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
            'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
            'filter_date_session_from' => $this->input->post('filter_date_session_from'),
            'filter_date_session_to' => $this->input->post('filter_date_session_to'),
            'filter_status' => $filterstatus,
            'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
            'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
            'filter_date_ticket' => $this->input->post('filter_date_ticket'),
            'filter_date_session' =>    $session_time_param,
            'filter_tim' => $filtertime,
            'filter_sta' => $filtersta
        );
        //pend($array);
        $this->session->set_userdata('order_filter',$array);
        redirect('admin/agent/adminmanagement?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));

    }

    public function adminmanagement()
    {

        //pend($this->session->userdata('order_filter'));
        if($this->session->userdata('order_filter'))
        {
            $parameter1 = array('act_mode' => 'S_vieworder_adminpanel',
                'Param1' => $this->session->userdata('order_filter')['branchids'],
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $this->session->userdata('order_filter')['filter_email'],
                'Param4' => $this->session->userdata('order_filter')['filter_ticket'],
                'Param5' => $this->session->userdata('order_filter')['filter_mobile'],
                'Param6' => $this->session->userdata('order_filter')['filter_payment'],
                'Param7' => $this->session->userdata('order_filter')['filter_date_booking_from'],
                'Param8' => $this->session->userdata('order_filter')['filter_date_booking_to'],
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],
                'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],
                'Param11' => $this->session->userdata('order_filter')['filter_status'],
                'Param12' => $this->session->userdata('order_filter')['filter_date_printed_from'],
                'Param13' => $this->session->userdata('order_filter')['filter_date_printed_to'],
                'Param14' => $this->session->userdata('order_filter')['filter_date_ticket'],
                'Param15' => $this->session->userdata('order_filter')['filter_date_session'],
                'Param16' => $this->session->userdata('order_filter')['filter_tim'],
                'Param17' => $this->session->userdata('order_filter')['filter_sta'],
                'Param18' => '',
                'Param19' =>''
            );

           //pend($parameter1 );
            foreach($parameter1 as $key=>$val)
            {
                if($parameter1[$key] == '')
                {
                    $parameter1[$key] =-1;
                }
            }

            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_filter_v', $parameter1);
            $this->session->unset_userdata('order_filter');

        }
        else
        {
            $parameter1 = array('act_mode' => 'S_vieworder_new_adminpanel',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' =>''
            );

            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_partner', $parameter1);

           // pend($response['vieww_order']);
        }

        $parameter_time = array('act_mode' => 'branch_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');
        $response['vieww_time'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter_time);

        $parameter_status = array('act_mode' => 'status_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');
        $response['vieww_status'] = $this->supper_admin->call_procedure('proc_order_filter_s',$parameter_status);
//pend($response['vieww_status']);
        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);


        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,


        );
        $response['branch'] = $this->supper_admin->call_procedurerow('proc_select_branch_v', $parameterbranch);
//select banner images
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' =>  $response['branch']->branch_id,


        );
        $response['banner'] = $this->supper_admin->call_procedurerow('proc_select_banner_v', $parameterbanner);

        $parametertearms = array(
            'act_mode' => 'selecttearms',
            'branchid' => $response['branch']->branch_id,


        );

        $response['tearmsgatway'] = $this->supper_admin->call_procedure('proc_select_banner_v', $parametertearms);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('agent/agentmgmt',$response);

    }

}