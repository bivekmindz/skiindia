<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class B2b extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        $this->load->library('session');
 $this->load->helper('adminmenu_helper');
    }

    public function agentedit()
    {

        if($this->input->post('submit'))

        {

            $configUpload['upload_path']    = './assets/admin/images/partner';              #the folder placed in the root of project
            $configUpload['allowed_types']  = 'gif|jpg|png|bmp|jpeg';       #allowed types description
            $configUpload['max_size']       = '0';                          #max size
            $configUpload['max_width']      = '0';                          #max width
            $configUpload['max_height']     = '0';                          #max height
            $configUpload['encrypt_name']   = true;                         #encrypt name of the uploaded file
            $this->load->library('upload', $configUpload);
            if( $_FILES['copypancart']['name']!='') {
                $this->upload->do_upload('copypancart');
                $pancart=($this->upload->data('copypancart')['file_name']);
            }
            else {  $pancart=$_POST['copypancartdata']; }
            if( $_FILES['copyservicetax']['name']!='') {
                $this->upload->do_upload('copyservicetax');
                $servicetax=($this->upload->data('copyservicetax')['file_name']);
            }
            else {  $servicetax=$_POST['copyservicetaxdata']; }
            if( $_FILES['copytan']['name']!='') {
                $this->upload->do_upload('copytan');
                $tan=($this->upload->data('copytan')['file_name']);
            }
            if( $_FILES['copyaddressproof']['name']!='') {
                $this->upload->do_upload('copyaddressproof');
                $addressproof=($this->upload->data('copyaddressproof')['file_name']);
            } else {  $addressproof=$_POST['copyaddressproofdata']; }



            $parameterregupdate=array(
                'act_mode' => 'updatepartner',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => base64_decode($_GET['edid']),
                'logintype' => $this->input->post('logintype'),
                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => $this->input->post('mobile'),
                'fax' => $this->input->post('fax'),
                'website' => $this->input->post('website'),
                'email' => $this->input->post('email'),
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),
                'username' => $this->input->post('username'),
                'password' => base64_encode($this->input->post('password')),

                'countryid' => '',
                'stateid' => '',
                'cityid' => '',
                'branch_id' => $this->input->post('username'),
                'locationid' => '',
                'bank_creditamount' => $this->input->post('bank_creditamount'),
                'bank_agentcommision' => $this->input->post('bank_agentcommision'),
                'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
            );

            $response['regselupdate'] =   $this->supper_admin->call_procedure('proc_partnerregister_v', $parameterregupdate);

            $response['emsg'] ="Added Sucessfully";
            redirect("admin/b2b/agent?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


        }



        $parametersel=array(
            'act_mode' => 'selectpartnerdata',
            'title' => '',
            'firstname' => '',
            'lastname' => '',
            'row_id' => base64_decode($_GET['edid']),
            'logintype' => '',
            'agencyname' => '',
            'office_address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'telephone' => '',
            'mobile' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'primarycontact_name' => '',
            'primarycontact_email_mobile' => '',
            'primarycontact_telephone' => '',
            'secondrycontact_name' => '',
            'secondrycontact_email_phone' => '',
            'secondrycontact_telephone' => '',
            'management_executives_name' => '',
            'management_executives_email_phone' =>'',
            'management_executives_telephone' => '',
            'branches' => '',

            'yearofestablism' => '',
            'organisationtype' => '',
            'lstno' =>'',
            'cstno' => '',
            'registrationno' =>'',
            'vatno' => '',
            'panno' => '',
            'servicetaxno' => '',
            'tanno' => '',
            'pfno' => '',
            'esisno' => '',
            'officeregistrationno' => '',
            'exceptiontax' => '',
            'otherexceptiontax' => '',
            'bank_benificialname' => '',
            'bank_benificialaccno' => '',
            'bank_benificialbankname' => '',
            'bank_benificialbranchname' => '',
            'bank_benificialaddress' => '',
            'bank_benificialifsc' => '',
            'bank_benificialswiftcode' => '',
            'bank_benificialibanno' => '',
            'intermidiatebankname' => '',
            'intermidiatebankaddress' => '',

            'intermidiatebankswiftcode' => '',
            'bank_ecs' => '',
            'copypancart' => '',
            'copyservicetax' => '',
            'copytan' => '',
            'copyaddressproof' => '',
            'emailstatus' => '',
            'emailcurrency' => '',
            'emailcountry' => '',
            'username' =>'',
            'password' => '',

            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => '',
            'locationid' => '',
            'bank_creditamount' => '',
            'bank_agentcommision' => '',
            'aadhaar_number' =>$this->input->post('aadhaar_number'),
            'ppid' =>$this->input->post('ppid'),
            'pppermission' =>$this->input->post('pppermission'),

        );

        $response['agentviewwdata'] =   $this->supper_admin->call_procedurerow('proc_partnerregister_v', $parametersel);

       // p($response['agentviewwdata']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('agent/agent_edit', $response);

    }

    /*ADD,VIEW,UPDATE COMPANY BY biv*/
    public function agent()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
        $parameterbranch=array(
            'act_mode' =>'s_viewbranch',
            'Param1' =>'',
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'',
            'Param13' =>'',
            'Param14' =>'',
            'Param15' =>'',
           


        );

        $response['branch'] =   $this->supper_admin->call_procedure('proc_branch_s', $parameterbranch);


        if ($this->input->post('submit')) {


            $configUpload['upload_path']    = './assets/admin/images/partner';              #the folder placed in the root of project
            $configUpload['allowed_types']  = 'gif|jpg|png|bmp|jpeg';       #allowed types description
            $configUpload['max_size']       = '0';                          #max size
            $configUpload['max_width']      = '0';                          #max width
            $configUpload['max_height']     = '0';                          #max height
            $configUpload['encrypt_name']   = true;                         #encrypt name of the uploaded file
            $this->load->library('upload', $configUpload);

            $this->upload->do_upload('copypancart');
            $pancart=($this->upload->data('copypancart')['file_name']);
            $this->upload->do_upload('copyservicetax');
            $servicetax=($this->upload->data('copyservicetax')['file_name']);
            $this->upload->do_upload('copytan');
            $tan=($this->upload->data('copytan')['file_name']);
            $this->upload->do_upload('copyaddressproof');
            $addressproof=($this->upload->data('copyaddressproof')['file_name']);






            $parameterregsel=array(
                'act_mode' => 'insertpartner',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => '',


                'logintype' => $this->input->post('logintype'),
                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => $this->input->post('mobile'),
                'fax' => $this->input->post('fax'),
                'website' => $this->input->post('website'),
                'email' => $this->input->post('email'),
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),
                'username' => $this->input->post('username'),
                'password' => base64_encode($this->input->post('password')),

                'countryid' => '',
                'stateid' => '',
                'cityid' => '',
                'branch_id' => $this->input->post('username'),
                'locationid' => '',
                'bank_creditamount' => $this->input->post('bank_creditamount'),
                'bank_agentcommision' => $this->input->post('bank_agentcommision'),
                'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
            );

            $response['regsel'] =   $this->supper_admin->call_procedure('proc_partnerregister_v', $parameterregsel);

            $response['emsg'] ="Added Sucessfully";
        }

        $parameterregsel=array(
            'act_mode' => 'selectpartner',
            'title' => '',
            'firstname' => '',
            'lastname' => '',
            'row_id' => '',

            'logintype' => '',
            'agencyname' => '',
            'office_address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'telephone' => '',
            'mobile' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'primarycontact_name' => '',
            'primarycontact_email_mobile' => '',
            'primarycontact_telephone' => '',
            'secondrycontact_name' => '',
            'secondrycontact_email_phone' => '',
            'secondrycontact_telephone' => '',
            'management_executives_name' => '',
            'management_executives_email_phone' =>'',
            'management_executives_telephone' => '',
            'branches' => '',

            'yearofestablism' => '',
            'organisationtype' => '',
            'lstno' =>'',
            'cstno' => '',
            'registrationno' =>'',
            'vatno' => '',
            'panno' => '',
            'servicetaxno' => '',
            'tanno' => '',
            'pfno' => '',
            'esisno' => '',
            'officeregistrationno' => '',
            'exceptiontax' => '',
            'otherexceptiontax' => '',
            'bank_benificialname' => '',
            'bank_benificialaccno' => '',
            'bank_benificialbankname' => '',
            'bank_benificialbranchname' => '',
            'bank_benificialaddress' => '',
            'bank_benificialifsc' => '',
            'bank_benificialswiftcode' => '',
            'bank_benificialibanno' => '',
            'intermidiatebankname' => '',
            'intermidiatebankaddress' => '',

            'intermidiatebankswiftcode' => '',
            'bank_ecs' => '',
            'copypancart' => '',
            'copyservicetax' => '',
            'copytan' => '',
            'copyaddressproof' => '',
            'emailstatus' => '',
            'emailcurrency' => '',
            'emailcountry' => '',
            'username' =>'',
            'password' => '',

            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => '',
            'locationid' => '',
            'bank_creditamount' => '',
            'bank_agentcommision' => '',
            'aadhaar_number' =>$this->input->post('aadhaar_number'),
            'ppid' =>$this->input->post('ppid'),
            'pppermission' =>$this->input->post('pppermission'),
        );
        $response['agentvieww'] =   $this->supper_admin->call_procedure('proc_partnerregister_v', $parameterregsel);


//p($response['agentvieww']);exit();

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('agent/agent_reg', $response);










    }

    public function agentdel($id)
    {
        $parameter = array('act_mode' => 'agentdelete',
            'title' => '',
            'firstname' => '',
            'lastname' => '',
            'row_id' => $id,
            'logintype' => '',
            'agencyname' => '',
            'office_address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'telephone' => '',
            'mobile' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'primarycontact_name' => '',
            'primarycontact_email_mobile' => '',
            'primarycontact_telephone' => '',
            'secondrycontact_name' => '',
            'secondrycontact_email_phone' => '',
            'secondrycontact_telephone' => '',
            'management_executives_name' => '',
            'management_executives_email_phone' =>'',
            'management_executives_telephone' => '',
            'branches' => '',

            'yearofestablism' => '',
            'organisationtype' => '',
            'lstno' =>'',
            'cstno' => '',
            'registrationno' =>'',
            'vatno' => '',
            'panno' => '',
            'servicetaxno' => '',
            'tanno' => '',
            'pfno' => '',
            'esisno' => '',
            'officeregistrationno' => '',
            'exceptiontax' => '',
            'otherexceptiontax' => '',
            'bank_benificialname' => '',
            'bank_benificialaccno' => '',
            'bank_benificialbankname' => '',
            'bank_benificialbranchname' => '',
            'bank_benificialaddress' => '',
            'bank_benificialifsc' => '',
            'bank_benificialswiftcode' => '',
            'bank_benificialibanno' => '',
            'intermidiatebankname' => '',
            'intermidiatebankaddress' => '',

            'intermidiatebankswiftcode' => '',
            'bank_ecs' => '',
            'copypancart' => '',
            'copyservicetax' => '',
            'copytan' => '',
            'copyaddressproof' => '',
            'emailstatus' => '',
            'emailcurrency' => '',
            'emailcountry' => '',
            'username' =>'',
            'password' => '',

            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => '',
            'locationid' => '',
            'bank_creditamount' => '',
            'bank_agentcommision' => '',
            'aadhaar_number' =>$this->input->post('aadhaar_number'),
            'ppid' =>$this->input->post('ppid'),
            'pppermission' =>$this->input->post('pppermission'),);
        $response = $this->supper_admin->call_procedure('proc_partnerregister_v', $parameter);




    }


    public function agentstatus()
    {
        $rowid = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $act_mode = $status == '1' ? 'activeandinactive' : 'inactiveandinactive';
        $parameter = array('act_mode' => $act_mode,
            'title' => '',
            'firstname' => '',
            'lastname' => '',
            'row_id' => $rowid,
            'logintype' => '',
            'agencyname' => '',
            'office_address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'telephone' => '',
            'mobile' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'primarycontact_name' => '',
            'primarycontact_email_mobile' => '',
            'primarycontact_telephone' => '',
            'secondrycontact_name' => '',
            'secondrycontact_email_phone' => '',
            'secondrycontact_telephone' => '',
            'management_executives_name' => '',
            'management_executives_email_phone' =>'',
            'management_executives_telephone' => '',
            'branches' => '',

            'yearofestablism' => '',
            'organisationtype' => '',
            'lstno' =>'',
            'cstno' => '',
            'registrationno' =>'',
            'vatno' => '',
            'panno' => '',
            'servicetaxno' => '',
            'tanno' => '',
            'pfno' => '',
            'esisno' => '',
            'officeregistrationno' => '',
            'exceptiontax' => '',
            'otherexceptiontax' => '',
            'bank_benificialname' => '',
            'bank_benificialaccno' => '',
            'bank_benificialbankname' => '',
            'bank_benificialbranchname' => '',
            'bank_benificialaddress' => '',
            'bank_benificialifsc' => '',
            'bank_benificialswiftcode' => '',
            'bank_benificialibanno' => '',
            'intermidiatebankname' => '',
            'intermidiatebankaddress' => '',

            'intermidiatebankswiftcode' => '',
            'bank_ecs' => '',
            'copypancart' => '',
            'copyservicetax' => '',
            'copytan' => '',
            'copyaddressproof' => '',
            'emailstatus' => '',
            'emailcurrency' => '',
            'emailcountry' => '',
            'username' =>'',
            'password' => '',

            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => '',
            'locationid' => '',
            'bank_creditamount' => '',
            'bank_agentcommision' => '',
            'aadhaar_number' =>$this->input->post('aadhaar_number'),
            'ppid' =>$this->input->post('ppid'),
            'pppermission' =>$this->input->post('pppermission'),);
        //pend($parameter);
        $response = $this->supper_admin->call_procedure('proc_partnerregister_v', $parameter);


    }

    public function agentamount(){
        $edid = base64_decode($this->input->get('edid'));
       
      
        if($this->input->post('submit'))
        {

            //agentamount_add_new_credit
            //agentamount_add_new_debit
            $parameter = array('act_mode' => 'agentamount_add_new_credit',
                'Param1' => $edid,
                'Param2' => $this->input->post('com_name'),
                'Param3' => 'C',
                'Param4' => '0',
                'Param5' => 'Ad',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            //pend($parameter);
    $response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);

    //p($response);exit();
 
    redirect('admin/b2b/agentamount?edid='.$this->input->get('edid').
        '&empid='.$this->input->get('empid').'&uid='.
str_replace(".html","",$this->input->get('uid')));
     }
       


        $parameter = array('act_mode' => 'agentamount_new',
            'Param1' => $edid,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
     
        $response['agent_his'] = (array)$this->supper_admin->call_procedure('proc_agent_s', $parameter);


        $parameter = array('act_mode' => 'agentamount_show',
            'Param1' => base64_decode($this->input->get('edid')),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
     

   // p($parameter);exit();
        $response['agent_show'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);

               // p($response['agent_show']);exit();
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('agent/agentamount', $response);
    }


    public function agentwallet_v(){
        $edid = base64_decode($this->input->get('edid'));

        if($this->input->post('submit'))
        {
            $parameter = array('act_mode' => 'agentamount_comission',
                'Param1' => $edid,
                'Param2' => $this->input->post('com_name'),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            //pend($parameter);
            $response = (array)$this->supper_admin->call_procedure('proc_agent_s', $parameter);

        }
        $parameter = array('act_mode' => 'agentamount',
            'Param1' => $edid,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['agent_his'] = (array)$this->supper_admin->call_procedure('proc_agent_s', $parameter);
        //p($response['agent_his']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('agent/agentwallet', $response);
    }




}// end class
?>