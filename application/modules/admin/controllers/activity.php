<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Activity extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
 $this->load->library('session');

    }

    /*Add activity by zzz*/
    public function addactivity()
    {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('act_name', 'name', 'required');
            $this->form_validation->set_rules('act_desc', 'description', 'required');
            $this->form_validation->set_rules('act_price', 'price', 'required|numeric|xss_clean');
             $this->form_validation->set_rules('locationid', 'location', 'required');

            if ($this->form_validation->run() != FALSE) {

                $configUpload['upload_path']    = './assets/admin/images';              #the folder placed in the root of project
                $configUpload['allowed_types']  = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                $configUpload['max_size']       = '0';                          #max size
                $configUpload['max_width']      = '0';                          #max width
                $configUpload['max_height']     = '0';                          #max height
                $configUpload['encrypt_name']   = true;                         #encrypt name of the uploaded file
                $this->load->library('upload', $configUpload);                  #init the upload class
                if(!$this->upload->do_upload('act_image')){
                    $uploadedDetails    = $this->upload->display_errors();
                    $this->session->set_flashdata('message', $uploadedDetails);
                }else{
                    $uploadedDetails    = $this->upload->data();
                    $this->session->set_flashdata('message', 'inserted sucessfully');
                    $parameter = array('act_mode' => 's_addactivity',
                        'Param1' => $this->input->post('act_name'),
                        'Param2' => $this->input->post('act_desc'),
                        'Param3' => $this->input->post('act_price'),
                        'Param4' => $uploadedDetails['file_name'],
                        'Param5' => $this->input->post('locationid'),
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                    //pend($parameter);
                    $response = $this->supper_admin->call_procedure('proc_activity_s', $parameter);
                }
                /*print_r($uploadedDetails['file_name']);
                die();*/


            }
        }

        if ($this->input->post('submit_update')) {
            $this->form_validation->set_rules('act_name_update', 'name', 'required');
            $this->form_validation->set_rules('act_desc_update', 'description', 'required');
            $this->form_validation->set_rules('act_price_update', 'price', 'required|numeric');
            if ($this->form_validation->run() != FALSE) {
                $parameter = array('act_mode' => 's_addactivity_update',
                    'Param1' => $this->input->post('act_name_update'),
                    'Param2' => $this->input->post('act_desc_update'),
                    'Param3' => $this->input->post('act_price_update'),
                    'Param4' => $this->input->post('act_id'),
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend( $parameter);
                $response = $this->supper_admin->call_procedure('proc_location_v', $parameter);
                $this->session->set_flashdata('message', 'Updated sucessfully');
            }
        }

        $parameter1 = array('act_mode' => 's_viewactivity',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_activity_s', $parameter1);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww_country'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
       //pend($response['vieww']);
        //pend($this->session->all_userdata());
        //$response['employee_role'] = $this->session->all_userdata();
        //pend($response['employee_role']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('activity/addactivity', $response);

    }

    /*activity delete by zzz*/
    public function activity_delete_new()
    {
        $parameter = array('act_mode' => 'delete_activity',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter);
        redirect(base_url() . 'admin/activity/addactivity');

    }


}// end class
?>