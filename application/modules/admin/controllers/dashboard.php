<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 13/12/17
 * Time: 5:57 PM
 */

class Dashboard extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        $this->load->library('session');
    }


    public function index()
    {

         if($this->input->post('search') == 'Search')
         {
         $paramater = array(
            'act_mode'=>'count_order_by_month_date',
            'Param1'=>date('Y-m-d',strtotime($this->input->post('datepicker1'))),
            'Param2'=>date('Y-m-d',strtotime($this->input->post('datepicker2'))),
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        //pend($param);
        $response['data'] = $this->supper_admin->call_procedure('proc_order_s',$paramater);
       //redirect('User/dashboard',$response);
       //p($response['dataselect']);exit;
       }

    else{
        $paramater = array(
            'act_mode'=>'count_order_by_month',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        //pend($param);
        $response['data'] = $this->supper_admin->call_procedure('proc_order_s',$paramater);
      }
        $paramater = array(
            'act_mode'=>'count_total_order',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        //pend($param);
        $response['countorder'] = $this->supper_admin->call_procedureRow('proc_order_s',$paramater);


        $paramater = array(
            'act_mode'=>'count_total_agent',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        //pend($param);
        $response['countagent'] = $this->supper_admin->call_procedureRow('proc_order_s',$paramater);

        $paramater = array(
            'act_mode'=>'total_trans_amount',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        //pend($param);
        $response['totaltrans'] = $this->supper_admin->call_procedureRow('proc_order_s',$paramater);

        $paramater = array(
            'act_mode'=>'total_print_ticket',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        //pend($param);
        $response['totalpticket'] = $this->supper_admin->call_procedureRow('proc_order_s',$paramater);

        $paramater = array(
            'act_mode'=>'total_users',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        //pend($param);
        $response['totalusers'] = $this->supper_admin->call_procedureRow('proc_order_s',$paramater);


        $paramater = array(
            'act_mode'=>'total_guest',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );
        //pend($param);
        $response['totalguest'] = $this->supper_admin->call_procedureRow('proc_order_s',$paramater);
 //pend($response['data'] );
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('user/dashboard',$response);
        // pend($this->session->all_userdata());
    }


}