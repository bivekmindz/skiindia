<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_package").validate({
            ignore:".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                package_name: "required",
                package_desc: "required",
                branchids: "required",
                act_ids: "required",
                package_price: {
                    "required": true,
                    "number": true
                },

            },
            // Specify validation error messages
            messages: {
                package_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                $("#branchids").attr('disabled',false);
                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
    .btne_active{    text-decoration: none;
        background-color: #c7921b;
        padding: 6px 5px 9px 6px;
        color: #fff;
        border-radius: 5px;}
</style>





<!-- <div class="wrapper"> -->

    <div class="col-lg-10">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Update Package</h2>
                    </div>
                    <div class='flashmsg'>
                        <?php echo validation_errors(); ?>
                        <?php
                        if($this->session->flashdata('message')){
                            echo $this->session->flashdata('message');
                        }
                        ?>
                    </div>
                    <div class="page_box">

<form action="" name="update_package" id="update_package" method="post" enctype="multipart/form-data" >

    <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Package Name<span style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input">
                        <input type="text" id="package_name" name="package_name" value="<?php echo $view->package_name ?>" >
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Package Description<span style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input">
                        <textarea  id="package_desc" name="package_desc" ><?php  echo $view->package_description  ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Select Branch
                        <span style="color:red;font-weight: bold;">*</span>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input">
                        <?php
                        if($this->session->userdata('snowworld')->EmployeeId)
                        {
                            ?>
                            <script>
                                $(function(){
                                    $('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
                                    $("#branchids").attr('disabled',true);
                                    //$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
                                });
                            </script>
                            <?php } ?>


                        <select name="branchids[]" required multiple id="branchids">

                            <?php foreach ($s_viewbranch as $key => $value) { ?>
                                <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Select Activity <span style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input">
                        <select disabled="disabled" name="act_ids[]" required multiple id="act_ids">
                            <option selected="selected" value="1">ALL ACTIVITIES</option>
                            <?php foreach ($vieww_act as $key => $value) { ?>

                                <option value="<?php echo $value->activity_id; ?>"><?php echo $value->activity_name; ?></option>
                            <?php } ?>
                        </select></div>
                </div>
            </div>
        </div>
    </div>


    <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Package Price<span style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input">
                        <input type="text" id="package_price" name="package_price"  value="<?php echo $view->package_price  ?>" >
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sep_box" >
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Package For<span style="color:red;font-weight: bold;">*</span>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input">
                        <select name="package_for" id="">
                            <option value="0">Users/Guests</option>
                            <option value="1">Partners</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Package Image<span style="color:red;font-weight: bold;">*</span>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input">
                        <input type="file" id="pack_image" value="<?php echo $view->package_image; ?>" name="pack_image" >
                        Prevoius Image : <img src="<?php echo base_url('assets/admin/images/'.$view->package_image); ?>">
                        <input type="hidden" name="update_pack_img" value="<?php  echo $view->package_image;?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--    <div class="sep_box">-->
<!--        <div class="col-lg-6">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-4">-->
<!--                    <div class="tbl_text">Package Image<span style="color:red;font-weight: bold;">*</span>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-8">-->
<!--                    <div class="tbl_input">-->
<!--                        <input type="file" id="pack_image" name="pack_image" >-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->



    <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <div class="submit_tbl">
                        <input id="submitBtn" type="submit" name="submit" value="Update" class="btn_button sub_btn" /> &nbsp;
                        <a  href="<?= base_url('admin/packages/addPackage'); ?>" class="btn btn-primary">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


</form>
</div>