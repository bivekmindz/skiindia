<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>


$(function() {
// Initialize form validation on the registration form.
// It has the name attribute "registration"
$("#add_com").validate({
// Specify validation rules
rules: {
// The key name on the left side is the name attribute
// of an input field. Validation rules are defined
// on the right side
com_name: "required",
},
// Specify validation error messages
messages: {
com_name: "Please enter name",
},
// Make sure the form is submitted to the destination defined
// in the "action" attribute of the form when valid
submitHandler: function(form) {
form.submit();
}
});



$("#update_com").validate({
// Specify validation rules
rules: {
// The key name on the left side is the name attribute
// of an input field. Validation rules are defined
// on the right side
com_name_update: "required",
},
// Specify validation error messages
messages: {
com_name_update: "Please enter name",
},
// Make sure the form is submitted to the destination defined
// in the "action" attribute of the form when valid
submitHandler: function(form) {
form.submit();
}
});
});


</script>
<style>
input.error{border:1px solid red;}
label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

<div class="col-lg-10 col-lg-push-2">
<div class="row">
<div class="page_contant">
<div class="col-lg-12">
<div class="page_name">

<h2>HISTORY</h2>
</div>
<div class="page_box">





<form action="" name="" id="" method="post" >
<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">WALLET AMT.<strong>
    <?php  if(!empty($agent_show->total_amount)) echo $agent_show->total_amount; else echo "0";  ?></strong> </div>
</div>
<div class="col-lg-8">
<div class="tbl_input">
AMOUNT
<input min="0" type="number" id="com_name" name="com_name" >
</div>

</div>
</div>
</div>
</div>



<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4"></div>
<div class="col-lg-8">
<div class="submit_tbl">
<input id="submitBtn" type="submit" name="submit" value="Add" class="btn_button sub_btn" />

<!-- <input id="submitBtn_sub" class="btn btn-danger"  type="submit" name="submit_sub" value="-" class="btn_button sub_btn" /> -->

<a href="<?php echo base_url('admin/b2b/agent?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>">BACK</a>
</div>
</div>
</div>
</div>

</div>
</form>


<div class="col-md-12">
<h2>WALLET HISTORY</h2>
<div class="table-responsive" style="width: 100%;">
<table id="table1" class="table">
<thead>
<tr>
<th>S.NO</th>
<th>Credit/Debit</th>
<th>AMOUNT</th>
<th>TYPE</th>
<th>PaymentBy</th>
<th>TRANSICTION TIME</th>

</tr>
</thead>
<tbody>
<?php $i=1; foreach((array)$agent_his as $v) {?>
<tr>
<td><?= $i;?></td>
<td><?php  echo "Rs. ".$v->credit_debit_amount; ?></td>
<td><?php  echo "Rs. ".$v->total_amount; ?></td>
<td><?php if($v->credit_debit_pety=='C')echo "Credit";else echo "Debit"; ?></td>
<!--<td><?php /*echo $v->ph_agentdiscount." %"; */?></td>-->
<td>
<?php if($v->paymentby=='Ag')echo "Agent";else echo "Admin"; ?>
</td>
<td><?php echo $v->Created_at; ?></td>
</tr>
<?php $i++;} ?>
</tbody>
</table>
</div>
</div>




</div>

</div>
</div>
</div>
</div>
</div>

<script>

function update_com(a,b){
//alert(a +b);
$("#com_name_update").val(a);
$("#com_id").val(b);
$("#update_com").css('display','block');
$("#add_com").css('display','none');
$("#com_name_update").focus();

}
function UpdateCancel() {
$("#add_com").css('display','block');
$("#update_com").css('display','none');
return false;
}


$(function(){
var table = $('#table1').DataTable();

$('#submitBtn_sub').click(function(){
var a =  $("#com_name").val();
var b = parseInt('<?php echo ($agent_his['0']->bank_creditamount); ?>');
if((b-a) < 0 )
{
alert('Its greater than wallet amount,Try another amount');
return false;
}
})
})

</script>


<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>