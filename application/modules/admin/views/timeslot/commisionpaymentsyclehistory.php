<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>
<!-- <div class="wrapper"> -->
<style>
.error {
color: red;

}
.tab-pane h4 b {
color: #1b78c7;
}
.modal-body {
width: 100%;
float: left;
position: relative;
}
</style>
<div class="col-lg-10">
<div class="row">
<div class="page_contant">
<div class="col-lg-12">
<div class="page_name">
<h2>Manage Agent Commission History</h2>
</div>
<div class="page_box">
<div class="col-lg-12">
<div id="content">
<form id="addCont" action="" method="post" enctype="multipart/form-data">
<div id="my-tab-content" class="tab-content">
<!--  session flash message  -->
<div class='flashmsg'>
<?php echo validation_errors(); ?>
<?php
if ($this->session->flashdata('message')) {
echo $this->session->flashdata('message');
}
?>
</div>
<div id="agentdiv">
<table class="table table-bordered table-striped" id="table1">
<thead>
<tr>
<th>S:NO</th>
<th>Agency Name</th>
<th>User Name</th>
<th>Invoice ID</th>
<th>Contact</th>
<th>Mail id</th>
<th>Comission</th>
<th>Commistion Amt</th>

</tr>
</thead>
<tbody>
<?php $i = 0;
//p($walletprices);

foreach ($agentvieww as $key => $value) { 

$parameter2 = array( 'act_mode'=>'get_commission_payment_history_detail',
  'Param1'=>$value['agent_id'],
  'Param2'=>'',
  'Param3'=>'',
  'Param4'=>'',
  'Param5'=>'',
  'Param6'=>'',
  'Param7'=>'',
  'Param8'=>'',
  'Param9'=>'');
$data['commission_history'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2); 

foreach ($data['commission_history'] as $key => $valuePrice) {
$i++;
if($value['per_amount']){
  $per_ammount  = $value['per_amount'];
}else{
  $per_ammount = 0;
}
?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $value['agent_agencyname']; ?></td>
<td><?php echo $value['agent_username']; ?></td>
<td><button style="background-color:orange "><?php echo $valuePrice->invoice_id; ?></button></td>
<td><?php echo $value['agent_mobile']; ?></td>
<td><?php echo $value['agent_email']; ?></td>
<td><a  title="Manage Comission Amount" href="#"><i
class="btn fa fa"><?php echo " " . $valuePrice->commission. " %"; ?>
</a></i></td>
<td>
    <a title="Manage Wallet Amount" href="#"><i
class="btn fa fa-inr"><?php //echo " " . $value->bank_creditamount; ?>
<?php if(!empty($valuePrice->commprice)){ echo $valuePrice->commprice; } else{ echo"0";}?>
</a></i>
</td>
</tr>
<?php } } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script>
function RemoveCompany(a) {
if (confirm("Are you sure to delete?") == true) {
$.ajax({
type: "POST",
url: "<?php echo base_url()?>admin/b2b/agentdel/" + a,
cache: false,
contentType: false,
processData: false,
success: function (data) {
// alert(data);
$("#agentdiv").load(location.href + " #agentdiv");
$("#msgdiv").html('deleted sucessfully');
},
error: function (data) {
// alert(data);
}
});
} else {
return false;
}
}
</script>
<script>
$(function(){
var table = $('#table1').DataTable({"scrollY": 400,"scrollX": true });
})
</script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>