<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>



<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="threshold">
                    
                    <div class="head_top_ba">
                    
                    <div class="col-md-12">
                    <div class="left_te">
                    <h1>Inventory <span>-Booked Event- <?php echo $departDate; ?></span></h1>
                    </div>
                    </div>

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href="<?php echo base_url('admin/inventory/calenderview') ?>">
                                        <button>CANCEL</button>
                                    </a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if ($this->session->flashdata('message')) {
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                   
                    
                    </div>
                    
                  
                    
                    <div class="threshole_cont_tab">
                    <table width="100%" border="1" cellspacing="5" cellpadding="5">
  <tr>
     <th>S.NO</th>
                                        <th>From To</th>
                                        <th>Total Number Of Seat</th>
                                        <th>Total Booked Seat</th>
                                          <th>Canceled Seat</th>
                                        <th>Remaining Seat</th>
  </tr>

 <?php  $i=1; foreach($s_viewtimeslot as $v) { ?>
   <tr>
                                            <td><?= $i;?></td>
                                            <td id="t_com_name"><?php echo $v->dailyinventory_from; ?>:<?php echo $v->dailyinventory_minfrom; ?> Hrs - <?php echo $v->dailyinventory_to; ?>:<?php echo $v->dailyinventory_minto; ?> Hrs </td>
                                            <td> <?php echo $v->dailyinventory_seats; ?> </td>
                                            <td> <?php echo $v->coun; ?> </td>
                                            <td> <?php echo $v->Aborted; ?> </td>
                                            <td> <?php echo ( $v->dailyinventory_seats-$v->coun+$v->Aborted); ?> </td>
                                        </tr>

<?php $i++;} ?>



</table>


                    
                    </div>
                     </div>
                    

                </div>
            </div>
        </div>
    </div>
</div>
