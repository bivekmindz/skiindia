<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function() {
                // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_package").validate({
            ignore:".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                package_name: "required",
                package_desc: "required",
                branchids: "required",
                act_ids: "required",
                package_price: {
                    "required": true,
                    "number": true
                },

            },
            // Specify validation error messages
            messages: {
                package_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Add Package</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href=""><button>CANCEL</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <form action="<?= base_url('admin/geographic/addPackage') ?>" name="add_package" id="add_package" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Package Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="package_name" name="package_name" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Package Description<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="package_desc" name="package_desc" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="branchids[]" multiple id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Activity <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="act_ids[]" multiple id="act_ids">
                                                    <?php foreach ($vieww_act as $key => $value) { ?>
                                                        <option value="<?php echo $value->activity_id; ?>"><?php echo $value->activity_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Package Price<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="package_price" name="package_price" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="container">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Package Name</th>
                                        <th>Package Description</th>
                                        <th>Package Branch</th>
                                        <th>Package Activity</th>
                                        <th>Package Price</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($vieww_pack as $v) {?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="u_pack_name"><?php echo $v->package_name; ?></td>
                                            <td id="u_pack_desc"><?php echo $v->package_description; ?></td>
                                            <td id="u_pack_price"><?php


//echo $v->bp_id;

                                                $exp_bp =  explode(',',$v->bp_id);
                                                foreach($exp_bp as $bp_id)
                                                {
                                                    $parameter = array( 'act_mode'=>'getbranch',
                                                        'Param1'=>$bp_id,
                                                        'Param2'=>'',
                                                        'Param3'=>'',
                                                        'Param4'=>'',
                                                        'Param5'=>'',
                                                        'Param6'=>'',
                                                        'Param7'=>'',
                                                        'Param8'=>'',
                                                        'Param9'=>'');
                                                    //p($parameter);
                                                    $path = api_url()."main_snowworld_v/getlocation/format/json/";
                                                    $br_name = curlpost($parameter,$path);
                                                    //p($br_name);
                                                    $arr = (array)$br_name;
                                                    //p($arr);
                                                    echo "<ul id='branch_map'>";
                                                if($arr[0]['branch_name'] != ''){
                                                    echo("<li value=".$arr[0]['branch_id'].">".$arr[0]['branch_name']."</li>");}
                                                    echo "</ul>";

                                                }





                                                ?></td>
                                            <td id="t_act_price"><?php
                                                $exp_ap =  explode(',',$v->ap_id);
                                                foreach($exp_ap as $ap_id)
                                                {
                                                    //echo $ap_id;
                                                    $parameter1 = array( 'act_mode'=>'getactivity',
                                                        'Param1'=>$ap_id,
                                                        'Param2'=>'',
                                                        'Param3'=>'',
                                                        'Param4'=>'',
                                                        'Param5'=>'',
                                                        'Param6'=>'',
                                                        'Param7'=>'',
                                                        'Param8'=>'',
                                                        'Param9'=>'');
                                                    //p($parameter);
                                                    $path1 = api_url()."main_snowworld_v/getlocation/format/json/";
                                                    $act_name = curlpost($parameter1,$path1);
                                                    //p($br_name);
                                                    $arr1 = (array)$act_name;
                                                    echo "<ul id='activity_map'>";
                                                    if($arr1[0]['activity_name'] != ''){
                                                        echo("<li value=".$arr1[0]['activity_id'].">".$arr1[0]['activity_name']."</li>");}
                                                        echo "</ul>";
                                                  

                                                }





                                            ?></td>
                                            <td id="t_act_price"><?php echo $v->package_price; ?></td>
                                            <td>

                                                <a class="btn btn-primary" id="updatePackageButton" data-toggle="modal" data-target="#myModal1" onclick="return update_pack()">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>




                                                <a href="<?= base_url('admin/geographic/packageDelete1/')."/".$v->package_id ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>







                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Package</h4>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('admin/geographic/updatePackage') ?>" name="update_package" id="update_package" method="post" enctype="multipart/form-data" >


                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update Package Name<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="text" id="update_package_name" name="update_package_name" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update Package Description<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <textarea  id="update_package_desc" name="update_package_desc" ></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="update_branchids[]" multiple id="update_branchids">
                                                <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                    <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Activity <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="update_act_ids[]" multiple id="update_act_ids">
                                                <?php foreach ($vieww_act as $key => $value) { ?>
                                                    <option value="<?php echo $value->activity_id; ?>"><?php echo $value->activity_name; ?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update Package Price<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="text" id="update_package_price" name="update_package_price" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>

        </div>
    </div>

</div>

<script>
    $(function () {
        $("#branchids,#act_ids").select2({
            placeholder: "Select the Data",
            allowClear: true
        }).select2('val', $('.select2 option:eq(0)').val());

        $("#updatePackageButton").on('click',function () {

            $("#update_act_ids,#update_branchids").select2({
                placeholder: "Select the Data",
                allowClear: true
            });



            /*var defaultData = [{id:1, text:'Item1'},{id:2,text:'Item2'},{id:3,text:'Item3'}];
            $('#update_act_ids').data().select2.updateSelection(defaultData);
            var a = $(this).parent('tr').find('ul [id=branch_map]').html('');
            console.log(a);*/

        });


        $("#act_ids").on('change',function () {

            var act =$(this).val().toString();
            var finalPrice = 0;
            var act_split = act.split(',');
            for(var count = 0 ; count < act_split.length ; count++)
            {
                $.ajax({
                    url: '<?php echo base_url()?>admin/geographic/activityPrice',
                    type: 'POST',
                    dataType : "json",
                    data: {'act_id': act_split[count]},
                    success: function(data){
                        if(data.length > 0) {
                            finalPrice = finalPrice +  parseInt(data[0].activity_price);
                        }
                        $("#package_price").val(finalPrice);
                    }
                });
            }

            //console.log(act_split.length);
            return false;

        })
    })







</script>