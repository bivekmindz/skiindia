<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>


    $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_branch").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                countryid: "required",
                stateid: "required",
                cityid: "required",
                locationid: "required",
                companyid: "required",
                branch_name: "required",
            },
            // Specify validation error messages
            messages: {
                countryid: "Please select country",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });


</script>
<style>
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD PROMO CODE</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <!--<div style="text-align:right;">
                                    <a href=""><button>CANCEL</button></a>
                                </div>-->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if ($this->session->flashdata('message')) {
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>




                        <form action="<?= base_url('admin/geographic/addbranch') ?>" name="add_branch" id="add_branch"
                              method="post" enctype="multipart/form-data">


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Country <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="countryid" id="countryid" onchange="selectstates();">
                                                    <option value="">select Country</option>
                                                    <?php foreach ($vieww as $key => $value) { ?>
                                                        <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select State <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select id="stateid" name="stateid" value="stateid"
                                                        onchange="selectcity();">
                                                    <option value="">Select State</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select City <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select id="cityid" onchange="selectlocation()" name="cityid"
                                                        value="cityid">
                                                    <option value="">Select city</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Location <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select id="locationid" name="locationid" value="locationid">
                                                    <option value="">Select Location</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Company <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="companyid" id="companyid">
                                                    <option value="">select Company</option>
                                                    <?php foreach ($vieww_company as $key => $value) { ?>
                                                        <option value="<?php echo $value->comp_id; ?>"><?php echo $value->comp_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Branch Name<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="branch_name" name="branch_name">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit"
                                                       class="btn_button sub_btn"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="container">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Branch Name</th>
                                        <th>Branch location</th>
                                        <th>Branch Company</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1;
                                    foreach ($s_viewbranch as $v) {
                                        //pend($v);?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td id="t_act_name"><?php echo $v->branch_name; ?></td>
                                            <td id="t_act_desc"><?php


                                                $parameter = array('act_mode' => 'getlocation',
                                                    'Param1' => $v->branch_location,
                                                    'Param2' => '',
                                                    'Param3' => '',
                                                    'Param4' => '',
                                                    'Param5' => '',
                                                    'Param6' => '',
                                                    'Param7' => '',
                                                    'Param8' => '',
                                                    'Param9' => '');
                                                //p($parameter);
                                                $path = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                $loc_name = curlpost($parameter, $path);
                                                //pend($loc_name);
                                                $arr = (array)$loc_name;
                                                echo($arr[0]['locationname']);


                                                ?></td>
                                            <td id="t_act_price"><?php


                                                $parameter1 = array('act_mode' => 'getcompany',
                                                    'Param1' => $v->branch_company,
                                                    'Param2' => '',
                                                    'Param3' => '',
                                                    'Param4' => '',
                                                    'Param5' => '',
                                                    'Param6' => '',
                                                    'Param7' => '',
                                                    'Param8' => '',
                                                    'Param9' => '');
                                                //p($parameter1);
                                                $path1 = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                $com_name = curlpost($parameter1, $path1);
                                                //pend($com_name);
                                                $arr1 = (array)$com_name;
                                                echo($arr1[0]['comp_name']);


                                                //echo $v->branch_company;


                                                ?></td>
                                            <td>

                                                <a class="btn btn-primary" data-toggle="modal" id="update_branch" data-target="#myModal"
                                                   onclick="update_branch('<?php echo  $v->branch_id; ?>')">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>


                                                <a href="<?= base_url('admin/geographic/branchdelete1/') . "/" . $v->branch_id ?>"
                                                   onclick="return confirm('Are you sure you want to delete this item?');"
                                                   class="btn btn-danger btn">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>


                                            </td>

                                        </tr>

                                        <?php $i++;
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<div class="container">
    <!-- Trigger the modal with a button -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Branch</h4>
                </div>
                <div class="modal-body">

                    <form action="<?= base_url('admin/geographic/updatebranch') ?>" name="add_branch" id="add_branch"
                          method="post" enctype="multipart/form-data">

                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">Update Location <span
                                                style="color:red;font-weight: bold;">*</span></div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input">
                                        <input type="hidden" id="update_branch_id" name="update_branch_id">
                                        <select id="locationid_update" name="locationid_update">
                                           <!-- <option value="">Select Location</option>-->


                                            <?php

                                            $parameter2 = array('act_mode' => 'getlocation_update',
                                                'Param1' => '',
                                                'Param2' => '',
                                                'Param3' => '',
                                                'Param4' => '',
                                                'Param5' => '',
                                                'Param6' => '',
                                                'Param7' => '',
                                                'Param8' => '',
                                                'Param9' => '');
                                            //p($parameter2);
                                            echo $path2 = api_url() . "main_snowworld_v/getlocation/format/json/";
                                            $l_name_update = curlpost($parameter2, $path2);
                                            //p($l_name_update);
                                            foreach($l_name_update as $c)
                                            {
                                                echo "<option value=".$c['locationid'].">".$c['locationname']."</option>";
                                            }



                                            ?>
                                        </select>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">Update Company <span
                                                style="color:red;font-weight: bold;">*</span></div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input">
                                        <select name="companyid_update" id="companyid_update">
                                            <?php foreach ($vieww_company as $key => $value) { ?>
                                                <option value="<?php echo $value->comp_id; ?>"><?php echo $value->comp_name; ?></option>
                                            <?php } ?>
                                        </select></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">Branch Name<span style="color:red;font-weight: bold;">*</span>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input">
                                        <input type="text" id="branch_name_update" style="width: 100%" name="branch_name_update">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <div class="submit_tbl">
                                        <input id="submitBtn" type="submit" name="submit" value="Update"
                                               class="btn_button sub_btn"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                </form>
                <div class="modal-footer">

                </div>
            </div>

        </div>
    </div>

</div>
<script>


    function selectstates() {

        var countryid = $('#countryid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/countrystate',
            type: 'POST',
            //dataType: 'json',
            data: {'countryid': countryid},
            success: function (data) {
                var option_brand = '<option value="">Select State</option>';
                $('#stateid').empty();
                $("#stateid").append(option_brand + data);

            }
        });

    }

    function selectcity() {

        var stateid = $('#stateid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/statecity',
            type: 'POST',
            //dataType: 'json',
            data: {'stateid': stateid},
            success: function (data) {
                var option_brand = '<option value="">Select City</option>';
                $('#cityid').empty();
                $("#cityid").append(option_brand + data);

            }
        });

    }

    function selectlocation() {
        var cityid = $('#cityid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/citylocation',
            type: "POST",
            data: {'cityid': cityid},
            success: function (data) {
                var option_brand = '<option value="">Select Location</option>';
                $('#locationid').empty();
                $("#locationid").append(option_brand + data);

            }

        })

    }

    function update_branch(a) {
        $("#companyid_update").find("option").removeAttr('selected');
        $("#locationid_update").find("option").removeAttr('selected');
        //alert("hello");
        //var a = $(this).domElement();
        //console.log(a);
        $.ajax({
            url: '<?php echo  api_url()."main_snowworld_v/getlocation/format/json/" ?>',
            type: "POST",
            dataType :"json",
            data: {'act_mode':'getbranch_update','Param1': a},
            success: function (data) {
                $("#update_branch_id").val(a);
                $("#branch_name_update").val(data[0].branch_name);
                 $("#companyid_update").find("option[value='"+data[0].branch_company+"']").attr('selected',true);
                $("#locationid_update").find("option[value='"+data[0].branch_location+"']").attr('selected',true);
                return false;
            }

        })


    }


</script>