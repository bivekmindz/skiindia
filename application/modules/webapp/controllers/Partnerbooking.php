<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Partnerbooking extends MX_Controller {

    public function __construct() {
        $this->load->model("supper_admin");
        $this->load->library('session');
		$this->load->helper('url');
         session_start();
    }
    //addones page
    public function partnerbookingsearch(){
		
//		 p( $this->session->userdata ); exit;
/*


        $this->session->unset_userdata('billing_name');
        $this->session->unset_userdata('billing_email');
        $this->session->unset_userdata('billing_tel');
        $this->session->unset_userdata('billing_address');
        $this->session->unset_userdata('billing_city');
        $this->session->unset_userdata('billing_state');

        $this->session->unset_userdata('billing_zip');
        $this->session->unset_userdata('billing_country');
*/
		
		
      	if( empty( $this->session->userdata('user_id') ) ) {  
            
            redirect( 'partnerlogin' );
        }		
		
        $this->session->unset_userdata('orderlastinsertid');
		
        $this->session->unset_userdata('Promocodeprice');
		
        // unset the no. of visitor when comming back
        if( !empty( $this->session->userdata('package_total_qty') ) ){  //echo 1;
            
            $this->session->unset_userdata('package_total_qty');
        }
		
        if( !empty( $this->session->userdata('package_total_price') ) ){  //echo 1;
            
            $this->session->unset_userdata('package_total_price');
        }		
        
        if( !empty( $this->session->userdata('final_selected_package_data') ) ){  //echo 2;
            
            $this->session->unset_userdata('final_selected_package_data');
        }   
		   
		if( !empty( $this->session->userdata('final_selected_addon_data') ) ){

		    $this->session->unset_userdata('final_selected_addon_data');
		}
		
		if( ! empty( $this->session->userdata( 'addon_total_price' ) ) ) {

			$this->session->unset_userdata( 'addon_total_price' );
		}						

		if( ! empty( $this->session->userdata( 'addon_total_qty' ) ) ) {

			$this->session->unset_userdata( 'addon_total_qty' );
		}		
		   
		   
		// sessioon destroy start here 
		if( !empty( $this->session->userdata('orderlastinsertid') ) ){

			$this->session->unset_userdata('orderlastinsertid');

		}		

		if( ! empty( $this->session->userdata( 'before_payment_agent_wallet_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_total_purchase_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_total_purchase_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ) ) {  echo 777;

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'payment_flag' ) ) ) {

			$this->session->unset_userdata( 'payment_flag' );
		}			
		// sessioon destroy end here
		
		// promo variable unset start here 
		if( ! empty( $this->session->userdata( 'total_promo_price' ) ) ) {

			$this->session->unset_userdata( 'total_promo_price' );
		}	

		if( ! empty( $this->session->userdata( 'promo_id' ) ) ) {

			$this->session->unset_userdata( 'promo_id' );
		}			

		if( ! empty( $this->session->userdata( 'saving_price_promocode' ) ) ) {

			$this->session->unset_userdata( 'saving_price_promocode' );
		}				

		if( ! empty( $this->session->userdata( 'promocode_name' ) ) ) {

			$this->session->unset_userdata( 'promocode_name' );
		}				

		if( ! empty( $this->session->userdata( 'handling_charge_with_no_of_person' ) ) ) {

			$this->session->unset_userdata( 'handling_charge_with_no_of_person' );
		}						

		if( ! empty( $this->session->userdata( 'final_cost_package_addon_handling' ) ) ) {

			$this->session->unset_userdata( 'final_cost_package_addon_handling' );
		}	
		
		// promo variable unset end here 		
		
        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);

        $data = array(


            'countryid' => $data['branch']->countryid,
            'stateid' => $data['branch']->stateid,
            'cityid' => $data['branch']->cityid,
            'branch_id' => $data['branch']->branch_id,
            'locationid' => $data['branch']->branch_location
        );
        $this->session->set_userdata($data);
        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


//select banner images
        $parameterbanner=array(
            'act_mode' =>'selectbannerimages',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/banner/format/json/';
        $data['banner']=curlpost($parameterbanner,$path);

        $datenew=date("Y-m-d");
        $parameterdatediff = array( 'act_mode'=>'s_viewtimeslotdate',
            'nextDate'=>$datenew,
            'starttime'=>'',
            'branchid'=>$data['branch']->branch_id,
            'dailyinventory_to' =>'',
            'dailyinventory_seats' =>'',
            'dailyinventory_status' =>'',
            'dailyinventory_createdon' =>'',
            'dailyinventory_modifiedon' =>'',
            'dailyinventory_minfrom' =>'',
            'dailyinventory_minto' =>'',
        );

        $data['s_viewdate'] = $this->supper_admin->call_procedurerow('proc_crone_v',$parameterdatediff);

//Select Time
        $parametertimeslot=array(
            'act_mode' => 'selecttimeslot',
            'branchid' => $data['branch']->branch_id,

            'destinationType' => $this->session->userdata('destinationType'),

            'type'=>'web',

        );

        $path = api_url().'selecttimesloturl/timeslot/format/json/';
        $data['timeslot'] = curlpost($parametertimeslot,$path);
//Select Time slot
        $parametertimeslot=array(
            'act_mode' =>'selecttimeslot',
            'branchid' =>$data['branch']->branch_id,
            'destinationType' =>$this->session->userdata('destinationType'),
            'type'=>'web',

        );

        $path12=api_url().'selecttimesloturl/timeslotses/format/json/';
        $data['timeslotses']=curlpost($parametertimeslot,$path12);


        //Search PACKAGES
        if($_POST['destinationType']!='')
        {

				
            $timeexplode=explode("-",$this->input->post('destinationType'));
            $timeexplodevalue=explode(":",$timeexplode[1]);
            $timeexplodevaluetime=$timeexplodevalue[0];
           
            $parameter2 = array( 'act_mode'=>'s_viewtimeslotf',
                'nextDate'=> date('Y-m-d', strtotime( $this->input->post('txtDepartDate') ) ) ,
                'starttime'=>$timeexplodevaluetime,
                'branchid'=>$data['branch']->branch_id,
                'dailyinventory_to' =>$this->input->post('txtDepartDate'),
                'dailyinventory_seats' =>'',
                'dailyinventory_status' =>'',
                'dailyinventory_createdon' =>'',
                'dailyinventory_modifiedon' =>'',
                'dailyinventory_minfrom' =>'',
                'dailyinventory_minto' =>'',
            );

            $data['s_viewtimeslot'] = $this->supper_admin->call_procedurerow('proc_crone_v',$parameter2);
		
			$dailyinventory_status = isset( $data['s_viewtimeslot']->dailyinventory_seats ) ? $data['s_viewtimeslot']->dailyinventory_seats : 0;
			
			$ddAdult = $this->input->post( 'ddAdult' ) ;
		
				if( $ddAdult > $dailyinventory_status ) { 
			
					$this->session->set_flashdata( 'current_timeslot_seat_limit_error_msg', 'No. of visitor must be less then ' . $dailyinventory_status );
					
					redirect('partnerbookingsearch') ;
				}

			$this->session->set_userdata( 'time_slot_limit_value', $data['s_viewtimeslot'] );			
            			
			$destinationType =  explode('-', trim( $_POST['destinationType'] ) ); 
			$time_slot_id = array_shift( $destinationType );

            $data = array(  
            'uniqid' => uniqid(),
            'txtDepartDate' => trim($_POST['txtDepartDate']),
            'destinationType' => trim($_POST['destinationType']),
			'select_time_slot' => implode('-', $destinationType ),
			'select_time_slot_id' => $time_slot_id,
            'ddAdult' => trim($_POST['ddAdult'])
            );

            $this->session->set_userdata($data);
            
			$this->session->set_userdata( 'dailyinventory_seats', $dailyinventory_status ) ; 
			

           	redirect('agentpackagesstep', 'refresh');
			exit;
        }

        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/bookingsearch",$data);
        $this->load->view("agent/helper/footer");

    }
    
    
    public function agentpackagesstep(){
 
		//Select branch
       	if( empty($this->session->userdata('txtDepartDate')) ){
        
	      redirect(base_url('/partnerbookingsearch'));
    	}else{     
		   
        //Select branch
        $siteurl= base_url();
        $parameterbranch=array(
                  'act_mode' =>'selectbranch',
                  'weburl' =>$siteurl,
                  'type'=>'web',

                  );

        $path = api_url().'selectsiteurl/branch/format/json/'; 
        $data['branch'] = curlpost($parameterbranch,$path); 



        //select banner images
        $parameterbanner=array(
                  'act_mode' =>'selectbannerimages',
                  'branchid' =>$data['branch']->branch_id,
                  'type'=>'web',

                  );

       $path = api_url().'selectsiteurl/banner/format/json/'; 
       $data['banner'] = curlpost($parameterbanner,$path);    

        //Select Time slot 
       $parametertimeslot=array(
              'act_mode' => 'selectsestimeslot',
              'branchid' => $data['branch']->branch_id,
              'destinationType' => $this->session->userdata('destinationType'),
              'type'=>'web',
              );

       $path=api_url().'selecttimesloturl/timeslotses/format/json/'; 
       $data['timeslotses'] =curlpost($parametertimeslot,$path);
        
       /* if(($this->session->userdata['ppid'])>0){

            $parameterpac = array('act_mode' => 'select_packagepartner',
                'branchid' => $this->session->userdata['branch_id'],
                'type' => 'web',
                'packageqty' => '',
                'packageid' => '',
                'txtDepartDate' => getDBDateFormat($this->session->userdata['txtDepartDate'])
            );
        }else{*/

            $parameterpac = array('act_mode' => 'select_packageuserguest',
                'branchid' => $this->session->userdata['branch_id'],
                'type' => 'web',
                'packageqty' => 'null',
                'packageid' => 'null',
                'txtDepartDate' => getDBDateFormat($this->session->userdata['txtDepartDate'])     
            );
       // }

        $path = api_url()."Packages/packageselect/format/json/";
        $get_selected_package_by_date_timeslot = curlpost($parameterpac,$path);
        
        // unset the no. of visitor when comming back
		   
		//p( $this->session->userdata );
   
        if( !empty( $this->session->userdata('package_total_qty') ) ){   // echo 1;
            
            $this->session->unset_userdata('package_total_qty');
        }  

        if( !empty( $this->session->userdata('package_total_price') ) ){  // echo 2;
            
            $this->session->unset_userdata('package_total_price');
        }  		   
        
        if( !empty( $this->session->userdata('final_selected_package_data') ) ){  // echo 3;
            
            $this->session->unset_userdata('final_selected_package_data');
        }   
		   
		if( !empty( $this->session->userdata('final_selected_addon_data') ) ){

		   $this->session->unset_userdata('final_selected_addon_data');
		} 
		   
		if( ! empty( $this->session->userdata( 'addon_total_price' ) ) ) {

			$this->session->unset_userdata( 'addon_total_price' );
		}						

		if( ! empty( $this->session->userdata( 'addon_total_qty' ) ) ) {

			$this->session->unset_userdata( 'addon_total_qty' );
		}		   
		// sessioon destroy start here 
		if( !empty( $this->session->userdata('orderlastinsertid') ) ){

			$this->session->unset_userdata('orderlastinsertid');

		}		

		if( ! empty( $this->session->userdata( 'before_payment_agent_wallet_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_total_purchase_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_total_purchase_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'payment_flag' ) ) ) {

			$this->session->unset_userdata( 'payment_flag' );
		}			
		// sessioon destroy end here	

		// promo variable unset start here 
		if( ! empty( $this->session->userdata( 'total_promo_price' ) ) ) {

			$this->session->unset_userdata( 'total_promo_price' );
		}	

		if( ! empty( $this->session->userdata( 'promo_id' ) ) ) {

			$this->session->unset_userdata( 'promo_id' );
		}			

		if( ! empty( $this->session->userdata( 'saving_price_promocode' ) ) ) {

			$this->session->unset_userdata( 'saving_price_promocode' );
		}				

		if( ! empty( $this->session->userdata( 'promocode_name' ) ) ) {

			$this->session->unset_userdata( 'promocode_name' );
		}				

		if( ! empty( $this->session->userdata( 'handling_charge_with_no_of_person' ) ) ) {

			$this->session->unset_userdata( 'handling_charge_with_no_of_person' );
		}						

		if( ! empty( $this->session->userdata( 'final_cost_package_addon_handling' ) ) ) {

			$this->session->unset_userdata( 'final_cost_package_addon_handling' );
		}			   
		// promo variable unset end here
		
		   
		$data['no_of_visitors'] = $this->session->userdata( 'ddAdult' );		
		   
		$data['dailyinventory_seats'] = $this->session->userdata( 'dailyinventory_seats' ) ;

		$data['current_time_slot_limit_value'] =  count( $this->session->userdata('time_slot_limit_value') ) ? $this->session->userdata('time_slot_limit_value')->dailyinventory_seats : 0;		 
		   
        $selected_time_slots = explode('-', $this->session->userdata['destinationType']);
        array_shift($selected_time_slots);
        
        $temp = array();
        foreach($selected_time_slots as $val){
            $hr_min_array = explode(':', $val);
            $temp = array_merge($temp, $hr_min_array);
        }

        $temp = array_map('trim',$temp);
        
        $filter_data = [];
        foreach($get_selected_package_by_date_timeslot as $val){
            
            $val = array_map('trim',$val);
            if( $val['dailyinventory_from'] == $temp[0] && 
                $val['dailyinventory_minfrom'] == $temp[1] &&
                $val['dailyinventory_to'] == $temp[2] && 
                $val['dailyinventory_minto'] == $temp[3] ){
                //print_r($val);
                $filter_data[] = $val;
                
            }
        }
        $data['vieww'] = $filter_data;
        
        $this->session->set_userdata('filter_package_by_date_timeslot', $filter_data);

        //continue to 2nd stage
        
        if($this->input->post('submit')=='CONTINUE package'){
            
            //$this->session->unset_userdata('addonsval');
            
            $final_selected_package_data =  json_decode( $this->input->post('final_selected_data') );
          
            $tmp_price = $tmp_qty = [];
            
            foreach( $final_selected_package_data as $v ){
                
                  $tmp_price[] = $v->package_price * $v->qty;
                    $tmp_qty[] = $v->qty;
            }
                        
            $data['package_total_price'] = getSumAllArrayElement( calculatePriceByQty( $tmp_price, $tmp_qty ) );
            //die;          
            $data = array(
                'uniqid' => uniqid(),
                'packageval' => 1,
                'packageimageval' => '',
                'packagenameval' => '',
                'packageqtyval' => getTotalPrice($tmp_qty) ? getTotalPrice($tmp_qty) : 0,   // getTotalPrice contain intiger element and sum all integer
                'packagepriceval' => getTotalPrice($tmp_price) ? getTotalPrice($tmp_price) : 0,
                'packagedescription' => ''
            );
           
            $validate = [];
            foreach( $this->session->userdata('filter_package_by_date_timeslot') as $kk => $val ){
                 
                if( isset( $final_selected_package_data[ $kk] ) ){
                    
                    // all key value must match therefore 12 
                    count( array_intersect_assoc( $val, (array) $final_selected_package_data[$kk]  ) )  > 12 ?  array_push($validate, 1) : 
                    array_push($validate, 0);
                }
                
            }
            
            // validate and check - harmful attack
            if( count($validate) ){
                 
                //$this->session->set_userdata($data);   not in used
                $this->session->set_userdata('final_selected_package_data', $final_selected_package_data); // to appy forward on all pages
                header("location:".base_url()."agentaddones");
            }else{
                
                echo '<center>unauthorise access or invalid parameter</center>';
                exit;               
            }

          /*  if($_POST['packageid']!=''){

                        $data = array(
                             'uniqid' => uniqid(),
                             'packageval' => $_POST['packageid'],
                             'packageimageval' => $_POST['packageimage'],
                             'packagenameval' => $_POST['packagename'],
                             'packageqtyval' => $_POST['packageqty'],
                             'packagepriceval' => $_POST['packageprice'],
                             'packagedescription' => $_POST['packagedescription']
                        );


                 $this->session->set_userdata($data);
                 header("location:".base_url()."addones");
            }else{

                header("location:".base_url()."packagesstep");
            }*/
            
        }
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/agentpackagesstep",$data);
        $this->load->view("agent/helper/footer");
    }
}



    public function agentaddones(){

   		if( $this->session->userdata('final_selected_package_data') == '' ){

            redirect(base_url('/agentpackagesstep'));
        }else{
			
			//p( $this->session->userdata );
//$this->session->sess_destroy();
            //Select Time slot
			//Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);
            
			//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);


            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path = api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses'] = curlpost($parametertimeslot,$path);
            

            // emty if already set
			
		
			if( ! empty( $this->session->userdata( 'final_selected_addon_data') ) ) { echo 1;

				$this->session->unset_userdata( 'final_selected_addon_data' );
			}
			
			if( ! empty( $this->session->userdata( 'addon_total_price' ) ) ) { echo 2;

				$this->session->unset_userdata( 'addon_total_price' );
			}						
			
			if( ! empty( $this->session->userdata( 'addon_total_qty' ) ) ) {  echo 7;

				$this->session->unset_userdata( 'addon_total_qty' );
			}					
			
			// sessioon destroy start here 
			if( !empty( $this->session->userdata('orderlastinsertid') ) ){

				$this->session->unset_userdata('orderlastinsertid');

			}		

			if( ! empty( $this->session->userdata( 'before_payment_agent_wallet_amount' ) ) ) {

				$this->session->unset_userdata( 'before_payment_agent_wallet_amount' );
			}

			if( ! empty( $this->session->userdata( 'before_payment_total_purchase_amount' ) ) ) {

				$this->session->unset_userdata( 'before_payment_total_purchase_amount' );
			}

			if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) ) ) {

				$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' );
			}

			if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) ) ) {

				$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' );
			}

			if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ) ) {

				$this->session->unset_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' );
			}

			if( ! empty( $this->session->userdata( 'payment_flag' ) ) ) {

				$this->session->unset_userdata( 'payment_flag' );
			}
			
			// promo sessionn start here
			if( ! empty( $this->session->userdata( 'total_promo_price' ) ) ) {

				$this->session->unset_userdata( 'total_promo_price' );
			}	
			
			if( ! empty( $this->session->userdata( 'promo_id' ) ) ) {

				$this->session->unset_userdata( 'promo_id' );
			}			
			
			if( ! empty( $this->session->userdata( 'saving_price_promocode' ) ) ) {

				$this->session->unset_userdata( 'saving_price_promocode' );
			}				
			
			if( ! empty( $this->session->userdata( 'promocode_name' ) ) ) {

				$this->session->unset_userdata( 'promocode_name' );
			}				

			if( ! empty( $this->session->userdata( 'handling_charge_with_no_of_person' ) ) ) {

				$this->session->unset_userdata( 'handling_charge_with_no_of_person' );
			}						
			
			if( ! empty( $this->session->userdata( 'final_cost_package_addon_handling' ) ) ) {

				$this->session->unset_userdata( 'final_cost_package_addon_handling' );
			}
			
			// sessioon destroy end here

            //p($obj);
            $dataaddonedisplay = array(
                'act_mode' =>'aaddonedisplay',
                'categories1' => '',
                'addonid' => '',
                'addonprice' => '',
                'addonqty' =>'',
                'addonimage' => '',
                'addonname' =>'',
                'uniqueid' => $this->session->userdata['uniqid'],
                'type' => 'web'
            );
            $pathdisplay= api_url()."Cart/displayaddonscartses_select/format/json/";
            $data['addonsdisplay'] = curlpost($dataaddonedisplay,$pathdisplay);

// MASTER ADDON            

            $parameterbranch        = array('act_mode'=>'addonscartses_package',
                'branch_id'=>$this->session->userdata['branch_id'],
                'type'=>'web',
            );
            $pathbranch= api_url()."Cart/getaddonscartses_select/format/json/";
            $data['addonscart']= curlpost($parameterbranch,$pathbranch);
			
			$final_selected_addon_data = $final_selected_package_data_validated = $validate = $data['addonsdisplay'] = [];
			

// addon data manipulation start here   
			
			if( !empty( $this->input->post( 'final_selected_addon_data' ) ) ){ 
				
				$final_selected_addon_data = json_decode($this->input->post( 'final_selected_addon_data' ) ); 

				if( count( json_decode($this->input->post( 'final_selected_addon_data' ) ) ) ) {
					
					
					foreach( $data['addonscart'] as $kkk => $vvv ){
						
						foreach( $final_selected_addon_data as $kkkk => $vvvv){
							
							if( count( array_intersect_assoc( $vvv, (array) $vvvv ) ) >= 10 ) {
									
								 $final_selected_package_data_validated[] = (array) $vvvv;
								
								array_push($validate, 1);
									
							}else{
								
								array_push($validate, 0);
							}
						} 
						
					}

					if( array_sum($validate) && ( array_sum($validate) == count($final_selected_addon_data) ) ){
						
						$this->session->set_userdata( 'final_selected_addon_data', $data['addonsdisplay'] = count( $final_selected_package_data_validated ) ? $final_selected_package_data_validated : [] );
						
					}else{

						echo '<center>unauthorise access or invalid parameter</center>';
						exit;				
					}	
					
				}
				
			
			}
				
			             
            $grand_total = []; // final price 
            $data['addon_total_price'] = $data['addon_total_qty'] = $data['package_total_price'] = $data['package_total_qty'] = 0;
           
            $addon_price_qty_array = getAddonkeyValue( $data['addonsdisplay'] );

            if( count($addon_price_qty_array) ){
				
                $data['addon_total_price'] = $grand_total[] = getSumAllArrayElement( $addon_price_qty_array['addon_price_array'] );
                $data['addon_total_qty'] = getSumAllArrayElement( $addon_price_qty_array['addon_qty_array'] );
            }   
            

            if( ! isset( $data['addonsdisplay']->scalar ) ) {
                
                $this->session->set_userdata( 'final_selected_addon_data', $data['addonsdisplay'] );
            }
    
// addon data manipulation end here            
            
            
            // package data manipulation start here
            $data['tmp_array'] = $this->session->userdata('final_selected_package_data');
             
            $package_price_qty_array = getPackageTotalPrice( $this->session->userdata('final_selected_package_data') );

            $data['package_total_price'] =  $package_price_qty_array['package_price_array'];

            $data['package_total_qty'] = $package_price_qty_array['package_qty_array'];
            
            $grand_total[] = $data['package_total_price'] = getSumAllArrayElement( calculatePriceByQty( $data['package_total_price'], $data['package_total_qty'] ) );
            
            // no of visitors revised
            $data['package_total_qty'] = getSumAllArrayElement( $data['package_total_qty'] );
            
            // add to session
            
            $this->session->set_userdata( [ 'package_total_qty' =>  $data['package_total_qty'], 
                                           'package_total_price' => $data['package_total_price'], 
                                           'addon_total_price' => $data['addon_total_price'],
                                           'addon_total_price' => $data['addon_total_price'],
                                           'addon_total_qty' => $data['addon_total_qty']
                                          ] );
                

            $data['package_addon_total'] = getSumAllArrayElement( $grand_total ) ? getSumAllArrayElement( $grand_total ) : 0;
            // package data manipulation end here




            if($this->input->post('submit')=='cartses'){
			
                    redirect(base_url()."partnersummary");
         			exit;

            }
                 

        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/agentaddones",$data);
        $this->load->view("agent/helper/footer");
       }
    }

public function partnersummary()
{
     	if( empty( $this->session->userdata('final_selected_package_data') ) ){
            
			redirect(base_url('/agentpackagesstep'));
			
        } else {
			
			if( empty( $this->session->userdata('user_id') ) ) {  

				redirect( 'partnerlogin' ); 
				
				exit;
			}			
			
			//p( $this->session->userdata );
			
			//Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);
			
			//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);

			//Select Time slot
            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path);

    		$grand_total = []; // final price
            
            // unset last handling charge AND OTHER VARIABLE -  controller on load
			
/*			
            if ( !empty($this->session->userdata('handling_charge_with_no_of_person') ) ){

                $this->session->unset_userdata('handling_charge_with_no_of_person');
            }   

            if( !empty( $this->session->userdata('final_cost_package_addon_handling') ) ) {

                $this->session->unset_userdata('final_cost_package_addon_handling');
            }   
		
			if( !empty( $this->session->userdata('orderlastinsertid') ) ){

				$this->session->unset_userdata('orderlastinsertid');

			}		

			if( ! empty( $this->session->userdata( 'before_payment_agent_wallet_amount' ) ) ) {

				$this->session->unset_userdata( 'before_payment_agent_wallet_amount' );
			}

			if( ! empty( $this->session->userdata( 'before_payment_total_purchase_amount' ) ) ) {

				$this->session->unset_userdata( 'before_payment_total_purchase_amount' );
			}

			if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) ) ) {

				$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' );
			}

			if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) ) ) {

				$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' );
			}

			if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ) ) {

				$this->session->unset_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' );
			}

			if( ! empty( $this->session->userdata( 'payment_flag' ) ) ) {

				$this->session->unset_userdata( 'payment_flag' );
			}		
	*/			
		
	/*	
            if( !empty( $this->session->userdata('ccavenue_param') ) ) {

                $this->session->unset_userdata('ccavenue_param');
            }	
		
	*/
            
            $data['internethandlingcharge'] = $data['branch']->branch_internet_handling_charge;

            $data['final_selected_package_data'] = $this->session->userdata('final_selected_package_data');                     
            
            $get_package_price_qty_array = (object) getPackageTotalPrice( $data['final_selected_package_data'] );
                
            $data['package_total_price'] = $get_package_price_qty_array->package_price_array;
            
            // no. of visitors
            $data['package_total_qty'] = $get_package_price_qty_array->package_qty_array;
            
            // get revised internet handling charge
            
            $data['internethandlingcharge_revised'] = $grand_total[] = getHandlingChargeByNoOfPerson( $data['internethandlingcharge'], getSumAllArrayElement( $data['package_total_qty'] ) ); 

            // add internet handling charge by no of person
            $this->session->set_userdata('handling_charge_with_no_of_person', $data['internethandlingcharge_revised']);         
            
            // send hr min time slot to view page           
            $data['to_render_array'] = getTimeSlotInArray( $this->session->userdata('destinationType') );           
            
            // revised price 
            $grand_total[] = $data['package_total_price'] = getSumAllArrayElement( calculatePriceByQty( $data['package_total_price'], $data['package_total_qty'] ) );
            
            // no. of visitors revised send to view file 
            $data['package_total_qty'] = getSumAllArrayElement( $data['package_total_qty'] );
            
            
            
            $data['final_selected_addon_data'] = $this->session->userdata( 'final_selected_addon_data');
            

            $get_price_qty_array = (object) getAddonkeyValue( $data['final_selected_addon_data'] );

            //$data['addon_total_price'] = $grand_total[] = getSumAllArrayElement($get_price_qty_array->addon_price_array);
            
            $data['addon_total_price'] = $grand_total[] = getSumAllArrayElement( calculatePriceByQty( $get_price_qty_array->addon_price_array , $get_price_qty_array->addon_qty_array ) );

            
            $data['addon_total_qty'] = $addon_total_qty = getSumAllArrayElement($get_price_qty_array->addon_qty_array);
          
            // grand cost - (package + addon + handling charge )

            $data['grand_total'] = getSumAllArrayElement( $grand_total );
    
            // add final cost - (package + addon + handling charge ) to session
            $this->session->set_userdata( [ 'final_cost_package_addon_handling' =>  $data['grand_total'] ] );

            //Sachin ------------------
			// Promocode start here
            if($_POST['promoFormdata']!='')
            {
                $epromodate=explode("/",$this->session->userdata('txtDepartDate'));
                $promodatevalda=($epromodate[1]."/".$epromodate[0]."/".$epromodate[2]);
                $parameter = array(
					'act_mode'=>'promoselect_package',
					'promovalue'=>$_POST['promoFormdata'],
					'locationid'=>$this->session->userdata['locationid'],
                    'branch_id'=>$this->session->userdata['branch_id'],
                    'userid'=>$promodatevalda
                );
                $path = api_url()."Cart/getpromoselect_package/format/json/";
                $data['promocode']= curlpost($parameter,$path);
				
                if($data['promocode']->scalar=='Something Went Wrong'){ $data['msg']="Invalid Promocode"; }
                else
                {
                    $dateses=explode("/",$data['promocode']->p_expiry_date);
                    $datepromo=explode("/",rtrim($this->session->userdata('txtDepartDate')," "));
                    $date2=date_create($dateses[2]."-".$dateses[0]."-".$dateses[1]);
                    $date1=date_create($datepromo[2]."-".$datepromo[1]."-".$datepromo[0]);
                    $diff=date_diff($date1,$date2);
                    $datesesval= $diff->format("%R%a");


                    if($datesesval>=0) {

                        if ($data['promocode']->p_addonedata != '') {


//p($this->session->userdata);
                            $promodate = explode(",", $data['promocode']->p_addonedata);

                            foreach ($promodate as $v) {

                                $data13 = array(
                                    'act_mode' => 'selectaddone',
                                    'categories1' => '',
                                    'addonid' => $v,
                                    'addonprice' => '',
                                    'addonqty' => '',
                                    'addonimage' => '',
                                    'addonname' => '',
                                    'uniqueid' => $this->session->userdata['uniqid'],
                                    'type' => 'web');
                                $pathaddone13 = api_url() . "Cart/insertaddonscartses_select/format/json/";
                                $data['addonscartselectd'] = curlpost($data13, $pathaddone13);


                                $arr = (array)$data['addonscartselectd'];

                                if($data['addonscartselectd']->scalar != "Something Went Wrong") {
                                    $parameter4 = array('act_mode' => 'addonviewupdate',
                                        'Param1' => $v,
                                        'Param2' => '',
                                        'Param3' => '',
                                        'Param4' => '',
                                        'Param5' => '',
                                        'Param6' => '',
                                        'Param7' => '',
                                        'Param8' => '',
                                        'Param9' => '');
                                    //pend($parameter);
                                    $response['vieww'] = $this->supper_admin->call_procedurerow('proc_addon_s', $parameter4);

                                    $data[$i] = array(
                                        'act_mode' => 'updateaddonedata',
                                        'categories1' => $response['vieww']->addon_id,
                                        'addonid' => $response['vieww']->addon_id,
                                        'addonprice' => $response['vieww']->addon_price,
                                        'addonqty' => '',
                                        'addonimage' => $response['vieww']->addon_image,
                                        'addonname' => $response['vieww']->addon_name,
                                        'uniqueid' => $this->session->userdata['uniqid'],
                                        'type' => 'web'
                                    );

                                    $pathaddone = api_url() . "Cart/insertaddonscartses_select/format/json/";
                                    $data['addonscart'] = curlpost($data[$i], $pathaddone);

                                } else {


                                    $parameter4 = array('act_mode' => 'addonviewupdate',
                                        'Param1' => $v,
                                        'Param2' => '',
                                        'Param3' => '',
                                        'Param4' => '',
                                        'Param5' => '',
                                        'Param6' => '',
                                        'Param7' => '',
                                        'Param8' => '',
                                        'Param9' => '');
                                    //pend($parameter);
                                    $response['vieww'] = $this->supper_admin->call_procedurerow('proc_addon_s', $parameter4);

                                    $data = array(
                                        'act_mode' => 'insertaddone',
                                        'categories1' => $response['vieww']->addon_id,
                                        'addonid' => $response['vieww']->addon_id,
                                        'addonprice' => 0,
                                        'addonqty' => 1,
                                        'addonimage' => $response['vieww']->addon_image,
                                        'addonname' => $response['vieww']->addon_name,
                                        'uniqueid' => $this->session->userdata['uniqid'],
                                        'type' => 'web'
                                    );

                                    $pathaddone = api_url() . "Cart/insertaddonscartses_select/format/json/";
                                    $data['addonscart'] = curlpost($data, $pathaddone);
                                }
                            }

                            $data1 = array(
                                'act_mode' => 'addonselectbyuniqid',
                                'categories1' => '',
                                'addonid' => '',
                                'addonprice' => '',
                                'addonqty' => '',
                                'addonimage' => '',
                                'addonname' => '',
                                'uniqueid' => $this->session->userdata['uniqid'],

                            );

                            $data['vieww1'] = $this->supper_admin->call_procedurerow('proc_tempaddoneadd_v', $data1);

                            $data = array(


                                'cartaddoneid' => $data['vieww1']->addonid,
                                'cartaddoneimage' => $data['vieww1']->addonimage,
                                'cartaddonname' => $data['vieww1']->addonname,
                                'cartaddonqty' => $data['vieww1']->addonqty,
                                'cartaddonprice' => $data['vieww1']->addonprice,

                            );
                            $this->session->set_userdata($data);
                            $siteurl = base_url();
                            $parameterbranch = array(
                                'act_mode' => 'selectbranch',
                                'weburl' => $siteurl,
                                'type' => 'web',

                            );

                            $path = api_url() . 'selectsiteurl/branch/format/json/';
                            $data['branch'] = curlpost($parameterbranch, $path);

                            $parameter = array('act_mode' => 'promoselect_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => $this->session->userdata['locationid'],
                                'branch_id' => $this->session->userdata['branch_id'],
                                'userid' => $this->session->userdata['userid'],
                            );
                            $path = api_url() . "Cart/getpromoselect_package/format/json/";
                            $data['promocode'] = curlpost($parameter, $path);

                            $data = array(
                                'ppromo_id' => $data['promocode']->promo_id,
                                'ppcodename' => $data['promocode']->p_codename,
                                'pp_type' => $data['promocode']->p_type,
                                'pp_addonedata' => $data['promocode']->p_addonedata,
                                'pp_locationid' => $data['promocode']->p_locationid,
                                'pp_branchid' => $data['promocode']->p_branchid,
                                'pp_discount' => $data['promocode']->p_discount,
                                'pp_status' => $data['promocode']->p_status,
                                'pp_expiry_date' => $data['promocode']->p_expiry_date,
                                'pp_allowed_per_user' => $data['promocode']->p_allowed_per_user,
                                'pp_start_date' => $data['promocode']->p_start_date,
                                'pp_allowed_times' => $data['promocode']->p_allowed_times,
                                'pp_createdon' => $data['promocode']->p_createdon,
                                'pp_modifiedon' => $data['promocode']->p_modifiedon,
                                'pp_logintype' => $data['promocode']->p_logintype,
                                'pp_addoneqty' => $data['promocode']->p_addoneqty,
                                'internethandlingcharge' => $data['branch']->branch_internet_handling_charge,
                            );
                            $this->session->set_userdata($data);
                            redirect("summary");
                            //  echo "dddd"; exit;
                        } else {
                            $originalDate = $this->session->userdata('txtDepartDate');
                            $Date = explode("/", $this->session->userdata('txtDepartDate'));
                            $newDate = $Date[1] . "/" . $Date[0] . "/" . $Date[2];
                            if ($data['promocode']->p_logintype == '0' or $data['promocode']->p_logintype == '1') {

//echo $newDate."---".$data['promocode']->p_start_date."---".$data['promocode']->p_expiry_date;

                           

                                    //Complete Promo Select
                                    $parametercompuser = array('act_mode' => 'promoselectcompuser_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => '',
                                        'branch_id' => '',
                                        'userid' => '',
                                    );
                                    $pathcompuser = api_url() . "Cart/getpromoselect_packageuser/format/json/";
                                    $data['promocompuser'] = curlpost($parametercompuser, $pathcompuser);


                                    if ($data['promocompuser']->num < $data['promocode']->p_allowed_times) {
//Userwise Promo select

                                        $parameteruser = array('act_mode' => 'promoselectuser_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => '',
                                            'branch_id' => '',
                                            'userid' => $this->session->userdata('skiindia'),
                                        );
                                        $pathuser = api_url() . "Cart/getpromoselect_packageuser/format/json/";
                                        $data['promocodeuser'] = curlpost($parameteruser, $pathuser);


                                        $data = array(
                                            'ppromo_id' => $data['promocode']->promo_id,
                                            'ppcodename' => $data['promocode']->p_codename,
                                            'pp_type' => $data['promocode']->p_type,
                                            'pp_addonedata' => $data['promocode']->p_addonedata,
                                            'pp_locationid' => $data['promocode']->p_locationid,
                                            'pp_branchid' => $data['promocode']->p_branchid,
                                            'pp_discount' => $data['promocode']->p_discount,
                                            'pp_status' => $data['promocode']->p_status,
                                            'pp_expiry_date' => $data['promocode']->p_expiry_date,
                                            'pp_allowed_per_user' => $data['promocode']->p_allowed_per_user,
                                            'pp_start_date' => $data['promocode']->p_start_date,
                                            'pp_allowed_times' => $data['promocode']->p_allowed_times,
                                            'pp_createdon' => $data['promocode']->p_createdon,
                                            'pp_modifiedon' => $data['promocode']->p_modifiedon,
                                            'pp_logintype' => $data['promocode']->p_logintype,
                                            'pp_addoneqty' => $data['promocode']->p_addoneqty,
                                            'internethandlingcharge' => $data['branch']->branch_internet_handling_charge,
                                        );
                                        $this->session->set_userdata($data);
                                    // echo($this->session->userdata('txtDepartDate'));
                                } else {
                                    $data['msg'] = "Invalid Promocode";
                                }
                            } else {
                                $data['msg'] = "Invalid Promocode";
                            }
                        }
                    }   else
                    {
                        $data['msg'] = "Invalid Promocode";
                    }
                    // pend($data['promocode']->p_addonedata);
                }
            }
            else
            {
                $data['msg']="Promo Not Add";
            }


            if($_POST['submit']=='final add')
            {
                redirect(base_url()."partnerpaymentagent");
            }
             $data['internethandlingcharge']=($data['branch']->branch_internet_handling_charge);
            $this->load->view("agent/helper/header",$data);
            $this->load->view("agent/helper/leftbar",$data);
            $this->load->view("agent/partnersummary",$data);
            $this->load->view("agent/helper/footer");
        }
}



 //Promocode Process

        public function checkpromocode(){ 
 

        $promocode = $this->input->post('promocode');
        $txtDepartDate =$this->session->userdata['txtDepartDate'];
        $uid = $this->input->post('uid');
        $totalhourmint=   getTimeSlotInArray($this->session->userdata['destinationType']);
        $txfromhrs = $totalhourmint[0];
        $txtfrommin =  $totalhourmint[1];
        $txttohrs =  $totalhourmint[2];
        $txttomin = $totalhourmint[3];
        $txtfromd = $totalhourmint[4];
        $txttod = $this->input->post('txttod');
        $type = $this->input->post('type');
        $totalprice = $this->session->userdata['final_cost_package_addon_handling'];         


        $cartaddoneiddata = $this->input->post('cartaddoneiddata');
// p($ccartpackageimage);p($ccartaddonname);p($ccartaddonprice);exit;
//Check Promocode Condition Exits
     $checkpromo = array('act_mode'=>'checkpromocode',
                                    'Param1'=>'',
                                    'Param2'=>'',
                                    'Param3'=>$promocode,
                                    'Param4'=>'',
                                    'Param5'=>'',
                                    'Param6'=>'',
                                    'Param7'=>'',
                                    'Param8'=>'',
                                    'Param9'=>'',
                                    'Param10'=>'',
                                    'Param11'=>'',
                                    'Param12'=>'',
                                 );
        $checkPromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $checkpromo); 
        //p($checkpromo);echo 'sd';exit;
       if($checkPromocode->cnt>0){
    // Check Start date and end date exits promocode
         $checkpromodate = array('act_mode'=>'datevalidation',
                                    'Param1'=>'',
                                    'Param2'=>'',
                                    'Param3'=>date('Y-m-d',strtotime($txtDepartDate)),
                                    'Param4'=>'',
                                    'Param5'=>'',
                                    'Param6'=>'',
                                    'Param7'=>'',
                                    'Param8'=>'',
                                    'Param9'=>'',
                                    'Param10'=>'',
                                    'Param11'=>'',
                                    'Param12'=>'',
                                 );
        $datepromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $checkpromodate);
      if($datepromocode->pcnt>0){
      //Check user type exit;
         $parameter1 = array('act_mode'=>'get_user_detail_agent',
                                    'Param1'=>'',
                                    'Param2'=>$uid,
                                    'Param3'=>'',
                                    'Param4'=>'',
                                    'Param5'=>'',
                                    'Param6'=>'',
                                    'Param7'=>'',
                                    'Param8'=>'',
                                    'Param9'=>'',
                                    'Param10'=>'',
                                    'Param11'=>'',
                                    'Param12'=>'',
                                 );

        $getudetail = $this->supper_admin->call_procedurerow('proc_promocode_s', $parameter1); 
       $promocode1 = array('act_mode'=>'checkpromode_user_type',
                                    'Param1'=>'',
                                    'Param2'=>'',
                                    'Param3'=>$promocode,
                                    'Param4'=>'',
                                    'Param5'=>'',
                                    'Param6'=>'',
                                    'Param7'=>'',
                                    'Param8'=>'',
                                    'Param9'=>'',
                                    'Param10'=>'',
                                    'Param11'=>'',
                                    'Param12'=>'',
                                 );
      $promocodedetail = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocode1);//p($getudetail);exit;
     //Check Promo User Type Exits
     $checkusertype = $this->checkusertype('Agent',$promocodedetail->p_usertype); 
     if($checkusertype==1){ 
     //Check Promo date time exits
      $promocodedate = array('act_mode'=>'checkpromode_date_time',
                                    'Param1'=>  '',
                                    'Param2'=>  '',
                                    'Param3'=>  date('Y-m-d',strtotime($txtDepartDate)),
                                    'Param4'=>  '',
                                    'Param5'=>  '',
                                    'Param6'=>  $promocodedetail->promo_id,
                                    'Param7'=>  $txfromhrs,//"4"
                                    'Param8'=>  $txtfrommin,//"04"
                                    'Param9'=>  $txttohrs,//"8"
                                    'Param10'=> $txttomin,//"08"
                                    'Param11'=> '',
                                    'Param12'=> '',
                                 );
      $promocodedatetime = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodedate);
     if(!empty($promocodedatetime->promopackagedailyinventory_id)){
        //Check promocode count by based inventry id
             $promocode1 = array('act_mode'=>'checkpromocodecount_inventery',
                                    'Param1'=>'',
                                    'Param2'=>'',
                                    'Param3'=>$promocodedetail->promo_id,
                                    'Param4'=>'',
                                    'Param5'=>$promocodedatetime->promopackagedailyinventory_id,
                                    'Param6'=>'',
                                    'Param7'=>'',
                                    'Param8'=>'',
                                    'Param9'=>'',
                                    'Param10'=>'',
                                    'Param11'=>'',
                                    'Param12'=>'',
                                 );
      $getcountpromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocode1);
     if(($promocodedatetime->promopackage_timesallowed>$getcountpromocode->cntpromo) || ($promocodedatetime->promopackage_timesallowed==0)){
        //Check user promocode count by based inventry id
             $promocodeuser = array('act_mode'=>'checkpromocodecount_user_inventery',
                                    'Param1'=>'',
                                    'Param2'=>'',
                                    'Param3'=>$promocodedetail->promo_id,
                                    'Param4'=>$uid,
                                    'Param5'=>$promocodedatetime->promopackagedailyinventory_id,
                                    'Param6'=>'',
                                    'Param7'=>'',
                                    'Param8'=>'',
                                    'Param9'=>'',
                                    'Param10'=>'',
                                    'Param11'=>'',
                                    'Param12'=>'',
                                 );
      $getcountpromocodeuser = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodeuser);
        if(($promocodedatetime->promopackage_peruserallowed>$getcountpromocodeuser->cntpromouid) || ($promocodedatetime->promopackage_peruserallowed==0)){
 
         $getflatprice = $this->getpromocodepriceflat($totalprice,$promocodedetail->p_type,$promocodedetail->p_discount);
         $getpromocodeprice = $this->getpromocodepricecart($getflatprice,$cartaddoneiddata,$promocodedetail->p_addonedata);
          /* $promocodeuser = array('act_mode'=>'getaddonlist',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>$promocodedetail->p_addonedata,
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                 );
         $getcountpromocodeuser = $this->supper_admin->call_procedure('proc_promocode_s', $promocodeuser); 
           foreach($getcountpromocodeuser as $pvalue){
            $dataList[] = $pvalue->addon_name;
           }

            $addon = implode(',',$dataList);
            $discountype = $promocodedetail->p_type;
            $dicountvalue = $promocodedetail->p_discount;
            $getpromocodeprice = $getpromocodeprice;
            //echo '<p>Free: - </p>(<span>'.$addon.'</span>),'.$discountype.'-'.$dicountvalue;
           echo $getpromocodeprice;

           }*/
   
          // echo $getpromocodeprice;
           //saving price
            $savingpromoprice= $totalprice-$getpromocodeprice; 
              //Total Price
            $data = array(
            'total_promo_price' => $getpromocodeprice,
            'promo_id' =>$promocodedetail->promo_id,
            'saving_price_promocode' =>$savingpromoprice,
            'promocode_name' =>$promocodedetail->p_codename
            );

            $this->session->set_userdata($data);
            echo 1;
            }else{
             echo 0;
            }
          }else
          echo 0;
          }else{
          echo 0;
          }
         }else{
         echo 0;
         } }else{
         echo 0;
        } }else{
        echo 0;
       } 
     }


 
 public function getpromocodepriceflat($price,$discountype,$dicountvalue){
    if($discountype =='flat'){  
     $getpromoprice = $price-$dicountvalue;
     return $getpromoprice;
     }
     if($discountype =='percent'){
         $getpromoprice = $price%$dicountvalue;
       return $getpromoprice;
        
    }
 }
  
 public function getpromocodepricecart($flatprice,$cardid,$addonpromid){

        //Get addon price
    $getcardexpprice =explode(',', $cardid);
   $getpromoexpprice =explode(',', $addonpromid);
	 
       foreach($getpromoexpprice as $value){
		   
            if(in_array($value, $getcardexpprice)){
				
          $promocodeuser = array('act_mode'=>'getaddonlist',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>$value,
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                 );
         $getcountpromocodeuser = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodeuser); 
          $getaddonprices += $getcountpromocodeuser->addon_price;
				
            }
		   
         }
	 
         $getaddonprice  = ($getaddonprices) ? $getaddonprices : 0;
         $finalprice = $flatprice-$getaddonprices;
        return $finalprice;
 	}

	public function checkusertype($usertype,$promotype){
		if($promotype ==3 && $usertype =='Agent'){
		   return 1;
		  }
		 else{
			return 0;
		}

	 }

	public function partnerpaymentagent()
	{   
		// p( $this->session->userdata );
		
	   	if( empty( $this->session->userdata('final_selected_package_data') ) ){
            
			redirect(base_url('/agentpackagesstep'));
			
        }
		
      	if( empty( $this->session->userdata('user_id') ) ) {  
            
            redirect( 'partnerlogin' );
        }		
		
		$siteurl= base_url();
		$parameterbranch=array(
			'act_mode' =>'selectbranch',
			'weburl' =>$siteurl,
			'type'=>'web',

		);

		$path=api_url().'selectsiteurl/branch/format/json/';
		$data['branch']=curlpost($parameterbranch,$path);



		$parameterccgatway=array(
			'act_mode' =>'selectccavenue',
			'branchid' =>$data['branch']->branch_id,
			'type'=>'web',

		);

		$path=api_url().'ccavenue/ccavRequestHandler/format/json/';
		$data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

	
		$parameterbanner=array(
			'act_mode' =>'selectbannerimages',
			'branchid' =>$data['branch']->branch_id,
			'type'=>'web',
		);

		$path=api_url().'selectsiteurl/banner/format/json/';
		$data['banner']=curlpost($parameterbanner,$path);

		$parameter=array(
			'act_mode' =>'memusersesiid',
			'userid' =>$this->session->userdata['skiindia'],
			'type'=>'web',
		);
		$path=api_url().'userapi/usersesion/format/json/';
		$data['memuser']=curlpost($parameter,$path);

		$parametertimeslot=array(
			'act_mode' =>'selectsestimeslot',
			'branchid' =>$data['branch']->branch_id,
			'destinationType' =>$this->session->userdata('destinationType'),
			'type'=>'web',
		);

		$path=api_url().'selecttimesloturl/timeslotses/format/json/';
		$data['timeslotses']=curlpost($parametertimeslot,$path);
		
		$data['state'] = $state_array = [
			'Andhra Pradesh',
			'Arunachal Pradesh'	,
			'Assam' ,
			'Bihar'	,
			'Chhattisgarh' ,
			'Chandigarh',
			'Delhi',
			'Dadra and Nagar Haveli' ,
			'Daman and Diu' ,	
			'Goa', 
			'Gujarat'	,
			'Haryana'	,
			'Himachal Pradesh'	,
			'Jammu and Kashmir'	,
			'Jharkhand'	,
			'Karnataka'	,
			'Kerala'	,
			'Madhya Pradesh' ,
			'Maharashtra'	,
			'Manipur'	,
			'Meghalaya'	,
			'Mizoram'	,
			'Nagaland'	,
			'Odisha'	,
			'Punjab'	,
			'Rajasthan'	,
			'Sikkim'	,
			'Tamil Nadu' ,	
			'Telangana'	,
			'Tripura'	,
			'Uttar Pradesh'	 ,
			'Uttarakhand'	,
			'West Bengal' ,
			'Puducherry' ,
			'Lakshadweep'
		];		
		
		$data['country'] = $country_array = array("Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Barbuda", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Trty.", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Caicos Islands", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Futuna Islands", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard", "Herzegovina", "Holy See", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Jan Mayen Islands", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea", "Korea (Democratic)", "Kuwait", "Kyrgyzstan", "Lao", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "McDonald Islands", "Mexico", "Micronesia", "Miquelon", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "Nevis", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Principe", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre", "Saint Vincent", "Samoa", "San Marino", "Sao Tome", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia", "South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "The Grenadines", "Timor-Leste", "Tobago", "Togo", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Turks Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Minor Outlying Islands", "Uzbekistan", "Vanuatu", "Vatican City State", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (US)", "Wallis", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");		
/*
		if( !empty( $this->session->userdata('orderlastinsertid') ) ){

			$this->session->unset_userdata('orderlastinsertid');

		}		

		if( ! empty( $this->session->userdata( 'before_payment_agent_wallet_amount' ) ) ) {   

			$this->session->unset_userdata( 'before_payment_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_total_purchase_amount' ) ) ) {   

			$this->session->unset_userdata( 'before_payment_total_purchase_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) ) ) {   

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) ) ) {   

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ) ) {  
 
			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' );
		}
		
		if( ! empty( $this->session->userdata( 'payment_flag' ) ) ) {  

			$this->session->unset_userdata( 'payment_flag' );
		}		
		
	*/
		
#p( $this->session->userdata );		

		if( ! empty( $this->input->post('submit') ) && ( $this->input->post('submit') == 'cartses' ) ){  

			// get checkout form data 
			$checkout_form_data_array = $this->input->post();
			$checkout_form_data_array['cancel_url'] = 'http://192.168.1.65/snowworldnew/agentamountfail';
			$checkout_form_data_array['redirect_url'] = 'http://192.168.1.65/snowworldnew/agentamountsucess';

			$to_send_to_wallet = $agent_wallet_custom = $purchase_total_amount_custom = 0;
			
			$parameter = array('act_mode' => 'get_agent_wallet',
				'param'=> $this->session->userdata('user_id'),
				'param1'=>'web',
			);

			$path1 = api_url()."Cart/getagentwallet/format/json/";
			$get_agent_wallet_data = curlpost($parameter,$path1);
//P( $get_agent_wallet_data ) ; EXIT;
			$get_agent_wallet_data->total_amount ? $data['agent_wallet'] = $agent_wallet_custom = $get_agent_wallet_data->total_amount : '' ; 
			//$data['agent_wallet'] = $agent_wallet_custom = 0 ;
			
			// without promocode 
			//$data['purchase_total_amount'] = $purchase_total_amount_custom = $this->session->userdata('final_cost_package_addon_handling');
			//p($this->session->userdata);
			// with promocode check 
			
			// promo check here
			
			
			
			empty( $this->session->userdata('total_promo_price') ) ? $data['purchase_total_amount'] = $this->session->userdata('final_cost_package_addon_handling') : $data['purchase_total_amount'] = $this->session->userdata('total_promo_price');  
			
			    //$data['purchase_total_amount'] = 0 ;  
			
//p( $this->session->userdata) ;
			
// testing purpose 
//$data['agent_wallet'] = 3000;			 
			
// testing purpose 			
//$data['purchase_total_amount'] = 2000;			
//p( $data['purchase_total_amount'] );	
			//p( $this->session->userdata );
			
			// PURCHASE & WALLET DATA ADDED TO SESSION			
				
			$this->session->set_userdata( 'before_payment_agent_wallet_amount', $data['agent_wallet'] );
			$this->session->set_userdata( 'before_payment_total_purchase_amount', $data['purchase_total_amount'] );
			
		//	$this->session->set_userdata( $param );

			$payWithWallet = function( &$checkout_form_data_array, &$data, $payment_flag ){    
					
							$temp_agent_wallet = $to_pay_to_ccavenue_final_amount = $before_payment_after_wallet_deduction_agent_wallet_amount = 0;
							
							// only wallet
							if( array_sum( $payment_flag ) == 0 || array_sum( $payment_flag ) == 1 ){ 
								
								
									$checkout_form_data_array['before_payment_after_wallet_deduction_to_pay_ccavenue'] = $to_pay_to_ccavenue_final_amount = 0;
								
									$checkout_form_data_array['before_payment_after_wallet_deduction_to_pay_wallet'] = $data['purchase_total_amount'];
								
									# actual deduction from wallet start here
									$before_payment_after_wallet_deduction_agent_wallet_amount = abs( $data['agent_wallet'] - $data['purchase_total_amount'] ) ;
								
									$this->session->set_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount', $before_payment_after_wallet_deduction_agent_wallet_amount );								
								
									
							}
				
				
					// genertate client order end here
				
				
					// order email to client start here
					
					/*

					$this->load->library('session');
					$pdf_name = time() . "download.pdf";
					base_url("assets/admin/pdfstore/" . $pdf_name);
					$this->load->helper('file');
				
					$pdfFilePath = FCPATH . "assets/admin/pdfstore/" . $pdf_name;
					$this->load->library('m_pdf');

					$this->m_pdf->pdf->WriteHTML($message_pdf . $message_pdf1 . $message_pdf2);
					$a = $this->m_pdf->pdf->Output($pdfFilePath, 'F');


					//Load email library
					$this->load->library('email');
					$this->email->from($from_email, $data['banner']->bannerimage_top3);
					$this->email->reply_to($from_email, $data['banner']->bannerimage_top3);
					$this->email->to($to_email);
					$this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Booking Confirmation Voucher ');
					$this->email->message($mess);
					$this->email->attach(FCPATH . "assets/admin/pdfstore/" . $pdf_name);
					//Send mail
					
					$this->email->send();
				
					*/
							// order email to client end here
				
				
					// prepare user notification here 
				
				
							// prepare user notification here
				
				//	p( $this->session->userdata );
				


			};

			$payWithCcavenue = function( &$checkout_form_data_array, &$data, $payment_flag ){   
				
				
							// wallet + ccavenue
							if( array_sum( $payment_flag ) == 2 || array_sum( $payment_flag ) == 3 ){
								
								// before payment
								// check wallet
								// check to-purchase-amount
								// set payment key-values 
								// render wallet & payment summary
								// generate order 
								// redirect to payment gateway / wallet
								// return success 
							
								if( $data['agent_wallet'] ) {    
									
									$temp_agent_wallet = $data['agent_wallet'];

									$to_pay_to_ccavenue_final_amount = abs( $data['purchase_total_amount'] - $temp_agent_wallet ) ;	
									
									if( $to_pay_to_ccavenue_final_amount ) { 

										$checkout_form_data_array['before_payment_after_wallet_deduction_to_pay_ccavenue'] = abs( $to_pay_to_ccavenue_final_amount ) ;

										$checkout_form_data_array['before_payment_after_wallet_deduction_to_pay_wallet'] = abs( $temp_agent_wallet ) ;	

											$before_payment_after_wallet_deduction_agent_wallet_amount = ( $data['agent_wallet'] - $temp_agent_wallet ) ;

											$this->session->set_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount', $before_payment_after_wallet_deduction_agent_wallet_amount );								
										

									}else{
										
										//echo 'to_pay_to_ccavenue_final_amount' . $to_pay_to_ccavenue_final_amount ;
											
										// echo ' temp_agent_wallet' . $temp_agent_wallet ;										
										
										$this->session->set_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount', 0 );										
									}									
									
								}else{
									
									// echo 'agent_wallet' . $data['agent_wallet'] ;
									
									$checkout_form_data_array['before_payment_after_wallet_deduction_to_pay_ccavenue'] = 0;
									
									$checkout_form_data_array['before_payment_to_pay_ccavenue'] =  $data['purchase_total_amount'] ; 
									
									$this->session->set_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount', 0 );
									
								}
								
								
							}
				
				# save to session
                //$this->session->set_userdata( $param );
				
			};

          //  pend($data);
			// echo $data['agent_wallet'] .'--'. $data['purchase_total_amount'] ; exit;
			
			if( $data['purchase_total_amount'] ) {   
				 
				$payment_flag = [];
				
				# use wallet				
				if( $data['agent_wallet'] >= $data['purchase_total_amount'] ){  
					
					$checkout_form_data_array['amount'] = ( (float) $data['purchase_total_amount'] ) ;
					 
					$payment_flag[] = 0;
					
					$this->session->set_userdata('payment_flag', $payment_flag );
																		 
					log_message('DEBUG', 'amount is deducted with wallet' . date('d-m-Y H:i:s') );																		 
																		 
					$payWithWallet( $checkout_form_data_array, $data, $payment_flag );

				}else{    # use ccavenue payment gateway  
					//echo 88888888888888;
					  
					  if( $data['agent_wallet'] ){   // ONLY AMOUNT IS DEDUCTED HERE nothing else
						  
						  $payment_flag[] = 1;
						  
						  $this->session->set_userdata('payment_flag', $payment_flag );
						  
						  $payWithWallet( $checkout_form_data_array, $data, $payment_flag );
							// session data added twice		
							// $payWithWallet( $checkout_form_data_array );
						  
						 # log_message('DEBUG', 'amount is deducted with wallet' . date('d-m-Y H:i:s') );
					  }
					$payment_flag[] = 2;  
					$this->session->set_userdata('payment_flag', $payment_flag ); 
					  
						# log_message('DEBUG', 'amount is deducted with CCavenue' . date('d-m-Y H:i:s') );					  

					$payWithCcavenue( $checkout_form_data_array, $data, $payment_flag );
					
				}
				
				$this->session->set_userdata( $checkout_form_data_array );
				
				//redirect( 'agentwalletsummary' );   #agentwalletsummary
		
				?>
				
					<script>
						window.location = "agentwalletsummary";
					</script>
				
				<?php
				
			    exit ;
			
			}else{
				
				log_message('DEBUG', 'Some variable did not contain a value.');
			}
			
		}

		$this->load->view("agent/helper/header",$data);
		$this->load->view("agent/helper/leftbar",$data);
		$this->load->view("agent/partnerpaymentagent",$data);
		$this->load->view("agent/helper/footer");
	}
	
}//end of class
?>