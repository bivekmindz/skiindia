<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ccavenue extends MX_Controller {
	
	private $current_order_payment_flag = 0 ;
	
	private $current_order_status_flag = 0 ;
	
	private $credit_debit_flag = 'D' ;
	
	private $agent_wallet_last_insert_id = 0 ;
	
	
    public function __construct() {
		
        $this->load->model("supper_admin");
        $this->load->library('session');
         session_start();
    }
	
	

	public function sendEmail( $param = 0, $agent_order_last_insert_id = 0  ) {

		$total_array = [];
		
		$total = 0;		
		
		$data['orderlastinsertid'] = $agent_order_last_insert_id;
		
		
# generate email template start here 

		$siteurl= base_url();

		$parameterbranch=array(
			'act_mode' =>'selectbranch',
			'weburl' =>$siteurl,
			'type'=>'web',

		);

		$path=api_url().'selectsiteurl/branch/format/json/';

		$data['branch'] = $branch = curlpost( $parameterbranch, $path );	

//select banner images
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/banner/format/json/';
        $data['banner'] = curlpost($parameterbanner, $path);

		$orderdisplay = array('act_mode' => 'select_order',
		   'orderid' => $data['orderlastinsertid'],
			'type' => 'web', );
		$path = api_url() . "Ordersucess/selectorder/format/json/";

		$data['orderdisplaydataval'] = $orderdisplaydataval = curlpost($orderdisplay, $path);	
		

		$addon_total_price_with_quantity = $package_total_price = '';
		$package_name_array = $data['package_name_array'] = explode('#', $orderdisplaydataval->packproductname );
		$package_img_array = $data['package_img_array'] = explode('#', $orderdisplaydataval->packimg );
		$package_qty_array = $data['package_qty_array'] = explode('#', $orderdisplaydataval->package_qty );
		$package_price_array = $data['package_price_array'] =  explode('#', $orderdisplaydataval->package_price );

		$package_total_price = $total_array[] = getSumAllArrayElement( calculatePriceByQty( explode('#', $orderdisplaydataval->package_price ), explode('#', $orderdisplaydataval->package_qty ) ) );		
		
//p( $data['orderdisplaydataval'] );		
		
        $orderaddone = array('act_mode' => 'select_addone',
            'orderid' => $data['orderlastinsertid'],
            'type' => 'web'
        );
		
        $path2 = api_url() . "Ordersucess/selectaddone/format/json/";
		
        $data['orderaddonedisplaydata'] = $orderaddonedisplaydata = curlpost($orderaddone, $path2);
		
			$addon_data = getAddonTotalPricePrint( $orderaddonedisplaydata ) ;

			$addon_total_price_with_quantity = $total_array[] =  getSumAllArrayElement( calculatePriceByQty( $addon_data['addon_price_array'], $addon_data['addon_qty_array'] ) );

			$total_array[] = $orderdisplaydataval->internethandlingcharges;

		$data['total']	= $total = ( $orderdisplaydataval->promocodeprice ) ?   ( getSumAllArrayElement( $total_array ) - $orderdisplaydataval->promocodeprice )  :  getSumAllArrayElement( $total_array );		
		
		
        $parametertearms = array(
            'act_mode' => 'selecttearms',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/bannern/format/json/';
		
        $data['tearmsgatway'] = curlpost($parametertearms, $path);		
		
//p( $data['orderaddonedisplaydata'] ) ;		
		
		// if source is from db then following will work

		$created_date = $data['orderdisplaydataval']->addedon;
		
		$to_visit_date = $data['orderdisplaydataval']->departuredate;
		
        $data['d1'] = date(' jS F Y', strtotime( $created_date ) );

        $data['d2'] = date('h:i:s a', strtotime( $created_date ) );

        $data['d3'] = date("D", strtotime( $to_visit_date ) ) . "\n";

        $data['d4'] = date(' jS M Y', strtotime( $created_date ) );

 		$data['prepare_time_slot_from_date'] = $prepare_time_slot_from_date = $data['orderdisplaydataval']->departuretime .':'.$data['orderdisplaydataval']->frommin .' '.$data['orderdisplaydataval']->txtfromd .' - '.
            $data['orderdisplaydataval']->tohrs.':'.$data['orderdisplaydataval']->tomin.' '.$data['orderdisplaydataval']->txttod;

		# get formatted email template 
		
		$formatted_pdf_template = '';
		
		( $param ) ? $formatted_pdf_template = $this->load->view( '/agent/email_template/sucess_pdf_template' , $data, true ) : $formatted_pdf_template = '' ;
		
		
# generate email end here 
		
		
		
 
		
       //Load email library
		
		//p( $data['banner'] ); exit;
		/*
stdClass Object
(
    [bannerimage_id] => 1
    [bannerimage_top] => background.jpg
    [bannerimage_country] => 
    [bannerimage_state] => 
    [bannerimage_city] => 
    [bannerimage_location] => 
    [bannerimage_branch] => 1
    [bannerimage_logo] => 376e67e3adb2bd7ca6463fdf0c2f85d2.jpg
    [bannerimage_index] => 
    [bannerimage_top1] => Snow World Mumbai 
    [bannerimage_top2] => A Brand of Chiliad Procons Pvt. Ltd
    [bannerimage_top3] => Snow World
    [bannerimage_top4] => Phoenix Market City, Lower Ground Level 58 - 61, B
    [bannerimage_mailtype] => 
    [bannerimage_subject] => Snow World Mumbai
    [bannerimage_from] => customerservice@snowworldmumbai.com
    [bannerimage_branch_contact] => 022-6180 1591 / 92 / 93
    [bannerimage_branch_email] => info@snowworldmumbai.com
    [bannerimage_status] => 1
    [internethandlingcharge] => 30
    [bannerimage_apikey] => ff08d81897faf0d072cc62d5b81165f1
    [bannerimage_gstno] => GSTIN: 27AAFCC7259M1Z3
)
	*/
		
		
# generate PDF start here	
		
	if( $param ) {  // send pdf only when success 
		
       $pdf_name = time() . "download.pdf";
		
       base_url("assets/admin/pdfstore/" . $pdf_name);
		
       $this->load->helper('file');
		
       $pdfFilePath = FCPATH . "assets/admin/pdfstore/" . $pdf_name;

       $this->load->library('m_pdf');

       $this->m_pdf->pdf->WriteHTML( $formatted_pdf_template );
		
       $a = $this->m_pdf->pdf->Output( $pdfFilePath, 'F');
		
	}	
		
# generate PDF end here		
		
		
# trigger email start here
		
       $this->load->library('email');
		
/*		
		$config['protocol']='smtp';
		$config['smtp_host']='smtp.gmail.com';
		$config['smtp_port']='465';
		$config['smtp_timeout']='30';
		$config['smtp_user']='raj.gta92@gmail.com';
		$config['smtp_pass']='rajuprasadgupta92';
		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['wordwrap'] = TRUE;

		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);		

		*/
		

		
	$from = 'raj.gta92@gmail.com';
		
	$formatted_email_template = $custom_mail_subject = '';
		
       $this->email->from($from_email, $from);
		
       $this->email->reply_to($from_email, $from);
		
       // $this->email->reply_to($from_email, $data['banner']->bannerimage_top3);
       $this->email->to($from);
		
       $this->email->cc($from);
		
       // $this->email->to($from_email);
       $this->email->$data['banner']->bannerimage_apikey;
		
		( $param ) ? $custom_mail_subject = '' . $data['banner']->bannerimage_top3 . ' - Booking Confirmation Voucher' : $custom_mail_subject = '' . $data['banner']->bannerimage_top3 . ' - Your Request for Cancellation of Bookings' ;

       $this->email->subject( $custom_mail_subject );
		
		( $param ) ? $formatted_email_template = $this->load->view( '/agent/email_template/sucess_mail_template' , $data, true ) : $formatted_email_template = $this->load->view( '/agent/email_template/fail_mail_template' , $data, true ) ;		
//p( $formatted_email_template ); die;
       $this->email->message( $formatted_email_template );

       $this->email->attach(FCPATH . "assets/admin/pdfstore/" . $pdf_name);
		
       
		
		
        if ($data['orderdisplaydataval']->ordermailstatus == 0) {
			
				$from = $from_email;
				$fromname = $data['banner']->bannerimage_top3;
				$to = $to_email; //Recipients list (semicolon separated)
				$api_key = $data['banner']->bannerimage_apikey;
				$subject = ('' . $data['banner']->bannerimage_top3 . ' - Your Request for Cancellation of Bookings');
			
				$content = $formatted_email_template ;
			
			if( $param ) {
				
				$this->email->send();
				
			} else {

				$content =$mess;

				$mail1=array();
				$mail1['subject']= ($subject);
				$mail1['fromname']= ($fromname);
				$mail1['api_key'] = $api_key;
				$mail1['from'] = $from;
				$mail1['content']= ($content);
				$mail1['recipients']= $to;
				$apiresult = callApi(@$api_type,@$action,$mail1);


				$mail2 =array();
				$mail2['subject']= ($subject);
				$mail2['fromname']= ($fromname);
				$mail2['api_key'] = $api_key;
				$mail2['from'] = $to;
				$mail2['content']= ($content);
				$mail2['recipients']= $from;
				$apiresult2 = callApi(@$api_type,@$action,$mail2);
	//echo trim($apiresult);
				//$this->email->send();
				
			}
        }
		
		# save mail send history start here 
		# to send mail only once
		
		$ordermailupdate = array('act_mode' => 'orderpaymentupdatedata',
            'orderid' => $this->session->userdata['orderlastinsertid'],

            'type' => 'web',

        );

        $path = api_url() . "Ordersucess/selectorder/format/json/";
        $data['ordermaildataupdate'] = curlpost($ordermailupdate, $path);

		# save mail send history end here
		
		
		
		#echo $this->email->print_debugger();
		
# trigger email end here 
		
	
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function agentPayWallet( ) {
		
		
        if( !empty( $this->session->userdata('agent_order_last_insert_id') ) ){  //echo 1;
            
            $this->session->unset_userdata('agent_order_last_insert_id');
        }
		
		//p( $this->session->userdata );
		
/* agnet payemnt start here */
		
	   		$total_promo_price = '';
	   		
	   		$payment_flag = '' ;
	   		
	   		$to_pay_amount = 0 ;  #***
				
			$final_selected_package_data_array = getPackageTotalPrice( $this->session->userdata( 'final_selected_package_data' ) );
		
			$before_payment_after_wallet_deduction_agent_wallet_amount = $this->session->userdata('before_payment_after_wallet_deduction_agent_wallet_amount') ;
			
	   
	   			$total_promo_price	= $this->session->userdata( 'total_promo_price' ) ;
		// p( $this->session->userdata ) ; 
	   
	   			$user_id = $this->session->userdata( 'user_id' );
	   			
	   			
        		/* used only to pass data to request handler start here */
        		
        		$payment_flag = empty( $this->session->userdata( 'payment_flag' ) ) ? 0 :  array_sum( $this->session->userdata( 'payment_flag' ) ) ;
        
        		// wallet
        	  	if( ( $payment_flag == 0 ) ) {
        			
        			$to_pay_amount =  abs( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) );
        			
        		}	  
        	  
        	    // wallet + ccavenue
        	   	if( ( $payment_flag == 3 ) ) {  
        			
        			$to_pay_amount =  abs( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) );
        			
        		}
        		// ccavenue
        		if ( $payment_flag == 2 ) { 
        		
        			$to_pay_amount =  abs( $this->session->userdata( 'before_payment_to_pay_ccavenue' ) );
        			
        		}
        		
        		
        		/* used only to pass data to request handler end here */
		
		
		
		
       // $user_id = $this->session->userdata( 'ppid' );
				 $prepare_proc_param_package_addon_promocode = [

					    'param' => 'agent_payment' ,
						'param1' => $this->session->userdata( 'package_total_qty' ) ,
						'param2' => $this->session->userdata( 'package_total_price' ) ,
						'param3' => $this->session->userdata( 'addon_total_price' ),
						'param4' => $this->session->userdata( 'addon_total_qty' ),
						'param5' => implode( '#', $final_selected_package_data_array['package_name_array'] ) ,
						'param6' => implode( '#', $final_selected_package_data_array['get_package_img_array'] ) , 
						'param7' => implode( '#', $final_selected_package_data_array['package_price_array'] ) , 
						'param8' => empty( $this->session->userdata( 'total_promo_price' ) ) ? $this->session->userdata( 'before_payment_total_purchase_amount' ) : $this->session->userdata( 'total_promo_price' ),
						'param9' => '',
						'param10' => ! empty( $this->session->userdata( 'saving_price_promocode' ) ) ? $this->session->userdata( 'saving_price_promocode' ) : 0,
						'param11' => ! empty( $this->session->userdata( 'promocode_name' ) ) ? $this->session->userdata( 'promocode_name' ) : 0 ,    // 11
						'param12' => $this->session->userdata( 'handling_charge_with_no_of_person' ),
						'param13' => $this->session->userdata( 'final_cost_package_addon_handling' ),
						'param14' => ! empty( $this->session->userdata( 'totalvalue' ) ) ? 0 : 0 ,
						'param15' => ! empty( $this->session->userdata( 'orderlastinsertid' ) ) ? 0 : 0 
					 
				 		];

				$get_inventory_id = explode('-', $this->session->userdata( 'destinationType' ) ) [ 0 ];

				$prepare_proc_param_billingid_orderid_userid_urls = [

						'param16' => $this->session->userdata( 'titlewallet' ),
						'param17' => $this->session->userdata( 'billing_namewallet' ) ,
						'param18' => $this->session->userdata( 'billing_emailwallet' ) ,
						'param19' => $this->session->userdata( 'billing_telwallet' ) ,
						'param20' => $this->session->userdata( 'billing_cometoknow' ) ,
						'param21' => $this->session->userdata( 'billing_addresswallet' ) ,
						'param22' => $this->session->userdata( 'billing_citywallet' ) ,
						'param23' => $this->session->userdata( 'billing_statewallet' ) ,
						'param24' => $this->session->userdata( 'billing_zipwallet' ) ,
						'param25' => $this->session->userdata( 'billing_countrywallet' ) ,
						'param26' => $this->session->userdata( 'CaptchaInputwallet' ) ,
						'param27' => $this->session->userdata( 'txtDepartDate' ) ,
						'param28' => ! empty( $this->session->userdata( 'tid' ) ) ? $this->session->userdata( 'tid' ) : 0 ,
						'param29' => ! empty( $this->session->userdata( 'merchant_id' ) ) ? $this->session->userdata( 'merchant_id' ) : 0 ,
						'param30' => ! empty( $this->session->userdata( 'order_id' ) ) ? 0 : 0 ,
						'param31' => ! empty( $this->session->userdata( 'order_idval' ) ) ? 0 : 0 ,
						'param32' => '' ,
						'param33' => $this->session->userdata( 'amount' ) ,
						'param34' => ! empty( $this->session->userdata( 'currency' ) ) ? $this->session->userdata( 'currency' ) : 0,
						'param35' => ( $get_inventory_id ) ? $get_inventory_id : 0 ,
						'param36' => $this->session->userdata( 'cancel_url' )
						];				
				//p( $prepare_proc_param_billingid_orderid_userid_urls );
					$time_slot_array = getTimeSlotInArray( $this->session->userdata( 'destinationType' ) );
				
					$final_selected_addon_data_array = getAddonkeyValue( $this->session->userdata( 'final_selected_addon_data' ) );
				    
					$payment_flag = empty( $this->session->userdata( 'payment_flag' ) ) ? 0 :  array_sum( $this->session->userdata( 'payment_flag' ) ) ;
		
	   // 0 =  wallet
	   // 2 = ccavenue 
	   // 3	= wallet + ccavenue
	   				$payment_mode = ( $payment_flag == 0 ) ? 'Wallet' : 'NULL';
	   				$payment_mode = ( $payment_flag == 2 ) ? 'Ccavenue' : 'NULL';		
	   				$payment_mode = ( $payment_flag == 3 ) ? 'Wallet & Ccavenue' : 'NULL';

	   
	  				$get_formatted_hr_min_am_pm_array =	explode('-', completeTimeSlotWithAmPmAgent( getTimeSlotInArray( $this->session->userdata( 'destinationType' ) ) ) );
		
	  				$get_formatted_timeslot_start =	explode(':', $get_formatted_hr_min_am_pm_array[ 0 ] );
	   				$get_formatted_timeslot_end =	explode(':', $get_formatted_hr_min_am_pm_array[ 1 ] );
		
	   		#before_payment_after_wallet_deduction_to_pay_ccavenue
					$prepare_proc_param_future_param = [ 
						'param37' => $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ),    // wallet amount
						'param38' => $this->session->userdata( 'final_cost_package_addon_handling' ),      // subtotal
						'param39' => $this->session->userdata( 'countryid' ),
						'param40' => $this->session->userdata( 'stateid' ),
						'param41' => $this->session->userdata( 'cityid' ),
						'param42' => $this->session->userdata( 'branch_id' ),
						'param43' => $this->session->userdata( 'locationid' ),
						'param44' => $this->session->userdata( 'uniqid' ),
						
						'param45' => trim( $get_formatted_timeslot_start[ 0 ] ),
						
						'param46' => trim( $get_formatted_timeslot_start[ 1 ] ),
						
						'param47' => trim( $get_formatted_timeslot_end[ 0 ] ),
						'param48' => trim( $get_formatted_timeslot_end[ 1 ] ),
						'param49' => trim( $get_formatted_timeslot_start[ 2 ] ),
						'param50' => trim( $get_formatted_timeslot_end[ 2 ] ),
						'param51' => $this->session->userdata( 'ddAdult' ),
						// used later 
						'param52' => ( ( $payment_flag == 0 ) || ( $payment_flag == 2 ) || ( $payment_flag == 3 ) ) ? 1 : 0 ,
						'param53' => $payment_mode ,
						'param54' => '' ,
						'param55' => implode( '#', $final_selected_package_data_array['package_qty_array'] ) ,
						'param56' => '' ,
						'param57' => '' ,
						
						'param58' => uniqid() ,
						'param59' => 1 ,
						'param60' => 'paid successfully' ,
						'param61' => '' ,
						'param62' => '' ,
						'param63' => 'wallet',
						'param64' => empty( $this->session->userdata( 'total_promo_price' ) ) ? $this->session->userdata( 'before_payment_total_purchase_amount' ) : $this->session->userdata( 'total_promo_price' ) ,
						'param65' => $user_id 
						];
	   
					$agent_payment_response = $agent_agentcustomer_inserted_id = '';
	   
					$agent_customer_response_object = $agent_payment_response = $agent_agentcustomer_inserted_id = $agent_commission_inserted_id = '';	
	   
					$agent_payment_param  = array_merge( $prepare_proc_param_package_addon_promocode, $prepare_proc_param_billingid_orderid_userid_urls, $prepare_proc_param_future_param ) ;
			
	   // p( $agent_payment_param ); 
	   // p( $this->session->userdata );
	   // exit;
		
		//		try{			
					
//					$this->db->trans_begin();	
		
				   if( $user_id ){
					
					  /* insert agent customer details  */

					   		$agent_payment_param[ 'param' ] = 'insert_agent_customer_details' ;
														
							$agent_customer_response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );
					   
							$agent_agentcustomer_inserted_id = (int) $agent_customer_response_object->lastinsert_agentcustomer_id;

				   }	   
	   
	   				/*  this param32 is acutal agent customer id */
	   
		/*
						switch( $paymentmode ){
								
							case 1 :	
								
							break;	
								
							case 2 :
								
							break;	
								
							case 3 :									
								
							break;	
						
						}		
						
		*/
	   				
	   
	   				if( $agent_agentcustomer_inserted_id ) {
						
	   						$agent_payment_param[ 'param' ] = 'agent_payment';
						
			   				$agent_payment_param[ 'param64' ] = (int) empty( $agent_agentcustomer_inserted_id ) ? 0 : $agent_agentcustomer_inserted_id ;
						
//			   				$agent_payment_param[ 'param64' ] = (int) empty( $agent_agentcustomer_inserted_id ) ? 0 : $agent_agentcustomer_inserted_id ;
					
							 /*  insert - tbl_orderpackage  order details saved  */	   
							$agent_payment_response = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );				
					}
	   
					// hold addon last insert ids 
					$agent_payment_addon_inserted_id = [];
		
					$agent_payment_addon_response_object = '';
		
	   				$order_status = 0;
		
	   				$agent_payment_param[ 'param64' ] = $agent_payment_param[ 'param8' ];
				
					if( isset( $agent_payment_response->lastinsert_id ) ) {
						
						// set order status flag ( 1 ( success ), 0 ( failed ) )  
						
						$this->session->set_userdata( 'agent_order_last_insert_id', $order_status = $agent_payment_response->lastinsert_id );
						
						foreach( $final_selected_addon_data_array['addon_name_array'] as $kk => $vv ) {

							$agent_payment_param[ 'param' ] = 'insert_addon' ;
							$agent_payment_param[ 'param52' ] = $final_selected_addon_data_array['addon_name_array'][ $kk ] ; 
							$agent_payment_param[ 'param32' ] = $agent_commission_inserted_id ; 
							$agent_payment_param[ 'param53' ] = $final_selected_addon_data_array['addon_qty_array'][ $kk ] ; 
							$agent_payment_param[ 'param54' ] = $final_selected_addon_data_array['addon_price_array'][ $kk ] ;
							$agent_payment_param[ 'param55' ] = $final_selected_addon_data_array['addon_image_array'][ $kk ] ;
							$agent_payment_param[ 'param56' ] = $final_selected_addon_data_array['addon_id_array'][ $kk ] ;
							$agent_payment_param[ 'param57' ] = ( $agent_payment_response->lastinsert_id ) ? $agent_payment_response->lastinsert_id : 0 ;
							  
							$agent_payment_addon_response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );
							
							$agent_payment_addon_inserted_id[] = $agent_payment_addon_response_object->lastinsert_addon_id;
						}
						
					}
	   
					// hold last insert id
   				    $agent_payment_promo_inserted_id = $response_object = '';
	   				
				  /*
				   * if promo code is applied
				   * insert promo code details 
				   */		
		
				   if( $total_promo_price ){
					

					   		$agent_payment_param[ 'param' ] = 'insert_promo_data' ;
					        $agent_payment_param[ 'param56' ] = $this->session->userdata( 'promo_id' ) ;
					   
							$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );
							$agent_payment_promo_inserted_id = $response_object->lastinsert_userpromo_id;					   
				   	}
					
//					$this->db->trans_commit();
		
		//		}catch (Exception $e) {
					
//					$this->db->trans_rollback();
					
		//		}
									
				  /*
				   * insert agent agent commission details 
				   * not in used 
				   */
		
			 		$insert_agentcommission_data = function ( $agent_payment_param ) use ( &$agent_commission_inserted_id, $order_status ) {   # NOT IN USED 
						
								$agent_payment_param[ 'param' ] = 'insert_agentcommission_details' ;
						
						   		$agent_payment_param[ 'param32' ] = $order_status ;	
						
								$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );

								$agent_commission_inserted_id = $response_object->lastinsert_agentcommission_id;						
					
					};
		
		
		
	    // unset all user data start here


        if( !empty( $this->session->userdata('agent_order_last_insert_id') ) ){  //echo 1;
            
            $this->session->unset_userdata('agent_order_last_insert_id');
        }
		
		
        if( !empty( $this->session->userdata('payment_flag') ) ){  //echo 1;
            
            $this->session->unset_userdata('payment_flag');
        }		
		
        if( !empty( $this->session->userdata('package_total_qty') ) ){  //echo 1;
            
            $this->session->unset_userdata('package_total_qty');
        }
		
        if( !empty( $this->session->userdata('package_total_price') ) ){  //echo 1;
            
            $this->session->unset_userdata('package_total_price');
        }		
        
        if( !empty( $this->session->userdata('final_selected_package_data') ) ){  //echo 1;
            
            $this->session->unset_userdata('final_selected_package_data');
        }   
		   
		if( !empty( $this->session->userdata('final_selected_addon_data') ) ){   //echo 2;

		   $this->session->unset_userdata('final_selected_addon_data');
		}
		
		if( ! empty( $this->session->userdata( 'addon_total_price' ) ) ) {

			$this->session->unset_userdata( 'addon_total_price' );
		}						

		if( ! empty( $this->session->userdata( 'addon_total_qty' ) ) ) {

			$this->session->unset_userdata( 'addon_total_qty' );
		}		
		
		if( !empty( $this->session->userdata('orderlastinsertid') ) ){

			$this->session->unset_userdata('orderlastinsertid');

		}		

		if( ! empty( $this->session->userdata( 'before_payment_agent_wallet_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_total_purchase_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_total_purchase_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'payment_flag' ) ) ) {

			$this->session->unset_userdata( 'payment_flag' );
		}
		
		// promo variable unset start here 
		if( ! empty( $this->session->userdata( 'total_promo_price' ) ) ) {

			$this->session->unset_userdata( 'total_promo_price' );
		}	

		if( ! empty( $this->session->userdata( 'promo_id' ) ) ) {

			$this->session->unset_userdata( 'promo_id' );
		}			

		if( ! empty( $this->session->userdata( 'saving_price_promocode' ) ) ) {

			$this->session->unset_userdata( 'saving_price_promocode' );
		}				

		if( ! empty( $this->session->userdata( 'promocode_name' ) ) ) {

			$this->session->unset_userdata( 'promocode_name' );
		}				

		if( ! empty( $this->session->userdata( 'handling_charge_with_no_of_person' ) ) ) {

			$this->session->unset_userdata( 'handling_charge_with_no_of_person' );
		}						

		if( ! empty( $this->session->userdata( 'final_cost_package_addon_handling' ) ) ) {

			$this->session->unset_userdata( 'final_cost_package_addon_handling' );
		}
		
		
		if( ! empty( $this->session->userdata( 'uniqid' ) ) ) {

			$this->session->unset_userdata( 'uniqid' );
		}		
		
		if( ! empty( $this->session->userdata( 'txtDepartDate' ) ) ) {

			$this->session->unset_userdata( 'txtDepartDate' );
		}				
		
		if( ! empty( $this->session->userdata( 'destinationType' ) ) ) {

			$this->session->unset_userdata( 'destinationType' );
		}						
		
		if( ! empty( $this->session->userdata( 'ddAdult' ) ) ) {

			$this->session->unset_userdata( 'ddAdult' );
		}		
		
		if( ! empty( $this->session->userdata( 'select_time_slot' ) ) ) {

			$this->session->unset_userdata( 'select_time_slot' );
			$this->session->unset_userdata( 'select_time_slot_id' );
		}				
				
		
		
	    // unset all user data end here					
		
		//echo $payment_flag . '---' . $order_status . ' --- ' . $before_payment_after_wallet_deduction_agent_wallet_amount ;
		//exit;
		
		
					// wallet redirect
		
					if( ( $payment_flag == 0 ) ) {
						
						if( $order_status ) { 
							
							$this->ccavResponseHandler( $payment_flag, $order_status, $before_payment_after_wallet_deduction_agent_wallet_amount );   // to keep
							exit;
							
						} else {
							
							//redirect('/agentfail') ;
							$this->ccavResponseHandler( $payment_flag, $order_status, $before_payment_after_wallet_deduction_agent_wallet_amount );
							exit;
						}

					}	  

					// wallet + ccavenue redirect
					if ( $payment_flag == 3 ) {
					    

$before_payment_after_wallet_deduction_agent_wallet_amount = $to_pay_amount;					    
						
						$this->ccavRequestHandler( $payment_flag, $order_status, $before_payment_after_wallet_deduction_agent_wallet_amount, $agent_payment_param );						  // to keep
						exit;
						
					}
		
					// ccavenue redirect
					if ( $payment_flag == 2 ) { 
						
$before_payment_after_wallet_deduction_agent_wallet_amount = $to_pay_amount;
					    
						$this->ccavRequestHandler( $payment_flag, $order_status, $before_payment_after_wallet_deduction_agent_wallet_amount, $agent_payment_param );    // to keep
						exit;

					} 		

	 
	/* agent end here	     */
		
	
	}

    public function ccavResponseHandler( $payment_flag = 0, $agent_order_last_insert_id = 0, $agent_wallet_amount = 0 ){
		
		
		
      /*
	   * agent order payment response
	   * 	success handler for wallet payment (1) 
	   *    success handler for ccavenue payment (2)
	   *    success handler for wallet + ccavenue payment (3)	    
	   */
		
		
	   /*
		* get following param from ccavenue 
		* agent_order_last_insert_id  
		* before_payment_after_wallet_deduction_agent_wallet_amount
		*
		*/
		
		
		
   		$total_promo_price = '';
	
		$final_prepare_data_array = [] ;   // tmp

		$final_selected_package_data_array = [] ;

		$agentamount_add_new_debit_array = [] ;
		
		$agent_order_last_insert_id_array = $payment_flag_array = [] ;
		
		$arr1 = $arr2 = $arr3 = $arr4 = $arr5 = $arr6 = [] ; 
		
			   $paymentmode = '' ;
	
			   $session_to_pay_wallet = $total_promo_price = $user_id = '' ;

			   $session_to_pay_wallet = $session_agent_wallet_amount = $session_to_pay_ccavenue =   '' ;
			   
				//$session_before_payment_to_pay_ccavenue = 0 ;
	
				$before_payment_to_pay_ccavenue = 0 ;
				
					
				$before_payment_total_purchase_amount = $final_payment_amount = 0 ;
				$before_payment_after_wallet_deduction_agent_wallet_amount = 0 ;
	
			   //$orderdata = explode( $data['ccavRequestHandler']->pg_agent_prefix.'_', $arr5[1] );

			   $payment_flag_int = 0 ; 

			   $agent_id_wallet = $user_id = 0 ;		
		
		
		
		
		
		# get ccavenue response only if payment status is 2 (ccavenue) || 3 ( wallet + ccavenue)
		
		if ( ! empty( $this->input->post( 'encResp' ) ) ) {
	   
		$siteurl= base_url();
		
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


        $parameterccgatway = array(
            'act_mode' => 'selectccavenue',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
		
		#  get working key 
        $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);   
		
		
	   
	   /*  $data['ccavRequestHandler']
	   
			stdClass Object
			(
				[pg_id] => 2
				[pg_branchid] => 1
				[pg_merchant_id] => 49646
				[pg_access_key] => AVUS71EG70AF13SUFA
				[pg_working_key] => B01CEF5819806E9315FF0CADC5D94051
				[pg_created] => 2017-07-03 04:46:02
				[pg_updated] => 2018-01-04 15:56:35
				[pg_status] => 1
				[pg_sucess_link] => http://192.168.1.65/snowworldnew/ordersucess
				[pg_fail_link] => http://192.168.1.65/snowworldnew/orderfail
				[pg_currency] => INR
				[pg_language] => EN
				[pg_prefix] => Snow
				[pg_agent_prefix] => Snowagentorder
				[pg_order_sucess_url] => http://192.168.1.65/snowworldnew/agentsucess
				[pg_order_fail_url] => http://192.168.1.65/snowworldnew/fail
				[pg_wallet_sucess_url] => http://192.168.1.65/snowworldnew/agentwalletsucess
				[pg_wallet_fail_url] => http://192.168.1.65/snowworldnew/agentwalletfail
				[pg_prefix_agent_wallet] => Snowagentwallet
			)
	*/
	   
        $workingKey = $data['ccavRequestHandler']->pg_working_key;
	   
        $encResponse = $_POST["encResp"];
	   
        /*
		* pend($encResponse);//This is the response sent by the CCAvenue Server
		* decrupt response and get data, flag etc.
		*/
	   
        $rcvdString = decrypt($encResponse, $workingKey);        //Crypto Decryption used as per the specified working key.
	   
	   
        log_message( 'payment-response', json_encode( $rcvdString ) ) ;	   
	   
        $order_status = "";
	   
        $decryptValues = explode('&', $rcvdString);
        
        
        
		
			/*
		$decryptValues = array(
			0 => 'order_id=Snow_11',
			1 => 'tracking_id=107321264680',
			2 => 'bank_ref_no=null',
			3 => 'order_status=Aborted',
			4 => 'failure_message=',
			5 => 'payment_mode=null',
			6 => 'card_name=null',
			7 => 'status_code=',
			8 => 'status_message=I have second thoughts about making this payment',
			9 => 'currency=INR',
			10 => 'amount=610.0',
			11 => 'billing_name=bivke',
			12 => 'billing_address=sdfsdf',
			13 => 'billing_city=delhi',
			14 => 'billing_state=delhi',
			15 => 'billing_zip=110014',
			16 => 'billing_country=India',
			17 => 'billing_tel=9810668829',
			18 => 'billing_email=bivek11@gmail.com',
			19 => 'delivery_name=',
			20 => 'delivery_address=',
			21 => 'delivery_city=',
			22 => 'delivery_state=',
			23 => 'delivery_zip=',
			24 => 'delivery_country=',
			25 => 'delivery_tel=',
			26 => 'merchant_param1=',
			27 => 'merchant_param2=',
			28 => 'merchant_param3=',
			29 => 'merchant_param4=',
			30 => 'merchant_param5=',
			31 => 'vault=N',
			32 => 'offer_type=null',
			33 => 'offer_code=null',
			34 => 'discount_value=0.0',
			35 => 'mer_amount=610.0',
			36 => 'eci_value=',
			37 => 'retry=null',
			38 => 'response_code=',
			39 => 'billing_notes=',
			40 => 'trans_date=null',
			41 => 'bin_country='
		);	
			
			*/
	   
        $dataSize = sizeof($decryptValues);
	   
        for ($i = 0; $i < $dataSize; $i++) {
			
            $information = explode('=', $decryptValues[$i]);
			
            if ($i == 3) $order_status = $information[1];
        }
        
$tmp_param = [];

		//p( $decryptValues ); exit;
		
			$arr1 = explode("=", $decryptValues[1]);
			$arr2 = explode("=", $decryptValues[3]);
			$arr3 = explode("=", $decryptValues[8]);
			$arr4 = explode("=", $decryptValues[5]);
			$arr5 = explode("=", $decryptValues[0]);
			$arr6 = explode("=", $decryptValues[35]);       
			$arr7 = explode("=", $decryptValues[18]);
			
			$payment_flag_array = explode("=", $decryptValues[26]);
			
			$payment_flag = $payment_flag_array[ 1 ] ;
			
		    $agent_order_last_insert_id_array = explode("=", $decryptValues[27]);
			
			$agent_order_last_insert_id = $agent_order_last_insert_id_array[ 1 ] ;
			
// p( $decryptValues ) ;



		//echo '<pre>';
		//var_dump ( $arr1, $arr2, $arr3, $arr4, $arr5, $arr6, $arr7 );
		
							isset( $arr1[1] ) ? $tmp_param['tracking_id'] = $arr1[1] : '' ;    // two in one  // get from ccavenue 
							
							isset( $arr2[1] ) ? $tmp_param['order_status'] = $arr2[1] : '' ;   // two in one  # current order  0 = failed,1 = success, 2 = pending
							
							isset( $arr3[1] ) ? $tmp_param['status_message'] = $arr3[1] : '' ; // two in one  // current order message
							
							isset( $arr4[1] ) ? $tmp_param['paymentmode'] = $arr4[1] : '' ; // two in one  // current order payment mode
							
							isset( $arr6[1] ) ? $tmp_param['agent_amount'] = $arr6[1] : '' ;	//  // current order amount	 		
		
         $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/banner/format/json/';
		
        $data['banner'] = curlpost($parameterbanner, $path);

			
		}
		

				// ASSIGN DATA TO RESPECTIVE VAR

				$payment_flag_int = $payment_flag ;		
		
				( $payment_flag_int == 0 ) ? $data['orderlastinsertid'] = $agent_order_last_insert_id : $data['orderlastinsertid'] = $agent_order_last_insert_id ;	 // get from cc response param 
		
		
				$orderdisplay = array(
					'act_mode' => 'select_order',
				   	'orderid' => $data['orderlastinsertid'],
					'type' => 'web'
					);
		
				$path = api_url() . "Ordersucess/selectorder/format/json/";

				$data['orderdisplaydataval'] = curlpost($orderdisplay, $path);		
		
				
					//	$final_selected_package_data_array = getPackageTotalPrice( $this->session->userdata( 'final_selected_package_data' ) );

						$total_promo_price	= $data['orderdisplaydataval']->promocodeprice ;

						$user_id = $this->session->userdata( 'user_id' );

						//p( $this->session->userdata );

						//$session_to_pay_wallet = $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' );
						$session_to_pay_wallet = $data['orderdisplaydataval']->waletAmount ;

						//$session_agent_wallet_amount = $this->session->userdata('before_payment_after_wallet_deduction_agent_wallet_amount') ;  // get from cc response param
						( $payment_flag_int == 0 ) ? $session_agent_wallet_amount = $before_payment_after_wallet_deduction_agent_wallet_amount : $session_agent_wallet_amount = $before_payment_after_wallet_deduction_agent_wallet_amount ;    // get from cc response param

						//$session_to_pay_ccavenue = $this->session->userdata('before_payment_after_wallet_deduction_to_pay_ccavenue') ;   // get from cc response param
						$session_to_pay_ccavenue = ( int ) ( $data['orderdisplaydataval']->total - $data['orderdisplaydataval']->waletAmount  ) ;   // get from cc response param

						//$session_before_payment_to_pay_ccavenue = $session_to_pay_ccavenue ;  
		
    					$before_payment_total_purchase_amount = $data['orderdisplaydataval']->total ; 
		 
 						$final_payment_amount = empty( $total_promo_price ) ? $before_payment_total_purchase_amount : $total_promo_price ;		
		
				//$agent_order_last_insert_id = $this->session->userdata('agent_order_last_insert_id');

		
		// check order status here

					switch ( $payment_flag_int ) {

						#  wallet 				
						case 0 : 
							//	echo $payment_flag_int;
							$this->current_order_status_flag = 1;	

						break;

						#  ccavenue 
						case 2 :  
							
							$this->current_order_status_flag = in_array( $arr2[1] , ['Success'] ) ;				 	
							
						break;							

						#  wallet + ccavenue	
						case 3 :
							
							//$ccavenue_status_type_array = ['Aborted', 'Failed', 'Succes'];
							
							$this->current_order_status_flag = in_array( $arr2[1] , ['Success'] ) ; 

						break;				

						}		
		
		// CRUD operation based on status is perform
		//echo $this->current_order_status_flag ;
		
		if( $this->current_order_status_flag ) {
			
		
     		//p($decryptValues);
            //agentamount_add_new_credit
            //agentamount_add_new_debit

       /* agnet Mail start here and order update and  */
		
       

//p(  $data['orderdisplaydataval'] ) ;  
/*		
stdClass Object
(
    [pacorderid] => 29
    [userid] => 28
    [billing_by] => 7
    [promocodeprice] => 0
    [total] => 1660
    [waletAmount] => 1660
    [subtotal] => 1660
    [addedon] => 2018-01-11 11:45:25
    [countryid] => 1
    [stateid] => 1
    [cityid] => 1
    [branch_id] => 1
    [locationid] => 1
    [departuredate] => 12-01-2018
    [paymentstatus] => 1
    [paymentmode] => 0
    [ticketid] => 5a5700fdaef69
    [tracking_id] => NULL
    [packproductname] => Regular#Combo
    [packimg] => 1c73516272f31851c4b2e9c2a09f7957.jpg#af264b35318be1024a8eec44cad64b0d.jpg
    [package_qty] => 1#1
    [package_price] => 500#1000
    [packpkg] => 2
    [packprice] => 1500
    [orderstatus] => 1
    [op_ticket_print_status] => 0
    [op_printed_date] => 0000-00-00 00:00:00
    [internethandlingcharges] => 50
    [op_usertype] => Partner
    [order_status] => NULL
    [status_message] => NULL
    [paymenttype] => NULL
    [title] => Mr
    [billing_name] => Raju Gupta
    [billing_email] => raju@gmail.com
    [billing_tel] => 9810668829
    [billing_address] => MANTRIPUKHEI,NEAR RPF CAMP,IMPHAL
    [billing_city] => IMPHAL
    [billing_state] => MANIPUR
    [billing_zip] => 897878
    [billing_country] => India
    [billing_pdf] => 1515653763download.pdf
    [ordersucesmail] => 0
    [ordermailstatus] => 0
    [departuretime] => 07
    [frommin] => 30
    [tohrs] => 08
    [tomin] => 30
    [txtfromd] => PM
    [txttod] => PM
    [p_codename] => 0
    [op_ordertrackingupdate] => 0
    [order_status_date_time] => 2018-01-11 11:45:25
    [op_orderjason] => 0
    [op_billing_cometoknow] => DD
    [billing_promocode] => 
    [order_statuspayment] => 
)
	*/
		
      // $arr = (array)$data['orderdisplaydataval'];

    //   pend($this->session->userdata('agent_order_last_insert_id'));

/* agnet payemnt start here */
			    

				$agentamount_add_new_debit_array = [
										'agent_id' => $user_id ,
										'to_pay_wallet' => $session_to_pay_wallet ,
										'credit_debit_flag' => $this->credit_debit_flag , 
										 ] ; 		
				
	   			// empty array required as per procedure
				 $prepare_proc_param_package_addon_promocode = [

					    'param' => '' ,
						'param1' => '' ,
						'param2' => '' ,
						'param3' => '' ,
						'param4' => '' ,
						'param5' => '' ,
						'param6' => '' , 
						'param7' => '' , 
						'param8' => $before_payment_total_purchase_amount ,
						'param9' => '' ,
						'param10' => '' ,
						'param11' => '' ,    
						'param12' => '' ,
						'param13' => '' ,
						'param14' => '' ,
						'param15' => ''  
				 		];

	   			// empty array required as per procedure	   
				$prepare_proc_param_billingid_orderid_userid_urls = [

						'param16' => '' ,
						'param17' => '' ,
						'param18' => '' ,
						'param19' => '' ,
						'param20' => '' ,
						'param21' => '' ,
						'param22' => '' ,
						'param23' => '' ,
						'param24' => '' ,
						'param25' => '' ,
						'param26' => '' ,
						'param27' => '' ,
						'param28' => '' ,
						'param29' => '' ,
						'param30' => '' ,
						'param31' => '' ,
						'param32' => '' ,
						'param33' => '' ,
						'param34' => '' ,
						'param35' => '' ,
						'param36' => '' 
						];			
	   
	   			// empty array required as per procedure	   
					$prepare_proc_param_future_param = [ 
						'param37' => '',    // wallet amount
						'param38' => '',      // subtotal
						'param39' => '',
						'param40' => '',
						
						'param41' => '',
						'param42' => '' ,
						'param43' => '' ,
						'param44' => '' ,
						'param45' => '' ,
						'param46' => '' ,
						'param47' => '' ,
						'param48' => '' ,
						'param49' => '' ,
						'param50' => '' ,
						
						'param51' => '' ,
						// used later 
						'param52' => '' ,
						'param53' => '' ,
						'param54' => '' ,
						'param55' => '' ,
						'param56' => '' ,
						'param57' => '' ,
						'param58' => '' ,
						'param59' => '' ,
						'param60' => '' ,
						
						'param61' => '' ,
						'param62' => '' ,
						'param63' => 'wallet',
						'param64' => $before_payment_total_purchase_amount ,
						'param65' => $this->session->userdata( 'user_id' ) 
						];
	   
	
	
	
	
	
	
	
	
					$response_object = $agent_commission_inserted_id = '';	
	   
					$agent_payment_param  = array_merge( $prepare_proc_param_package_addon_promocode, $prepare_proc_param_billingid_orderid_userid_urls, $prepare_proc_param_future_param ) ;
	   
//					   if( $user_id ) {

						  /*
						   * insert agent commission details 
						   */
								$agent_payment_param[ 'param' ] = 'insert_agentcommission_details' ;
						   		$agent_payment_param[ 'param32' ] = ( $agent_order_last_insert_id ) ? $agent_order_last_insert_id : 0 ;
								//$agent_payment_param[ 'param8' ] = $before_payment_total_purchase_amount ;	
						
								$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );

								$agent_commission_inserted_id = $response_object->lastinsert_agentcommission_id;					   
						   
//					   	}	   
	   
		# agent end here
			
			
		$update_order_status = function( &$agent_payment_param ) {

					$data['orderdisplaydata'] = $response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );
		
		} ;	
			
			
				   
		#  only set order payment status ( 0 / 1 )
					
		$set_order_payment_status = $prepare_log_msg = '' ;
		
			$set_order_payment_status = function( $response ) {

				 if( isset( $response->last_id ) && ( $response->last_id ) ) {

					 $this->current_order_payment_flag = 1 ;   // ***

					 $this->agent_wallet_last_insert_id = $response->last_id ;

				 }else{

					 $prepare_log_msg = ['log_created_date' => date('d-m-Y-h:i:s') ]; 

					 $prepare_log_msg = array_merge( $prepare_log_msg,  [] ); 

					 log_message( 'info', json_encode( $prepare_log_msg ) ) ;

				 }				

			};
		
		$get_ccavenue_response_data = '' ;
		
		   $get_ccavenue_response_data = function( $payment_flag_int ) use ( $arr1, $arr2, $arr3, $arr4, $arr5, $arr6, $arr7, $user_id ) {
			   
					$tmp_param = array(
								'tracking_id' => '' ,
								'order_status' => '' ,
								'status_message' => '' ,
								'paymentmode' => '' ,
								'agent_amount' => '' ,
								'agent_id' => '' ,
								'agent_wallet_last_insert_id' => '' ,
								'session_to_pay_wallet' => '' ,
								'credit_debit_flag_wallet' => '' ,
								'to_pay_amount' => '' ,
								'order_status_wallet' => '' , 
								'status_message_wallet' => '' , 
								'agent_id_wallet' => '' ,
								'tracking_id_wallet' => '' ,
								'paymentmode_wallet_ccavenue' => '' 
							);  
			   
					switch ( $payment_flag_int ) {

						#  wallet 				
						case 0 : 

						break;

						#  ccavenue 
						case 2 :  

						#  wallet + ccavenue	
						case 3 :
							
							//var_dump( $arr1, $arr2, $arr3, $arr4, $arr5, $arr6, $arr7, $user_id );

							isset( $arr1[1] ) ? $tmp_param['tracking_id'] = $arr1[1] : '' ;    // two in one  // get from ccavenue 
							
							isset( $arr2[1] ) ? $tmp_param['order_status'] = $arr2[1] : '' ;   // two in one  # current order  0 = failed,1 = success, 2 = pending
							
							isset( $arr3[1] ) ? $tmp_param['status_message'] = $arr3[1] : '' ; // two in one  // current order message
							
							isset( $arr4[1] ) ? $tmp_param['paymentmode'] = $arr4[1] : '' ; // two in one  // current order payment mode
							
							isset( $arr6[1] ) ? $tmp_param['agent_amount'] = $arr6[1] : '' ;	//  // current order amount			

						break;				

					}
			   
			   return $tmp_param ;
		   
		   } ;
					
		
		 //echo $payment_flag_int;
		 //$payment_flag_int = 3 ;
	   
		   if ( ( $payment_flag_int == 0 ) ) {
	   
			   /*
				$update_current_order_status_param = 
					array(
					'act_mode' => 'orderpaymentupdatesucess',
					'orderid' => $agent_order_last_insert_id,						
					'paymentstatus' => 1,					
					'paymentmode' => 1,						
					'tracking_id' => '',						
					'order_statuspayment' => 1,	
					'order_status' => 1,						
					'status_message' => 'payment successfull',
					'paymenttype' => 'wallet',	
					'ordersucesmail' => '',						
					'ordermailstatus' => '',
					'op_ordertrackingupdate' => ''
					);
			   */
			   
			   // update_current_order_status 
			   
			   
			   $agent_payment_param[ 'param' ] = 'current_order_status_update' ; 
			   $agent_payment_param[ 'param53' ] = $agent_order_last_insert_id ; // int
			   $agent_payment_param[ 'param54' ] = 1 ;  // int
			   $agent_payment_param[ 'param55' ] = 'payment successfull' ;  // varchar
			   $agent_payment_param[ 'param56' ] = '' ;  // int
			   $agent_payment_param[ 'param57' ] = '' ;  // int
			   $agent_payment_param[ 'param58' ] = 1 ;  // varchar
			   
			   
			   $update_order_status( $agent_payment_param ) ;    #***
			   
			   $get_ccavenue_response_data_array = [] ; 
			   
			   $parameter = array('act_mode' => 'agentamount_add_new_debit',
							'Param1' => $agentamount_add_new_debit_array['agent_id']  ,
							'Param2' => $agentamount_add_new_debit_array['to_pay_wallet']  ,
							'Param3' => $agentamount_add_new_debit_array['credit_debit_flag']  ,
							'Param4' => '0',
							'Param5' => 'Ag',
							'Param6' => '',
							'Param7' => '',
							'Param8' => '',
							'Param9' => '');

			   
				$response = $this->supper_admin->call_procedurerow( 'proc_agent_s', $parameter ); 			   
 
			   	$set_order_payment_status( $response ); 
			   
			   // prepare param to save history details 
			   
					$get_ccavenue_response_data_array[ 'agent_id' ] = $user_id;

					// required for history table entry
			   
					$get_ccavenue_response_data_array[ 'agent_wallet_last_insert_id' ] = $this->agent_wallet_last_insert_id ;  // current order table last insert id

					$get_ccavenue_response_data_array[ 'to_pay_wallet' ] = $agentamount_add_new_debit_array['to_pay_wallet'] ;   // to pay by wallet amount
					
					$get_ccavenue_response_data_array[ 'credit_debit_flag_wallet' ] = 'D';   // to pay by wallet amount	
					
					$get_ccavenue_response_data_array[ 'to_pay_ccavenue' ] = 0 ;
			   
					$get_ccavenue_response_data_array[ 'order_status' ] = 1;
					
					$get_ccavenue_response_data_array[ 'paymentmode' ] = 1;  //ccavenue
			   
					$get_ccavenue_response_data_array[ 'status_message' ] = 'payment success'; 
			   
					$get_ccavenue_response_data_array[ 'agent_amount' ] = $session_to_pay_wallet ;
					
					$get_ccavenue_response_data_array[ 'agent_id_wallet' ] = $user_id;
					
					$get_ccavenue_response_data_array[ 'tracking_id_wallet' ] = '';
			   
			   		$final_prepare_data_array = $get_ccavenue_response_data_array ;
			   
			   //p( $final_prepare_data_array ); 
			   
		   }
	   
		/*
		 * 	ccavenue + wallet  transaction
		 */
	   
		
	   	  if ( $payment_flag_int == 3 ) {
			  
			  if( count( $agentamount_add_new_debit_array ) ) {
			  
			   $parameter = array('act_mode' => 'agentamount_add_new_debit',
							'Param1' => $agentamount_add_new_debit_array['agent_id']  ,
							'Param2' => $agentamount_add_new_debit_array['to_pay_wallet']  ,
							'Param3' => $agentamount_add_new_debit_array['credit_debit_flag']  ,
							'Param4' => '0',
							'Param5' => 'Ag',
							'Param6' => '',
							'Param7' => '',
							'Param8' => '',
							'Param9' => '');
			  
				$response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter); 
			  
					$set_order_payment_status( $response ); 

						$this->current_order_payment_flag = 0 ;			  // do not remove this line
			  }
			  
			  
			  $get_ccavenue_response_data_array = [] ;
			  
			  $get_ccavenue_response_data_array = $get_ccavenue_response_data( $payment_flag_int ) ;
			  
				if( count( $get_ccavenue_response_data_array ) && ( $get_ccavenue_response_data_array['order_status'] != 'failed' || $get_ccavenue_response_data_array['order_status'] != 'fail' ) ) {   // payment success to check on live data 

					// prepare cutom parameter to save into db				  	
					$get_ccavenue_response_data_array[ 'agent_id' ] = $user_id;

					// required for history table entry
					$get_ccavenue_response_data_array[ 'agent_wallet_last_insert_id' ] = $this->agent_wallet_last_insert_id;  // current order table last insert id

					$get_ccavenue_response_data_array[ 'to_pay_wallet' ] = $session_to_pay_wallet;   // to pay by wallet amount
					
					$get_ccavenue_response_data_array[ 'to_pay_ccavenue' ] = $session_to_pay_ccavenue ; // not in used

					$get_ccavenue_response_data_array[ 'credit_debit_flag_wallet' ] = 'D';   // to pay flag
					
					$get_ccavenue_response_data_array[ 'agent_amount' ] = abs( ( $before_payment_total_purchase_amount + $session_to_pay_wallet ) ) ;  // param6  // to pay by wallet amount	

					$get_ccavenue_response_data_array[ 'order_status_wallet' ] = 1;

					$get_ccavenue_response_data_array[ 'paymentmode' ] = 3;  //ccavenue

                    $get_ccavenue_response_data_array[ 'status_message' ] = 'payment success';

					$get_ccavenue_response_data_array[ 'status_message_wallet' ] = 'payment successful';

					$get_ccavenue_response_data_array[ 'agent_id_wallet' ] = $user_id;

					$get_ccavenue_response_data_array[ 'tracking_id_wallet' ] = '';

					$response = ( object ) [ 'last_id' => 1 ];   

					$set_order_payment_status( $response );
					
				}else {
				
					// send failed email
					
					// render failed template
					$response = ( object ) [ 'last_id' => 0 ];   

					$set_order_payment_status( $response );

				}	
			  
					$final_prepare_data_array = $get_ccavenue_response_data_array ; 				
			  
					   $agent_payment_param[ 'param' ] = 'current_order_status_update' ; 
					   $agent_payment_param[ 'param53' ] = $agent_order_last_insert_id ; // int
					   $agent_payment_param[ 'param54' ] = $final_prepare_data_array['order_status'] ;  // int
					   $agent_payment_param[ 'param55' ] = $final_prepare_data_array['status_message'] ;  // varchar
					   $agent_payment_param[ 'param56' ] = '' ;  // int
					   $agent_payment_param[ 'param57' ] = '' ;  // int
					   $agent_payment_param[ 'param58' ] = $final_prepare_data_array['paymentmode'] ;  // varchar			  
					   $agent_payment_param[ 'param59' ] = $final_prepare_data_array['tracking_id'] ;  // varchar			  

				   // update_current_order_status 

				   $update_order_status( $agent_payment_param ) ;    #***			  
			  
			  

		   }
	   
		
		 # 	only ccavenue transaction
		
	   	  if ( $payment_flag_int == 2 ) {
			  
			    //$agent_id_wallet = $user_id;
			  $get_ccavenue_response_data_array = [] ;
			  
			  
			  	// get ccavenue data 
			  	$get_ccavenue_response_data_array = $get_ccavenue_response_data( $payment_flag_int ) ;
			  
			  	if( count( $get_ccavenue_response_data_array ) ) {
			  
					// prepare cutom parameter to save into db				  	
					$get_ccavenue_response_data_array[ 'agent_id' ] = $user_id;

					// required for history table entry
					$get_ccavenue_response_data_array[ 'agent_wallet_last_insert_id' ] = $this->agent_wallet_last_insert_id;  // current order table last insert id

					$get_ccavenue_response_data_array[ 'to_pay_wallet' ] = 0;   // to pay by wallet amount
					
					$get_ccavenue_response_data_array[ 'credit_debit_flag_wallet' ] = 'D';   // to pay by wallet amount	
					
					$get_ccavenue_response_data_array[ 'agent_amount' ] = $before_payment_to_pay_ccavenue ;
					
					$get_ccavenue_response_data_array[ 'to_pay_ccavenue' ] = 0 ;
					
					$get_ccavenue_response_data_array[ 'order_status_wallet' ] = 1;
					
					$get_ccavenue_response_data_array[ 'order_status' ] = 1;					
                    $get_ccavenue_response_data_array[ 'status_message' ] = 'payment success';					
					
					$get_ccavenue_response_data_array[ 'paymentmode' ] = 2;  //ccavenue
					
					$get_ccavenue_response_data_array[ 'status_message_wallet' ] = 'success';
					
					$get_ccavenue_response_data_array[ 'agent_id_wallet' ] = $user_id;
					
					$get_ccavenue_response_data_array[ 'tracking_id_wallet' ] = '';
					

					$response = ( object ) [ 'last_id' => 1 ];   

					$set_order_payment_status( $response );
					
				}else {
				
					// send failed email
					
					// render failed template
					$response = ( object ) [ 'last_id' => 0 ];   

					$set_order_payment_status( $response );
					
								
				}
			  
			  // assign to comman variable
			  
			  $final_prepare_data_array = $get_ccavenue_response_data_array ;
			  
				   $agent_payment_param[ 'param' ] = 'current_order_status_update' ; 
				   $agent_payment_param[ 'param53' ] = $agent_order_last_insert_id ; // int
				   $agent_payment_param[ 'param54' ] = $final_prepare_data_array['order_status'] ;  // int
				   $agent_payment_param[ 'param55' ] = $final_prepare_data_array['status_message'] ;  // varchar
				   $agent_payment_param[ 'param56' ] = '' ;  // int
				   $agent_payment_param[ 'param57' ] = '' ;  // int
				   $agent_payment_param[ 'param58' ] = $final_prepare_data_array['paymentmode'] ;  // varchar			  
				   $agent_payment_param[ 'param59' ] = $final_prepare_data_array['tracking_id'] ;  // varchar			  

			   // update_current_order_status 

			   $update_order_status( $agent_payment_param ) ;    #***			  
			  
		   }	 

		
		#	Agent history is saved for all successfull orders
		
		 $parameter = array(
			 		'act_mode' => 'agentamount_add_new_history' ,
					'Param1' => $final_prepare_data_array['agent_id_wallet'] ,
					'Param2' => $final_prepare_data_array['tracking_id'] , 
					'Param3' => $final_prepare_data_array['order_status'] , 
					'Param4' => $final_prepare_data_array['status_message'] , 
					'Param5' => $final_prepare_data_array['paymentmode'] , 
					'Param6' => $final_prepare_data_array['agent_amount'] , 
					'Param7' => $final_prepare_data_array['agent_wallet_last_insert_id'] , 
					'Param8' => $final_prepare_data_array['to_pay_wallet'] ,   // to pay only wallet
					'Param9' => ''
		 			);
		
		/*
		INSERT INTO tbl_agent_payment_history( `ph_agent_id`, `ph_tracking_id`, `ph_order_status`, `ph_status_message`,`ph_paymentmode`,`ph_mer_amount`, `ph_wallet_amount`, `ph_wid`) 
		VALUES ( Param1, Param2, Param3, Param4, Param5, Param6, Param8, Param7);
		*/
	   
		 		$response = $this->supper_admin->call_procedurerow( 'proc_agent_s', $parameter );
			
			 # final order status redirect 
			 
				$this->session->set_flashdata('agent_order_last_insert_id', $agent_order_last_insert_id );			 
			
				switch ( $payment_flag_int ) {

					#  wallet 				
					case 0 : 

					( $this->current_order_payment_flag ) ? $this->sendEmail( 1, $agent_order_last_insert_id )  : $this->sendEmail( 0 ) ;
						
					    if ( $this->current_order_payment_flag ) { 
					        
					        echo '<script>window.location = "'.base_url("agentsucess").'";'.'</script>' ;
					        
					      } else { 
					            
					        echo '<script>window.location = "'.base_url("agentfail").'";'.'</script>' ;
					        
					        }
						
						exit; // do not remove
						
						
						//( $this->current_order_payment_flag ) ? redirect( '/agentsucess/' . $agent_order_last_insert_id )  : redirect( '/agentfail' . $agent_order_last_insert_id ) ;
					

					break;

					#  ccavenue 
					case 2 :  
						
					( $this->current_order_payment_flag ) ? $this->sendEmail( 1, $agent_order_last_insert_id )  : $this->sendEmail( 0 ) ;

					//( $this->current_order_payment_flag ) ? redirect('/agentsucess')  :redirect('/agentfail') ;		
					
					    if ( $this->current_order_payment_flag ) { 
					        
					        echo '<script>window.location = "'.base_url("agentsucess").'";'.'</script>' ;
					        
					      } else { 
					            
					        echo '<script>window.location = "'.base_url("agentfail").'";'.'</script>' ;
					        
					        }
					
					exit; // do not remove


					break;				

					#  wallet + ccavenue	
					case 3 : 
						
					 	
					( $this->current_order_payment_flag ) ? $this->sendEmail( 1, $agent_order_last_insert_id )  : $this->sendEmail( 0 ) ;

//					( $this->current_order_payment_flag ) ? redirect('/agentsucess')  : redirect('/agentfail') ;

					    if ( $this->current_order_payment_flag ) { 
					        
					        echo '<script>window.location = "'.base_url("agentsucess").'";'.'</script>' ;
					        
					      } else { 
					            
					        echo '<script>window.location = "'.base_url("agentfail").'";'.'</script>' ;
					        
					        }
					
					exit; // do not remove

					break;				

				}
			
			
	} else {
			
		//redirect('/agentfail') ;
		flush();
					
				$this->session->set_flashdata('agent_order_last_insert_id', $agent_order_last_insert_id );  					
		
			        echo '<script>window.location = "'.base_url("agentfail").'";'.'</script>' ;		
		
	}
	
	// check order status end here
		

    }
	
	
    public function ccavRequestHandler( $payment_flag = 0, $order_status = 0, $before_payment_after_wallet_deduction_agent_wallet_amount = 0, $agent_payment_param = [] ) {
		
		// $payment_flag_int = empty( $this->session->userdata( 'payment_flag' ) ) ? 0 :  array_sum( $this->session->userdata( 'payment_flag' ) ) ;
		
		$payment_flag_int = $payment_flag ;
		
		$ccavenue_final_request_param = [] ;
		
		//exit; 
		
		// 0 wallet , 1 wallet + ccavenue , 2 ccavenue
		
		
		if( $payment_flag_int == 0 && $order_status ){
			
			redirect('agentpackagesstep');
			
			exit;
		}
		
		# p( $agent_payment_param ) ; 

        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


        $parameterccgatway=array(
            'act_mode' =>'selectccavenue',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
		
        $data['ccavResponse'] = curlpost($parameterccgatway,$path);
		
/*		

p(  $data['ccavResponse']  ); exit;

stdClass Object
(
    [pg_id] => 2
    [pg_branchid] => 1
    [pg_merchant_id] => 49646
    [pg_access_key] => #######
    [pg_working_key] => ##########
    [pg_created] => 2017-07-03 04:46:02
    [pg_updated] => 2018-01-05 11:21:40
    [pg_status] => 1
    [pg_sucess_link] => http://192.168.1.65/snowworldnew/ordersucess
    [pg_fail_link] => http://192.168.1.65/snowworldnew/orderfail
    [pg_currency] => INR
    [pg_language] => EN
    [pg_prefix] => Snow
    [pg_agent_prefix] => Snowagentorder
    [pg_order_sucess_url] => http://192.168.1.65/snowworldnew/agentsucess
    [pg_order_fail_url] => http://192.168.1.65/snowworldnew/fail
    [pg_wallet_sucess_url] => http://192.168.1.65/snowworldnew/agentamountwalletsucess
    [pg_wallet_fail_url] => http://192.168.1.65/snowworldnew/agentamountwalletfail
    [pg_prefix_agent_wallet] => Snowagentwallet
)
			
*/		


		
		if( ( $payment_flag == 0 || $payment_flag == 2 || $payment_flag == 3 ) && $order_status ) {
			
			$payment_type = 'B 2 B' ;			
	//	pend($agent_payment_param);
			$ccavenue_final_request_param = array(
				'tid' => '12345',
				'title' => $agent_payment_param['param16'] ,
				'billing_name' => $agent_payment_param['param17'] ,
				'billing_email' => $agent_payment_param['param18'] ,
				'billing_tel' => $agent_payment_param['param19'] ,
				'billing_address' => $agent_payment_param['param21'] ,
				'billing_city' => $agent_payment_param['param22'] ,
				'billing_state' => $agent_payment_param['param23'] ,
				'billing_zip' => $agent_payment_param['param24'] ,
				'billing_country' => $agent_payment_param['param25'] = 'India',
				'merchant_id' => $data['ccavResponse']->pg_merchant_id,
				'order_id' => $data['ccavResponse']->pg_agent_prefix . '_' . $order_status ,  
				'order_idval' =>$order_status,  
				'user_id' => $agent_payment_param['param65'] ,  
				'amount' =>  $before_payment_after_wallet_deduction_agent_wallet_amount ,   #***
				'currency' => $data['ccavResponse']->pg_currency,
				'redirect_url' => $data['ccavResponse']->pg_order_sucess_url,
				'cancel_url' => $data['ccavResponse']->pg_order_fail_url,
				'language' => $data['ccavResponse']->pg_language,
				
				'merchant_param1' => $payment_flag ,
				'merchant_param2' => $order_status ,
				'merchant_param3' => json_encode( $agent_payment_param ) ,
				'merchant_param4' => ''
			);
			
		} else {
			
			$payment_type = 'B 2 C' ;
				
			 empty ( $this->session->userdata('ccavenue_param') ) ? $ccavenue_final_request_param = [] : $ccavenue_final_request_param = $this->session->userdata('ccavenue_param') ; 


			$data['userid']=($this->session->userdata['skiindia']>0) ? $this->session->userdata['skiindia'] : $this->session->userdata['skiindia_guest'];

			$parameter=array(
				'act_mode' =>'memusersesiid',
				'userid' =>($this->session->userdata['skiindia']>0) ? $this->session->userdata['skiindia'] : $this->session->userdata['skiindia_guest'],
				'type'=>'web',
			);

			$path=api_url().'userapi/usersesion/format/json/';
			$data['memuser']=curlpost($parameter,$path);

			$parameter = array('act_mode'=>'select_order',

				'orderid'=>$this->session->userdata('orderlastinsertid'),
				'type'=>'web',

			);
			$path1 = api_url()."Cart/getpayment_package/format/json/";
			$data['paymentpac']= curlpost($parameter,$path1);



			# Userlog    
			# Bivek please check if this #orderuserupdate is used   // if not please remove

			$parameter4=array(
				'act_mode' =>'orderuserupdate',
				'user_id' =>$data['memuser']->user_id,
				'order_id'=>$data['paymentpac']->pacorderid,
				'title' =>'Mr',
				'billing_name' =>$data['memuser']->user_firstname."".$data['memuser']->user_lastname,
				'billing_email' =>$data['memuser']->user_emailid,
				'billing_tel'=>$data['memuser']->user_mobileno,
				'billing_address' =>$data['memuser']->user_Address,
				'billing_city'=>$data['memuser']->user_city,
				'billing_state' =>$data['memuser']->user_state,
				'billing_zip'=>$data['memuser']->user_zip,
				'billing_country' =>$data['memuser']->user_country,
				'type'=>'web'
			);

			$path=api_url().'userapi/userorderupdate/format/json/';
			$data['userregister']=curlpost($parameter4,$path);			
		
		}
// p( $ccavenue_final_request_param ) ;  exit;	 


        ?>
	<html>
		<head>
			<title> Custom Form Kit </title>
		</head>
    <body>
    <center>


        <?php

        error_reporting(0);

        $merchant_data = 49646;
		
        $working_key = 'B01CEF5819806E9315FF0CADC5D94051';//Shared by CCAVENUES
		
        $access_code = 'AVUS71EG70AF13SUFA';//Shared by CCAVENUES
		
/* for debuging purpose only
$prepare_request_param = array(
	'title' => 'Mr',
	'billing_name' => 'sfsdfd',
	'billing_email' => 'nikhil.rane@gmail.com',
	'billing_tel' => 9810668829,
	'billing_address' => 'fhfghf',
	'billing_city' => 'fghfg',
	'billing_state' => 'hfhfg',
	'billing_zip' => 444444,
	'billing_country' => 'India',
	'tid' => 1513601010767,
	'merchant_id'=> 87391,
	'order_id'=> 'Snow_55',
	'order_idval' => 55,
	'user_id' => 232,
	'amount' => 1,
	'currency' => 'INR',
	'redirect_url' => 'http://115.124.98.243/~skiindia/snowworldnew/ordersucess',
	'cancel_url' => 'http://115.124.98.243/~skiindia/snowworldnew/orderfail',
	'language' => 'EN',
	'submit'=> 'cartses'
);
*/
// echo '<pre>';
// print_r( $prepare_request_param );

// print_r( $_POST)
// exit;
		
		
		# $ccavenue_final_request_param = array_filter( $ccavenue_final_request_param );  // it breaks when state and city is same
		
/*
 *  $ccavenue_final_request_param contain 19 key 
 *  must contain a valid value
 *  if any is empty - ccavenue return order-status = failed
 *  array_filter must return 19 
 *
 *  it breaks when state and city is same
 */
		
//		if ( count( $ccavenue_final_request_param ) ! = 19 ) { 
	
					 $prepare_log_msg = [ 'agent_payment_log' => 
										  [ 'log_created_date' => date('d-m-Y-h:i:s') ,
										   'payment_flag' => $payment_flag ,
										   'order_status' => $order_status ,
										   'payment_type' => $payment_type ,
										   'before_payment_after_wallet_deduction_agent_wallet_amount' => $before_payment_after_wallet_deduction_agent_wallet_amount 
										  ] 
										];

					 $prepare_log_msg = array_merge( $prepare_log_msg,  $ccavenue_final_request_param ); 

					 log_message( 'info', json_encode( $prepare_log_msg ) ) ;
			
					// echo '<center> something went wrong. please try again ... </center>';
//		} 

		if ( count ( $ccavenue_final_request_param ) ) {
		
			foreach ( $ccavenue_final_request_param as $key => $value ){
				
				$merchant_data.=$key.'='.urlencode($value).'&';
			}
			//pend($merchant_data);
			
			$encrypted_data=encrypt($merchant_data,$working_key); // Method for encrypting the data.
			
		}

        ?>
        <form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction">
            <?php
            echo "<input type=hidden name=encRequest value=$encrypted_data>";
            echo "<input type=hidden name=access_code value=$access_code>";
            ?>
        </form>

    <script language='javascript'>document.redirect.submit();</script>
    </body>
</html>

<?php



    }

	

}//end of class
?>