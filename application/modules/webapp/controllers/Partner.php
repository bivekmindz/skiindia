<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Partner extends MX_Controller {

   public function __construct() {
      $this->load->model("supper_admin");
   $this->load->library('session');
    session_start();
   }
	
   public function dashboard(){   
        //p( $this->session->userdata );
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/partnerdashboard",$data);
        $this->load->view("agent/helper/footer");

    }
    // Agent wallet transaction  update
  public function agentwalletsummary(){
	  
	   //p( $this->session->userdata );
	   //p( $this->session->userdata );
	  
	   	
        if( !empty( $this->session->userdata('agent_order_last_insert_id') ) ){  //echo 1;
            
            $this->session->unset_userdata('agent_order_last_insert_id');
        }
// empty( $this->session->userdata('agent_order_last_insert_id')
		if( empty( $this->session->userdata('final_selected_package_data') ) ) {
            
			redirect(base_url('/agentpackagesstep'));
			
        }
	  
	  
	  	//empty( $this->session->userdata('total_promo_price') ) ? $data['purchase_total_amount'] = $this->session->userdata('final_cost_package_addon_handling') : $data['purchase_total_amount'] = $this->session->userdata('total_promo_price');  
	  
		$data['payment_flag'] = empty( $this->session->userdata( 'payment_flag' ) ) ? 0 :  array_sum( $this->session->userdata( 'payment_flag' ) ) ;
	   
	   	$data['before_payment_agent_wallet_amount'] = empty( $this->session->userdata( 'before_payment_agent_wallet_amount' ) ) ? 0 : $this->session->userdata( 'before_payment_agent_wallet_amount' ) ;
	   
	    $data['before_payment_total_purchase_amount'] = empty( $this->session->userdata( 'before_payment_total_purchase_amount' ) ) ? 0 : $this->session->userdata( 'before_payment_total_purchase_amount' ) ;

		// wallet
	  	if( ( $data['payment_flag'] == 0 ) ) {
			
			$data['to_payment_amount'] =  abs( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) );
			
		}	  
	  
	    // wallet + ccavenue
	   	if( ( $data['payment_flag'] == 3 ) ) {  
			
			$data['to_payment_amount'] =  abs( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) ); 
			
		}
		// ccavenue
		if ( $data['payment_flag'] == 2 ) { 
		
			$data['to_payment_amount'] =  abs( $this->session->userdata( 'before_payment_to_pay_ccavenue' ) );
			
		} 
			
	    $data['before_payment_after_wallet_deduction_agent_wallet_amount'] = empty( $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ) ? 0 : $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ;
		
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/agentwalletsummary",$data);
        $this->load->view("agent/helper/footer");	   	
   }

    //Agent Wallet transaction Amount sucess   update Raju Do not touch this code this is not related to you

    public function agentamountwalletsucess(){

        //echo "hii";

        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


        $parameterccgatway=array(
            'act_mode' =>'selectccavenue',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
        $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

//p($data['ccavRequestHandler']);
        $workingKey = $data['ccavRequestHandler']->pg_working_key;
        $encResponse = $_POST["encResp"];
        //pend($encResponse);//This is the response sent by the CCAvenue Server
        $rcvdString = decrypt($encResponse, $workingKey);        //Crypto Decryption used as per the specified working key.
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            if ($i == 3) $order_status = $information[1];
        }



        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/branch/format/json/';
        $data['branch'] = curlpost($parameterbranch, $path);
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/banner/format/json/';
        $data['banner'] = curlpost($parameterbanner, $path);


        $parameterccgatway=array(
            'act_mode' =>'selectccavenue',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
        $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);


        //   p($decryptValues);
        //agentamount_add_new_credit
        //agentamount_add_new_debit

        $arr1 = explode("=", $decryptValues[1]);
        $arr2 = explode("=", $decryptValues[3]);
        $arr3 = explode("=", $decryptValues[8]);
        $arr4 = explode("=", $decryptValues[5]);
        $arr5 = explode("=", $decryptValues[0]);
        $arr6 = explode("=", $decryptValues[35]);
        $arr7 = explode("=", $decryptValues[18]);


        $orderdata = explode($data['ccavRequestHandler']->pg_agent_prefix.'_', $arr5[1]);

        $tracking_id = $arr1[1];

        $order_status = $arr2[1];
        $status_message = $arr3[1];
        $paymentmode = $arr4[1];
        $agent_id = $orderdata[1];
        $agent_amount = $arr6[1];

        $parameter = array('act_mode' => 'agentamount_add_new_credit',
            'Param1' =>  $this->session->userdata['pid'],
            'Param2' => $agent_amount,
            'Param3' => 'C',
            'Param4' => '0',
            'Param5' => 'Ag',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);

        $parameter = array('act_mode' => 'agentamount_add_new_history',
            'Param1' =>  $this->session->userdata['pid'],
            'Param2' => $tracking_id,
            'Param3' => $order_status,
            'Param4' => $status_message,
            'Param5' =>$paymentmode,
            'Param6' => $agent_amount,
            'Param7' => $response->last_id,
            'Param8' => '',
            'Param9' => '');

        $response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);



        $mess = '<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
	<tr><td>Payment Sucess at '.$data['banner']->bannerimage_top3 .' !</td></tr>

	<tr><td>Greetings from '.$data['banner']->bannerimage_top3 .'</td></tr>

	<tr><td>Payment Sucess</td></tr>
	<tr><td> join the fun at '.$data['banner']->bannerimage_top3 .'!</td></tr>

	<tr><td>Yours sincerely,<br>
	' . $data['banner']->bannerimage_top3 . ' Team</td></tr>
	</table>
	';
        $from_email = $data['banner']->bannerimage_from;
        $from = $from_email;
        $fromname = $data['banner']->bannerimage_top3;
        $to =  $arr7[1]; //Recipients list (semicolon separated)
        $api_key = $data['banner']->bannerimage_apikey;
        $subject = ('' . $data['banner']->bannerimage_top3 . ' - Your Request for Agent Sucess');


        $content =$mess;

        $mail1=array();
        $mail1['subject']= ($subject);
        $mail1['fromname']= ($fromname);
        $mail1['api_key'] = $api_key;
        $mail1['from'] = $from;
        $mail1['content']= ($content);
        $mail1['recipients']= $to;
        $apiresult = callApi(@$api_type,@$action,$mail1);
        redirect("agentsucesswallet");
    }
    //Agent Wallet transaction Amount sucess page  update Raju Do not touch this code this is not related to you
    public function agentsucesswallet(){

        $parameter = array('act_mode' => 'agentamount_select_new_history',
            'Param1' =>  $this->session->userdata['pid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $date['agentdatapayment'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);
//p( $date['agentdatapayment'] );   data not relevent
        $parameterreg = array(
            'act_mode' => 'selectsubpartner',
            'title' =>'',
            'firstname' =>'',
            'lastname' =>'',
            'row_id' =>'',
            'logintype' => '',
            'agencyname' =>'',
            'office_address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'telephone' => '',
            'mobile' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'primarycontact_name' => '',
            'primarycontact_email_mobile' => '',
            'primarycontact_telephone' => '',
            'secondrycontact_name' => '',
            'secondrycontact_email_phone' => '',
            'secondrycontact_telephone' => '',
            'management_executives_name' => '',
            'management_executives_email_phone' => '',
            'management_executives_telephone' => '',
            'branches' =>'',

            'yearofestablism' =>'',
            'organisationtype' =>'',
            'lstno' => '',
            'cstno' => '',
            'registrationno' => '',
            'vatno' =>'',
            'panno' => '',
            'servicetaxno' => '',
            'tanno' => '',
            'pfno' => '',
            'esisno' => '',
            'officeregistrationno' => '',
            'exceptiontax' => '',
            'otherexceptiontax' => '',
            'bank_benificialname' => '',
            'bank_benificialaccno' => '',
            'bank_benificialbankname' => '',
            'bank_benificialbranchname' => '',
            'bank_benificialaddress' => '',
            'bank_benificialifsc' => '',
            'bank_benificialswiftcode' => '',
            'bank_benificialibanno' => '',
            'intermidiatebankname' => '',
            'intermidiatebankaddress' => '',
            'intermidiatebankswiftcode' => '',
            'bank_ecs' => '',
            'copypancart' =>'',
            'copyservicetax' => '',
            'copytan' => '',
            'copyaddressproof' => '',
            'emailstatus' => '',
            'emailcurrency' => '',
            'emailcountry' => '',
            'username' => '',
            'password' => '',

            'countryid' => $data['branch']->countryid,
            'stateid' => $data['branch']->stateid,
            'cityid' => $data['branch']->cityid,
            'branch_id' => $data['branch']->branch_id,
            'locationid' => $data['branch']->branch_location,
            'bank_creditamount' =>($this->session->userdata('ppid') != 0) ? $this->session->userdata('ppid') : '',
            'bank_agentcommision' =>'',

            'aadhaar_number' =>'',
            'ppid' =>'',
            'pppermission' =>'',
            'type' => 'web',
        );


        $path = api_url() . 'partnerapi/partnerregisterpartner/format/json/';
        $data['reg'] = curlpost($parameterreg, $path);
// p( $data['reg'] );   data not relevent


        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/agentwaletsucess",$data);
        $this->load->view("agent/helper/footer");
    }
    //Agent Wallet transaction Amount Fail  update Raju Do not touch this code this is not related to you
 public function agentamountwalletfail(){

     $siteurl= base_url();
     $parameterbranch=array(
         'act_mode' =>'selectbranch',
         'weburl' =>$siteurl,
         'type'=>'web',

     );

     $path=api_url().'selectsiteurl/branch/format/json/';
     $data['branch']=curlpost($parameterbranch,$path);


     $parameterccgatway=array(
         'act_mode' =>'selectccavenue',
         'branchid' =>$data['branch']->branch_id,
         'type'=>'web',

     );

     $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
     $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

//p($data['ccavRequestHandler']);
     $workingKey = $data['ccavRequestHandler']->pg_working_key;
     $encResponse = $_POST["encResp"];
     //pend($encResponse);//This is the response sent by the CCAvenue Server
     $rcvdString = decrypt($encResponse, $workingKey);        //Crypto Decryption used as per the specified working key.
     $order_status = "";
     $decryptValues = explode('&', $rcvdString);
     $dataSize = sizeof($decryptValues);
     for ($i = 0; $i < $dataSize; $i++) {
         $information = explode('=', $decryptValues[$i]);
         if ($i == 3) $order_status = $information[1];
     }



     $siteurl = base_url();
     $parameterbranch = array(
         'act_mode' => 'selectbranch',
         'weburl' => $siteurl,
         'type' => 'web',

     );

     $path = api_url() . 'selectsiteurl/branch/format/json/';
     $data['branch'] = curlpost($parameterbranch, $path);
     $parameterbanner = array(
         'act_mode' => 'selectbannerimages',
         'branchid' => $data['branch']->branch_id,
         'type' => 'web',

     );

     $path = api_url() . 'selectsiteurl/banner/format/json/';
     $data['banner'] = curlpost($parameterbanner, $path);


     $parameterccgatway=array(
         'act_mode' =>'selectccavenue',
         'branchid' =>$data['branch']->branch_id,
         'type'=>'web',

     );

     $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
     $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

     /*
     Array
     (
         [0] => order_id=agent_snow_7
         [1] => tracking_id=106310685039
         [2] => bank_ref_no=null
         [3] => order_status=Aborted
         [4] => failure_message=
         [5] => payment_mode=null
         [6] => card_name=null
         [7] => status_code=
         [8] => status_message=I have second thoughts about making this payment
         [9] => currency=INR
         [10] => amount=100.0
         [11] => billing_name=simerjeet singh
         [12] => billing_address=test addressdd
         [13] => billing_city=delhi
         [14] => billing_state=delhii
         [15] => billing_zip=110014
         [16] => billing_country=India
         [17] => billing_tel=9810668829
         [18] => billing_email=simer@elitehrpractices.com
         [19] => delivery_name=
         [20] => delivery_address=
         [21] => delivery_city=
         [22] => delivery_state=
         [23] => delivery_zip=
         [24] => delivery_country=
         [25] => delivery_tel=
         [26] => merchant_param1=
         [27] => merchant_param2=
         [28] => merchant_param3=
         [29] => merchant_param4=
         [30] => merchant_param5=
         [31] => vault=N
         [32] => offer_type=null
         [33] => offer_code=null
         [34] => discount_value=0.0
         [35] => mer_amount=100.0
         [36] => eci_value=
         [37] => retry=null
         [38] => response_code=
         [39] => billing_notes=
         [40] => trans_date=null
         [41] => bin_country=
     )
     */


     $arr1 = explode("=", $decryptValues[1]);
     $arr2 = explode("=", $decryptValues[3]);
     $arr3 = explode("=", $decryptValues[8]);
     $arr4 = explode("=", $decryptValues[5]);
     $arr5 = explode("=", $decryptValues[0]);
     $arr6 = explode("=", $decryptValues[35]);
     $arr7 = explode("=", $decryptValues[18]);


     $orderdata = explode($data['ccavRequestHandler']->pg_agent_prefix.'_', $arr5[1]);

     $tracking_id = $arr1[1];

     $order_status = $arr2[1];
     $status_message = $arr3[1];
     $paymentmode = $arr4[1];
     $agent_id = $orderdata[1];
     $agent_amount = $arr6[1];



     $parameter = array('act_mode' => 'agentamount_add_new_history',
         'Param1' =>  $this->session->userdata['pid'],
         'Param2' => $tracking_id,
         'Param3' => $order_status,
         'Param4' => $status_message,
         'Param5' =>$paymentmode,
         'Param6' => $agent_amount,
         'Param7' => '',
         'Param8' => '',
         'Param9' => '');

     $response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);



     $mess = '<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
<tr><td>You missed the FUN at '.$data['banner']->bannerimage_top3 .' !</td></tr>

<tr><td>Greetings from '.$data['banner']->bannerimage_top3 .'</td></tr>

<tr><td>Oops! By Some Reason You Missed the Payment</td></tr>
<tr><td>Book now and join the fun at '.$data['banner']->bannerimage_top3 .'!</td></tr>

<tr><td>Yours sincerely,<br>
' . $data['banner']->bannerimage_top3 . ' Team</td></tr>
</table>
';
     $from_email = $data['banner']->bannerimage_from;
     $from = $from_email;
     $fromname = $data['banner']->bannerimage_top3;
     $to =  $arr7[1]; //Recipients list (semicolon separated)
     $api_key = $data['banner']->bannerimage_apikey;
     $subject = ('' . $data['banner']->bannerimage_top3 . ' - Your Request for Agent Cancellation');


     $content =$mess;

     $mail1=array();
     $mail1['subject']= ($subject);
     $mail1['fromname']= ($fromname);
     $mail1['api_key'] = $api_key;
     $mail1['from'] = $from;
     $mail1['content']= ($content);
     $mail1['recipients']= $to;
     $apiresult = callApi(@$api_type,@$action,$mail1);

     redirect("agentwalletfail");
 }
    //Agent Wallet transaction Amount Fail   update Raju Do not touch this code this is not related to you

    public function agentwalletfail(){
    $parameter = array('act_mode' => 'agentamount_select_new_history',
        'Param1' =>  $this->session->userdata['pid'],
        'Param2' => '',
        'Param3' => '',
        'Param4' => '',
        'Param5' =>'',
        'Param6' => '',
        'Param7' => '',
        'Param8' => '',
        'Param9' => '');

    $date['agentdatapayment'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);


    $parameterreg = array(
        'act_mode' => 'selectsubpartner',
        'title' =>'',
        'firstname' =>'',
        'lastname' =>'',
        'row_id' =>'',
        'logintype' => '',
        'agencyname' =>'',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' => '',
        'management_executives_telephone' => '',
        'branches' =>'',

        'yearofestablism' =>'',
        'organisationtype' =>'',
        'lstno' => '',
        'cstno' => '',
        'registrationno' => '',
        'vatno' =>'',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',
        'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' =>'',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' => '',
        'password' => '',

        'countryid' => $data['branch']->countryid,
        'stateid' => $data['branch']->stateid,
        'cityid' => $data['branch']->cityid,
        'branch_id' => $data['branch']->branch_id,
        'locationid' => $data['branch']->branch_location,
        'bank_creditamount' =>($this->session->userdata('ppid') != 0) ? $this->session->userdata('ppid') : '',
        'bank_agentcommision' =>'',

        'aadhaar_number' =>'',
        'ppid' =>'',
        'pppermission' =>'',
        'type' => 'web',
    );

    $path = api_url() . 'partnerapi/partnerregisterpartner/format/json/';
    $data['reg'] = curlpost($parameterreg, $path);


    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/agentwalletfailn",$data);
    $this->load->view("agent/helper/footer");
}


    
	
	
//Agent Amount Fail 
	
// only template rendering	
	
public function agentfail() {
	
	//$data['orderlastinsertid'] = $this->session->userdata['agent_order_last_insert_id'];
	
	if( empty( $this->session->flashdata('agent_order_last_insert_id') ) ) {
			
			echo '<center> Unauthorise Access Detected </center>' ;
			
			exit;
		}
		
	echo $data['orderlastinsertid'] = $this->session->flashdata('agent_order_last_insert_id'); 	


	$user_id = $this->session->userdata( 'user_id' );
    
     $parameter = array('act_mode' => 'agentamount_select_new_history',
                'Param1' => $user_id,
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' =>'',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
          
    $date['agentdatapayment'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);
  	
		/* Render booking details */

		$orderdisplay = array('act_mode' => 'select_order',
				   'orderid' => $data['orderlastinsertid'],
					'type' => 'web', );

				   $path = api_url() . "Ordersucess/selectorder/format/json/";

				   $data['orderdisplaydataval'] = curlpost( $orderdisplay, $path );	
		//p( $data['orderdisplaydataval'] );
		// $data['orderdisplaydataval'] = is_object( $data['orderdisplaydataval'] ) ? (array) $data['orderdisplaydataval'] : [] ;
		
         $prepare_time_slot_from_date = $data['orderdisplaydataval']->departuretime .':'.$data['orderdisplaydataval']->frommin .' '.$data['orderdisplaydataval']->txtfromd .' - '.
             $data['orderdisplaydataval']->tohrs.':'.$data['orderdisplaydataval']->tomin.' '.$data['orderdisplaydataval']->txttod;
		
		$data['prepare_time_slot_from_date'] =$prepare_time_slot_from_date;		

        $date_array1 = explode("-", $data['orderdisplaydataval']->addedon); // split the array
        $var_day1 = $date_array1[2]; //day seqment
        $var_month1 = $date_array1[1]; //month segment
        $var_year1 = $date_array1[0]; //year segment
        $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

        $d1 = date(' jS F Y', $new_date_format1);

        $date = '19:24:15 ';
        $d2 = date('h:i:s a ');

        $date_array = explode("-", $data['orderdisplaydataval']->departuredate); // split the array
        $var_day = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_year = rtrim($date_array[2]," "); //year segment
        $new_date_format = strtotime("2017-$var_month-$var_day"); // join them together

        $input = ("$var_year$var_month$var_day");

        $d3 = date("D", strtotime($input)) . "\n";

        //$d4= date(' jS F Y', $new_date_format);

        $row['date'] = trim($var_year, " ") . "-" . $var_month . "-" . $var_day; // this is for example only - comment out when tested
        $d4 = date("F j, Y", strtotime( $data['orderdisplaydataval']->departuredate ));
 		$data['d4'] = $d4;
	
		$data['d1'] = $d1;
		$data['d2'] = $d2;
		$data['d3'] = $d3;
	
// Addone Type
        $orderaddone = array('act_mode' => 'select_addone',
            'orderid' => $data['orderlastinsertid'],
            'type' => 'web'
        );
	
        $path2 = api_url() . "Ordersucess/selectaddone/format/json/" ;
	
        $data['orderaddonedisplaydata'] = curlpost($orderaddone, $path2);
	
        $total_array = $addon_data = [];
        $total = 0;
        
        $package_name_array = explode('#', $data['orderdisplaydataval']->packproductname );
        $package_img_array = explode('#', $data['orderdisplaydataval']->packimg );
        $package_qty_array = explode('#', $data['orderdisplaydataval']->package_qty );
        $package_price_array = explode('#', $data['orderdisplaydataval']->package_price );

        $package_total_price = $total_array[] = getSumAllArrayElement( calculatePriceByQty( explode('#', $data['orderdisplaydataval']->package_price ), explode('#', $data['orderdisplaydataval']->package_qty ) ) );       
        
        $addon_data =  getAddonTotalPricePrint( $data['orderaddonedisplaydata'] ) ; 
         //p( $addon_data['addon_price_array'] );
//p($addon_data);
        $data['addon_price_with_quantity'] = 0;
        $addon_total_price_with_quantity = $total_array[] =  getSumAllArrayElement( calculatePriceByQty( $addon_data['addon_price_array'], $addon_data['addon_qty_array'] ) );
//p( $addon_total_price_with_quantity );        
        $data['addon_price_with_quantity'] = $addon_total_price_with_quantity;	
	
	
			$siteurl = base_url();

			$parameterbranch = array(
				'act_mode' => 'selectbranch',
				'weburl' => $siteurl,
				'type' => 'web',

			);

			$path = api_url() . 'selectsiteurl/branch/format/json/';
			$data['branch'] = curlpost($parameterbranch, $path);	
	
		/* Render booking details */
	

        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agentamountfail",$data);
        $this->load->view("agent/helper/footer");
	
	// good design agentfail template
	
	}
    
    

	
	
    
    
    public function agentsucess(){
		
		
		/*  to uncomment when the order details will be fetch from ccavenue response
		
        if( !empty( $this->session->userdata('agent_order_last_insert_id') ) ){  //echo 1;
            
            $this->session->unset_userdata('agent_order_last_insert_id');
        }

		if( empty( $this->session->userdata('final_selected_package_data') ) || empty( $this->session->userdata('agent_order_last_insert_id') ) ){
            
			redirect(base_url('/agentpackagesstep'));
			
        }
		
		*/
		
		// $data['orderlastinsertid'] = $this->session->userdata['agent_order_last_insert_id'];
				
		
		if( empty( $this->session->flashdata('agent_order_last_insert_id') ) ) {
			
			echo '<center> Unauthorise Access Detected </center>' ;
			
			exit;
		}
		
		$data['orderlastinsertid'] = $this->session->flashdata('agent_order_last_insert_id'); 

		$user_id = $this->session->userdata( 'user_id' );
		
    //Select branch
        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/branch/format/json/';
        $data['branch'] = curlpost($parameterbranch, $path);

		$parametertearms = array(
            'act_mode' => 'selecttearms',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/bannern/format/json/';
        $data['tearmsgatway'] = curlpost($parametertearms, $path);
		 $parameter = array('act_mode' => 'agentamount_select_new_history',
					'Param1' =>  $this->session->userdata['pid'],
					'Param2' => '',
					'Param3' => '',
					'Param4' => '',
					'Param5' =>'',
					'Param6' => '',
					'Param7' => '',
					'Param8' => '',
					'Param9' => '');

		$date['agentdatapayment'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);

		
	//	p( $this->session->userdata ); exit;


		$orderdisplay = array('act_mode' => 'select_order',
				   'orderid' => $data['orderlastinsertid'],
					'type' => 'web', );

				   $path = api_url() . "Ordersucess/selectorder/format/json/";

				   $data['orderdisplaydataval'] = curlpost( $orderdisplay, $path );	
		
		// $data['orderdisplaydataval'] = is_object( $data['orderdisplaydataval'] ) ? (array) $data['orderdisplaydataval'] : [] ;
		
         $prepare_time_slot_from_date = $data['orderdisplaydataval']->departuretime .':'.$data['orderdisplaydataval']->frommin .' '.$data['orderdisplaydataval']->txtfromd .' - '.
             $data['orderdisplaydataval']->tohrs.':'.$data['orderdisplaydataval']->tomin.' '.$data['orderdisplaydataval']->txttod;
		
		$data['prepare_time_slot_from_date'] =$prepare_time_slot_from_date;		
		

        $date_array1 = explode("-", $data['orderdisplaydataval']->addedon); // split the array
        $var_day1 = $date_array1[2]; //day seqment
        $var_month1 = $date_array1[1]; //month segment
        $var_year1 = $date_array1[0]; //year segment
        $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

        $d1 = date(' jS F Y', $new_date_format1);


        $date = '19:24:15 ';
        $d2 = date('h:i:s a ');

        $date_array = explode("-", $data['orderdisplaydataval']->departuredate); // split the array
        $var_day = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_year = rtrim($date_array[2]," "); //year segment
        $new_date_format = strtotime("2017-$var_month-$var_day"); // join them together

        $input = ("$var_year$var_month$var_day");

        $d3 = date("D", strtotime($input)) . "\n";


        //$d4= date(' jS F Y', $new_date_format);

        $row['date'] = trim($var_year, " ") . "-" . $var_month . "-" . $var_day; // this is for example only - comment out when tested
        $d4 = date("F j, Y", strtotime( $data['orderdisplaydataval']->departuredate ));
 		$data['d4'] = $d4;
		$data['d1'] = $d1;
		$data['d2'] = $d2;
		$data['d3'] = $d3;
		
// Addone Type
        $orderaddone = array('act_mode' => 'select_addone',
            'orderid' => $data['orderlastinsertid'],
            'type' => 'web',

        );
        $path2 = api_url() . "Ordersucess/selectaddone/format/json/";
		
        $data['orderaddonedisplaydata'] = curlpost($orderaddone, $path2);
		
        $total_array = $addon_data = [];
        $total = 0;
        
        $package_name_array = explode('#', $data['orderdisplaydataval']->packproductname );
        $package_img_array = explode('#', $data['orderdisplaydataval']->packimg );
        $package_qty_array = explode('#', $data['orderdisplaydataval']->package_qty );
        $package_price_array = explode('#', $data['orderdisplaydataval']->package_price );

        $package_total_price = $total_array[] = getSumAllArrayElement( calculatePriceByQty( explode('#', $data['orderdisplaydataval']->package_price ), explode('#', $data['orderdisplaydataval']->package_qty ) ) );       
        
        $addon_data =  getAddonTotalPricePrint( $data['orderaddonedisplaydata'] ) ; 
         //p( $addon_data['addon_price_array'] );
//p($addon_data);
        $data['addon_price_with_quantity'] = 0;
        $addon_total_price_with_quantity = $total_array[] =  getSumAllArrayElement( calculatePriceByQty( $addon_data['addon_price_array'], $addon_data['addon_qty_array'] ) );
//p( $addon_total_price_with_quantity );        
        $data['addon_price_with_quantity'] = $addon_total_price_with_quantity;

        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/agentsucess",$data);
        $this->load->view("agent/helper/footer");
	}
    

 
   
//partnerregister update   page
public function agentform1()
{

   
    if($this->input->post('submit')=='Send Pin')

    {
    $smsphnon = $this->input->post('umob');

    $num = rand(1111, 9999);
    $parameter =array('act_mode'=>'chmobileotp',
        'row_id'=>'',
        'p_userid'=>'',
        'p_email'=>$this->session->userdata['pemailid'],
        'p_mobilenum'=>$smsphnon,
        'p_mas'=>'');
    $path1=api_url().'partnerapi/chmobileotp/format/json/';
    $record=curlpost($parameter,$path1);
   // p($record);exit();
        $smsmsgg = urlencode('Hi User, your OTP Password is ' . $num . ' for Mobile Number Verification.');

        $sms_url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=356637&username=9867622549&password=mtpjp&To=" . $this->input->post('umob') . "&Text=" . $smsmsgg;
        $sms = file_get_contents($sms_url);

        $data = array(
            'agentmobile' => $this->input->post('umob'),
            'p_email' => $this->session->userdata['pemailid'],
            'agentotp' => $num,
        );

        $this->session->set_userdata($data);

        header("Location:agentformverify");

    }
 $this->load->view("helper/header",$data);
    $this->load->view("helper/topbar",$data);
    $this->load->view("agent/form1",$data);
    $this->load->view("helper/footer");

}
    public function agentformverify()
    {

        if($this->input->post('submit')=='Send Pin')

        {

            if($this->input->post('umob')==$this->session->userdata('agentotp'))

            {

                $smsphnon = $this->session->userdata('agentmobile');
                $parameter =array('act_mode'=>'chmobileotpinsert',
                    'row_id'=>'',
                    'p_userid'=>'',
                    'p_email'=>$this->session->userdata['pemailid'],
                    'p_mobilenum'=>$smsphnon,
                    'p_mas'=>'');
                $path1=api_url().'partnerapi/chmobileotp/format/json/';
                $record=curlpost($parameter,$path1);

               header("location:agentform2");
            }
            else{

                $data['message']="Invalid Promocode";
            }

        }

        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/agentformverify",$data);
        $this->load->view("helper/footer");

    }



    public function agentform2()
    {


        if($this->input->post('Submit')=='Submit') {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep1',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',
                'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),


                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);
header("location:agentform3");

        }


        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form2",$data);
        $this->load->view("helper/footer");

    }
    public function agentform3()
    {


        if($this->input->post('Submit')=='Submit')
        {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep2',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',
'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);
            header("location:agentform4");
        }
        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form3",$data);
        $this->load->view("helper/footer");

    }

    public function agentform4()
    {

        if($this->input->post('Submit')=='Submit')
        {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep4',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',

'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);
           header("location:agentform5");
        }

        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form4",$data);
        $this->load->view("helper/footer");

    }
    public function agentform5()
    {

        if($this->input->post('Submit')=='Submit')
        {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep5',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',
'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);

        header("location:agentform6");
        }

        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form5",$data);
        $this->load->view("helper/footer");

    }

    public function agentform6()
    {

        if($this->input->post('Submit')=='Submit')
        {




            $configUpload['upload_path']    = 'assets/admin/images/partner';              #the folder placed in the root of project
            $configUpload['allowed_types']  = 'gif|jpg|png|bmp|jpeg';       #allowed types description
            $configUpload['max_size']       = '0';                          #max size
            $configUpload['max_width']      = '0';                          #max width
            $configUpload['max_height']     = '0';                          #max height
            $configUpload['encrypt_name']   = true;                         #encrypt name of the uploaded file
            $this->load->library('upload', $configUpload);

            $this->upload->do_upload('copypancart');
           // echo $this->upload->display_errors();

            $pancart=($this->upload->data('copypancart')['file_name']);

            
            $this->upload->do_upload('copyservicetax');
            $servicetax=($this->upload->data('copyservicetax')['file_name']);
            $this->upload->do_upload('copytan');
            $tan=($this->upload->data('copytan')['file_name']);
            $this->upload->do_upload('copyaddressproof');
           $addressproof=($this->upload->data('copyaddressproof')['file_name']);




            $parameterreg = array(
                'act_mode' => 'insertpartnerstep6',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',

'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);

            header("location:partnerlogin");
        }


        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form6",$data);
        $this->load->view("helper/footer");

    }
    public function agentform7()
    {
        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form7",$data);
        $this->load->view("helper/footer");

    }

public function partnerlogout(){

    $this->session->sess_destroy();
    redirect(base_url('partnerlogin'));
}
  //partnerlogin  page
public function partnerlogin(){
	
    if ($this->session->userdata['ppid'] > 0) {
    redirect(base_url()."partnerdashboard");
		
  }else{
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>19);
     $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

  if($this->input->post('submit')=='LOGIN'){

      $parameter=array(
          'act_mode' =>'partnerlogincorporate',
         'pusername' =>$this->input->post('emailmob'),
          'ppassword'=>base64_encode($this->input->post('pwd')),
         'type'=>'web',
          );

      $parameterofficer=array(
          'act_mode' =>'partnerloginofficer',
         'pusername' =>$this->input->post('emailmob'),
          'ppassword'=>base64_encode($this->input->post('pwd')),
         'type'=>'web',
          );

      $path=api_url().'partnerapi/partnerlogin/format/json/';

      $responsecorporate=curlpost($parameter,$path);
      $responseofficer=curlpost($parameterofficer,$path);


      if($responsecorporate->scalar!='Something Went Wrong' && !empty($responsecorporate) && isset($responsecorporate))
         {



if($responsecorporate->agentprofilecomplete!=0)
{
 $data = array(
     'ppid' => $responsecorporate->agent_id,
     'pemailid' => $responsecorporate->agent_username,
     'pid' => $responsecorporate->agent_id,
     'user_id' => $responsecorporate->agent_id,

     'utype' => "partner",


);
$this->session->set_userdata($data);

   redirect(base_url()."partnerdashboard");
}
else
{
    redirect(base_url()."agentform1");
}

         }

elseif($responseofficer->scalar!='Something Went Wrong' && !empty($responseofficer) && isset($responseofficer))
         {

         	$data = array(
				 'ppid' => $responseofficer->agent_id,
				 'user_id' => $responseofficer->agent_id,
				 'pemailid' => $responseofficer->agent_username,
				 'pid' => $responseofficer->agent_id,
				 'utype' => "partner"
				);
			 
			$this->session->set_userdata($data);


             if($responseofficer->agentprofilecomplete!=0){
				 
				 redirect(base_url()."partnerdashboard");
				 
			 }else{
				 
                 redirect(base_url()."agentform1");
             }
			 
        }else{
			 
		   $this->session->set_flashdata('message', 'Invalid login details');
		}

    }

      $siteurl= base_url();
      $parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
          'type'=>'web',

      );

      $path=api_url().'selectsiteurl/branch/format/json/';
      $data['branch']=curlpost($parameterbranch,$path);

      $this->load->view("helper/header",$data);
      $this->load->view("helper/topbar",$data);
 $this->load->view("partnerlogin",$data);
     $this->load->view("helper/footer");
  }
}




//partnerregister update   page
public function agentregistrationupdate(){
    if ($this->session->userdata['ppid'] > 0) {
        redirect(base_url()."partnerdashboard");
    }
  else{
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>14);
     $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

  if($this->input->post('submit')=='submit'){

    //  p($_POST);




      $siteurl= base_url();
      $parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
          'type'=>'web',

      );

      $path=api_url().'selectsiteurl/branch/format/json/';
      $data['branch']=curlpost($parameterbranch,$path);











      $configUpload['upload_path']    = 'assets/admin/images/partner';              #the folder placed in the root of project
      $configUpload['allowed_types']  = 'gif|jpg|png|bmp|jpeg';       #allowed types description
      $configUpload['max_size']       = '0';                          #max size
      $configUpload['max_width']      = '0';                          #max width
      $configUpload['max_height']     = '0';                          #max height
      $configUpload['encrypt_name']   = true;                         #encrypt name of the uploaded file
      $this->load->library('upload', $configUpload);

      $this->upload->do_upload('copypancart');
      $this->upload->do_upload('copyservicetax');
      $this->upload->do_upload('copytan');
      $this->upload->do_upload('copyaddressproof');
      $pancart=($this->upload->data('copypancart')['file_name']);
      $servicetax=($this->upload->data('copyservicetax')['file_name']);
      $tan=($this->upload->data('copytan')['file_name']);
      $addressproof=($this->upload->data('copyaddressproof')['file_name']);






      $parameterregsel=array(
          'act_mode' =>'selectpartneremail',
           'row_id' =>'',
          'logintype' =>'',
          'agencyname' =>'',
          'office_address' =>'',
          'city' =>'',
          'state' =>'',
          'pincode' =>'',
          'telephone' =>'',
          'mobile' =>'',
          'fax' =>'',
          'website' =>'',
          'email' =>'',
          'primarycontact_name' =>'',
          'primarycontact_email_mobile' =>'',
          'primarycontact_telephone' =>'',
          'secondrycontact_name' =>'',
          'secondrycontact_email_phone' =>'',
          'secondrycontact_telephone' =>'',
          'management_executives_name' =>'',
          'management_executives_email_phone' =>'',
          'management_executives_telephone' =>'',
          'branches' =>'',

          'yearofestablism' =>'',
          'organisationtype' =>'',
          'lstno' =>'',
          'cstno' =>'',
          'registrationno' =>'',
          'vatno' =>'',
          'panno' =>'',
          'servicetaxno' =>'',
          'tanno' =>'',
          'pfno' =>'',
          'esisno' =>'',
          'officeregistrationno' =>'',
          'exceptiontax' =>'',
          'otherexceptiontax' =>'',
          'bank_benificialname' =>'',
          'bank_benificialaccno' =>'',
          'bank_benificialbankname' =>'',
          'bank_benificialbranchname' =>'',
          'bank_benificialaddress' =>'',
          'bank_benificialifsc' =>'',
          'bank_benificialswiftcode' =>'',
          'bank_benificialibanno' =>'',
          'intermidiatebankname' =>'',
          'intermidiatebankaddress' =>'',

          'intermidiatebankswiftcode' =>'',
          'bank_ecs' =>'',
          'copypancart' => '',
          'copyservicetax' =>'',
          'copytan' =>'',
          'copyaddressproof' =>'',
          'emailstatus' =>'',
          'emailcurrency' =>'',
          'emailcountry' =>'',
          'username' =>$this->input->post('username'),
          'password' =>'',
          'countryid' => $data['branch']->countryid,
          'stateid' => $data['branch']->stateid,
          'cityid' => $data['branch']->cityid,
          'branch_id' => $data['branch']->branch_id,
          'locationid' => $data['branch']->branch_location,
            'bank_creditamount' =>'',
              'bank_agentcommision' =>'',
              'aadhaar_number' =>'',
          'ppid' =>$this->input->post('ppid'),
          'pppermission' =>$this->input->post('pppermission'),
          'type'=>'web',
      );

      $pathsel=api_url().'partnerapi/partnerregister/format/json/';
      $data['regsel']=curlpost($parameterregsel,$pathsel);


if($data['regsel']->scalar!='Something Went Wrong')
{
   $data['mesg']="Email Id Already Register";
}
else {











    $parameterreg = array(
        'act_mode' => 'insertpartner',
         'row_id' =>'',
        'logintype' => $this->input->post('logintype'),
        'agencyname' => $this->input->post('agencyname'),
        'office_address' => $this->input->post('office_address'),
        'city' => $this->input->post('city'),
        'state' => $this->input->post('state'),
        'pincode' => $this->input->post('pincode'),
        'telephone' => $this->input->post('telephone'),
        'mobile' => $this->input->post('mobile'),
        'fax' => $this->input->post('fax'),
        'website' => $this->input->post('website'),
        'email' => $this->input->post('email'),
        'primarycontact_name' => $this->input->post('primarycontact_name'),
        'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
        'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
        'secondrycontact_name' => $this->input->post('secondrycontact_name'),
        'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
        'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
        'management_executives_name' => $this->input->post('management_executives_name'),
        'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
        'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
        'branches' => $this->input->post('branches'),

        'yearofestablism' => $this->input->post('yearofestablism'),
        'organisationtype' => $this->input->post('organisationtype'),
        'lstno' => $this->input->post('lstno'),
        'cstno' => $this->input->post('cstno'),
        'registrationno' => $this->input->post('registrationno'),
        'vatno' => $this->input->post('vatno'),
        'panno' => $this->input->post('panno'),
        'servicetaxno' => $this->input->post('servicetaxno'),
        'tanno' => $this->input->post('tanno'),
        'pfno' => $this->input->post('pfno'),
        'esisno' => $this->input->post('esisno'),
        'officeregistrationno' => $this->input->post('officeregistrationno'),
        'exceptiontax' => $this->input->post('exceptiontax'),
        'otherexceptiontax' => $this->input->post('otherexceptiontax'),
        'bank_benificialname' => $this->input->post('bank_benificialname'),
        'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
        'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
        'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
        'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
        'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
        'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
        'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
        'intermidiatebankname' => $this->input->post('intermidiatebankname'),
        'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

        'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
        'bank_ecs' => $this->input->post('bank_ecs'),
        'copypancart' => $pancart,
        'copyservicetax' => $servicetax,
        'copytan' => $tan,
        'copyaddressproof' => $addressproof,
        'emailstatus' => $this->input->post('emailstatus'),
        'emailcurrency' => $this->input->post('emailcurrency'),
        'emailcountry' => $this->input->post('emailcountry'),
        'username' => $this->input->post('username'),
        'password' => base64_encode($this->input->post('password')),

        'countryid' => $data['branch']->countryid,
        'stateid' => $data['branch']->stateid,
        'cityid' => $data['branch']->cityid,
        'branch_id' => $data['branch']->branch_id,
        'locationid' => $data['branch']->branch_location,
          'bank_creditamount' =>'',
              'bank_agentcommision' =>'',
'aadhaar_number' =>$this->input->post('aadhaar_number'),
        'ppid' =>$this->input->post('ppid'),
        'pppermission' =>$this->input->post('pppermission'),
        'type' => 'web',
    );

    $path = api_url() . 'partnerapi/partnerregister/format/json/';
    $data['reg'] = curlpost($parameterreg, $path);
     $data['mesg'] ="Agent Added Sucessfully";
}}
      $siteurl= base_url();
      $parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
          'type'=>'web',

      );

      $path=api_url().'selectsiteurl/branch/format/json/';
      $data['branch']=curlpost($parameterbranch,$path);
      $this->load->view("helper/header",$data);
      $this->load->view("helper/topbar",$data);
 $this->load->view("agentregistrationupdate",$data);
     $this->load->view("helper/footer");
  }
}



    //partnerregister update   page
    public function agentregistration(){
        if ($this->session->userdata['ppid'] > 0) {
            redirect(base_url()."partnerdashboard");
        }
        else{
            $parameterseo=array('act_mode'=>'viewseotags','rowid'=>14);
            $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

            if($this->input->post('submit')=='Register'){
                 $siteurl= base_url();
                  $parameterbranch=array(
                    'act_mode' =>'selectbranch',
                    'weburl' =>$siteurl,
                    'type'=>'web',

                );

                $path=api_url().'selectsiteurl/branch/format/json/';
                $data['branch']=curlpost($parameterbranch,$path);
                    $parameterregsel=array(
                    'act_mode' =>'selectpartneremail',
                        'title' =>'',
                        'firstname' =>'',
                        'lastname' =>'',
                    'row_id' =>'',
                    'logintype' =>'',
                    'agencyname' =>'',
                    'office_address' =>'',
                    'city' =>'',
                    'state' =>'',
                    'pincode' =>'',
                    'telephone' =>'',
                    'mobile' =>'',
                    'fax' =>'',
                    'website' =>'',
                    'email' =>'',
                    'primarycontact_name' =>'',
                    'primarycontact_email_mobile' =>'',
                    'primarycontact_telephone' =>'',
                    'secondrycontact_name' =>'',
                    'secondrycontact_email_phone' =>'',
                    'secondrycontact_telephone' =>'',
                    'management_executives_name' =>'',
                    'management_executives_email_phone' =>'',
                    'management_executives_telephone' =>'',
                    'branches' =>'',

                    'yearofestablism' =>'',
                    'organisationtype' =>'',
                    'lstno' =>'',
                    'cstno' =>'',
                    'registrationno' =>'',
                    'vatno' =>'',
                    'panno' =>'',
                    'servicetaxno' =>'',
                    'tanno' =>'',
                    'pfno' =>'',
                    'esisno' =>'',
                    'officeregistrationno' =>'',
                    'exceptiontax' =>'',
                    'otherexceptiontax' =>'',
                    'bank_benificialname' =>'',
                    'bank_benificialaccno' =>'',
                    'bank_benificialbankname' =>'',
                    'bank_benificialbranchname' =>'',
                    'bank_benificialaddress' =>'',
                    'bank_benificialifsc' =>'',
                    'bank_benificialswiftcode' =>'',
                    'bank_benificialibanno' =>'',
                    'intermidiatebankname' =>'',
                    'intermidiatebankaddress' =>'',

                    'intermidiatebankswiftcode' =>'',
                    'bank_ecs' =>'',
                    'copypancart' => '',
                    'copyservicetax' =>'',
                    'copytan' =>'',
                    'copyaddressproof' =>'',
                    'emailstatus' =>'',
                    'emailcurrency' =>'',
                    'emailcountry' =>'',
                    'username' =>$this->input->post('email'),
                    'password' =>'',
                    'countryid' => $data['branch']->countryid,
                    'stateid' => $data['branch']->stateid,
                    'cityid' => $data['branch']->cityid,
                    'branch_id' => $data['branch']->branch_id,
                    'locationid' => $data['branch']->branch_location,
                    'bank_creditamount' =>'',
                    'bank_agentcommision' =>'',
                    'aadhaar_number' =>'',
                        'ppid' =>$this->input->post('ppid'),
                        'pppermission' =>$this->input->post('pppermission'),
                    'type'=>'web',
                );

                $pathsel=api_url().'partnerapi/partnerregister/format/json/';
                $data['regsel']=curlpost($parameterregsel,$pathsel);


                if($data['regsel']->scalar!='Something Went Wrong')
                {
                    $data['mesg']="Email Id Already Register";
                }
                else {


                    $siteurl= base_url();
                    $parameterbranch=array(
                        'act_mode' =>'selectbranch',
                        'weburl' =>$siteurl,
                        'type'=>'web',

                    );

                    $path=api_url().'selectsiteurl/branch/format/json/';
                    $data['branch']=curlpost($parameterbranch,$path);



                    //select banner images
                    $parameterbanner = array(
                        'act_mode' => 'selectbannerimages',
                        'branchid' => $data['branch']->branch_id,
                        'type' => 'web',

                    );

                    $path = api_url() . 'selectsiteurl/banner/format/json/';
                    $data['banner'] = curlpost($parameterbanner, $path);





                    $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $this->input->post("title").". " . ucfirst($this->input->post("first_name")).'&nbsp;'. $this->input->post("last_name") . ',</td></tr>

<tr><td>You have been successfully registered with  ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
You have been successfully registered with ' . $data['banner']->bannerimage_top3 . '. You can use ' . $this->input->post("email") . ' as your Login ID to access your ' . $data['banner']->bannerimage_top3 . ' Account online. 
</td></tr>
<tr><td>Please do not disclose/share your password to anyone.</td></tr>

<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


                    $from_email = $data['banner']->bannerimage_from ;
                    $to_email = $this->input->post('email');
                    //Load email library
                    $this->load->library('email');
                    $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
                    $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
                    $this->email->to($to_email);
                    $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Registration Details');
                    $this->email->message($message_new);
                    //Send mail
                    $this->email->send();
                  //  pend($this->email->print_debugger());


                    $parameterreg = array(
                        'act_mode' => 'insertpartnerstartstage',
                        'title' =>$this->input->post('title'),
                        'firstname' =>$this->input->post('first_name'),
                        'lastname' =>$this->input->post('last_name'),
                        'row_id' =>'',
                        'logintype' => '',
                        'agencyname' =>'',
                        'office_address' => '',
                        'city' => '',
                        'state' => '',
                        'pincode' => '',
                        'telephone' => '',
                        'mobile' => $this->input->post('mobile_number'),
                        'fax' => '',
                        'website' => '',
                        'email' => $this->input->post('email'),
                        'primarycontact_name' => '',
                        'primarycontact_email_mobile' => '',
                        'primarycontact_telephone' => '',
                        'secondrycontact_name' => '',
                        'secondrycontact_email_phone' => '',
                        'secondrycontact_telephone' => '',
                        'management_executives_name' => '',
                        'management_executives_email_phone' => '',
                        'management_executives_telephone' => '',
                        'branches' =>'',

                        'yearofestablism' =>'',
                        'organisationtype' =>'',
                        'lstno' => '',
                        'cstno' => '',
                        'registrationno' => '',
                        'vatno' =>'',
                        'panno' => '',
                        'servicetaxno' => '',
                        'tanno' => '',
                        'pfno' => '',
                        'esisno' => '',
                        'officeregistrationno' => '',
                        'exceptiontax' => '',
                        'otherexceptiontax' => '',
                        'bank_benificialname' => '',
                        'bank_benificialaccno' => '',
                        'bank_benificialbankname' => '',
                        'bank_benificialbranchname' => '',
                        'bank_benificialaddress' => '',
                        'bank_benificialifsc' => '',
                        'bank_benificialswiftcode' => '',
                        'bank_benificialibanno' => '',
                        'intermidiatebankname' => '',
                        'intermidiatebankaddress' => '',
                        'intermidiatebankswiftcode' => '',
                        'bank_ecs' => '',
                        'copypancart' =>'',
                        'copyservicetax' => '',
                        'copytan' => '',
                        'copyaddressproof' => '',
                        'emailstatus' => '',
                        'emailcurrency' => '',
                        'emailcountry' => '',
                        'username' => $this->input->post('email'),
                        'password' => base64_encode($this->input->post('password')),

                        'countryid' => $data['branch']->countryid,
                        'stateid' => $data['branch']->stateid,
                        'cityid' => $data['branch']->cityid,
                        'branch_id' => $data['branch']->branch_id,
                        'locationid' => $data['branch']->branch_location,
                        'bank_creditamount' =>'',
                        'bank_agentcommision' =>'',

'aadhaar_number' =>$this->input->post('aadhaar_number'),
                        'ppid' =>$this->input->post('ppid'),
                        'pppermission' =>$this->input->post('pppermission'),
                        'type' => 'web',
                    );

                    $path = api_url() . 'partnerapi/partnerregister/format/json/';
                    $data['reg'] = curlpost($parameterreg, $path);


                    $data = array(

                        'pemailid' => $this->input->post('email'),
                        'pid' => $data['reg']->lid,

                    );
    $this->session->set_userdata($data);


                 header("location:agentform1");
                  //  $data['mesg'] ="Agent Added Sucessfully";
                }}
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);
            $this->load->view("helper/header",$data);
            $this->load->view("helper/topbar",$data);
            $this->load->view("agentregistration",$data);
            $this->load->view("helper/footer");
        }
    }


    public function agentforget(){
        if ($this->session->userdata['pid'] > 0) {
            redirect(base_url()."partnerdashboard");
        }
else {
    $siteurl= base_url();
    $parameterbranch=array(
        'act_mode' =>'selectbranch',
        'weburl' =>$siteurl,
        'type'=>'web',
    );

    $path=api_url().'selectsiteurl/branch/format/json/';
    $data['branch']=curlpost($parameterbranch,$path);

    $new_pass = base64_encode(rand(000000,999999));


    $parameterupdatepassword_newpass = array(
        'act_mode' => 'forgetpassword_newpassagent',
        'email' => $this->input->post('emailmob'),
        'Param3' => $new_pass,
        'type' => 'web'
    );
//p($parameterupdatepassword_newpass);
    $path_newpass = api_url() . 'userapi/updatepassword/format/json/';
    $data['updateacc_new'] = curlpost($parameterupdatepassword_newpass, $path_newpass);

    $parameterupdatepassword = array(
        'act_mode' => 'forgetpasswordagent',
        'email' => $this->input->post('emailmob'),
        'type' => 'web'
    );

    $path = api_url() . 'userapi/updatepassword/format/json/';
    $data['updateacc'] = curlpost($parameterupdatepassword, $path);




    //pend($data['updateacc_new']);

    $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $data['updateacc']->agent_agencyname.',</td></tr>
<tr><td>Greetings from ' . $data['banner']->bannerimage_top3 . '..!</td></tr>
<tr><td>You have received this communication in response to your request for your ' . $data['banner']->bannerimage_top3 . '. Account password to be sent to you via e-mail and sms.</td></tr>
<tr><td>Your temporary password is: '.base64_decode($data['updateacc']->agent_password).' </td></tr>
<tr><td>Please use the password exactly as it appears above.</td></tr>
<tr><td>We request you immediately change your password by logging on to '.base_url().'</td></tr>
<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


    $from_email = $data['banner']->bannerimage_from ;
    $to_email = $this->input->post('emailmob');
    //Load email library
    $this->load->library('email');
    $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
    $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
    $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Request For Password Reset');
    $this->email->message($message_new);
    //Send mail
    $this->email->send();
    //pend($this->email->print_debugger());


    $this->load->view("helper/header",$data);
    $this->load->view("helper/topbar",$data);
    $this->load->view("agentforget",$data);
    $this->load->view("helper/footer");
}
}

//update partner profile   updatepartnerrprofile
public function updatepartnerrprofile(){


    if ($this->input->post('submit')) {


        $configUpload['upload_path'] = 'assets/admin/images/partner';              #the folder placed in the root of project
        $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
        $configUpload['max_size'] = '0';                          #max size
        $configUpload['max_width'] = '0';                          #max width
        $configUpload['max_height'] = '0';                          #max height
        $configUpload['encrypt_name'] = true;                         #encrypt name of the uploaded file
        $this->load->library('upload', $configUpload);
        if ($_FILES['copypancart']['name'] != '') {
            $this->upload->do_upload('copypancart');
            $pancart = ($this->upload->data('copypancart')['file_name']);
        } else {
            $pancart = $_POST['copypancartdata'];
        }
        if ($_FILES['copyservicetax']['name'] != '') {
            $this->upload->do_upload('copyservicetax');
            $servicetax = ($this->upload->data('copyservicetax')['file_name']);
        } else {
            $servicetax = $_POST['copyservicetaxdata'];
        }
        if ($_FILES['copytan']['name'] != '') {
            $this->upload->do_upload('copytan');
            $tan = ($this->upload->data('copytan')['file_name']);
        }

        else {
            $tan = $_POST['copytandata'];
        }


        if ($_FILES['copyaddressproof']['name'] != '') {
            $this->upload->do_upload('copyaddressproof');
            $addressproof = ($this->upload->data('copyaddressproof')['file_name']);
        } else {
            $addressproof = $_POST['copyaddressproofdata'];
        }


        $parameterregupdate = array(
            'act_mode' => 'updatepartner',
            'title' =>$this->input->post('title'),
            'firstname' =>$this->input->post('firstname'),
            'lastname' =>$this->input->post('lastname'),
            'row_id' => $this->session->userdata['ppid'],


            'logintype' => $this->input->post('logintype'),
            'agencyname' => $this->input->post('agencyname'),
            'office_address' => $this->input->post('office_address'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'),
            'pincode' => $this->input->post('pincode'),
            'telephone' => $this->input->post('telephone'),
            'mobile' => $this->input->post('mobile'),
            'fax' => $this->input->post('fax'),
            'website' => $this->input->post('website'),
            'email' => $this->input->post('email'),
            'primarycontact_name' => $this->input->post('primarycontact_name'),
            'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
            'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
            'secondrycontact_name' => $this->input->post('secondrycontact_name'),
            'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
            'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
            'management_executives_name' => $this->input->post('management_executives_name'),
            'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
            'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
            'branches' => $this->input->post('branches'),

            'yearofestablism' => $this->input->post('yearofestablism'),
            'organisationtype' => $this->input->post('organisationtype'),
            'lstno' => $this->input->post('lstno'),
            'cstno' => $this->input->post('cstno'),
            'registrationno' => $this->input->post('registrationno'),
            'vatno' => $this->input->post('vatno'),
            'panno' => $this->input->post('panno'),
            'servicetaxno' => $this->input->post('servicetaxno'),
            'tanno' => $this->input->post('tanno'),
            'pfno' => $this->input->post('pfno'),
            'esisno' => $this->input->post('esisno'),
            'officeregistrationno' => $this->input->post('officeregistrationno'),
            'exceptiontax' => $this->input->post('exceptiontax'),
            'otherexceptiontax' => $this->input->post('otherexceptiontax'),
            'bank_benificialname' => $this->input->post('bank_benificialname'),
            'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
            'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
            'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
            'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
            'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
            'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
            'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
            'intermidiatebankname' => $this->input->post('intermidiatebankname'),
            'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

            'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
            'bank_ecs' => $this->input->post('bank_ecs'),
            'copypancart' => $pancart,
            'copyservicetax' => $servicetax,
            'copytan' => $tan,
            'copyaddressproof' => $addressproof,
            'emailstatus' => $this->input->post('emailstatus'),
            'emailcurrency' => $this->input->post('emailcurrency'),
            'emailcountry' => $this->input->post('emailcountry'),
            'username' => $this->input->post('username'),
            'password' => base64_encode($this->input->post('password')),

            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => $this->input->post('username'),
            'locationid' => '',
            'bank_creditamount' => $this->input->post('bank_creditamount'),
            'bank_agentcommision' => $this->input->post('bank_agentcommision'),
            'bank_agentcommision' => $this->input->post('bank_agentcommision'),
            'aadhaar_number' =>$this->input->post('aadhaar_number'),

            'ppid' =>$this->input->post('ppid'),
            'pppermission' =>$this->input->post('pppermission'),
        );

        $response['regselupdate'] = $this->supper_admin->call_procedure('proc_partnerregister_v', $parameterregupdate);

        $data['emsg'] = "Edit Sucessfully";

       // header("Location:agent");

    }



    $parametersel = array(


        'act_mode' => 'selectpartnerdata',
        'title' =>'',
        'firstname' =>'',
        'lastname' =>'',
        'row_id' => $this->session->userdata['ppid'],

        'logintype' => '',
        'agencyname' => '',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' => '',
        'management_executives_telephone' => '',
        'branches' => '',

        'yearofestablism' => '',
        'organisationtype' => '',
        'lstno' => '',
        'cstno' => '',
        'registrationno' => '',
        'vatno' => '',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',

        'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' => '',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' => '',
        'password' => '',

        'countryid' => '',
        'stateid' => '',
        'cityid' => '',
        'branch_id' => '',
        'locationid' => '',
        'bank_creditamount' => '',
        'bank_agentcommision' => '',
        'aadhaar_number' =>'',
        'ppid' =>$this->input->post('ppid'),
        'pppermission' =>$this->input->post('pppermission'),
       
    );

 $data['agentviewwdata'] = $this->supper_admin->call_procedurerow('proc_partnerregister_v', $parametersel);




    //$path = api_url() . 'partnerapi/partnerregister/format/json/';
    //$data['agentviewwdata'] = curlpost($parametersel, $path);


    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/updatepartnerrprofile",$data);
    $this->load->view("agent/helper/footer");
}

    public function changepartnerpassword(){


        if ($this->input->post('submit') == 'changePswrd') {
            $this->form_validation->set_rules('currentpassword', 'Enter Current  password ', 'required');
            $this->form_validation->set_rules('newpassword', 'Enter  password ', 'required');
          $this->form_validation->set_rules('confirmpassword', 'Enter Confirm  password ', 'required');


            if ($this->input->post('confirmpassword') != $this->input->post('newpassword')) {

                $this->form_validation->set_rules('confirmpassword', ' Password Not Match', 'required');
            }



            if ($this->form_validation->run() != FALSE) {

                $parametercheckpasswrd = array(
                    'act_mode' => 'partncheckpassword',
                    'userid' => $this->session->userdata['ppid'],
                    'currentpassword' => $this->input->post('currentpassword'),
                    'newpassword' => '',
                    'confirmpassword' => '',

                    'type' => 'web',
                );

                $path = api_url() . 'userapi/usercheckchangepassword/format/json/';
                $data['checkpasswrd'] = curlpost($parametercheckpasswrd, $path);
              
//echo (base64_decode($data['checkpasswrd']->agent_password)."---".$this->input->post('currentpassword');
                if ((base64_decode($data['checkpasswrd']->agent_password)) == $this->input->post('currentpassword')) {

                    $parameter = array(
                        'act_mode' => 'partnchangepassword',
                        'userid' => $this->session->userdata['ppid'],
                        'currentpassword' => $this->input->post('currentpassword'),
                        'newpassword' => $this->input->post('newpassword'),
                        'confirmpassword' => $this->input->post('confirmpassword'),
                        'type' => 'web',
                    );

                    $path = api_url() . 'userapi/userchangepassword/format/json/';
                    $data['userlog'] = curlpost($parameter, $path);

                    $data['message'] = "Password Change Sucessfully";

                } else {


                    $data['message'] = " Invalid Password ";

                }

            }
        }



        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/changepartnerpassword",$data);
        $this->load->view("agent/helper/footer");
    }


     public function order_sess(){
		 
        if ($_POST['Clear'] == 'Show All Records')
        {
            $this->session->unset_userdata('order_filter');
        }
else {
        $a = explode(":", $this->input->post('filter_date_session'));
        $b = explode(" ", $this->input->post('filter_date_session'));

        if ($b['1'] == 'PM') {
            $session_time = $a['0'] + 12;
        } else {
            $session_time = $a['0'];
        }

        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');
        $branchids = implode(',', $a);
        $filterstatus = implode(',', $b);
        $filtertime = implode(',', $c);
        $filtersta = implode(',', $d);

        $array = array('branchids' => $branchids,
        'filter_name' => $this->input->post('filter_name'),
        'filter_email' => $this->input->post('filter_email'),
        'filter_ticket' => $this->input->post('filter_ticket'),
        'filter_mobile' => $this->input->post('filter_mobile'),
        'filter_payment' => $this->input->post('filter_payment'),
        'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
        'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
        'filter_date_session_from' => $this->input->post('filter_date_session_from'),
        'filter_date_session_to' => $this->input->post('filter_date_session_to'),
        'filter_status' => $filterstatus,
        'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
        'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
        'filter_date_ticket' => $this->input->post('filter_date_ticket'),
        'filter_date_session' => $session_time,
        'filter_tim' => $filtertime,
        'filter_sta' => $filtersta
        );

        $this->session->set_userdata('order_filter', $array);
}
        redirect('order');
    }


 //Show Order List Of Agent
 public function order(){ 
     if($this->session->userdata('order_filter'))
       { 
                 $parameter1 = array('act_mode' => 'Svieworderagent',
                'Param1' => $this->session->userdata('order_filter')['branchids'],
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $this->session->userdata('order_filter')['filter_email'],
                'Param4' => $this->session->userdata('order_filter')['filter_ticket'],
                'Param5' => $this->session->userdata('order_filter')['filter_mobile'],
                'Param6' => $this->session->userdata('order_filter')['filter_payment'],
                'Param7' => $this->session->userdata('order_filter')['filter_date_booking_from'],
                'Param8' => $this->session->userdata('order_filter')['filter_date_booking_to'],
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],
                'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],
                'Param11' => $this->session->userdata('order_filter')['filter_status'],
                'Param12' => $this->session->userdata('order_filter')['filter_date_printed_from'],
                'Param13' => $this->session->userdata('order_filter')['filter_date_printed_to'],
                'Param14' => $this->session->userdata('order_filter')['filter_date_ticket'],
                'Param15' => $this->session->userdata('order_filter')['filter_date_session'],
                'Param16' => $this->session->userdata('order_filter')['filter_tim'],
                'Param17' => $this->session->userdata('order_filter')['filter_sta'],
                'Param18' => '',
                'Param19' =>$this->session->userdata('ppid')
                 );
            foreach($parameter1 as $key=>$val){
                if($parameter1[$key] == '')
                {
                    $parameter1[$key] =-1;
                }
            }
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_filter_v', $parameter1);
            $this->session->unset_userdata('order_filter');


        }else{
		   
            $parameter1 = array(
				'act_mode' => 'Sviewordernewpartner',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => $this->session->userdata('ppid') 
            ); 
      
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_partner', $parameter1);
     }

        $parameter_time = array('act_mode' => 'branch_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');
        $response['vieww_time'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter_time);

        $parameter_status = array('act_mode' => 'status_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');
        $response['vieww_status'] = $this->supper_admin->call_procedure('proc_order_filter_s',$parameter_status);
//pend($response['vieww_status']);
        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        
        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,


        );
        $response['branch'] = $this->supper_admin->call_procedurerow('proc_select_branch_v', $parameterbranch);
//select banner images
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' =>  $response['branch']->branch_id,


        );
        $response['banner'] = $this->supper_admin->call_procedurerow('proc_select_banner_v', $parameterbanner);
	 
     	$parametertearms = array(
            'act_mode' => 'selecttearms',
            'branchid' => $response['branch']->branch_id,


        );
        $response['tearmsgatway'] = $this->supper_admin->call_procedure('proc_select_banner_v', $parametertearms);

        $this->load->view("agent/helper/header",$response);
        $this->load->view("agent/helper/leftbar",$response);
        $this->load->view("agent/order",$response);
        $this->load->view("agent/helper/footer");
    }

    public function updateamount(){

        if ($this->session->userdata['ppid'] < 0) {
            redirect(base_url()."partnerdashboard");
        }
        else {
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',
            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);


            $userdisplay = array('act_mode'=>'select_partner',
                'userid'=>$this->session->userdata['ppid'],
                'type'=>'web',

            );
            $path3 = api_url()."Ordersucess/selectuser/format/json/";
            $data['memuser']= curlpost($userdisplay,$path3);
            $parameterccgatway=array(
                'act_mode' =>'selectccavenue',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
            $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);
           // pend($data['ccavRequestHandler']);
            $walletselect = array('act_mode' => 'selectwalletdata',
                'orderid' => '',
                'tracking_id' => '',
                'order_status' => '',
                'status_message' => '',
                'paymentmode' => '',
                'paymentstatus' => '',
                'ordersucesmail' => '',
                'ordermailstatus' => '',
                'agent_id' => '',
                'cred_debet' => '',
                'amount' => '',
                'bankagentcommision' => '',
                'type' => 'web',

            );

            $path = api_url() . "Ordersucess/selectwalletdata/format/json/";
            $data['orderwalletselect'] = curlpost($walletselect, $path);


      $parameter = array('act_mode' => 'agentamount_show',
            'Param1' => $this->session->userdata('ppid'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
      // p($parameter);exit();
        $data['agent_show'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);
       // p($data['agent_show']);exit();

           

 if($this->input->post('submit')=='cartses') {
     $data = array(
         'tid' => '12345',
         'title' => $this->input->post('title'),
         'billing_name' => $this->input->post('billing_name'),
         'billing_email' => $this->input->post('billing_email'),
         'billing_tel' => $this->input->post('billing_tel'),
         'billing_address' => $this->input->post('billing_address'),
         'billing_city' => $this->input->post('billing_city'),
         'billing_state' => $this->input->post('billing_state'),
         'billing_zip' => $this->input->post('billing_zip'),
         'billing_country' => 'India',
         'merchant_id' => $data['ccavRequestHandler']->pg_merchant_id,
         'order_id' => $data['ccavRequestHandler']->pg_prefix_agent_wallet . '_' . $this->session->userdata('ppid'),
         'order_idval' => $this->session->userdata('ppid'),
         'user_id' => $this->session->userdata('ppid'),
         'amount' =>  $this->input->post('amount'),
         'currency' => $data['ccavRequestHandler']->pg_currency,
         'redirect_url' => $data['ccavRequestHandler']->pg_wallet_sucess_url,
         'cancel_url' => $data['ccavRequestHandler']->pg_wallet_fail_url,
         'language' => $data['ccavRequestHandler']->pg_language,

     );
     $this->session->set_userdata('ccavenue_param', $data);
     redirect(base_url()."ccavRequestHandler");
 }
            $this->load->view("agent/helper/header",$data);
            $this->load->view("agent/helper/leftbar",$data);
            $this->load->view("agent/updateamount",$data);
            $this->load->view("agent/helper/footer");
        }
    }


public function partners(){

    $parameterreg = array(
        'act_mode' => 'selectsubpartner',
        'title' =>'',
        'firstname' =>'',
        'lastname' =>'',
        'row_id' =>'',
        'logintype' => '',
        'agencyname' =>'',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' => '',
        'management_executives_telephone' => '',
        'branches' =>'',

        'yearofestablism' =>'',
        'organisationtype' =>'',
        'lstno' => '',
        'cstno' => '',
        'registrationno' => '',
        'vatno' =>'',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',
        'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' =>'',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' => '',
        'password' => '',

        'countryid' => $data['branch']->countryid,
        'stateid' => $data['branch']->stateid,
        'cityid' => $data['branch']->cityid,
        'branch_id' => $data['branch']->branch_id,
        'locationid' => $data['branch']->branch_location,
        'bank_creditamount' =>($this->session->userdata('ppid') != 0) ? $this->session->userdata('ppid') : '',
        'bank_agentcommision' =>'',

        'aadhaar_number' =>'',
        'ppid' =>'',
        'pppermission' =>'',
        'type' => 'web',
    );


    $path = api_url() . 'partnerapi/partnerregisterpartner/format/json/';
    $data['reg'] = curlpost($parameterreg, $path);

    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/partners",$data);
    $this->load->view("agent/helper/footer");

}

public function agentedit()
{

    if($this->input->post('submit'))

    {



        $parameterregupdate=array(
            'act_mode' => 'updateagentpartner',
            'title' => $this->input->post('title'),
            'firstname' => $this->input->post('first_name'),
            'lastname' => $this->input->post('last_name'),
            'row_id' => ($_GET['edid']),
            'logintype' => '',
            'agencyname' => '',
            'office_address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'telephone' => '',
            'mobile' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'primarycontact_name' => '',
            'primarycontact_email_mobile' => '',
            'primarycontact_telephone' => '',
            'secondrycontact_name' => '',
            'secondrycontact_email_phone' => '',
            'secondrycontact_telephone' => '',
            'management_executives_name' => '',
            'management_executives_email_phone' =>'',
            'management_executives_telephone' => '',
            'branches' => '',
            'yearofestablism' => '',
            'organisationtype' => '',
            'lstno' =>'',
            'cstno' => '',
            'registrationno' =>'',
            'vatno' => '',
            'panno' => '',
            'servicetaxno' => '',
            'tanno' => '',
            'pfno' => '',
            'esisno' => '',
            'officeregistrationno' => '',
            'exceptiontax' => '',
            'otherexceptiontax' => '',
            'bank_benificialname' => '',
            'bank_benificialaccno' => '',
            'bank_benificialbankname' => '',
            'bank_benificialbranchname' => '',
            'bank_benificialaddress' => '',
            'bank_benificialifsc' => '',
            'bank_benificialswiftcode' => '',
            'bank_benificialibanno' => '',
            'intermidiatebankname' => '',
            'intermidiatebankaddress' => '',
            'intermidiatebankswiftcode' => '',
            'bank_ecs' => '',
            'copypancart' => '',
            'copyservicetax' => '',
            'copytan' => '',
            'copyaddressproof' => '',
            'emailstatus' => '',
            'emailcurrency' => '',
            'emailcountry' => '',
            'username' =>'',
            'password' => '',
            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => '',
            'locationid' => '',
            'bank_creditamount' => '',
            'bank_agentcommision' => '',
            'aadhaar_number' =>'',
            'ppid' =>'',
            'pppermission' =>'',
        );

        $data['regselupdate'] =   $this->supper_admin->call_procedure('proc_partnerregister_v', $parameterregupdate);

        $data['emsg'] ="Added Sucessfully";

     header("Location:partners");

    }



    $parametersel=array(
		'act_mode' => 'selectpartnerdatauser',
        'title' => '',
        'firstname' => '',
        'lastname' => '',
        'row_id' =>  ($_GET['edid']),
        'logintype' => '',
        'agencyname' => '',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' =>'',
        'management_executives_telephone' => '',
        'branches' => '',
'yearofestablism' => '',
        'organisationtype' => '',
        'lstno' =>'',
        'cstno' => '',
        'registrationno' =>'',
        'vatno' => '',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',
'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' => '',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' =>'',
        'password' => '',
'countryid' => '',
        'stateid' => '',
        'cityid' => '',
        'branch_id' => '',
        'locationid' => '',
        'bank_creditamount' => '',
        'bank_agentcommision' => '',
        'aadhaar_number' =>'',
        'ppid' =>'',
        'pppermission' =>'',

    );

 //$path = api_url() . 'partnerapi/partnerregisterpartner/format/json/';
  //  $data['agentviewwdata'] = curlpost($parameterreg, $path);

   $data['agentviewwdata'] =   $this->supper_admin->call_procedurerow('proc_partnerregister_v', $parametersel);




    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/partnersedit",$data);
    $this->load->view("agent/helper/footer");

}


public function agentstatus(){
    $rowid = $this->uri->segment(4);
    $status = $this->uri->segment(5);
    $act_mode = $status == '1' ? 'activeandinactive' : 'inactiveandinactive';
    $parameter = array('act_mode' => $act_mode,
        'title' => '',
        'firstname' => '',
        'lastname' => '',
        'row_id' => $rowid,
        'logintype' => '',
        'agencyname' => '',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' =>'',
        'management_executives_telephone' => '',
        'branches' => '',

        'yearofestablism' => '',
        'organisationtype' => '',
        'lstno' =>'',
        'cstno' => '',
        'registrationno' =>'',
        'vatno' => '',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',

        'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' => '',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' =>'',
        'password' => '',

        'countryid' => '',
        'stateid' => '',
        'cityid' => '',
        'branch_id' => '',
        'locationid' => '',
        'bank_creditamount' => '',
        'bank_agentcommision' => '',
        'aadhaar_number' =>'',
        'ppid' =>'',
        'pppermission' =>'',
    );
    //pend($parameter);
    $response = $this->supper_admin->call_procedure('proc_partnerregister_v', $parameter);


}
    public function partnerusercreate(){
        if($this->input->post('submit')=='Register'){
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);
            $parameterregsel=array(
                'act_mode' =>'selectpartneremail',
                'title' =>'',
                'firstname' =>'',
                'lastname' =>'',
                'row_id' =>'',
                'logintype' =>'',
                'agencyname' =>'',
                'office_address' =>'',
                'city' =>'',
                'state' =>'',
                'pincode' =>'',
                'telephone' =>'',
                'mobile' =>'',
                'fax' =>'',
                'website' =>'',
                'email' =>'',
                'primarycontact_name' =>'',
                'primarycontact_email_mobile' =>'',
                'primarycontact_telephone' =>'',
                'secondrycontact_name' =>'',
                'secondrycontact_email_phone' =>'',
                'secondrycontact_telephone' =>'',
                'management_executives_name' =>'',
                'management_executives_email_phone' =>'',
                'management_executives_telephone' =>'',
                'branches' =>'',

                'yearofestablism' =>'',
                'organisationtype' =>'',
                'lstno' =>'',
                'cstno' =>'',
                'registrationno' =>'',
                'vatno' =>'',
                'panno' =>'',
                'servicetaxno' =>'',
                'tanno' =>'',
                'pfno' =>'',
                'esisno' =>'',
                'officeregistrationno' =>'',
                'exceptiontax' =>'',
                'otherexceptiontax' =>'',
                'bank_benificialname' =>'',
                'bank_benificialaccno' =>'',
                'bank_benificialbankname' =>'',
                'bank_benificialbranchname' =>'',
                'bank_benificialaddress' =>'',
                'bank_benificialifsc' =>'',
                'bank_benificialswiftcode' =>'',
                'bank_benificialibanno' =>'',
                'intermidiatebankname' =>'',
                'intermidiatebankaddress' =>'',

                'intermidiatebankswiftcode' =>'',
                'bank_ecs' =>'',
                'copypancart' => '',
                'copyservicetax' =>'',
                'copytan' =>'',
                'copyaddressproof' =>'',
                'emailstatus' =>'',
                'emailcurrency' =>'',
                'emailcountry' =>'',
                'username' =>$this->input->post('email'),
                'password' =>'',
                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' =>'',
                'bank_agentcommision' =>'',
                'aadhaar_number' =>'',
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type'=>'web',
            );

            $pathsel=api_url().'partnerapi/partnerregister/format/json/';
            $data['regsel']=curlpost($parameterregsel,$pathsel);


            if($data['regsel']->scalar!='Something Went Wrong')
            {
                $data['mesg']="Email Id Already Register";
            }
            else {


                $siteurl= base_url();
                $parameterbranch=array(
                    'act_mode' =>'selectbranch',
                    'weburl' =>$siteurl,
                    'type'=>'web',

                );

                $path=api_url().'selectsiteurl/branch/format/json/';
                $data['branch']=curlpost($parameterbranch,$path);



                //select banner images
                $parameterbanner = array(
                    'act_mode' => 'selectbannerimages',
                    'branchid' => $data['branch']->branch_id,
                    'type' => 'web',

                );

                $path = api_url() . 'selectsiteurl/banner/format/json/';
                $data['banner'] = curlpost($parameterbanner, $path);





                $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $this->input->post("title").". " . ucfirst($this->input->post("first_name")).'&nbsp;'. $this->input->post("last_name") . ',</td></tr>

<tr><td>You have been successfully registered with  ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
You have been successfully registered with ' . $data['banner']->bannerimage_top3 . '. You can use ' . $this->input->post("email") . ' as your Login ID to access your ' . $data['banner']->bannerimage_top3 . ' Account online. 
</td></tr>
<tr><td>Please do not disclose/share your password to anyone.</td></tr>

<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


                $from_email = $data['banner']->bannerimage_from ;
                $to_email = $this->input->post('email');
                //Load email library
                $this->load->library('email');
                $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
                $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
                $this->email->to($to_email);
                $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Registration Details');
                $this->email->message($message_new);
                //Send mail
                $this->email->send();
                //  pend($this->email->print_debugger());


                $parameterreg = array(
                    'act_mode' => 'insertsubpartner',
                    'title' =>$this->input->post('title'),
                    'firstname' =>$this->input->post('first_name'),
                    'lastname' =>$this->input->post('last_name'),
                    'row_id' =>'',
                    'logintype' => '',
                    'agencyname' =>'',
                    'office_address' => '',
                    'city' => '',
                    'state' => '',
                    'pincode' => '',
                    'telephone' => '',
                    'mobile' => $this->input->post('mobile_number'),
                    'fax' => '',
                    'website' => '',
                    'email' => $this->input->post('email'),
                    'primarycontact_name' => '',
                    'primarycontact_email_mobile' => '',
                    'primarycontact_telephone' => '',
                    'secondrycontact_name' => '',
                    'secondrycontact_email_phone' => '',
                    'secondrycontact_telephone' => '',
                    'management_executives_name' => '',
                    'management_executives_email_phone' => '',
                    'management_executives_telephone' => '',
                    'branches' =>'',

                    'yearofestablism' =>'',
                    'organisationtype' =>'',
                    'lstno' => '',
                    'cstno' => '',
                    'registrationno' => '',
                    'vatno' =>'',
                    'panno' => '',
                    'servicetaxno' => '',
                    'tanno' => '',
                    'pfno' => '',
                    'esisno' => '',
                    'officeregistrationno' => '',
                    'exceptiontax' => '',
                    'otherexceptiontax' => '',
                    'bank_benificialname' => '',
                    'bank_benificialaccno' => '',
                    'bank_benificialbankname' => '',
                    'bank_benificialbranchname' => '',
                    'bank_benificialaddress' => '',
                    'bank_benificialifsc' => '',
                    'bank_benificialswiftcode' => '',
                    'bank_benificialibanno' => '',
                    'intermidiatebankname' => '',
                    'intermidiatebankaddress' => '',
                    'intermidiatebankswiftcode' => '',
                    'bank_ecs' => '',
                    'copypancart' =>'',
                    'copyservicetax' => '',
                    'copytan' => '',
                    'copyaddressproof' => '',
                    'emailstatus' => '',
                    'emailcurrency' => '',
                    'emailcountry' => '',
                    'username' => $this->input->post('email'),
                    'password' => base64_encode($this->input->post('password')),

                    'countryid' => $data['branch']->countryid,
                    'stateid' => $data['branch']->stateid,
                    'cityid' => $data['branch']->cityid,
                    'branch_id' => $data['branch']->branch_id,
                    'locationid' => $data['branch']->branch_location,
                    'bank_creditamount' =>'',
                    'bank_agentcommision' =>'',

                    'aadhaar_number' =>$this->input->post('aadhaar_number'),
                    'ppid' =>$this->session->userdata('ppid'),
                    'pppermission' =>$this->input->post('pppermission'),
                    'type' => 'web',
                );

                $path = api_url() . 'partnerapi/partnerregister/format/json/';
                $data['reg'] = curlpost($parameterreg, $path);


              $data['message'] ="Agent Added Sucessfully";
            }}
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/partnerusercreate",$data);
        $this->load->view("agent/helper/footer");

    }
    public function mywallet(){

        $edid=$this->session->userdata['ppid'];
       // pend($edid);

        $orderwalletselect = array('act_mode' => 'orderwalletselectagentamount',
            'orderid' => '',
            'tracking_id' => '',
            'order_status' => '',
            'status_message' => '',
            'paymentmode' => '',
            'paymentstatus' => '',
            'ordersucesmail' =>'',
            'ordermailstatus' => '',
            'agent_id' => $this->session->userdata['ppid'],
            'cred_debet' =>'',
            'amount' => '',
            'bankagentcommision' => '',
            'type' => 'web',

        );

        $path = api_url() . "Ordersucess/selectwalletdatadispall/format/json/";
        $data['orderwalletselect'] = curlpost($orderwalletselect, $path);

        $parameter = array('act_mode' => 'agentamount_new',
            'Param1' => $edid,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $data['agent_his'] = (array)$this->supper_admin->call_procedure('proc_agent_s', $parameter);
       // pend( $response['agent_his']);
        $parameter = array('act_mode' => 'agent_last_amount',
            'Param1' => $edid,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $data['agent_left_amt'] = (array)$this->supper_admin->call_procedureRow('proc_agent_s', $parameter);

        //pend($data['agent_last_amt']);



        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/mywallet",$data);
        $this->load->view("agent/helper/footer");
    }




  public function dashboardlist(){ 

if($this->session->userdata['pid']<=0 )
{
redirect("partnerlogin");
}

   if($this->input->post('search') == 'Search'){
         $paramater = array(
            'act_mode'=>'count_order_by_month_date',
            'Param1'=>date('Y-m-d',strtotime($this->input->post('datepicker1'))),
            'Param2'=>date('Y-m-d',strtotime($this->input->post('datepicker2'))),
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );

        $data['transamount'] = $this->supper_admin->call_procedure('proc_order_s',$paramater);
       //p($response['dataselect']);exit;
       }else{
     $param=array(
            'act_mode'=>'Total_order_agent',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['orderdata']= $this->supper_admin->call_procedureRow('proc_order_s',$param);

        $param=array(
            'act_mode'=>'last_transaction_amount',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['lastamount']= $this->supper_admin->call_procedureRow('proc_order_s',$param);

         $param=array(
            'act_mode'=>'getmonthtransationorder',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['transamount']= $this->supper_admin->call_procedure('proc_order_s',$param);

         $param=array(
            'act_mode'=>'getmonthtransationordercount',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['transamountcount']= $this->supper_admin->call_procedure('proc_order_s',$param);

         $param=array(
            'act_mode'=>'usertotalamount',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['totaluseramount']= $this->supper_admin->call_procedureRow('proc_order_s',$param);

             $param=array(
            'act_mode'=>'lastcreditamount',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['creditamount']= $this->supper_admin->call_procedureRow('proc_order_s',$param);
        }
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/partnerdashboards",$data);
        $this->load->view("agent/helper/footer");
  }
  
    //Promocode Process
	// check & validate promocode here

    public function checkpromocode(){


        $promocode = $this->input->post('promocode');
        $txtDepartDate =$this->session->userdata['txtDepartDate'];
        $uid = $this->input->post('uid');
        $totalhourmint=   getTimeSlotInArray($this->session->userdata['destinationType']);
        $txfromhrs = $totalhourmint[0];
        $txtfrommin =  $totalhourmint[1];
        $txttohrs =  $totalhourmint[2];
        $txttomin = $totalhourmint[3];
        $txtfromd = $totalhourmint[4];
        $txttod = $this->input->post('txttod');
        $type = $this->input->post('type');
        $totalprice = $this->session->userdata['final_cost_package_addon_handling'];


        $cartaddoneiddata = $this->input->post('cartaddoneiddata');
        $checkpromo = array('act_mode'=>'checkpromocode',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>$promocode,
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
        );
        $checkPromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $checkpromo);
        if($checkPromocode->cnt>0){
            // Check Start date and end date exits promocode
            $checkpromodate = array('act_mode'=>'datevalidation',
                'Param1'=>'',
                'Param2'=>'',
                'Param3'=>date('Y-m-d',strtotime($txtDepartDate)),
                'Param4'=>'',
                'Param5'=>'',
                'Param6'=>'',
                'Param7'=>'',
                'Param8'=>'',
                'Param9'=>'',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
            );
            $datepromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $checkpromodate);
            if($datepromocode->pcnt>0){
                //Check user type exit;
                $parameter1 = array('act_mode'=>'get_user_detail_agent',
                    'Param1'=>'',
                    'Param2'=>$uid,
                    'Param3'=>'',
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                );

                $getudetail = $this->supper_admin->call_procedurerow('proc_promocode_s', $parameter1);
                $promocode1 = array('act_mode'=>'checkpromode_user_type',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>$promocode,
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                );
                $promocodedetail = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocode1);
                //Check Promo User Type Exits  
              /*  p($getudetail->user_usertype);  p($promocodedetail->p_usertype);exit;*/
                $checkusertype = $this->checkusertype('Agent',$promocodedetail->p_usertype);
                if($checkusertype==1){
                    //Check Promo date time exits
                    $promocodedate = array('act_mode'=>'checkpromode_date_time',
                        'Param1'=>  '',
                        'Param2'=>  '',
                        'Param3'=>  date('Y-m-d',strtotime($txtDepartDate)),
                        'Param4'=>  '',
                        'Param5'=>  '',
                        'Param6'=>  $promocodedetail->promo_id,
                        'Param7'=>  $txfromhrs,//"4"
                        'Param8'=>  $txtfrommin,//"04"
                        'Param9'=>  $txttohrs,//"8"
                        'Param10'=> $txttomin,//"08"
                        'Param11'=> '',
                        'Param12'=> '',
                    );
                    $promocodedatetime = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodedate);
                    if(!empty($promocodedatetime->promopackagedailyinventory_id)){
                        //Check promocode count by based inventry id
                        $promocode1 = array('act_mode'=>'checkpromocodecount_inventery',
                            'Param1'=>'',
                            'Param2'=>'',
                            'Param3'=>$promocodedetail->promo_id,
                            'Param4'=>'',
                            'Param5'=>$promocodedatetime->promopackagedailyinventory_id,
                            'Param6'=>'',
                            'Param7'=>'',
                            'Param8'=>'',
                            'Param9'=>'',
                            'Param10'=>'',
                            'Param11'=>'',
                            'Param12'=>'',
                        );
                        $getcountpromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocode1);
                        if(($promocodedatetime->promopackage_timesallowed>$getcountpromocode->cntpromo) || ($promocodedatetime->promopackage_timesallowed==0)){
                            //Check user promocode count by based inventry id
                            $promocodeuser = array('act_mode'=>'checkpromocodecount_user_inventery',
                                'Param1'=>'',
                                'Param2'=>'',
                                'Param3'=>$promocodedetail->promo_id,
                                'Param4'=>$uid,
                                'Param5'=>$promocodedatetime->promopackagedailyinventory_id,
                                'Param6'=>'',
                                'Param7'=>'',
                                'Param8'=>'',
                                'Param9'=>'',
                                'Param10'=>'',
                                'Param11'=>'',
                                'Param12'=>'',
                            );
                            $getcountpromocodeuser = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodeuser);
                            if(($promocodedatetime->promopackage_peruserallowed>$getcountpromocodeuser->cntpromouid) || ($promocodedatetime->promopackage_peruserallowed==0)){
                                //echo 'dcsdcs';
                                $getflatprice = $this->getpromocodepriceflat($totalprice,$promocodedetail->p_type,$promocodedetail->p_discount);

                                $getpromocodeprice = $this->getpromocodepricecart($getflatprice,$cartaddoneiddata,$promocodedetail->p_addonedata);

                                /* $promocodeuser = array('act_mode'=>'getaddonlist',
                                          'Param1'=>'',
                                          'Param2'=>'',
                                          'Param3'=>$promocodedetail->p_addonedata,
                                          'Param4'=>'',
                                          'Param5'=>'',
                                          'Param6'=>'',
                                          'Param7'=>'',
                                          'Param8'=>'',
                                          'Param9'=>'',
                                          'Param10'=>'',
                                          'Param11'=>'',
                                          'Param12'=>'',
                                       );
                               $getcountpromocodeuser = $this->supper_admin->call_procedure('proc_promocode_s', $promocodeuser);
                                 foreach($getcountpromocodeuser as $pvalue){
                                  $dataList[] = $pvalue->addon_name;
                                 }

                                  $addon = implode(',',$dataList);
                                  $discountype = $promocodedetail->p_type;
                                  $dicountvalue = $promocodedetail->p_discount;
                                  $getpromocodeprice = $getpromocodeprice;
                                  //echo '<p>Free: - </p>(<span>'.$addon.'</span>),'.$discountype.'-'.$dicountvalue;
                                 echo $getpromocodeprice;

                                 }*/

                                // echo $getpromocodeprice;
                                //saving price
                                $savingpromoprice= $totalprice-$getpromocodeprice;
                                //Total Price
                                $data = array(
                                    'total_promo_price' => $getpromocodeprice,
                                    'promo_id' =>$promocodedetail->promo_id,
                                    'saving_price_promocode' =>$savingpromoprice,
                                    'promocode_name' =>$promocodedetail->p_codename
                                );
                                //p($data);
                                $this->session->set_userdata($data);
                                echo 1;
                            }else{
                                echo 0;
                            }
                        }else
                            echo 0;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                } }else{
                echo 0;
            } }else{
            echo 0;
        }
    }




    public function getpromocodepricecart($flatprice,$cardid,$addonpromid){

        //Get addon price
        $getcardexpprice =explode(',', $cardid);
        $getpromoexpprice =explode(',', $addonpromid);
        foreach($getpromoexpprice as $value){
            if(in_array($value, $getcardexpprice)){
                $promocodeuser = array('act_mode'=>'getaddonlist',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>$value,
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                );
                $getcountpromocodeuser = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodeuser);
                $getaddonprices += $getcountpromocodeuser->addon_price;
            }
        }
        $getaddonprice  = ($getaddonprices) ? $getaddonprices : 0;
        $finalprice = $flatprice-$getaddonprices;
        return $finalprice;
    }

    public function checkusertype($usertype,$promotype){
        if($promotype ==3 && $usertype =='Agent'){
            return 1;
        }

    }

    public function getpromocodepriceflat($price,$discountype,$discount_param){
        $getpromoprice='';
        $agent_discount_value = trim( $discount_param );
        if( $discountype == 'flat' ){
            $getpromoprice = ( $price - $agent_discount_value );
            return  $getpromoprice;
        }

        if($discountype =='percent'){

            $getpromoprice = ($price * ($agent_discount_value / 100));
            $getpromoprice = ($price - $getpromoprice);
            return $getpromoprice;
        }
    }

  
 

}//end of class
?>
