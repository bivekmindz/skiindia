<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Summary extends MX_Controller {

    public function __construct() {
        $this->load->model("supper_admin");
        $this->load->library('session');
         session_start();
        
    }



    public function summary(){
		
        if($this->session->userdata('packageval')==''){
            redirect(base_url());
        } else {
//Select branch

            if( !empty( $this->session->userdata('ccavenue_param') ) ){  //echo 1;

                $this->session->unset_userdata('ccavenue_param');
            }


            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);



//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);


            //CCAvenue
            $parameter = array('act_mode'=>'select_order',

                'orderid'=>$this->session->userdata('orderlastinsertid'),
                'type'=>'web',

            );
            $path1 = api_url()."Cart/getpayment_package/format/json/";
            $data['paymentpac']= curlpost($parameter,$path1);

            $parameterccgatway=array(
                'act_mode' =>'selectccavenue',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
            $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

//Select Time slot
            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path);

            $grand_total = []; // final price

            // unset last handling charge AND OTHER VARIABLE -  controller on load
            if ( !empty($this->session->userdata('handling_charge_with_no_of_person') ) ){

                $this->session->unset_userdata('handling_charge_with_no_of_person');
            }

            /*if( !empty( $this->session->userdata('total_promo_price') ) ) {

                $this->session->unset_userdata('total_promo_price');
            }
*/
            if( !empty( $this->session->userdata('final_cost_package_addon_handling') ) ) {

                $this->session->unset_userdata('final_cost_package_addon_handling');
            }

            if( !empty( $this->session->userdata('handling_charge_with_no_of_person') ) ) {

                $this->session->unset_userdata('handling_charge_with_no_of_person');
            }


            $data['internethandlingcharge'] = ($data['branch']->branch_internet_handling_charge);

            $data['final_selected_package_data'] = $this->session->userdata('final_selected_package_data');

            $get_package_price_qty_array = (object) getPackageTotalPrice( $data['final_selected_package_data'] );

            $data['package_total_price'] = $get_package_price_qty_array->package_price_array;

            // no. of visitors
            $data['package_total_qty'] = $get_package_price_qty_array->package_qty_array;

            // get revised internet handling charge

            $data['internethandlingcharge_revised'] = $grand_total[] = getHandlingChargeByNoOfPerson( $data['internethandlingcharge'], getSumAllArrayElement( $data['package_total_qty'] ) );

            // add internet handling charge by no of person
            $this->session->set_userdata('handling_charge_with_no_of_person', $data['internethandlingcharge_revised']);

            // send hr min time slot to view page
            $data['to_render_array'] = getTimeSlotInArray( $this->session->userdata('destinationType') );

            // revised price
            $grand_total[] = $data['package_total_price'] = getSumAllArrayElement( calculatePriceByQty( $data['package_total_price'], $data['package_total_qty'] ) );

            // no. of visitors revised send to view file
            $data['package_total_qty'] = getSumAllArrayElement( $data['package_total_qty'] );



            $data['final_selected_addon_data'] = $this->session->userdata( 'final_selected_addon_data');
//p( $this->session->userdata('final_selected_addon_data') );

            $get_price_qty_array = (object) getAddonkeyValue( $data['final_selected_addon_data'] );

            $data['addon_total_price'] = $grand_total[] = getSumAllArrayElement( calculatePriceByQty( $get_price_qty_array->addon_price_array, $get_price_qty_array->addon_qty_array ) );
            $data['addon_total_qty'] = $addon_total_qty = getSumAllArrayElement($get_price_qty_array->addon_qty_array );

            // grand cost - (package + addon + handling charge )

            $data['grand_total'] = getSumAllArrayElement( $grand_total );

            // add final cost - (package + addon + handling charge ) to session
            $this->session->set_userdata( [ 'final_cost_package_addon_handling' =>  $data['grand_total'] ] );


//remove promocode

            if($_POST['removepromocode']=='removepromocode')
            {
                if($this->session->userdata('pp_addonedata')!='')
                {

                    $promodate=explode(",",$this->session->userdata('pp_addonedata'));

                    foreach($promodate as $v)
                    {

                        $parameter4 = array('act_mode' => 'test_del',
                            'Param1' => $v,
                            'Param2' => $this->session->userdata['uniqid'],
                            'Param3' => '',
                            'Param4' => '',
                            'Param5' => '',
                            'Param6' => '',
                            'Param7' => '',
                            'Param8' => '',
                            'Param9' => '');
                        //pend($parameter);
                        $response['vieww_n'] = $this->supper_admin->call_procedurerow('proc_addon_s', $parameter4);




                    }
                    $parameter4 = array('act_mode' => 'addonselectbyuniqid',
                        'categories1' => '',
                        'addonid' => '',
                        'addonprice' =>'',
                        'addonqty' => '',
                        'addonimage' => '',
                        'addonname' => '',
                        'uniqueid' => $this->session->userdata['uniqid'],);

                    $response['selectpromoval'] = $this->supper_admin->call_procedurerow('proc_tempaddoneadd_v', $parameter4);



                    $data = array(


                        'cartaddoneid' =>  $response['selectpromoval']->addonid,
                        'cartaddoneimage' =>  $response['selectpromoval']->addonimage,
                        'cartaddonname' =>  $response['selectpromoval']->addonname,
                        'cartaddonqty' =>  $response['selectpromoval']->addonqty,
                        'cartaddonprice' =>  $response['selectpromoval']->addonprice

                    );

                    $this->session->set_userdata($data);

                    $this->session->unset_userdata('ppromo_id');
                    $this->session->unset_userdata('ppcodename');
                    $this->session->unset_userdata('pp_type');
                    $this->session->unset_userdata('pp_addonedata');
                    $this->session->unset_userdata('pp_locationid');
                    $this->session->unset_userdata('pp_branchid');
                    $this->session->unset_userdata('pp_discount');
                    $this->session->unset_userdata('pp_status');
                    $this->session->unset_userdata('pp_expiry_date');
                    $this->session->unset_userdata('pp_allowed_per_user');



                    $this->session->unset_userdata('pp_start_date');
                    $this->session->unset_userdata('pp_allowed_times');
                    $this->session->unset_userdata('pp_createdon');
                    $this->session->unset_userdata('pp_modifiedon');
                    $this->session->unset_userdata('pp_logintype');
                    $this->session->unset_userdata('pp_addoneqty');
                    redirect("summary");

                }
                else{
                    $this->session->unset_userdata('ppromo_id');
                    $this->session->unset_userdata('ppcodename');
                    $this->session->unset_userdata('pp_type');
                    $this->session->unset_userdata('pp_addonedata');
                    $this->session->unset_userdata('pp_locationid');
                    $this->session->unset_userdata('pp_branchid');
                    $this->session->unset_userdata('pp_discount');
                    $this->session->unset_userdata('pp_status');
                    $this->session->unset_userdata('pp_expiry_date');
                    $this->session->unset_userdata('pp_allowed_per_user');



                    $this->session->unset_userdata('pp_start_date');
                    $this->session->unset_userdata('pp_allowed_times');
                    $this->session->unset_userdata('pp_createdon');
                    $this->session->unset_userdata('pp_modifiedon');
                    $this->session->unset_userdata('pp_logintype');
                    $this->session->unset_userdata('pp_addoneqty');
                    $this->session->unset_userdata('internethandlingcharge');


                    redirect("summary");
                }

            }


            //Sachin ------------------
// Promocode
            if($_POST['promoFormdata']!='')
            {
                $epromodate=explode("/",$this->session->userdata('txtDepartDate'));
                $promodatevalda=($epromodate[1]."/".$epromodate[0]."/".$epromodate[2]);
                $parameter         = array('act_mode'=>'promoselect_package','promovalue'=>$_POST['promoFormdata'],'locationid'=>$this->session->userdata['locationid'],
                    'branch_id'=>$this->session->userdata['branch_id'],
                    'userid'=>$promodatevalda,
                );
                $path = api_url()."Cart/getpromoselect_package/format/json/";
                $data['promocode']= curlpost($parameter,$path);p( $data['promocode']);exit;
                if($data['promocode']->scalar=='Something Went Wrong'){ $data['msg']="Invalid Promocode"; }
                else
                {
                    $dateses=explode("/",$data['promocode']->p_expiry_date);
                    $datepromo=explode("/",rtrim($this->session->userdata('txtDepartDate')," "));
                    $date2=date_create($dateses[2]."-".$dateses[0]."-".$dateses[1]);
                    $date1=date_create($datepromo[2]."-".$datepromo[1]."-".$datepromo[0]);
                    $diff=date_diff($date1,$date2);
                    $datesesval= $diff->format("%R%a");


                    if($datesesval>=0) {

                        if ($data['promocode']->p_addonedata != '') {


//p($this->session->userdata);
                            $promodate = explode(",", $data['promocode']->p_addonedata);

                            foreach ($promodate as $v) {

                                $data13 = array(
                                    'act_mode' => 'selectaddone',
                                    'categories1' => '',
                                    'addonid' => $v,
                                    'addonprice' => '',
                                    'addonqty' => '',
                                    'addonimage' => '',
                                    'addonname' => '',
                                    'uniqueid' => $this->session->userdata['uniqid'],
                                    'type' => 'web');
                                $pathaddone13 = api_url() . "Cart/insertaddonscartses_select/format/json/";
                                $data['addonscartselectd'] = curlpost($data13, $pathaddone13);


                                $arr = (array)$data['addonscartselectd'];

                                if($data['addonscartselectd']->scalar != "Something Went Wrong") {
                                    $parameter4 = array('act_mode' => 'addonviewupdate',
                                        'Param1' => $v,
                                        'Param2' => '',
                                        'Param3' => '',
                                        'Param4' => '',
                                        'Param5' => '',
                                        'Param6' => '',
                                        'Param7' => '',
                                        'Param8' => '',
                                        'Param9' => '');
                                    //pend($parameter);
                                    $response['vieww'] = $this->supper_admin->call_procedurerow('proc_addon_s', $parameter4);

                                    $data[$i] = array(
                                        'act_mode' => 'updateaddonedata',
                                        'categories1' => $response['vieww']->addon_id,
                                        'addonid' => $response['vieww']->addon_id,
                                        'addonprice' => $response['vieww']->addon_price,
                                        'addonqty' => '',
                                        'addonimage' => $response['vieww']->addon_image,
                                        'addonname' => $response['vieww']->addon_name,
                                        'uniqueid' => $this->session->userdata['uniqid'],
                                        'type' => 'web'
                                    );

                                    $pathaddone = api_url() . "Cart/insertaddonscartses_select/format/json/";
                                    $data['addonscart'] = curlpost($data[$i], $pathaddone);

                                } else {


                                    $parameter4 = array('act_mode' => 'addonviewupdate',
                                        'Param1' => $v,
                                        'Param2' => '',
                                        'Param3' => '',
                                        'Param4' => '',
                                        'Param5' => '',
                                        'Param6' => '',
                                        'Param7' => '',
                                        'Param8' => '',
                                        'Param9' => '');
                                    //pend($parameter);
                                    $response['vieww'] = $this->supper_admin->call_procedurerow('proc_addon_s', $parameter4);

                                    $data = array(
                                        'act_mode' => 'insertaddone',
                                        'categories1' => $response['vieww']->addon_id,
                                        'addonid' => $response['vieww']->addon_id,
                                        'addonprice' => 0,
                                        'addonqty' => 1,
                                        'addonimage' => $response['vieww']->addon_image,
                                        'addonname' => $response['vieww']->addon_name,
                                        'uniqueid' => $this->session->userdata['uniqid'],
                                        'type' => 'web'
                                    );

                                    $pathaddone = api_url() . "Cart/insertaddonscartses_select/format/json/";
                                    $data['addonscart'] = curlpost($data, $pathaddone);
                                }
                            }

                            $data1 = array(
                                'act_mode' => 'addonselectbyuniqid',
                                'categories1' => '',
                                'addonid' => '',
                                'addonprice' => '',
                                'addonqty' => '',
                                'addonimage' => '',
                                'addonname' => '',
                                'uniqueid' => $this->session->userdata['uniqid'],

                            );

                            $data['vieww1'] = $this->supper_admin->call_procedurerow('proc_tempaddoneadd_v', $data1);

                            $data = array(


                                'cartaddoneid' => $data['vieww1']->addonid,
                                'cartaddoneimage' => $data['vieww1']->addonimage,
                                'cartaddonname' => $data['vieww1']->addonname,
                                'cartaddonqty' => $data['vieww1']->addonqty,
                                'cartaddonprice' => $data['vieww1']->addonprice,

                            );
                            $this->session->set_userdata($data);
                            $siteurl = base_url();
                            $parameterbranch = array(
                                'act_mode' => 'selectbranch',
                                'weburl' => $siteurl,
                                'type' => 'web',

                            );

                            $path = api_url() . 'selectsiteurl/branch/format/json/';
                            $data['branch'] = curlpost($parameterbranch, $path);

                            $parameter = array('act_mode' => 'promoselect_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => $this->session->userdata['locationid'],
                                'branch_id' => $this->session->userdata['branch_id'],
                                'userid' => $this->session->userdata['userid'],
                            );
                            $path = api_url() . "Cart/getpromoselect_package/format/json/";
                            $data['promocode'] = curlpost($parameter, $path);

                            $data = array(
                                'ppromo_id' => $data['promocode']->promo_id,
                                'ppcodename' => $data['promocode']->p_codename,
                                'pp_type' => $data['promocode']->p_type,
                                'pp_addonedata' => $data['promocode']->p_addonedata,
                                'pp_locationid' => $data['promocode']->p_locationid,
                                'pp_branchid' => $data['promocode']->p_branchid,
                                'pp_discount' => $data['promocode']->p_discount,
                                'pp_status' => $data['promocode']->p_status,
                                'pp_expiry_date' => $data['promocode']->p_expiry_date,
                                'pp_allowed_per_user' => $data['promocode']->p_allowed_per_user,
                                'pp_start_date' => $data['promocode']->p_start_date,
                                'pp_allowed_times' => $data['promocode']->p_allowed_times,
                                'pp_createdon' => $data['promocode']->p_createdon,
                                'pp_modifiedon' => $data['promocode']->p_modifiedon,
                                'pp_logintype' => $data['promocode']->p_logintype,
                                'pp_addoneqty' => $data['promocode']->p_addoneqty,
                                'internethandlingcharge' => $data['branch']->branch_internet_handling_charge,
                            );
                            $this->session->set_userdata($data);
                            redirect("summary");
                            //  echo "dddd"; exit;
                        } else {
                            $originalDate = $this->session->userdata('txtDepartDate');
                            $Date = explode("/", $this->session->userdata('txtDepartDate'));
                            $newDate = $Date[1] . "/" . $Date[0] . "/" . $Date[2];
                            if ($data['promocode']->p_logintype == '0' or $data['promocode']->p_logintype == '1') {

//echo $newDate."---".$data['promocode']->p_start_date."---".$data['promocode']->p_expiry_date;



                                //Complete Promo Select
                                $parametercompuser = array('act_mode' => 'promoselectcompuser_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => '',
                                    'branch_id' => '',
                                    'userid' => '',
                                );
                                $pathcompuser = api_url() . "Cart/getpromoselect_packageuser/format/json/";
                                $data['promocompuser'] = curlpost($parametercompuser, $pathcompuser);


                                if ($data['promocompuser']->num < $data['promocode']->p_allowed_times) {
//Userwise Promo select

                                    $parameteruser = array('act_mode' => 'promoselectuser_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => '',
                                        'branch_id' => '',
                                        'userid' => $this->session->userdata('skiindia'),
                                    );
                                    $pathuser = api_url() . "Cart/getpromoselect_packageuser/format/json/";
                                    $data['promocodeuser'] = curlpost($parameteruser, $pathuser);


                                    $data = array(
                                        'ppromo_id' => $data['promocode']->promo_id,
                                        'ppcodename' => $data['promocode']->p_codename,
                                        'pp_type' => $data['promocode']->p_type,
                                        'pp_addonedata' => $data['promocode']->p_addonedata,
                                        'pp_locationid' => $data['promocode']->p_locationid,
                                        'pp_branchid' => $data['promocode']->p_branchid,
                                        'pp_discount' => $data['promocode']->p_discount,
                                        'pp_status' => $data['promocode']->p_status,
                                        'pp_expiry_date' => $data['promocode']->p_expiry_date,
                                        'pp_allowed_per_user' => $data['promocode']->p_allowed_per_user,
                                        'pp_start_date' => $data['promocode']->p_start_date,
                                        'pp_allowed_times' => $data['promocode']->p_allowed_times,
                                        'pp_createdon' => $data['promocode']->p_createdon,
                                        'pp_modifiedon' => $data['promocode']->p_modifiedon,
                                        'pp_logintype' => $data['promocode']->p_logintype,
                                        'pp_addoneqty' => $data['promocode']->p_addoneqty,
                                        'internethandlingcharge' => $data['branch']->branch_internet_handling_charge,
                                    );
                                    $this->session->set_userdata($data);
                                    // echo($this->session->userdata('txtDepartDate'));
                                } else {
                                    $data['msg'] = "Invalid Promocode";
                                }
                            } else {
                                $data['msg'] = "Invalid Promocode";
                            }
                        }
                    }   else
                    {
                        $data['msg'] = "Invalid Promocode";
                    }
                    // pend($data['promocode']->p_addonedata);
                }
            }
            else
            {
                $data['msg']="Promo Not Add";
            }


            if($_POST['submit']=='final add')
            {

//p( $data['grand_total'] );

// P( ( $this->session->userdata('saving_price_promocode') ) ? (float) $this->session->userdata('total_promo_price') : (float) $this->session->userdata('final_cost_package_addon_handling') ) ;



                $parameterunique         = array('act_mode'=>'selectbooking',
                    'subtotal'=>'',
                    'discountamount'=>'',
                    'total'=>'',
                    'countryid'=>'',
                    'stateid'=>'',
                    'cityid'=>'',
                    'branch_id'=>'',
                    'locationid'=>'',
                    'userid'=>'',
                    'txtDepartDate1'=>'',
                    'txtDepartdata'=>'',
                    'paymentmode'=>'',
                    'ticketid'=>$this->session->userdata['uniqid'],
                    'packproductname'=>'',
                    'packimg'=>'',
                    'packpkg'=>'',
                    'packprice'=>'',
                    'promocodeprice'=>'',
                    'internethandlingcharges'=>'',
                    'txtfrommin'=>'',
                    'txttohrs'=>'',
                    'txttomin'=>'',
                    'txtfromd'=>'',
                    'txttod'=>'',
                    'p_codename'=>'',
                    'p_usertype'=>'',
                    'type'=>'web',

                );

                $path = api_url()."Cart/getorderadd_package/format/json/";
                $data['packageunique']= curlpost($parameterunique,$path);

                if($this->session->userdata('skiindia')>0)
                {
                    $_POST['p_usertype']="User";

                }
                else{
                    $_POST['p_usertype']="Guest";

                }

// code start here
// p( $this->session->userdata );
                $hr_min_array = $final_selected_package_data = [];
                $txtDepartDate = $package_total_price = $package_total_qty = '';

                $hr_min_array = getTimeSlotInArray( $this->session->userdata('destinationType') ) ;
                $txtDepartDate = $this->session->userdata('txtDepartDate');
                $datefromn = $hr_min_array[0].':'.$hr_min_array[1].':'.'00';
                $datefrom= date('h', strtotime($datefromn));

                $datetoo = $hr_min_array[2].':'.$hr_min_array[3].':'.'00';
                $dateto= date('h', strtotime($datetoo));

//pend( $hr_min_array);
                // this give you price and qty array
                $final_selected_package_data = getPackageTotalPrice( $this->session->userdata('final_selected_package_data') );

                implode('#' , $final_selected_package_data['package_price_array'] );
                implode('#' , $final_selected_package_data['package_qty_array'] );
                implode('#' , $final_selected_package_data['package_name_array'] );
                implode('#' , $final_selected_package_data['get_package_img_array'] );


                // get only package price
                $package_total_price = $this->session->userdata('package_total_price') ? $this->session->userdata('package_total_price') : 0;

                // get total no. of visitor's
                $package_total_qty = $this->session->userdata('package_total_qty') ? $this->session->userdata('package_total_qty') : 0;

                $final_cost_package_addon_handling = $this->session->userdata('final_cost_package_addon_handling') ? $this->session->userdata('final_cost_package_addon_handling') : 0;

                $handling_charge_with_no_of_person = $this->session->userdata('handling_charge_with_no_of_person') ? $this->session->userdata('handling_charge_with_no_of_person') : 0;
                // promocode start here

                // if promocode is applied then discount price
                // else Final_cost with package_addon_handling
                $total_promo_price = $this->session->userdata('total_promo_price') ? $this->session->userdata('total_promo_price') : $final_cost_package_addon_handling;

                $data['grand_total'] = $total_promo_price;

                $promo_id = $this->session->userdata('promo_id') ? $this->session->userdata('promo_id') : 0;

                $saving_price_promocode = $this->session->userdata('saving_price_promocode') ? $this->session->userdata('saving_price_promocode') : 0;

                $promocode_name = $this->session->userdata('promocode_name') ? $this->session->userdata('promocode_name') : 0;

                $orderlastinsertid = $this->session->userdata('orderlastinsertid') ? $this->session->userdata('orderlastinsertid') : 0;

                $promocode_name = $this->session->userdata('promocode_name') ? $this->session->userdata('promocode_name') : 0;

                // promocode end here
// code end here
// ( $this->session->userdata('saving_price_promocode') ) ? (float) $this->session->userdata('total_promo_price') : (float) $this->session->userdata('final_cost_package_addon_handling') )

                if($data['packageunique']->scalar=='Something Went Wrong')
                {


                    $parameter         = array('act_mode'=>'orderpackage_insert',
                        'subtotal' => $final_cost_package_addon_handling,
                        'discountamount'=> ($saving_price_promocode),
                        'total'=> $total_promo_price,
                        'countryid'=> $this->session->userdata['countryid'] ? $this->session->userdata['countryid'] : '',
                        'stateid'=> $this->session->userdata['stateid'] ? $this->session->userdata['stateid'] : '',
                        'cityid'=> $this->session->userdata['cityid'] ? $this->session->userdata['cityid'] : '',
                        'branch_id'=> $this->session->userdata['branch_id'] ? $this->session->userdata['branch_id'] : '',
                        'locationid'=> $this->session->userdata['locationid'] ? $this->session->userdata['locationid'] : '',
                        'userid'=> $this->session->userdata('skiindia') ? $this->session->userdata('skiindia') : '',
                        'txtDepartDate1'=> $txtDepartDate,
                        'txtDepartdata' => $datefrom,
                        'paymentmode' =>'NULL',
                        'ticketid' => $this->session->userdata['uniqid'],

                        'packproductname' => implode('#' , $final_selected_package_data['package_name_array'] ),
                        'packimg'=> implode('#' , $final_selected_package_data['get_package_img_array'] ),
                        'packpkg'=> $package_total_qty ,
                        'packprice'=> $package_total_price ,

                        'promocodeprice'=>   $this->session->userdata('total_promo_price') ? (float) $this->session->userdata('total_promo_price') : 0 ,

                        'internethandlingcharges' => $handling_charge_with_no_of_person,

                        'txtfrommin' => $hr_min_array[1],
                        'txttohrs' => $dateto,
                        'txttomin' => $hr_min_array[3],
                        'txtfromd' => date('A' ,strtotime( $hr_min_array[0] .':'. $hr_min_array[1] ) ),
                        'txttod'=> date('A' ,strtotime( $hr_min_array[2] .':'. $hr_min_array[3] ) ),

                        'p_codename' => $promocode_name,
                        'p_usertype'=> $_POST['p_usertype'],

                        'type'=>'web',

                        'package_qty' => implode( '#' , $final_selected_package_data['package_qty_array'] ),
                        'package_price' => implode( '#' , $final_selected_package_data['package_price_array'] ),

                    );

                    $path = api_url()."Cart/getorderadd_package/format/json/";
                    $data['packageadd']= curlpost($parameter,$path);
                }
                else
                {


                    $parameter = array('act_mode'=>'orderpackage_update',

                        'subtotal' => $final_cost_package_addon_handling,
                        'discountamount'=> ($saving_price_promocode),
                        'total'=> $total_promo_price,
                        'countryid'=> $this->session->userdata['countryid'] ? $this->session->userdata['countryid'] : '',
                        'stateid'=> $this->session->userdata['stateid'] ? $this->session->userdata['stateid'] : '',
                        'cityid'=> $this->session->userdata['cityid'] ? $this->session->userdata['cityid'] : '',
                        'branch_id'=> $this->session->userdata['branch_id'] ? $this->session->userdata['branch_id'] : '',
                        'locationid'=> $this->session->userdata['locationid'] ? $this->session->userdata['locationid'] : '',
                        'userid'=> $this->session->userdata('skiindia') ? $this->session->userdata('skiindia') : '',
                        'txtDepartDate1'=> $txtDepartDate,
                        'txtDepartdata' => $datefrom,
                        'paymentmode' =>'NULL',
                        'ticketid' => $this->session->userdata['uniqid'],

                        'packproductname' => implode('#' , $final_selected_package_data['package_name_array'] ),
                        'packimg' => implode('#' , $final_selected_package_data['get_package_img_array'] ),
                        'packpkg' => $package_total_qty ,
                        'packprice' => $package_total_price ,

                        'promocodeprice' => $_POST['discountamount'],

                        'internethandlingcharges' => $handling_charge_with_no_of_person,
                        'txtfrommin' => $hr_min_array[1],
                        'txttohrs' => $dateto,
                        'txttomin' => $hr_min_array[3],
                        'txtfromd' => date('A' ,strtotime( $hr_min_array[0] .':'. $hr_min_array[1] ) ),
                        'txttod'=> date('A' ,strtotime( $hr_min_array[2] .':'. $hr_min_array[3] ) ),
                        'p_codename' => $promocode_name,
                        'p_usertype' => $_POST['p_usertype'],
                        'type' => 'web',
                        'package_qty' => implode('#' , $final_selected_package_data['package_qty_array'] ),
                        'package_price' => implode('#' , $final_selected_package_data['package_price_array'] ),

                    );

                    $path = api_url()."Cart/getorderadd_package/format/json/";
                    $data['packageadd'] = curlpost($parameter,$path);

                }


//p( $parameter );
//				exit;








                $arr = (array)$data['packageadd'];


                $parameterdelete = array('act_mode'=>'orderaddone_delete',

                    'addonnameses'=>'',
                    'addonquantityses'=>'',
                    'addonidses'=>'',
                    'lastidses'=>$arr[0]['last_insert_id()'],
                    'addonpriceses'=>'',
                    'addonidses'=>'',
                    'ccartpackageimage'=>'',

                );
                $path1 = api_url()."Cart/getaddonadd_package/format/json/";
                $data['promoaddonedelete']= curlpost($parameterdelete,$path1);


                foreach($this->session->userdata('final_selected_addon_data') as $k => $v){

//echo (count($this->session->userdata['final_selected_addon_data'])) ;



                    //echo "hello";

                    $parameter1 = array('act_mode'=>'orderaddone_insert',

                        'addonnameses'=>$v['addon_name'],
                        'addonquantityses'=>$v['qty'],
                        'addonidses'=>$v['addon_id'],
                        'lastidses'=>$arr[0]['last_insert_id()'],
                        'addonpriceses'=>$v['addon_price'],
                        'addonidses'=>$v['addon_id'],
                        'ccartpackageimage'=>$v['addon_image'],

                    );
                    $path1 = api_url()."Cart/getaddonadd_package/format/json/";
                    $data['promoaddoneadd']= curlpost($parameter1,$path1);


                }

                //promoadd


                $parameterpromo = array('act_mode'=>'orderaddone_insert',
                    'promoname'=>$this->session->userdata['ppcodename'],

                    'lastidses'=>$arr[0]['last_insert_id()'],


                );
                $response['vieww_n'] = $this->supper_admin->call_procedurerow('proc_promodata_s', $parameterpromo);


                $data = array(
                    'orderlastinsertid' => $arr[0]['last_insert_id()'],
                );

                $this->session->set_userdata($data);

                $data['skiindases'] =($data['login']->user_id);

                //    redirect(base_url()."payment");
                //CC PAyment
                $siteurl= base_url();
                $parameterbranch=array(
                    'act_mode' =>'selectbranch',
                    'weburl' =>$siteurl,
                    'type'=>'web',

                );

                $path=api_url().'selectsiteurl/branch/format/json/';
                $data['branch']=curlpost($parameterbranch,$path);


                $parameter=array(
                    'act_mode' =>'memusersesiid',
                    'userid' =>($this->session->userdata['skiindia']>0) ? $this->session->userdata['skiindia'] : $this->session->userdata['skiindia_guest'],
                    'type'=>'web',
                );

                $path=api_url().'userapi/usersesion/format/json/';
                $data['memuser']=curlpost($parameter,$path);


                $parameter=array(
                    'act_mode'=>'ViewCountrys',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>'',
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                    'Param13'=>'',
                    'Param14'=>'',
                    'Param15'=>'',
                );


                $data['Countrys'] = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);

                $parameterccgatway=array(
                    'act_mode' =>'selectccavenue',
                    'branchid' =>$data['branch']->branch_id,
                    'type'=>'web',

                );

                $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
                $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

                $parameter = array('act_mode'=>'select_order',

                    'orderid'=>$this->session->userdata('orderlastinsertid'),
                    'type'=>'web',

                );
                $path1 = api_url()."Cart/getpayment_package/format/json/";
                $data['paymentpac']= curlpost($parameter,$path1);
                //  pend( $data['memuser']->user_id);
                // Userlog
                $parameter4=array(
                    'act_mode' =>'orderuserupdate',
                    'user_id' =>$data['memuser']->user_id,
                    'order_id'=>$data['paymentpac']->pacorderid,
                    'title' =>'Mr',

                    'billing_name' =>$data['memuser']->user_firstname."".$data['memuser']->user_lastname,
                    'billing_email' =>$data['memuser']->user_emailid,
                    'billing_tel'=>$data['memuser']->user_mobileno,
                    'billing_address' =>$data['memuser']->user_Address,
                    'billing_city'=>$data['memuser']->user_city,
                    'billing_state' =>$data['memuser']->user_state,

                    'billing_zip'=>$data['memuser']->user_zip,
                    'billing_country' =>$data['memuser']->user_country,

                    'type'=>'web',
                );

                $path=api_url().'userapi/userorderupdate/format/json/';
                $data['userregister']=curlpost($parameter4,$path);
                //  pend($parameter4);
                $data = array(
                    'tid' => '12345',
                    'title' => '',
                    'billing_name' => $data['memuser']->user_firstname.' '.$memuser->user_lastname,
                    'billing_email' => $data['memuser']->user_emailid,
                    'billing_tel' => $data['memuser']->user_mobileno,
                    'billing_address' => $data['memuser']->user_Address,
                    'billing_city' => $data['memuser']->user_city,
                    'billing_state' => $data['memuser']->user_state,
                    'billing_zip' => $data['memuser']->user_zip,
                    'billing_country' => 'India',
                    'merchant_id' => $data['ccavRequestHandler']->pg_merchant_id,
                    'order_id' =>$data['ccavRequestHandler']->pg_prefix.'_'.$data['paymentpac']->pacorderid,
                    'order_idval' => $data['paymentpac']->pacorderid,
                    'user_id' => $data['memuser']->user_id,
                    'amount' => $data['paymentpac']->total,
                    'currency' =>$data['ccavRequestHandler']->pg_currency,
                    'redirect_url' => $data['ccavRequestHandler']->pg_sucess_link,
                    'cancel_url' => $data['ccavRequestHandler']->pg_fail_link,
                    'language' => $data['ccavRequestHandler']->pg_language,

                );
//pend($data);
                $this->session->set_userdata('ccavenue_param', $data);





                redirect(base_url()."ccavRequestHandler");
                ?>


                <?php

                //   header("location:".base_url()."payment");




            }
            //  echo $this->session->userdata('skiindia'); exit;
            $data['internethandlingcharge']=($data['branch']->branch_internet_handling_charge);
            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("summary",$data);
            $this->load->view("helper/footer");
        }}


    //Session Unset
    public function resetsession(){

        echo $this->session->unset_userdata('total_promo_price');

    }


    //Promocode Process

    public function checkpromocode(){


        $promocode = $this->input->post('promocode');
        $txtDepartDate =$this->session->userdata['txtDepartDate'];
        $uid = $this->input->post('uid');
        $totalhourmint=   getTimeSlotInArray($this->session->userdata['destinationType']);
        $txfromhrs = $totalhourmint[0];
        $txtfrommin =  $totalhourmint[1];
        $txttohrs =  $totalhourmint[2];
        $txttomin = $totalhourmint[3];
        $txtfromd = $totalhourmint[4];
        $txttod = $this->input->post('txttod');
        $type = $this->input->post('type');
        $totalprice = $this->session->userdata['final_cost_package_addon_handling'];


        $cartaddoneiddata = $this->input->post('cartaddoneiddata');
// p($ccartpackageimage);p($ccartaddonname);p($ccartaddonprice);exit;
//Check Promocode Condition Exits
        $checkpromo = array('act_mode'=>'checkpromocode',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>$promocode,
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
        );
        $checkPromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $checkpromo);
        if($checkPromocode->cnt>0){
            // Check Start date and end date exits promocode
            $checkpromodate = array('act_mode'=>'datevalidation',
                'Param1'=>'',
                'Param2'=>'',
                'Param3'=>date('Y-m-d',strtotime($txtDepartDate)),
                'Param4'=>'',
                'Param5'=>'',
                'Param6'=>'',
                'Param7'=>'',
                'Param8'=>'',
                'Param9'=>'',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
            );
            $datepromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $checkpromodate);
            if($datepromocode->pcnt>0){
                //Check user type exit;
                $parameter1 = array('act_mode'=>'get_user_detail',
                    'Param1'=>'',
                    'Param2'=>$uid,
                    'Param3'=>'',
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                );

                $getudetail = $this->supper_admin->call_procedurerow('proc_promocode_s', $parameter1);
                $promocode1 = array('act_mode'=>'checkpromode_user_type',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>$promocode,
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                );
                $promocodedetail = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocode1);
                //Check Promo User Type Exits
                $checkusertype = $this->checkusertype($getudetail->user_usertype,$promocodedetail->p_usertype);
                if($checkusertype==1){
                    //Check Promo date time exits
                    $promocodedate = array('act_mode'=>'checkpromode_date_time',
                        'Param1'=>  '',
                        'Param2'=>  '',
                        'Param3'=>  date('Y-m-d',strtotime($txtDepartDate)),
                        'Param4'=>  '',
                        'Param5'=>  '',
                        'Param6'=>  $promocodedetail->promo_id,
                        'Param7'=>  $txfromhrs,//"4"
                        'Param8'=>  $txtfrommin,//"04"
                        'Param9'=>  $txttohrs,//"8"
                        'Param10'=> $txttomin,//"08"
                        'Param11'=> '',
                        'Param12'=> '',
                    );
                    $promocodedatetime = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodedate);
                    if(!empty($promocodedatetime->promopackagedailyinventory_id)){
                        //Check promocode count by based inventry id
                        $promocode1 = array('act_mode'=>'checkpromocodecount_inventery',
                            'Param1'=>'',
                            'Param2'=>'',
                            'Param3'=>$promocodedetail->promo_id,
                            'Param4'=>'',
                            'Param5'=>$promocodedatetime->promopackagedailyinventory_id,
                            'Param6'=>'',
                            'Param7'=>'',
                            'Param8'=>'',
                            'Param9'=>'',
                            'Param10'=>'',
                            'Param11'=>'',
                            'Param12'=>'',
                        );
                        $getcountpromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocode1);
                        if(($promocodedatetime->promopackage_timesallowed>$getcountpromocode->cntpromo) || ($promocodedatetime->promopackage_timesallowed==0)){
                            //Check user promocode count by based inventry id
                            $promocodeuser = array('act_mode'=>'checkpromocodecount_user_inventery',
                                'Param1'=>'',
                                'Param2'=>'',
                                'Param3'=>$promocodedetail->promo_id,
                                'Param4'=>$uid,
                                'Param5'=>$promocodedatetime->promopackagedailyinventory_id,
                                'Param6'=>'',
                                'Param7'=>'',
                                'Param8'=>'',
                                'Param9'=>'',
                                'Param10'=>'',
                                'Param11'=>'',
                                'Param12'=>'',
                            );
                            $getcountpromocodeuser = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodeuser);
                          if($this->session->userdata('skiindia')>0)
                          {
                              if((($promocodedatetime->promopackage_peruserallowed>$getcountpromocodeuser->cntpromouid) || ($promocodedatetime->promopackage_peruserallowed==0)) && ($promocodedatetime->promopackage_peruserallowed !='')){
                                //echo 'dcsdcs';
                                $getflatprice = $this->getpromocodepriceflat($totalprice,$promocodedetail->p_type,$promocodedetail->p_discount);

                                $getpromocodeprice = $this->getpromocodepricecart($getflatprice,$cartaddoneiddata,$promocodedetail->p_addonedata);


                                // echo $getpromocodeprice;
                                //saving price
                                $savingpromoprice= $totalprice-$getpromocodeprice;
                                //Total Price
                                $data = array(
                                    'total_promo_price' => $getpromocodeprice,
                                    'promo_id' =>$promocodedetail->promo_id,
                                    'saving_price_promocode' =>$savingpromoprice,
                                    'promocode_name' =>$promocodedetail->p_codename
                                );
                                //p($data);
                                $this->session->set_userdata($data);
                                echo 1;
                            }
                             
                          }
                          elseif((($promocodedatetime->promopackage_peruserallowed>$getcountpromocodeuser->cntpromouid) || ($promocodedatetime->promopackage_peruserallowed==0)) ){
                                //echo 'dcsdcs';
                                $getflatprice = $this->getpromocodepriceflat($totalprice,$promocodedetail->p_type,$promocodedetail->p_discount);

                                $getpromocodeprice = $this->getpromocodepricecart($getflatprice,$cartaddoneiddata,$promocodedetail->p_addonedata);


                                // echo $getpromocodeprice;
                                //saving price
                                $savingpromoprice= $totalprice-$getpromocodeprice;
                                //Total Price
                                $data = array(
                                    'total_promo_price' => $getpromocodeprice,
                                    'promo_id' =>$promocodedetail->promo_id,
                                    'saving_price_promocode' =>$savingpromoprice,
                                    'promocode_name' =>$promocodedetail->p_codename
                                );
                                //p($data);
                                $this->session->set_userdata($data);
                                echo 1;
                            }
                            
                            
                            
                            
                            
                            else{
                                echo 0;
                            }
                        }else
                            echo 0;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                } }else{
                echo 0;
            } }else{
            echo 0;
        }
    }




    public function getpromocodepricecart($flatprice,$cardid,$addonpromid){

        //Get addon price
        $getcardexpprice =explode(',', $cardid);
        $getpromoexpprice =explode(',', $addonpromid);
        foreach($getpromoexpprice as $value){
            if(in_array($value, $getcardexpprice)){
                $promocodeuser = array('act_mode'=>'getaddonlist',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>$value,
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                );
                $getcountpromocodeuser = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodeuser);
                $getaddonprices += $getcountpromocodeuser->addon_price;
            }
        }
        $getaddonprice  = ($getaddonprices) ? $getaddonprices : 0;
        $finalprice = $flatprice-$getaddonprices;
        return $finalprice;
    }

    public function checkusertype($usertype,$promotype){
        if($promotype ==0 && $usertype =='Guest'){
            return 1;
        }
        else if($promotype ==0 && $usertype =='User'){
            return 1;
        }
        else if($promotype ==1 && $usertype =='User'){
            return 1;
        }
        else if($promotype ==2 && $usertype =='Guest'){
            return 1;
        }else{
            return 0;
        }

    }









    public function getpromocodepriceflat($price,$discountype,$discount_param){
        $getpromoprice='';
        $agent_discount_value = trim( $discount_param );
        if( $discountype == 'flat' ){
            $getpromoprice = ( $price - $agent_discount_value );
            return  $getpromoprice;
        }

        if($discountype =='percent'){

            $getpromoprice = ($price * ($agent_discount_value / 100));
            $getpromoprice = ($price - $getpromoprice);
            return $getpromoprice;
        }
    }

}//end of class
?>