
 <div id="booking_container">
  <div class="snowEffect"></div>
  
  <div id="booking_bar">
    <div class="col-md-12">
      <form role="form" method="post" action="packages" autocomplete="off">
      <div class="col-md-2 bookbtn">
        <div class="form-group">
          <input type="submit" value="Book Now" class="animated fadeInUp btn_full" id="submit-booking">
          <span class="animated fadeInUp input-icon_nobg"><i class="icon-ticket"></i></span> 
        </div>
      </div>
      </form>
      <div class="col-md-2"> 
        <a href="<?php echo base_url();?>park_map.html" class="animated fadeInUp btn_2 button_intro outline"><i class="icon-map"></i> Park Map</a> 
      </div>
    </div>    
  </div>  
  <div class="iceWrap"></div>
</div>
   