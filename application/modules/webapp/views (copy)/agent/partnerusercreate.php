<form id="loginform" name="loginform" class="form_pa" method="post" action="" enctype="">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6">
                <div class="top_headadin">
                    <h1>Add Partner</h1>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="top_headadin1">
                    <a href=""><i class="glyphicon glyphicon-arrow-left"></i>  Back</a>
                </div>

            </div>

        </div>


        <div class="row">
            <div class='flashmsg'>

                <?php echo validation_errors(); ?>
                <?php

                echo $message;
                if($this->session->flashdata('message')){
                    echo $this->session->flashdata('message');
                }
                ?>
            </div>
            <div class="add-form">

                <h1> Basic Information</h1>
               

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Title</label>
                        <select class="form-control" name="title" required>
                            <option value="Mr">Mr.</option>
                            <option value="Mrs">Mrs.</option>
                            <option value="Ms">Ms.</option>
                        </select>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="first_name" class="form-control" placeholder="Enter First Name" pattern="[A-Za-z\s]+" required title="First Name should only contain  Alphabet. e.g. John" maxlength="60">

                    </div>
                </div>



                <div class="col-md-6">
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="form-control" placeholder="Enter Last Name" pattern="[A-Za-z\s]+" required title="Last Name should only contain  Alphabet. e.g. Singh" maxlength="60">

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" name="email" class="form-control" placeholder="Enter Your Email Address" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required title="The input is not a valid email address" maxlength="50">

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Enter Password" required title="Enter Password" maxlength="35" minlength="6" >

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Re-type Password</label>
                        <input type="password" id="confirm_password" name="cpassword" class="form-control" placeholder="Enter Confirm Password" required title="Enter Confirm Password" maxlength="35" minlength="6" >
                        <span class="err00"  style="color: red;display: none">PASSWORD DON'T MATCH</span>

                    </div>
                </div>

                <script>

                    function functi() {
                        var password = document.getElementById("password");
                        var confirm_password = document.getElementById("confirm_password");

                        if(password.value != confirm_password.value) {
                            $(".err00").css('display','block').fadeOut(3000);

                            return false;
                        } else {
                            confirm_password.setCustomValidity('');
                        }
                    }
                </script>


                <div class="lineal"></div>


                <h1>Contact Details</h1>


                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address Line 1</label>
                        <input type="text" name="office_address" class="form-control" placeholder="Enter Office Address" required="" title="Enter Registered / Head Office Address " maxlength="60" >

                    </div>
                </div>






                <div class="col-md-6">
                    <div class="form-group">
                        <label>Country</label>
                        <input type="text" name="emailcountry" class="form-control" placeholder="Enter Your Country" pattern="[A-Za-z\s]+" required="" title="Enter Your Country" maxlength="50" >

                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-group">
                        <label>State</label>
                        <input type="text" name="state" class="form-control" placeholder="Enter State" pattern="[A-Za-z\s]+" required="" title="State Name should only contain  letters. e.g. Delhi" maxlength="60" >

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>City</label>
                        <input type="text" name="city" class="form-control" placeholder="Enter City" pattern="[A-Za-z\s]+" required="" title="City Name should only contain  letters. e.g. Delhi" maxlength="60" >

                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-group">
                        <label>Zip Code</label>
                        <input type="text" name="pincode" class="form-control" placeholder="Enter Your Pincode" pattern="^\d{6}$" required="" title="Your Pincode Should Only Contain  Number. e.g. 110011" maxlength="10" >

                    </div>
                </div>

  <div class="serach_bar">
      <input type="submit" name="submit" id="submit" onclick="return functi()" value="Register" class="btn btn-lg btn-primary">
                </div>

                <div class="line-do"></div>


            </div>
        </div>




    </div>


</div>
</form>
