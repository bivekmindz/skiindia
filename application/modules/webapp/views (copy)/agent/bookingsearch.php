
                <!-- Profile Info and Notifications 
  to remove empty space -->
             <h1>Dashboard</h1>
             <div class="row">
                    <div class="stepl backgound_bg">
                    
                        <form id="rootwizard" method="post" action="#" class="form-horizontal form-wizard">
                            <div class="steps-progress">
                                <div class="progress-indicator"></div>
                            </div>
                            <ul>
                                <li class="completed"> <a href=""><span>1</span>Search</a> </li>
                                <li class="active"> <a href=""><span>2</span>Package</a> </li>
                                <li class="active"> <a href=""><span>3</span>Addons</a> </li>
                                <li class="active"> <a href=""><span>4</span>Summary</a> </li>
                                <li class="active"> <a href=""><span>5</span>Payment</a> </li>
                            </ul>
                        </form>
                    </div>
                    </div>
         
                <form role="form" name="excursionSearchForm" onSubmit="return validateModifySearch()" action="  partnerbookingsearch" method="post">
                    <div class="row" style="background-color: #f9f9f9;">
                        <div class="contactne">
                            <div class="flashmsg" style="text-align: center;color:red">
                                <?php echo $emsg; ?>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="step_1">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" id="datepicker12" data-date-format="dd-mm-yyyy" class="form-control datepicker c1"
													   placeholder="Select Date" name="txtDepartDate" value="<?php echo $this->session->userdata('txtDepartDate') ? $this->session->userdata('txtDepartDate') : '';  ?>">
                                                <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select class="form-control formmm c1" name="destinationType" id="sessionType1">
                                                    <option value="" disabled="disabled" selected="selected" >Select Time</option>
                                                </select>
                                                <div class="input-group-addon"> <a href="#"><i class="fa fa-clock-o"></i></a> </div>
                                            </div>
                                        </div>
                                    </div>

									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
									  <div class="form-group formmm animated fadeInUp">
                                      <div class="input-group">
										<input type="number" max="200" min="1" class="form-control formmm c1" id="inputVisitor1" name="ddAdult" placeholder="No. of visitor" value="<?php echo $this->session->userdata('ddAdult') ? $this->session->userdata('ddAdult') : '1';  ?>">
										<?php //echo $this->session->userdata('ddAdult'); ?> 
										<div class="input-group-addon"> <a href="#"><i class="fa fa-user"></i></a> </div>
									  </div>
                                       </div>
									</div>									
								
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                        <div class="form-group formmm fadeInUp">
                                            <input type="submit" value="Search" id="submit-booking" class="animated fadeInUp btn_full" id="submit-booking">
                                            <span class="animated fadeInUp input-icon_nobg"><i class="fa fa-search"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                
            
                
                <script type="text/javascript">
                    function validateModifySearch() {
                        var txtDepartDate = document.excursionSearchForm.txtDepartDate.value.trim();
                        var destinationType = document.excursionSearchForm.destinationType.value;
                        if (txtDepartDate == null || txtDepartDate == "" || txtDepartDate.length == 0) {
                            alert("Date can't be blank");
                            return false;
                        }
                        if (destinationType == null || destinationType == "") {
                            alert(txtDepartDate + "Booking Time can't be blank");
                            return false;
                        }
                        var getEXP = destinationType.split('-');
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth(); //January is 0!
                        var yyyy = today.getFullYear();
                        var hour12 = today.getHours();
                        var min1 = today.getMinutes();
                        es = getEXP[1].split(':');
                        var gethours = es[0].trim() - hour12;
                        var getminut = '';
                        if (es[1].trim() > min1) {
                            getminut = es[1].trim() - min1;
                        }
                        if (es[1].trim() < min1) {
                            getminut = min1 - es[1].trim();
                        }
                        var totalgetHM = gethours + ':' + getminut;
                        var hours = today.getHours();
                        var currentampm = hours >= 12 ? 'pm' : 'am';
                        var ampm = es[0] >= 12 ? 'pm' : 'am';
                        //--------------- Code---------------------//
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth() + 1; //January is 0!
                        var yyyy = today.getFullYear();
                        if (dd < 10) {
                            dd = '0' + dd;
                        }
                        if (mm < 10) {
                            mm = '0' + mm;
                        }
                        var today = dd + '-' + mm + '-' + yyyy;
                        today = today.trim();
                        var start = destinationType;
                        var date = new Date();
                        var hour1 = date.getHours();
                        var min1 = date.getMinutes();
                        var end = hour1 + ":" + min1;
                        e = start.split(':');
                        s = end.split(':');
                        min = e[1] - s[1];
                        hour_carry = 0;
                        if (min < 0) {
                            min += 60;
                            hour_carry += 1;
                        }
                        hour = e[0] - s[0] - hour_carry;
                        min = ((min / 60) * 100).toString();
                        diff = hour;

                        //--------------- End---------------------//
                        //$branch->branch_minrestriction
                        if (today == txtDepartDate) {
                            //if(ampm == currentampm){
                            if (hour12 < es[0]) {
                                if (gethours < <?php echo $branch->branch_hrsrestriction; ?>) {
                                    alert("Sorry, Online booking is allowed only <?php echo $branch->branch_hrsrestriction; ?> hours <?php echo $branch->branch_minrestriction; ?> minut prior to the session time");
                                    return false;
                                }
                                if (gethours == <?php echo $branch->branch_hrsrestriction; ?>) {
                                    if (getminut <= <?php echo $branch->branch_minrestriction; ?>) {
                                        alert("Sorry, Online booking is allowed only <?php echo $branch->branch_hrsrestriction; ?> hours <?php echo $branch->branch_minrestriction; ?> minut prior to the session time");
                                        return false;
                                    }
                                }

                            } else {
                                alert("Sorry, Online booking is allowed only <?php echo $branch->branch_hrsrestriction; ?> hours <?php echo $branch->branch_minrestriction; ?> minut prior to the session time");
                                return false;
                            }
                        }
                    }
                </script>
                <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
                <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                <script>
                    $("#datepicker12").datepicker({
                        minDate: 0,
                        maxDate: "+<?php echo $s_viewdate->date; ?>D ",
                        dateFormat: 'dd-mm-yy'
                    });
					
					var select_time_slot = '<?php echo $this->session->userdata('select_time_slot') ? $this->session->userdata('select_time_slot') : 0; ?>';
					var select_time_slot_id = '<?php echo $this->session->userdata('select_time_slot_id') ? $this->session->userdata('select_time_slot_id') : 0; ?>';
						
						if( select_time_slot ) {  
							$(document).ready(function(){
								$('#datepicker12').trigger('change');
							});
						};
						
                    $('#datepicker12').change(function() {
						
						if( $('#datepicker12').val() != '' ) {
							
							var request = $.ajax({
								url: "get-time-slot-by-date",
								method: "POST",
								data: {
									'booking_date': $('#datepicker12').val()
								}
							});
							request.done(function(response) { //alert(response);
								response = JSON.parse(response);
								console.log(response);
								var option_html = '';
								if (response.status) {
									var data_array = JSON.parse(response.data);
									data_array.forEach(function(value, key) {
										if( value.dailyinventory_id == select_time_slot_id ){
											option_html += '<option selected="selected" value="' + value.dailyinventory_id + '-' + +value.dailyinventory_from + ' : ' + value.dailyinventory_minfrom + '-' + value.dailyinventory_to + ' : ' + value.dailyinventory_minto + '">' + value.time_value_from + ' : ' + value.dailyinventory_minfrom + '  ' + value.AM_PM_from + '<b> - </b>' + value.time_value_to + ' : ' + value.dailyinventory_minto + '  ' + value.AM_PM_to + '</option>';
										}else{
											option_html += '<option value="' + value.dailyinventory_id + '-' + +value.dailyinventory_from + ' : ' + value.dailyinventory_minfrom + '-' + value.dailyinventory_to + ' : ' + value.dailyinventory_minto + '">' + value.time_value_from + ' : ' + value.dailyinventory_minfrom + '  ' + value.AM_PM_from + '<b> - </b>' + value.time_value_to + ' : ' + value.dailyinventory_minto + '  ' + value.AM_PM_to + '</option>';
										}
									});
								}
								$('#sessionType1').html(option_html);
							});
							request.fail(function(jqXHR, textStatus) {
								console.log("Request failed: " + jqXHR + '--' + textStatus);
							});
						}								  
                    });
                </script>
                