<?php
echo '';
?>
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"
></script> <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/boo
   tstrap.min.js"></script>

<div class="success-order">
    <div class="top_heading text-center">
        <h3>
        <span>SORRY FOR THE INCONVENIENCE. YOUR BOOKING IS FAILED</span>
        </h3>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="p-note">
                <p><span class="warning"> Please note:</span> Below is your booking ID for all communication with Snow world Mumbai. Please save and store the ID mentioned. You will have to carry a printout of this ticket when you visit the park. The details of your booking are mentioned below and a copy of the same has also been emailed to your email address.</p>
            </div>
        </div>
    </div>
    <div class="order-sucess-tab">
        <div class="order-success-tab-heading text-center">
            <h5>Booking Details - 1st Jan 1970</h5>
        </div>
        <div class="tab-inner-section">
            <div class="payment-detail-tab">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="type-id">
                            <h4><span class="heading-text">Ticket Type :</span></h4>
                            <h4><span class="heading-text">Booking ID :</span></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-package-detail">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="tab-package-detail-left border-right">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Visit Date</td>
                                        <td>:</td>
                                        <td>1st Jan 1970</td>
                                    </tr>
                                    <tr>
                                        <td>Start Time</td>
                                        <td>:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Visitor</td>
                                        <td>:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Addons</td>
                                        <td>:</td>
                                        <td>N/A</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="package-payment-detail-right block-width">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Ticket price</td>
                                        <td><i class="fa fa-inr" aria-hidden="true"></i></td>
                                        
                                    </tr>
                                    <tr>
                                        <td>Addons</td>
                                        <td><i class="fa fa-inr" aria-hidden="true"></i></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Discount (Promocode)</td>
                                        <td><i class="fa fa-inr" aria-hidden="true"></i></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Internet Handling Charge</td>
                                        <td><i class="fa fa-inr" aria-hidden="true"></i></td>
                                        <td>N/A</td>
                                    </tr>
                                    <tr class="total-payment">
                                        <td>PAYABLE PRICE</td>
                                        <td><i class="fa fa-inr" aria-hidden="true"></i></td>
                                        <td>N/A</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="total-price text-center block-width">
                <p>Total Ticket Price -
                    <span class="total-price-count">
                        <span class="rupees-icon"><i class="fa fa-inr" aria-hidden="true"></i></span>
                    </span>
                </p>
            </div>
        </div>
        <div class="custmer-info block-width">
            <div class="top_heading block-width text-center">
                <h3>CUSTOMER INFORMATION</h3>
            </div>
            <div class="custmer-info-inner block-width">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="custmer-info-inner-sec comman-sec block-width">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td>:</td>
                                        <td>NA</td>
                                    </tr>
                                    <tr>
                                        <td>Contact</td>
                                        <td>:</td>
                                        <td>NA</td>
                                    </tr>
                                    <tr>
                                        <td>Email </td>
                                        <td>:</td>
                                        <td>NA</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="custmer-info-inner-sec comman-sec block-width">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Address</td>
                                        <td>:</td>
                                        <td>NA</td>
                                    </tr>
                                    <tr>
                                        <td>Town/City</td>
                                        <td>:</td>
                                        <td>NA</td>
                                    </tr>
                                    <tr>
                                        <td>Town/City </td>
                                        <td>:</td>
                                        <td>NA</td>
                                    </tr>
                                    <tr>
                                        <td>Country </td>
                                        <td>:</td>
                                        <td>NA</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="disclaim">
            <div class="top_heading block-width text-center">
                <h3>DISCLAIMER</h3>
            </div>
            <div class="disclaim-info block-width">
                <ul>
                    <li>
                        Kindly report at least 15 minutes prior to your session timings to issue the admission pass.
                    </li>
                    <li>
                        Management is not responsible for the loss or miss of session time, if you reach late.
                    </li>
                    <li>
                        Kindly make sure you read the terms, conditions, safety rules and cancellation policy.
                    </li>
                    <li>
                        This booking is not transferable/refundable or used in exchange of cash.
                    </li>
                    <li>
                        The said booking is considered that you agree to all the terms and conditions of the Snow world Management.
                    </li>
                </ul>
                <p>You have now confirmed and guaranteed your booking has done by :</p>
            </div>
        </div>
        <div class="address-row block-width text-center">
            <span class="green-heading">VENUE ADDRESS</span>
            <p>Phoenix Market City, Lower Ground Level 58 - 61, Between Atrium 2 & 6, 15 L.B.S. Marg, Kamani Jn., Kurla (W), Mumbai 400 070, India..</p>
        </div>
        <div class="booking-btn block-width text-center">
            <button class="blue-btn" type="button">Print booking</button>
        </div>
    </div>
</div>
<style type="text/css">
.block-width{ width: 100%; float: left; }
.success-order{ width: 100%; float: left; padding: 15px 0; }
.top_heading{ width: 100%; float: left; }
.top_heading h3 span{ display: inline-block; background:#303235; color:#fff;padding: 10px 35px;border-radius: 5px; }
.p-note{width: 100%;float: left;padding: 25px 20px;box-sizing: border-box;color: #777676;font-size: 15px;line-height: 22px;border: 1px solid #a5a5a5; border-radius: 5px;margin: 20px 0;}
.p-note .warning{ color: #fd3535;padding: 0 10px 0 0 }
.order-sucess-tab{ width: 100%; float: left; }
.order-sucess-tab .order-success-tab-heading{ width: 100%; float: left; background:#303235; padding: 10px 0;}
.order-sucess-tab .order-success-tab-heading h5{ color: #fff; font-size: 22px; line-height: 25px; }
.tab-inner-section{ width: 100%; float: left; border: 1px solid #a5a5a5;}
.payment-detail-tab{ width: 100%; float: left; }
.payment-detail-tab{ width: 100%; float: left; padding: 15px 20px; box-sizing: border-box; }
.type-id{ width: 100%; float: left; }
.type-id h4 { width: 100%; float: left; margin: 12px 0; font-size: 16px; }
.type-id h4 .heading-text{ color: #0098da; font-weight: 600;}
.border-right{border-right:1px solid #a5a5a5; }
.tab-package-detail{ width: 100%; float: left; border-top: 1px solid #a5a5a5;border-bottom: 1px solid #a5a5a5; margin: 0 0 20px 0;  }
.tab-package-detail-left, .package-payment-detail-right{ width: 100%; float: left;padding: 30px 15px; box-sizing: border-box;}
.tab-package-detail-left table,.package-payment-detail-right table { width: 100%; max-width: 100%; border-collapse: collapse; display: table; }
table { width: 100%; max-width: 100%; border-collapse: collapse; display: table; }
table tr td{padding: 5px 0; font-size: 16px; line-height: 18px; border-top:1px solid #ddd; line-height: 25px; color: #393939; }
.package-payment-detail-right table tr td{ border-top: none; line-height: 18px; }
.package-payment-detail-right table tr td:first-child{ width: 50%; }
.tab-package-detail-left table tr:last-child td{border-bottom:1px solid #ddd;}
.package-payment-detail-right table tr.total-payment td{color: #4871b1;font-weight: 500;font-size: 20px;}
.total-price{ padding: 0 0 15px 0; }
.total-price p{ font-size: 18px; line-height: 35px; color: #639a4d;}
.total-price p .rupees-icon{ font-size: 25px; margin: 0 0 0 15px; }
.custmer-info .top_heading h3, .disclaim .top_heading h3{ color: #393939; font-weight: 600; }
.custmer-info-inner{ margin: 25px 0px;border-bottom: 1px solid #a5a5a5;border-top: 1px solid #a5a5a5; }
.custmer-info-inner .comman-sec{ padding: 30px 15px; box-sizing: border-box; }
.custmer-info-inner-sec table tr:first-child td{ width:35%;}
.disclaim-info{border-bottom:1px solid #a5a5a5;border-top:1px solid #a5a5a5;padding:20px 15px;box-sizing:border-box; }
.disclaim-info ul li, .disclaim-info p{padding: 10px 0; font-size: 16px; color:#335f9b; line-height: 19px; list-style-type: none; position: relative;}
.disclaim-info ul li:before{content: "\f046";display: inline-block;position: absolute;font: normal normal normal 14px/1 FontAwesome;text-rendering: auto;-webkit-font-smoothing: antialiased;color: #335f9b;left: -26px;top: 11px;}
.green-heading{font-weight: bold;font-size: 18px;text-transform: uppercase;color: #8BC34A;line-height: 20px;}
.address-row{ padding: 20px 0;border-bottom:1px solid #a5a5a5; }
.address-row p{
    font-size: 14px;
    line-height: 36px;
    margin: 10px 0 0 0;

}
.blue-btn {margin: 35px 0px 35px 0px;background: rgba(3,169,244,0.8);
    color: #ffffff; font-size: 16px; padding: 15px 35px; border: none;}
</style>

























   

    <!-- <div class="row">
        <div class="col-lg-3 col-md-3 col-xs-12"></div>
        <div class="col-lg-6
         col-md-6 col-xs-12">
            <div class="order_success">
                <div class="order_success_logo"></div>
                <div class="order_success_text text-center">
               <span class="success_icon"> 
               	<i class="fa fa-check-circle"></i>
               </span>
                    <h2>congratulations</h2>
                    <p>Your order has been successfully placed</p>
                </div>
                <div
                        class="order_success_detail">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <div class="order-id ods-ct"> Your order Id is
                                <span class="spf">#00221100</span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <div class="payed_amount ods-ct text-right">
                                Total Paid Amount is <span class="spf"><i class="fa fa-rupee"></i>3000</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            <div class="billing_address">
                                <h4>Shipping
                                    Address -
                                </h4>
                                <ul>
                                    <li><span class="bill_text-left">Name</span><span class="bill_text-right">
                              Simerjeet singh</span>
                                    </li>
                                    <li>
                                        <span class="bill_text-left">Billing Mobile</span>
                                        <span class="bill_text-right">9810668829</span>
                                    </li>
                                    <li>
                                        <span class="bill_text-left">Email Address</span>
                                        <span class="bill_text-right">simer@elitehrpractices.com</span>
                                    </li>
                                    <li>
                                        <span class="bill_text-left">Address</span>
                                        <span class="bill_text-right">Janak Puri</span>
                                        <span class="city bill-add"> New Delhi,</span>
                                        <span class="bill-add"> Delhi 110058</span>
                                        <span class="bill-add"> Delhi</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-xs-12"></div>
    </div>

<style type="text/css">
    .order_success{
        width: 100%;
        float: left;
        margin: 70px 0 0 0;
        border: 1px solid #e8e8e8;
        background: #fdfdfd;
        padding: 15px;
    }
    .success_icon{
        width: 100%;
        float: left;
    }
    .success_icon .fa{
        font-size: 100px;
        color: #71ca90;
        line-height: 110px;
    }
    h2{
        width: 100%;
        float: left;
        margin: 15px 0;
        text-transform: capitalize;
        color: #0098DA;
        font-weight: 700;
    }
    .order_success_detail{
        width: 100%;
        float: left;
        margin: 15px 0;
        border-top: 1px solid #ddd;
        padding: 15px 0 0 0;
    }
    .spf{
        font-size: 18px;
        color: #0098DA;
        line-height: 22px;
        padding: 0 0 0 10px;
        box-sizing: border-box;
        font-weight: 600;
    }
    .ods-ct{
        font-size: 16px;
        line-height: 22px;
        color: #393939;
    }
    .billing_address{
        width: 100%;
        margin: 15px 0 0 0;
        float: left;
    }
    ul{
        padding: 0px;
    }
    ul li{
        list-style-type: none;
        margin: 0 0 10px 0;
    }
    .billing_address h4{
        width: 100%;
        float: left;
        color: #71ca90;
        padding: 0 0 10px 0;
        border-bottom: 1px solid #a3d8b5;
        font-size: 20px;
    }
    .bill_text-left{width: 25%; position: relative;padding: 0 15px 0 0;box-sizing: border-box;float: left; font-weight: 600;}
    .bill_text-left:after{    position: absolute;
        right: 10px;
        top: 0px;
        content: ":";
        font-size: 16px;
        font-weight: bold;
        color: #3939396;
    }
</style> -->