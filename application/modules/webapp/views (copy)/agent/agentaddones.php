<!--   to remove empty space 
        <h1>Addones</h1>
        <div class="arrow-ri"><span><i class="fa fa-home"></i></span></div>
        <div class="show-data">
            <h1></h1>
        </div>
-->
		<?php
		$temp_array = [];
		foreach( $addonscart as $val ){

			$temp_array[] = $val;
		}
		?>
		<script>		  

			var select_package = []; 
			var	get_quantity_price = []; 

			(select_package.length) ? $('.continue button[type="submit"]').attr('disabled', false) : $('.continue button[type="submit"]').attr('disabled', true);
			(select_package.length) ? $('.package_addon_total_price').show() : $('.continue button[type="submit"]').hide();

			//showItem();  // show default price 

			var temp_array = <?php echo json_encode($temp_array); ?>;

			var package_price = <?php echo $package_addon_total; ?>;

			/*
		console.log( 'temp_array' );
		console.log( temp_array );

		console.log( 'select_package.length' );	
		console.log( select_package.length );

		console.log( 'temp_array.length' );
		console.log( temp_array.length );
			*/
			

			 (temp_array.length == 0) ? $('.continue button[type="submit"]').hide() : $('.continue button[type="submit"]').show();



			function showItem( param = 0, param1 = 0 ){

				//console.log('showItem');

				param ? $('.total-price-div').show() : $('.total-price-div').hide();
				param ? $('.addon_total_price').html( '<i class="icon-rupee" aria-hidden="true"></i>' + parseFloat(param) ) : $('.addon_total_price').html( '<i class="icon-rupee" aria-hidden="true"></i>' + 0 );
				param ? $('.package_addon_total_price').html( '<i class="icon-rupee" aria-hidden="true"></i>' + ( parseFloat(param) + parseFloat(param1) ) ) : $('.package_addon_total_price').html( '<i class="icon-rupee" aria-hidden="true"></i>' + parseFloat(param1) );

				//console.log('Final Price ');
				//console.log(param);
			} 

			function getCalculatedPrice(param){

				//console.log('getCalculatedPrice');
				//console.log(param);
				if( param.length ){

					return param.map(function(item){ return item.qty * item.addon_price }).reduce( (x, y) => parseInt(x) + parseInt(y) );

				}else{

					return 0;
				}
			}

			function updateQuantity(val, id){
			//console.log('method   ' + val +'--'+id);

				var updated_price_array = '';
				var to_remove_index = get_quantity_price.map(function(item) { return parseInt(item.addon_id); }).indexOf( parseInt(id) );
		//		console.log('to_remove_index');
		//		console.log(to_remove_index);
				(to_remove_index == -1) ? '' : get_quantity_price[to_remove_index].qty = parseInt(val);

				updated_price_array = getCalculatedPrice(get_quantity_price);


				$('#final-selected-data').val( JSON.stringify(get_quantity_price) );

				//console.log('updated price *****');
				//console.log( updated_price_array );
		//console.log('get_quantity_price array');
		//console.log(get_quantity_price);		

				showItem( updated_price_array, parseInt(package_price ) );

			}	



		</script>


  	<div class="row">
            <div class="stepl backgound_bg">
                <form id="rootwizard" method="post" action="#" class="form-horizontal form-wizard">
                    <div class="steps-progress">
                        <div class="progress-indicator"></div>
                    </div>
                    <ul>
                        <li class="active"> <a href="partnerbookingsearch" ><span>1</span>Search</a> </li>
                        <li class="active" > <a href="agentpackagesstep"><span>2</span>Package</a> </li>
                        <li class="completed"> <a href="agentaddones" ><span>3</span>Addons</a> </li>
                        <li class="active"> <a href="" ><span>4</span>Summary</a> </li>
                        <li class="active"> <a href="" ><span>5</span>Payment</a> </li>
                    </ul>
                </form>
            </div>
    </div>
       <div class="row" style="background-color: #f9f9f9;">
            <div class="contactne">
                <div class="col-md-12 col-md-offset-0">
                <?php  foreach($tmp_array as $v ){  ?>
                    <form name="formName" id="formName" action="" method="post">
                    <div class="package">
                    
                    
                      <div>
                        <div class="col-md-1">

                           <?php  $subtotal=0;
                          if( $v->package_image != '') { ?> 
                                <img style="max-width: 65px;" class="border-img" src="assets/admin/images/<?php echo $v->package_image; ?>">
                          <?php } else { ?>
                              <img style="max-width: 65px;" src="img/1463739380583.jpeg">
                          <?php } ?>
                            <input type="hidden" name="cartpackageimage" value="<?php echo $v->package_image; ?>">
                           </div>

                        <div class="col-md-4">

                            <div class="ragular"><?php echo $v->package_name; ?></div>
                          <input type="hidden" name="cartpackagename" value="<?php echo $v->package_name; ?>">
                        </div>


                        <div class="col-md-4">

                            <div class="visitors">
                                No. Of Visitors: <?php echo $v->qty ? $v->qty : 0; ?> </div>


                        </div>
                        <div class="col-md-3">

                            <div class="rupess-te"><span><i class="fa fa-rupee" aria-hidden="true"></i></span> <?php echo ( $v->package_price * $v->qty ) ? ( $v->package_price * $v->qty ) : 0;  ?></div>

                                <input type="hidden" name="cartpackageprice" value="<?php echo ( $v->package_price * $v->qty ) ? ( $v->package_price * $v->qty ) : 0; ?>">
                        </div>
                 </div>

       <?php }  ?>
                        <?php if($addonsdisplay->scalar!='Something Went Wrong') {  
                         foreach ($addonsdisplay as $key => $value) {  ?>
                        <div  style="width: 100%;float: left; border-top: 1px solid #dadada; margin-top: 20px;">
                            <div class="col-md-1"> <?php
                                if($value['addonimage']!='') {
                                    ?>  <img style="max-width: 52px;height:52px;" src="assets/admin/images/<?php echo $value['addonimage']; ?>" width="65px">
                                <?php } else { ?>
                                    <img style="max-width: 52px; height:52px;" src="img/1463739380583.jpeg">
                                <?php } ?>
                                <input type="hidden" name="cartaddoneimage[]" value="<?php echo $value['addonimage']; ?>">

                                <input type="hidden" name="cartaddoneid[]" value="<?php echo $value['addonid']; ?>">
                            </div>

                            <div class="col-md-4">

                                <div class="ragular"><?php echo $value['addonname']; ?>
                                    <input type="hidden" name="cartaddonname[]" value="<?php echo $value['addonname']; ?>"></div>

                            </div>


                            <div class="col-md-4">

                                <div class="visitors">
                                   Qty: <?php echo $value['addonqty']; ?>   <input type="hidden" name="cartaddonqty[]" value="<?php echo $value['addonqty']; ?>"></div>
                                <input type="hidden" name="cartaddonprice[]" value="<?php echo $value['addonprice']; ?>">

                            </div>
                            <div class="col-md-3">
                                <form name="formName" id="formName" action="" method="post">
                                <div class="rupess-te"><span><i class="fa fa-rupee" aria-hidden="true"></i></span>
                                   <?php echo $value['addonprice'];$subtotal+= $value['addonprice'];?> <span class="close_but">
<button id="" name="delete" class="btn btn-default1 "  type="submit" role="button" aria-disabled="false" value = "<?php echo $value['tempaddon']; ?>" onclick="this.form.submit();"><i class="fa fa-close" aria-hidden="true"></i></button>
</span>
                                        </div></form>


                            </div>
                        </div>
<?php }} ?>
<!--package total price start here --> 						
      				<span id="selectedAddonPanel">
							<div class="sencosn">

								<div class="col-md-6" >
									<div class="totaola">Packages</div>
								</div>

								<div class="col-md-6 pull-right" >
									
									  <p class="reupespps2 reupespps2ski" >
										  <span>
											  <i class="icon-rupee" aria-hidden="true"></i>
										  </span><?php echo $package_total_price; ?>
									  </p>
									
								</div>
								
							</div>
        			</span>
<!-- package price end here--> 								
			
<!-- addon price start here--> 							
      				<span id="selectedAddonPanel">
							<div class="sencosn">

								<div class="col-md-6" style="padding-left: 15px;">
									<div class="totaola">Addons</div>
								</div>

								<div class="col-md-6 txtright" style="padding-right:6px;">

								  <p class="reupespps2 reupespps2ski" >
									  <span class="addon_total_price">
										  <i class="icon-rupee" aria-hidden="true"></i>
										  0
									  </span>
									</p>
								</div>

							</div>
        			</span>
<!-- addon price end here--> 						
						
<!--package & addon total price start here --> 										
      				<span id="selectedAddonPanel">
							<div class="sencosn">

								<div class="col-md-6" style="padding-left: 15px;">
									<div class="totaola">Total</div>
								</div>

								<div class="col-md-6 txtright" style="padding-right:6px;">

								  <p class="reupespps2 reupespps2ski" >
									  <span class="package_addon_total_price">
										  <i class="icon-rupee" aria-hidden="true"></i>
										  <?php echo $package_total_price; ?>
									  </span>
								  </p>
								</div>

								<div class="show_dad">
									<div class="form-group formmm ">
										<button id="" name="submit" class="btn_full" type="submit" role="button" aria-disabled="false" value="cartses"><span class="ui-button-text ui-c">CONTINUE</span></button>
									</div>
								</div>								

							</div>
        			</span>						
<!-- package & addon price end here-->						

                    </div>
                    

                    <div class="package sep_2">
<div class="heding_titile">
<div class="h_con">Would you like some Addons?

<span class="addnew-all addnew-all-btn" name="" row-id="" id="" type="button">
										<span class="ui-button-text ui-c addnew-all-toggle">
											<button type="button" class="btn_add_a">Add All</button>
										</span>
									</span>	
</div>
									<!-- 	addnew-all, addnew-all-toggle
											- is bind with query 
											- no css is bind with it
									--> 
									
  </div>									

                                    
        <?php  $countaddone = 0; foreach ($addonscart as $key => $value) {
            if($value=='Something Went Wrong') {echo "No Records";} else {
                if($this->session->userdata('addonquantitys')!='') {


                    $addonqtyval= (explode(",",$this->session->userdata('addonquantitys')));
                    $addonqtyval[$countaddone]=$addonqtyval[$countaddone];

                }
                else
                {
                    $addonqtyval[$countaddone]=1;
                }
                ?>

                <script type="text/javascript">
                    function incrementValue<?php echo $value['addon_id']; ?>()
                    {
                        //  alert('number<?php echo $value['addon_id']; ?>');
                        var a = parseInt($("#mainnumbervalues<?php echo $value['addon_id']; ?>").val());
                        var b = parseInt($("#mainnumber<?php echo $value['addon_id']; ?>").html());
                        var c = parseInt($("#mainnumbervalue<?php echo $value['addon_id']; ?>").val());

                        var value = parseInt(document.getElementById('number<?php echo $value['addon_id']; ?>').value);
                        value = isNaN(value) ? 0 : value;
                        if(value<19){
                            value++;
                            b = b+a;
                            c = value*a;

                            $("#mainnumbervalue<?php echo $value['addon_id']; ?>").val(c);
updateQuantity( value, <?php echo $value['addon_id']; ?>);
                            $("#mainnumber<?php echo $value['addon_id']; ?>").html(b);
                            document.getElementById('number<?php echo $value['addon_id']; ?>').value = value;

                        }
                    }
                    function decrementValue<?php echo $value['addon_id']; ?>()
                    {

                        var a = parseInt($("#mainnumbervalues<?php echo $value['addon_id']; ?>").val());
                        var b = parseInt($("#mainnumber<?php echo $value['addon_id']; ?>").html());
                        var c = parseInt($("#mainnumbervalue<?php echo $value['addon_id']; ?>").val());
                        var value = parseInt(document.getElementById('number<?php echo $value['addon_id']; ?>').value);
                        value = isNaN(value) ? 0 : value;
                        if(value>1){
                            value--;
                            b = b-a;
                            c = value*a;

                            $("#mainnumbervalue<?php echo $value['addon_id']; ?>").val(c);
updateQuantity( value, <?php echo $value['addon_id']; ?>);							
                            $("#mainnumber<?php echo $value['addon_id']; ?>").html(b);

                            document.getElementById('number<?php echo $value['addon_id']; ?>').value = value;
                            document.cookie = "myJavascriptVar = " + value
                        }

                    }
                </script>
                <div class="package margin_b each-row-<?php echo $value['addon_id']; ?>">
						<div class="col-md-2"> 
							<img src="<?php if($value['addon_image']!='') { ?>assets/admin/images/<?php echo $value['addon_image']; ?><?php } else { ?>img/info.png<?php } ?>" width="65px">

						</div>
						<div class="col-md-4">
							<div class="regular">
								<h1> <?php echo $value['addon_name']; ?> </h1>
							</div>
						</div>
						<div class="col-md-2">
							<div class="qual i_icons">

								<h1>Qty</h1>

								<div class="input-spinner">

									<div>
										<input type="button" class="btn btn-default1 " onclick="decrementValue<?php echo $value['addon_id']; ?>()" value="-">


										<div class="test-incre">
											<input type="text" readonly id="number<?php echo $value['addon_id']; ?>" class="form-control size-5" name="addonqty[]" maxlength="2" value="<?php echo $addonqtyval[$countaddone]; ?>">
										</div>
										&nbsp;<input type="button" class="btn btn-default1" onclick="incrementValue<?php echo $value['addon_id']; ?>()" value="+">
									</div>
						</div>

                    </div>
                    </div>
                    <div class="col-md-2">
                        <div class="rupess">

                            <span><i class="fa fa-rupee" aria-hidden="true"></i> </span> <span id="mainnumber<?php echo $value['addon_id']; ?>"><?php echo $addonqtyval[$countaddone]*$value['addon_price']; ?></span>
                            <input type="hidden" name="addonprice[]" id="mainnumbervalue<?php echo $value['addon_id']; ?>" value="<?php echo $addonqtyval[$countaddone]*$value['addon_price']; ?>">
                            <input type="hidden" name="addonprice1[]" id="mainnumbervalues<?php echo $value['addon_id']; ?>" value="<?php echo $value['addon_price']; ?>">

                        </div>

                    </div>
                    <div class="col-md-2">

                        <div class="show_dad">
                            <div class="form-group ">
                                <button id="" name="categories1[]" class="btn_full addnew" name="" row-id="<?php echo $value['addon_id']; ?>" type="button" role="button" aria-disabled="false"  >
									<span class="ui-c addnew-<?php echo $value['addon_id']; ?>" row-id="<?php echo $value['addon_id']; ?>" >Add</span>
								</button>

                            </div>
                        </div>
                    </div>
                </div>
<?php $countaddone++;}} ?>
				<input type='hidden'id="final-selected-data" name="final_selected_addon_data" class="final-selected-data" value="" />
										
			</form>
	  <div > 
</div>

               </div>
            </div>
        </div>
            

        </div>
   
			
<script>

$(document).ready(function(){
	
	showItem(0, <?php echo $package_addon_total; ?>);  // show default price
	
	
	$('.addnew-all').on('click', function(){
		
		var selected_addon = [];
		
		if( (select_package.length < 3 || select_package.length >= 0) && (select_package.length != 3) ){  
			 
				temp_array.map(function(item){
					
						if( item.qty = parseInt(document.getElementById('number'+item.addon_id).value) ){
							
    						  if( select_package.indexOf( item.addon_id.trim() ) == -1 ){
   										
										select_package.push( item.addon_id );
										get_quantity_price.push( item );
								  
										$('.each-row-'+item.addon_id).css('background-color', 'lightgrey');

										updateQuantity( item.qty, item.addon_id );							

										$('.addnew-'+item.addon_id).html('<button type="button" >Remove</button>');

										$('.addnew-all-toggle').html('<button type="button" >Remove All</button>');
								  
                                }	
						}
					
					});
			
				 $('#final-selected-data').val( JSON.stringify(get_quantity_price) );
		}else{ 
		
							if( (select_package.length == temp_array.length) ){

									select_package.length = get_quantity_price.length = 0;

									temp_array.map(function(item){

											if( item.qty = parseInt(document.getElementById('number'+item.addon_id).value) ){

												$('.each-row-'+item.addon_id).css('background-color', '');

												$('.addnew-'+item.addon_id).html('<button type="button" >Add</button>');

												updateQuantity( item.qty, item.addon_id );							

												$('.addnew-all-toggle').html('<button type="button" >Add All</button>');			
											}

										});		

									$('#final-selected-data').val( JSON.stringify(get_quantity_price) );
								}
		}

	});
	
	$('.addnew').on('click', function(){  
		
		var row_id = $(this).attr('row-id');
		
		var package_id = row_id;
		var value = parseInt(document.getElementById('number'+row_id).value);


				if( ( select_package.indexOf(package_id) == -1 ) ) {

			
					temp_array.map(function(item){

						if( item.addon_id == (package_id) ){  

						select_package.push( item.addon_id );
							
						get_quantity_price.push( item );

						updateQuantity( value, package_id );

						$('.each-row-'+row_id).css('background-color', 'lightgrey');

						$('#final-selected-data').val( JSON.stringify(get_quantity_price) );

						$('.addnew-'+row_id).html('<button type="button" class="romoved_btn" > Remove </button>');

						}

					}); 

				}else{

						var item_index = select_package.indexOf(package_id);
						select_package.splice( item_index, 1 );
						
						$('.each-row-'+row_id).css('background-color', '');

						to_remove_index = get_quantity_price.map(function(item) { return item.addon_id; }).indexOf(package_id);

						get_quantity_price.splice(to_remove_index, 1);

						updateQuantity( value, package_id );

						$('#final-selected-data').val( JSON.stringify(get_quantity_price) );
						$('.addnew-'+row_id).html('<button type="button" >Add</button>');
						
				}

			//	(select_package.length) ? $('.continue button[type="submit"]').attr('disabled', false) : $('.continue button[type="submit"]').attr('disabled', true);

		});
	
	});	

</script>			


