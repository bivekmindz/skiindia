<link rel="stylesheet" HREF="<?php echo base_url();?>js/hotspot/css/imh-core.css">
    <link rel="stylesheet" HREF="<?php echo base_url();?>js/hotspot/css/main.css">
  
    <div class="container-fluid">
    	<div class="row">        	
        	<div class="col-md-12" style="padding: 30px;">
			<div id="main">
             <div class="ihs-slide" data-largeimg="img/parkmap-big.jpg">
                <img SRC="img/parkmap.jpg" />

                <div class="ihs-caption">
                    <p class="caption scifiheader">The Park Map</p>
                </div>

                <div class="ihs-hotspots" data-local-settings="true">
                
                	<div data-x="70" data-y="18" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>I-Slip <span>Ski Track</span></h2>
                        <p>Get on the pair of Skis and experience the ultimate transformation of freedom on our 100 feet slope.</p>
                    </div>
                    
                    <div data-x="75" data-y="28" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Multicoaster <span>Toboggan</span></h2>
                        <p>Get on the wildest steep slope to go down using various forms of sledges and toboggans.</p>
                    </div>
                    
                    <div data-x="75" data-y="35" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Frozen Odyssey <span>Cave Area</span></h2>
                        <p>A Dark maze with emphatic lighting takes guest through an expedition to meet species of our planet.</p>
                    </div>
                    
                    <div data-x="80" data-y="35" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Icy Luge <span>Bobsled</span></h2>
                        <p>With number of twist and turns on a continuous downhill trajectory, it is one of the most fun things to do in Ski India. It's certain to bring smiles to the faces of both young and old.</p>
                    </div>
                    
                    <div data-x="67" data-y="39" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Igloo</h2>
                        <p>Ever wondered how it feels to be an eskimo in  an igloo . Come experience it &amp; while you are at it, don’t forget to Star Gaze.</p>
                    </div>
                    
                    <div data-x="77" data-y="45" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Way to Snow Runner &amp; Frozen Odyssey</h2>
                        <p style="float:right;">Try your skill, ride the snow vehicle and master the art of driving.</p>
                    </div>
                    
                    <div data-x="77" data-y="53" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Way to I –Slip, Multicoaster &amp; Icy Luge</h2>
                        <p style="float:right;">The greatest experiences come after the toughest climb. Get on top to get your adrenaline pumping.</p>
                    </div>
                    
                    <div data-x="22" data-y="30" class="right defaultHotspot" data-label-align="right" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>View Pod <span>Viewing Gallery</span></h2>
                        <p>Climb on to the highest point in Ski India to feel like an astronomer &amp; get an aerial view on What’s Happening in the planet</p>
                    </div>
                    
                    <div data-x="28" data-y="43" class="right defaultHotspot" data-label-align="right" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Spinn <span>Kiddie Carousal</span></h2>
                        <p>Go round and bumping on the snow tubes with our &quot;Merry go round&quot;.</p>
                    </div>
                    
                    <div data-x="14" data-y="57" class="right defaultHotspot" data-label-align="right" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>H<sub>2</sub>O <span>Drinking Water</span></h2>
                        <p>Yes its true, water is found in the Ski Planet. Sipping off it after an adventurous time can definitely be relishing.</p>
                    </div>
                    
                    <div data-x="30" data-y="58" class="right defaultHotspot" data-label-align="right" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Suit Chamber <span>Jacket Collection/Drop Off</span></h2>
                        <p>Get geared up &amp; collect your Cosmo essentials like the spacesuit, hood, gloves &amp; boots at the Suit chamber. Dontcha forget to give it back coz some cold blooded aliens would need them.</p>
                    </div>
                    
                    <div data-x="36" data-y="60" class="right defaultHotspot" data-label-align="right" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Fiction Fair <span>Private Party lounge</span></h2>
                        <p>Be it earthlings or aliens everyone loves to celebrate their birthdays here.</p>
                    </div>
                    
                    <div data-x="43" data-y="62" class="right defaultHotspot" data-label-align="right" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Air-Lock Chamber <span>Way To Snow Area</span></h2>
                        <p>This air lock chamber like every other in spaceships, allow earthlings to step in the Snow Zone of the Planet Ski.</p>
                    </div>
                    
                    <div data-x="56" data-y="62" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Air-Lock Chamber <span>Snow Area Exit</span></h2>
                        <p style="float:right;">This air lock chamber like every other in spaceships, allow earthlings to step in the Snow Zone of the Planet Ski.</p>
                    </div>
                    
                    <div data-x="49" data-y="84" class="right defaultHotspot" data-label-align="right" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Wormhole <span>Ski India Entry</span></h2>
                        <p>A wormhole is a theoretically a passage through space and time that could create shortcuts through long journeys across the universe. Albert Einstein has it theoretically this wormhole will transport you to a snow planet full of surprises.</p>
                    </div>
                    
                    <div data-x="60" data-y="60" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Deposit <span>Baggage Counter</span></h2>
                        <p>Tour the planet baggage free, deposit your bags at the Deposit.</p>
                    </div>
                    
                    <div data-x="66" data-y="58" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Suit Chamber <span>Jacket Collection/Drop Off</span></h2>
                        <p style="float:right;">Get geared up &amp; collect your Cosmo essentials like the spacesuit, hood, gloves &amp; boots at the Suit chamber. Dontcha forget to give it back coz some cold blooded aliens would need them.</p>
                    </div>
                    
                    <div data-x="74" data-y="59" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Cafelanche <span>Cafe</span></h2>
                        <p>Feast on some heart - warming bites at the Cafelanche after a Chilled out time at Ski India.</p>
                    </div>
                                       
                    <div data-x="80" data-y="74" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Clone Machine <span>Photo Booth</span></h2>
                        <p>Take back memories of the planet in the form of photographs at the Clone Machine.</p>
                    </div>
                    
                    <div data-x="89" data-y="59" class="left defaultHotspot" data-label-align="left" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Snow Mine <span>Souvenir Store</span></h2>
                        <p>Loved the place? Take back a souvenir in the form of Soft Toys, T-Shirts &amp; lots more.</p>
                    </div>
                    
                    <div data-x="36" data-y="72" class="right defaultHotspot" data-label-align="right" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Power Lab <span>VIP Lounge</span></h2>
                        <p>Only the big shots are allowed in here.</p>
                    </div>


                    <div data-x="44" data-y="47" class="right defaultHotspot" data-label-align="right" data-label-distance-x="2"
                        data-stroke-width="5"
                        data-stroke-opacity="1"
                        data-stroke-color="#ffffff"
                        data-filter-opacity="0.8"
                        data-filter-blur="3"
                        data-filter-saturate="0.2"
                        data-mask-radius="10%"
                        data-zoom="1.6">
                        <h2>Drift on ice <span>Ice Skating</span></h2>
                        <p>Glide with the greatest of ease around the capital's indoor ice rink. Whether you dance on ice or just scramble on, it is one of the most magical location u'll see  at Ski India.</p>
                    </div>
                    
                </div>
            </div>
        </div>			 
            </div><!-- End col-md-8 -->
           
        </div><!-- End row -->
    </div><!-- End Container -->
    
    

<!-- Common scripts -->
<script SRC="<?php echo base_url();?>js/jquery-1.11.2.min.js"></script>
<script SRC="<?php echo base_url();?>js/common_scripts_min.js"></script>
<script SRC="<?php echo base_url();?>js/functions.js"></script>

<!-- Gsap Framework -->
<script SRC="<?php echo base_url();?>js/hotspot/js/jquery-migrate-1.2.1.min.js"></script>
    <script SRC="<?php echo base_url();?>js/hotspot/js/TweenMax.min.js"></script>
    <script SRC="<?php echo base_url();?>js/hotspot/js/utils/Draggable.min.js"></script>
    <script SRC="<?php echo base_url();?>js/hotspot/js/imageHotspot.js"></script>
    <script type="text/javascript">
         var main = new imageHotspot('#main', {
             setCurrent : 0,
             width : '100%',
             height : 400,
             mainAxis: 'x',
             strokeWidth: 10,
             autoheight: true,
             zoom: 1.5,
             maskRadius: 150,
             zoomOnHover: false,
             zoomDelay: 1,
             breakpoints: {
                tablet : {
                    maxWidth: 1024,
                    width : '80%',
                    autoheight : true
                },
                phone : {
                    maxWidth: 600,
                    width : '100%',
                    autoheight : true,
                    showCaptions: false
                }
             }
        });
    </script>
</body>
</html>