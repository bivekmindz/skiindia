<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<?php  ?>
    <!-- jQuery Form Validation code -->
    <style>
        input.error {
            border: 1px solid red!important;
        }
        
        select.error {
            border: 1px solid red!important;
        }
        
        textarea.error {
            border: 1px solid red!important;
        }
        
        .error {
            border: 1px solid red;
        }
        
        label.error {
            border: 0px solid red!important;
            color: red;
            font-weight: normal;
            display: inline;
        }
        
        .sessionerror {
            border: 0px solid red!important;
            color: red;
            font-weight: normal;
        }
        
        .skep {
            width: 100%;
            float: left;
            text-align: center;
            padding: 35px 0px 10px 0px;
        }
        
        .skep a {
            color: #fff;
            background-color: #2957A4;
            border-color: #2957A4;
            color: #fff;
            padding: 8px 20px 8px 20px;
            font-weight: 600;
            border-radius: 3px;
        }
        
        .or {
            width: 100%;
            float: left;
            margin-top: 20px;
        }
        
        .or_line {
            width: 100%;
            float: left;
            position: relative;
            height: 1px;
            background: #CCC;
        }
        
        .or_line .or_text {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            background: #fff;
            width: 40px;
            padding: 10px;
            margin: auto;
            display: table;
            border-radius: 50%;
        }
        
        .facenbook_link {
            width: 100%;
            float: left;
        }
        
        .face_div {
            width: 100%;
            float: left;
            text-align: center;
        }
        
        .face_div a {
            background: #3b5998;
            text-align: left;
            padding: 10px;
            color: #fff;
            border-radius: 3px;
            display: inline-block;
        }
        
        .f_color {
            background: #3b5998 !important;
        }
        
        .g_color {
            background: #fff !important;
            color: #333 !important;
            border: solid 1px #dedede !important;
        }
        
        .t_color {
            background: #39a0ce !important;
        }
        
        .div_height {
            height: 410px;
        }
        
        .f_color i {
            font-size: 16px;
        }
        
        .g_color i {
            font-size: 16px;
            color: #d62020;
        }
        
        .t_color i {
            font-size: 16px;
        }
        
        #CaptchaDiv {
            font: bold 30px verdana, arial, sans-serif;
            font-style: italic;
            color: #000000;
            background-color: #FFFFFF;
            padding: 11px 30px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
        }
        
        #CaptchaInput {
            width: 135px;
            font-size: 15px;
            min-height: 38px;
            margin: 7px 0px;
        }
    </style>

    <script language='javascript'>
        // When the browser is ready...
        $(document).ready(function() {

            // Setup form validation on the #register-form element
            $("#addCont").validate({
                // Specify the validation rules

                rules: {
                    emailmob: {
                        required: true,
                        email: true
                    },
                    acctype: "required",
                    pwd: {
                        required: true,
                        minlength: 6
                    },

                },

                submitHandler: function(form) {
                    form.submit();
                }
            });

        });
    </script>

    <section class="step_payment">

        <div class="container main-container">

            <div id="default-screen" class="content-part"><span id="updateFullPage">

                    <section class="row block" id="ticketsAddOnesBlock">
                        <div id="searchResult"><span id="ticketsAddOnsOpen"></span><span id="step1Done">
                                <div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
                                    <div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
                                        <?php require_once('helper/sessiondatetimenoofvisiter.php'); ?>
                                        <form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt135" value="j_idt135">
<a id="j_idt135:step1Edit" href="packages" class="ui-commandlink ui-widget edit" onclick="">
                                                <span class="icon-pencil"></span></a>
                <input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998" autocomplete="off">
                </form>
                <h2 class="relative">
                                            <span class="steps"><span class="stepNum">Step
                                                    1</span></span>
                                        </h2>
                <p class="steps-title title1">Search
                </p>
            </div>

        </div>
        </span>

        </div>
    </section>

    <section class="row block" id="ticketsAddOnesBlock">
        <div id="searchResult"><span id="ticketsAddOnsOpen"></span><span id="step1Done">
                                <div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
                                    <div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
                                        <?php require_once('helper/ticketcost.php'); ?>
<form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt135" value="j_idt135">
<a id="j_idt135:step1Edit" href="packagesstep" class="ui-commandlink ui-widget edit" onclick="">
                                                <span class="icon-pencil"></span></a>
            <input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998" autocomplete="off">
            </form>
            <h2 class="relative">
                                            <span class="steps"><span class="stepNum">Step
                                                    2</span></span>
                                        </h2>
            <p class="steps-title title2">Package
            </p>
        </div>

        </div>
        </span>

        </div>
    </section>
    <section class="row block" id="ticketsAddOnesBlock">
        <div id="searchResult"><span id="ticketsAddOnsOpen"></span><span id="step1Done">
                                <div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
                                    <div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
                                    <?php require_once('helper/addonscost.php'); ?>

<form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt135" value="j_idt135">
<a id="j_idt135:step1Edit" href="addones" class="ui-commandlink ui-widget edit" onclick="">
                                                <span class="icon-pencil"></span></a>
            <input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998" autocomplete="off">
            </form>
            <h2 class="relative">
                                            <span class="steps"><span class="stepNum">Step
                                                    3</span></span>
                                        </h2>
            <p class="steps-title title3">Addons
            </p>
        </div>

        </div>
        </span>

        </div>
    </section>

    <section class="row block" id="ticketsAddOnesBlock">
        <div id="searchResult"><span id="ticketsAddOnsOpen">
                                    <div id="ticketsAddOnsOpen" class="openBlock aboveAll"><span id="step1Header">
                                            <div class="stepHeader stepOpenHere row stepOpen">
                                                <p class="fRight subHead">Which ticket would you like to
                                                    book?</p>
                                                <h2 class="relative">
                                                    <span class="steps"><span class="stepNum">Step
                                                            4</span></span>
            </h2>
            <p class="steps-title title4">Login / Register</p>
        </div>
        </span><span id="refreshStep1Panel"><span id="step1AddonDisp">

<div id="AddOnsBlock" class="infoBlock relative addOnsBlock">
    <div class="ticketsWithAddOns">
        <div class="row ticketdescription mart20">

            <div class="row">
         <div class="col-md-12  col-bdr-right">

                <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading" style="background-color:#2EADF4;padding:4px 10px;">
        <h4 class="panel-title collapse1">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Login <i class="more-minus fa fa-minus" style="float:right;cursor:pointer;"></i> <i class="more-plus fa fa-plus" style="float:right;cursor:pointer;"></i></a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">

            <div class="panel panel-default">

                <div class="panel-body">

                <div class="flashmsg"><?php echo $message; ?></div>

                   <form method="post" action="" id="addCont" name="register-form"  class="formm">
                        <div class="form-group">
                            <label for="fname">Email Address</label>
                            <input type="email" name="emailmob" id="emailmob" class="form-control required_field" placeholder="Email " required>
                        </div>
                        <div class="form-group">
                            <label for="lname">Password</label>
                             <input type="password" class="form-control required_field" id="pwd" name="pwd" placeholder="Password" required>
                        </div>

                        <div class="form-inline">
                            <div class="form-group text-center">
                                <div class="buton" style="padding:0px 0px 0px 0px;">
                                <button  name="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime" type="submit" role="button" aria-disabled="false"  value="Sign In"><span class="ui-button-text ui-c">Sign In</span></button>&nbsp;
        </div>
        </div>

        <div class="form-group" style="float:right;">
            <label for="offers">
                <a href="forgetpassword" target="_blank">Forgot Password?</a>
            </label>
        </div>
        </div>

        </form>
        </div>

        </div>

        </div>
        </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color:#2EADF4;padding:4px 10px;">
                <h4 class="panel-title collapse2">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed">Register <i class="more-minus fa fa-minus" style="float:right;cursor:pointer;"></i> <i class="more-plus fa fa-plus" style="float:right;cursor:pointer;"></i></a>
        </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">

                    <div class="panel panel-default">

                        <div class="panel-body">
                            <form name="register-form" class="formm" id="registration-form" method="POST" action="">
                                <div class="guest-details">
                                    <div class="widget-header">
                                        <div class="widget-title"></div>
                                    </div>
                                    <div class="widget-content clearfix">
                                        <div class="flashmsg">
                                            <?php echo validation_errors(); ?>
                                                <?php echo $msg; ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <div class="form-group">
                                                    <select class="form-control " id="sel1" required style=" min-width: 72px;" name="regtitle">
                                                        <option>Mr</option>
                                                        <option>Mrs</option>
                                                        <option>Ms</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" name="fname" class="form-control-1 form-control_width" placeholder="Enter Your Name" pattern="[A-Za-z\s]+" required title="First Name should only contain  Alphabets. e.g. John" maxlength="60" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6 ">
                                                <div class="form-group">
                                                    <input type="email" name="email" class="form-control-1" placeholder="Enter Your Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required title="The input is not a valid email address" maxlength="50" value="">
                                                </div>
                                            </div>
                                            <div class="col-xs-6 ">
                                                <div class="form-group">
                                                    <input type="tel" name="mobileno" class="form-control-1" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Numbers. e.g. 9999999999" maxlength="10" value="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-6 ">
                                                <div class="form-group">
                                                    <div id="validationErrorMessage" class="has-error"><span class="help-block"></span></div>
                                                    <input type="password" id="password" name="password" class="form-control-1" placeholder="Enter Password" required title="Enter Password" maxlength="35" minlength="6">
                                                </div>
                                            </div>
                                            <div class="col-xs-6 ">
                                                <div class="form-group">
                                                    <input type="password" id="confirm_password" name="cpassword" class="form-control-1" placeholder="Enter Confirm Password" required title="Enter Confirm Password" maxlength="35" minlength="6">
                                                    <span class="err00" style="color: red;display: none">PASSWORD DON'T MATCH</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-xs-12 form-group">
                                                <textarea required id="message_contact" name="reg_address" class="form-control-1" placeholder="Write Your Address"></textarea>
                                            </div>
                                        </div>
                                        <div class="row"><span id="j_idt211:updateDetails">
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="reg_city" class="form-control-1" required placeholder="Enter Your City" pattern="[A-Za-z\s]+"  title="City Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="reg_state" class="form-control-1" required placeholder="Enter Your State" pattern="[A-Za-z\s]+"  title="State Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="">
                                                                                    </div>
                                                                                </div></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6 ">
                                                <div class="form-group">
                                                    <input type="text" name="reg_zip" required class="form-control-1" placeholder="Enter Your Zipcode" title="Zip should only contain  Number. e.g. 110014" maxlength="6" value="">
                                                </div>
                                            </div>
                                            <div class="col-xs-6 ">
                                                <div class="form-group">

                                                    <select class="form-control " id="sel1" required style="min-width: 72px;" name="reg_country">
                                                        <option value="">Enter Your Country</option>
                                                        <?php foreach ($Countrys as $key => $value) {?>
                                                            <option value="<?php echo $value->name?>">
                                                                <?php echo $value->name?>
                                                            </option>
                                                            <?php  }?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <input type="checkbox" id="creditTermsCheck" name="checkbox" required title="Accept Tearm And Condition"> &nbsp;I Agree to the <a href="tearmcondition" target="_blank"> Terms Conditions</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="sub_div text-center">

                                    <div class="continue">
                                        <button name="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime" type="submit" onclick="return functi()" role="button" aria-disabled="false" value="Sign up"><span class="ui-button-text ui-c">Sign up</span></button>&nbsp;
                                        <script>
                                            function functi() {
                                                var password = document.getElementById("password");
                                                var confirm_password = document.getElementById("confirm_password");

                                                if (password.value != confirm_password.value) {
                                                    $(".err00").css('display', 'block').fadeOut(3000);

                                                    return false;
                                                } else {
                                                    confirm_password.setCustomValidity('');
                                                }
                                            }
                                        </script>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color:#2EADF4;padding:4px 10px;">
                <h4 class="panel-title collapse2">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed">Guest <i class="more-minus fa fa-minus" style="float:right;cursor:pointer;"></i> <i class="more-plus fa fa-plus" style="float:right;cursor:pointer;"></i></a>
        </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <!-- Guest Payment -->
                            <?php // echo base_url().'summary' ?>
                                <form method="POST" name="customerData" action="<?php  echo base_url().'summary' ?>" id="ccform" onsubmit="return checkform(this);">
                                    <div class="guest-details">
                                        <div class="widget-header">
                                            <div class="widget-title"></div>
                                        </div>
                                        <div class="widget-content clearfix">
                                            <div class="row">
                                                <div class="col-xs-2">
                                                    <div class="form-group">
                                                        <select class="form-control " id="sel1" required style=" min-width: 72px;" name="title">
                                                            <option>Mr</option>
                                                            <option>Mrs</option>
                                                            <option>Ms</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                                    <div class="form-group">
                                                        <input type="text" name="billing_name" class="form-control-1 form-control_width" placeholder="Enter Your Name" pattern="[A-Za-z\s]+" required title="First Name should only contain  Alphabets. e.g. John" maxlength="60" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6 ">
                                                    <div class="form-group">
                                                        <input type="email" name="billing_email" class="form-control-1" placeholder="Enter Your Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required title="The input is not a valid email address" maxlength="50" value="">
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 ">
                                                    <div class="form-group">
                                                        <input type="tel" name="billing_tel" class="form-control-1" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Numbers. e.g. 9999999999" maxlength="10" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- <div class="col-xs-12 form-group">
                                                                                    <div class="widget-header">
                                                                                        <p>Contact Details</p>
                                                                                    </div>
                                                                                </div> -->
                                                <div class="col-xs-12 form-group">
                                                    <textarea required id="message_contact" name="billing_address" class="form-control-1" placeholder="Write Your Address"></textarea>
                                                </div>
                                            </div>
                                            <div class="row"><span id="j_idt211:updateDetails">
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="billing_city" class="form-control-1" required placeholder="Enter Your City" pattern="[A-Za-z\s]+"  title="City Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="billing_state" class="form-control-1" required placeholder="Enter Your State" pattern="[A-Za-z\s]+"  title="State Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="">
                                                                                    </div>
                                                                                </div></span>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6 ">
                                                    <div class="form-group">
                                                        <input type="text" name="billing_zip" required class="form-control-1" placeholder="Enter Your Zipcode" title="Zip should only contain  Number. e.g. 110014" maxlength="6" value="">
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 ">
                                                    <div class="form-group">

                                                        <select class="form-control " id="sel1" required style="min-width: 72px;" name="billing_country">
                                                            <option value="">Enter Your Country</option>
                                                            <?php foreach ($Countrys as $key => $value) {?>
                                                                <option value="<?php echo $value->name?>">
                                                                    <?php echo $value->name?>
                                                                </option>
                                                                <?php  }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <input type="checkbox" id="creditTermsCheck" name="checkbox" required title="Accept Tearm And Condition"> &nbsp;I Agree to the <a href="tearmcondition" target="_blank"> Terms Conditions</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sub_div text-center">

                                        <div class="continue">
                                            <button id="" name="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime" type="submit" role="button" aria-disabled="false" value="cartses" onclick=" cvalid()"><span class="ui-button-text ui-c">Submit</span>
                                        </div>
                                    </div>
                                </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>

        </div>

        <div class="or">

            <div class="facenbook_link hide">
                <div class="face_div">
                    <a href="#" class="f_color"><i class="fa fa-facebook" aria-hidden="true"></i> Continue with Facebook</a>
                    <a href="#" class="g_color"><i class="fa fa-google-plus" aria-hidden="true"></i> Continue with Google</a>
                    <a href="#" class="t_color"><i class="fa fa-twitter" aria-hidden="true"></i> Continue with Twitter</a>
                </div>
            </div>
        </div>

        <div class="skep hide">
            <a href="summary" style="font-size:18px;padding:6px 32px;">Skip</a>
        </div>

        </div>
        </div>

        </div>
        </div>

        </span>
        </span>
        </div>
        </span>

        </div>
    </section>

    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
                                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                                    <p class="fRight subHead"></p>
                                    <h2 class="relative">
                                        <span class="steps"><span class="stepNum">Step 5</span></span>
        </h2>
        <p class="steps-title title5">Summary</p>
        </div>
        </span><span id="step3open"></span>

    </section>

    <!--   <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
                                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                                    <p class="fRight subHead"></p>
                                    <h2 class="relative">
                                        <span class="steps"><span class="stepNum">Step 6</span></span>
                                    </h2>
                                    <p class="steps-title title6">Payment</p>
                                </div></span><span id="step3open"></span>

                    </section> -->

    </span>
    </div>
    </div>

    <!-- onclick="return cvalid()" -->
    <script type="text/javascript">
        function cvalid() {

            var billing_name = document.forms["customerData"]["billing_name"].value;
            var billing_email = document.forms["customerData"]["billing_email"].value;
            var billing_tel = document.forms["customerData"]["billing_tel"].value;
            var billing_address = document.forms["customerData"]["billing_address"].value;
            var billing_city = document.forms["customerData"]["billing_city"].value;
            var billing_state = document.forms["customerData"]["billing_state"].value;
            var billing_zip = document.forms["customerData"]["billing_zip"].value;
            var billing_country = document.forms["customerData"]["billing_country"].value;
            if (billing_name == "") {
                alert("Name must be filled out");
                return false;
            }
            if (billing_email == "") {
                alert("Email must be filled out");
                return false;
            }
            if (billing_tel == "") {
                alert("Telephone must be filled out");
                return false;
            }
            if (billing_address == "") {
                alert("Address must be filled out");
                return false;
            }
            if (billing_city == "") {
                alert("City must be filled out");
                return false;
            }
            if (billing_state == "") {
                alert("State must be filled out");
                return false;
            }

            if (billing_zip == "") {
                alert("Zipcode must be filled out");
                return false;
            }
            if (billing_country == "") {
                alert("Country must be filled out");
                return false;
            }

            var str3 = $("#amount_s").val();
            var str4 = "<?php echo $paymentpac->total; ?>";

            var formData = new FormData($("#ccform")[0]);

            $.ajax({
                url: '<?php echo base_url(); ?>webapp/Addones/guestregistered',
                type: 'POST',
                cache: false,
                data: formData,
                async: false,
                processData: false,
                contentType: false,
                success: function(data) {

                    // window.location = "<?php //echo base_url() ?>summary";
                    return false;
                }
            });
            return true;

        }
        // Remove the spaces from the entered and generated code
        function removeSpaces(string) {
            return string.split(' ').join('');
        }
    </script>
    </section>