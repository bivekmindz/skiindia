 
    
    
     <section class="step_payment">
    <div class="container main-container">

     
      <div id="default-screen" class="content-part"><span id="updateFullPage">
          
          <section class="row block" id="ticketsAddOnesBlock">

 


    

<div class="container margin_30">
      <h3>GROUP BOOKING</h3>
        <p class="margin_20_btm">Fill in the form below to request for a trip details @Snowworld (Minimum 20 people required)</p>
      <div class="row">         
          <div class="col-md-7">            
            <div id="message-contact"></div>

 <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php

                                    echo  $message;
                                    if($this->session->flashdata('message')){
                                       // echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
            
        <form role="form" name="groupBookingForm" id="groupBookingForm" action="" method="post">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Type of Booking</label>
                                <select class="form-control-1" name="bookingType" id="bookingType" required>
                                    <option value="" disabled="disabled" selected="selected">Select Type of Booking</option>
                                     <?php 
                                      if($bookingtype->scalar=='Something Went Wrong'){}
                                        else{
                                      foreach ($bookingtype as $key => $value) {   ?>
                                    <option value="<?php echo $value['bookingname']; ?>"><?php echo $value['bookingname']; ?></option>
                                 <?php }} ?>
                                </select>
                                <div class="error-div"></div>
              </div>
                            
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Your Full Name</label>


                 <input type="text" name="full_name" class="form-control-1" placeholder="Enter Full Name" pattern="[A-Za-z\s]+" required title="Full Name Should Only Contain  Alphabets. e.g. John" maxlength="30">


                                <div class="error-div"></div>
              </div>
                            
            </div>
          </div>
          <!-- End row -->
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Email Address</label>
 



                <input type="email" name="email_address" class="form-control-1" placeholder="Enter Your Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  required title="Enter Your Valid Email Address" maxlength="50">


                                 <div class="error-div"></div>
              </div>
                           
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Phone</label>
 <input type="text" name="phone_number" class="form-control-1" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Number. e.g. 9999999999" maxlength="10">


                                 <div class="error-div"></div>
              </div>
                           
            </div>
          </div>
                    <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Date of Visit</label>
                              <input class="date-pick form-control formmm1" data-date-format="dd/mm/yyyy" type="text" id="datepicker1" name="departDate" placeholder="Pick Session Date" required title="Pick Session Time">
                                 <div class="error-div"></div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Pick Session Time</label>
                <select class="form-control-1" name="sessionTime" id="sessionTime" required title="Select Pick Session Time">
                                    <option value="" disabled="disabled" selected="selected">Pick Session Time</option>
                                    <option value="11:00 AM">11:00 AM</option>
                                    <option value="12:15 PM">12:15 PM</option>
                                    <option value="01:30 PM">01:30 PM</option>
                                    <option value="03:00 PM">03:00 PM</option>
                                    <option value="04:30 PM">04:30 PM</option>
                                    <option value="06:00 PM">06:00 PM</option>
                                    <option value="07:30 PM">07:30 PM</option>
                                    <option value="09:00 PM">09:00 PM</option>
                                </select>
                                 <div class="error-div"></div>
              </div>
            </div>
          </div>
          <div class="row">
                      <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Number of People</label>
                <input type="text" id="numberPeople" name="number_people"  class="form-control-1" placeholder="Enter Number of People" min="20" pattern="[0-9]+" required title="Enter Minimum 20 Number of People">
                                 <div class="error-div"></div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Social Group</label>
                <div class="radio">
                  <label class="radio-inline"><input  type="radio" onclick="$('#gp_name').css('display','block');" name="socialGroupRadio"  value="Yes">Yes </label>
                                    <label class="radio-inline"><input checked="checked" onclick="$('#gp_name').css('display','none');" type="radio" name="socialGroupRadio" value="No">No </label>
                                </div>
              </div>
            </div>
          </div>
          <div class="row">                    
            <div class="col-md-6 col-sm-6">
              <div class="form-group" id="gp_name" style="display: none">
                <label>Social Group Name</label>
                <input type="text" id="inputSocialGroupName" name="socialGroupName" class="form-control-1" placeholder="Enter Social Group Name"  title="Enter Social Group Name">
                                 <div class="error-div"></div>
              </div>
            </div>
            <div class="col-md-6">
                          <div class="form-group">
                                &nbsp;
                            </div>
            </div>
            <div style="clear:both;"></div>
                        <div class="col-md-6">
                          <div class="form-group">
                                <input type="submit" value="Submit" name='Submit' class="btn_1" id="submit-contact">
                                <input type="hidden" name="sendTo" value="priyanka@chiliadprocons.in,paramjit@chiliadprocons.in">
                                <input type="hidden" name="affId" value="SNOW-CHILIAD">
                                <input type="hidden" name="redirectUrl" value="/group_booking.html">
                            </div>
            </div>            
          </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="groupbooking-response"></div>
                        </div>
                    </div>
        </form>
                  <div class="row">
            <div class="col-md-12">
                          <p class="text-justify">Note: Due to vast number of emails we receive every day we seek your understanding to allow us to get back to you within 2-3 working days. The corporate office is open from Mon to Sat from 10 am to 7 pm. So please note that the emails sent on Sundays, Public Holidays or after office hours will be received the next working day.</p>
            </div>
          </div>               
            </div><!-- End col-md-8 -->

            <div class="col-md-5">                  
              <div class="box_style_1" id="general_facilities">
                  <h3><i class="icon-calendar"></i> Session Information</h3>
                    <table class="table table-striped table-bordered session_table">
                      <thead>
                    <tr>
                      <th>Start Time</th>
                      <th>End Time</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>11:00 AM</td>
                      <td>12:30 PM</td>
                    </tr>
                    <tr>
                      <td>01:00 PM</td>
                      <td>02:30 PM</td>
                    </tr>
                    <tr>
                      <td>03:00 PM</td>
                      <td>04:30 PM</td>
                    </tr>
                    <tr>
                      <td>05:00 PM</td>
                      <td>06:30 PM</td>
                    </tr>
                    <tr>
                      <td>07:00 PM</td>
                      <td>08:30 PM</td>
                    </tr>
                    <tr>
                      <td>09:00 PM</td>
                      <td>10:30 PM</td>
                    </tr>
                  </tbody>

                    </table>
                </div>
              <div class=" box_style_2">
					<i class="icon_set_1_icon-90"></i>
					<h4>Need help? Call us</h4>
					<a href="tel://01202595150"> +91 120 259 51 50</a> /
                            <a href="tel://01202595151">51</a> /
                            <a href="tel://01202595152">52</a> /
                            <a href="tel://01202595153">53</a> <br>
					<small>Monday to Sunday 11.00am - 8.30pm</small>
				</div>
          </div><!-- End col-md-4 -->
        </div><!-- End row -->
    </div></div>


     
    </section> </div> </div>    </section>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>





     <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

     <?php //p($vieww); ?>
     <!-- jQuery Form Validation code -->
     <style>
         .error{
             color: red;
         }
     </style>
     <!-- <script>

         $(function() {


             $(".flashmsg").delay(1000).fadeOut();


             $("#groupBookingForm").validate({
                 rules: {
                     bookingType: "required",
                     full_name:{
                         required: true,


                     },
                     email_address: {
                         required: true,
                         email : true
                     },
                     phone_number: {
                         required: true,
                         number: true
                     },
                     departDate: "required",
                     sessionTime: "required",
                     number_people: {
                         required: true,
                         number: true
                     }
                 },
                 submitHandler: function (form) {
                     form.submit();
                 }
             });
         });
</script> -->