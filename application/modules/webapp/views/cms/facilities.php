


    
 <div class="container margin_60">
  <h3 class="text-center text-uppercase">Facilities</h3>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="row">
          <p class="text-center">All Gears to stay in minus temperature, Staff Assistance to enjoy all kind of Rides, Medical Assistance, Birthday Lounge, Cafe etc.</p>
          
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <section class="box_facilities_wp">
                        <div class="box_facilities facilities-hover">
                            <a href="#" class="facilities-overlay">
                            <h2>​​Minus Temperature</h2>                   
                            </a>
                            <div class="facilities-img">
                                <img SRC="img/facilities/temp.jpg" alt="​​​​Minus Temperature">
                            </div>
                        </div>
                        </section>
                    </div>
                
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <section class="box_facilities_wp">
                        <div class="box_facilities facilities-hover">
                            <a href="#" class="facilities-overlay">
                            <h2>Staff Assistance</h2>                            
                            </a>
                            <div class="facilities-img">
                                <img SRC="img/facilities/staff_assit.jpg" alt="Staff Assistance">
                            </div>
                        </div>
                        </section>
                    </div> 
                
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <section class="box_facilities_wp">
                        <div class="box_facilities facilities-hover">
                            <a href="#" class="facilities-overlay">
                            <h2>Medical Assistance</h2>                       
                            </a>
                            <div class="facilities-img">
                                <img SRC="img/facilities/medical_assit.jpg" alt="Medical Assistance">
                            </div>
                        </div>
                        </section>
                    </div> 
                
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <section class="box_facilities_wp">
                        <div class="box_facilities facilities-hover">
                            <a href="#" class="facilities-overlay">
                            <h2>Birthday Lounge</h2>                       
                            </a>
                            <div class="facilities-img">
                                <img SRC="img/facilities/lounge.jpg" alt="Birthday Lounge">
                            </div>
                        </div>
                        </section>
                    </div>
                
                          
                    
                    
                </div><!-- End row -->
            </div><!-- End col-lg-9 -->            
        </div><!-- End row -->
    </div><!-- End Container -->
    