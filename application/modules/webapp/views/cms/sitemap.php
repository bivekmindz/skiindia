
    
<div class="container margin_30">
      <h3>Site Map</h3>
      <div class="row">         
          <div class="col-md-12">             
          <div class="col-md-6">
            <h3 class="sitemap scifiheader">Top menu pages</h3>
            <ul>
              <li><a style="color: #000;" HREF="<?php echo base_url();?>">Home</a></li>
              <li>Explore
                <ul>
                  <li><a HREF="activities">Activities</a></li>
                  <li><a HREF="park_map">Park Map</a></li>
                </ul>
              </li>
              <li>Group Booking
                <ul>
                  <li><a HREF="group_booking">Group Booking</a></li>
                 
                </ul>
              </li>
              <li>Media Gallery
                <ul>
                  <li><a HREF="image_gallery">Photo Gallery</a></li>
                  <li><a HREF="press_releases">Video Gallery</a></li>
                 
                </ul>
              </li>
              <li><a style="color: #000;" HREF="contacts">Contact Us</a></li>              
            </ul>
          </div>
          <div class="col-md-6">
            <h3 class="sitemap scifiheader">Footer menu pages</h3>
            <ul>
              <li>Ski India<sup>®</sup>
                <ul>
                  <li><a HREF="about_us">About Us</a></li>
                  <li><a HREF="http://chiliadprocons.in/#team">Team</a></li>
                  <li><a HREF="marketing_promotion">Marketing &amp; Promotion</a></li>
                  <li><a HREF="park_rules">Park Rules</a></li>
                  <li><a HREF="privacy_policy">Privacy policy</a></li>
                  <li><a HREF="sitemap">Sitemap</a></li>
                </ul>
              </li>
              <li>Plan Your Visit
                <ul>
                  <li><a HREF="operating_hours">Operating Hours</a></li>
                  <li><a HREF="tickets">Tickets Price</a></li>
                  <li><a HREF="facilities">Facilities</a></li>
                  <li><a HREF="faqs">FAQs</a></li>
                 
                  <li><a HREF="http://chiliadprocons.in/" target="_blank">Visit Corporate Site</a></li>
                </ul>
              </li>
            </ul>
          </div>       
            </div><!-- End col-md-8 -->
           
        </div><!-- End row -->
    </div><!-- End Container -->
    