<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3 snowworld">
                    <h3>Ski India<sup>®</sup></h3>
                    <ul>
                        <li><a href="http://skiindiadelhi.com/about_us.html">About Us</a></li>
                        <li><a href="http://skiindiadelhi.com/marketing_promotion.html">Marketing &amp; Promotion</a></li>
                        <li><a href="http://skiindiadelhi.com/park_rules.html">Park Rules</a></li>
                        <li><a href="http://skiindiadelhi.com/privacy_policy.html">Privacy policy</a></li>
                        <li><a href="http://skiindiadelhi.com/sitemap.html">Sitemap</a></li>
                        <li><a href="http://chiliadprocons.in/" target="_blank">Visit Corporate Site</a></li>
                    </ul>
                </div>                
                <div class="col-md-3 business">
                    <h3>Plan Your Visit <i class="icon-briefcase"></i></h3>
                    <ul>                        
                        <li><a href="http://skiindiadelhi.com/operating_hours.html">Operating Hours</a></li>
                        <li><a href="http://skiindiadelhi.com/tickets.html">Tickets Price</a></li>
                        <li><a href="http://skiindiadelhi.com/facilities.html">Facilities</a></li>
                        <li><a href="http://skiindiadelhi.com/faqs.html">FAQs</a></li>
                        <li><a href="http://skiindiadelhi.com/careers.html">Careers</a></li>
                        <li><a href="<?php echo base_url();?>bookingquery">Booking Query</a></li>             
                        <li><a href="<?php echo base_url();?>partnerlogin">Partner</a></li>
                    </ul>
                </div>
                <div class="col-md-3 direction">
                    <h3>Get directions <i class="icon-location-3"></i></h3>                     
                    <p>Get direction from your destination.</p>
                    <form id="get_direction" method="get" action="https://www.google.co.in/maps/place/" target="_blank">
                    <div class="form-group">
                        <input id="saddr" name="saddr" value="" placeholder="Enter your location" class="form-control-1">
                        <input type="hidden" name="daddr" value="Ski+India,+DLF+Mall+of+India,+Plot+M-03,+Ecity+Bioscope+Rd,+Sector+18,+Noida,+Uttar+Pradesh+201301"/>
                    </div>
                        <input type="submit" value="Show on map" class="btn_1 white">
                    </form>
                </div>                
                <div class="col-md-3 address">
                    <h3>Find Us <i class="icon-location-inv"></i></h3>
                    <ul id="contact_details_footer">
                        <p class="footer_address">Ski India, L05 &amp; L06, DLF Mall of India, Sector 18, Noida, Gautam Buddha Nagar, Uttar Pradesh 201301, India.</p></li>
                        <li>
                            <i class="icon-phone"></i>
                            <a href="tel://01202595150"> +91 120 259 51 50</a> / 
                            <a href="tel://01202595151">51</a> / 
                            <a href="tel://01202595152">52</a> / 
                            <a href="tel://01202595153">53</a>
                        </li>
                        <li><i class="icon-mail"></i><a href="mailto:info@skiindiadelhi.com"> info@skiindiadelhi.com</a></li>
                    </ul> 
                </div>
            </div><!-- End row -->
            <div class="row">
                <div class="col-md-12">
                    <div id="social_footer">
                        <ul>
                            <li><a href="https://www.facebook.com/SkiIndiaDelhi/" target="_blank"><i class="icon-facebook"></i></a></li>
                            <li><a href="https://twitter.com/ski_india" target="_blank"><i class="icon-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/ski_india/" target="_blank"><i class="icon-instagram"></i></a></li>
                            <li><a href="https://plus.google.com/+Skiindiadelhi-Official" target="_blank"><i class="icon-google"></i></a></li>
                            <li><a href="https://www.youtube.com/c/Skiindiadelhi-Official" target="_blank"><i class="icon-youtube-play"></i></a></li>
                        </ul>
                        <p>© Ski India 2017</p>
                    </div>
                </div>
            </div><!-- End row -->
        </div><!-- End container -->
    </footer><!-- End footer -->
    
<div id="toTop"></div><!-- Back to top button -->

<!-- Common scripts -->
<script src="<?php echo base_url();?>js/jquery-1.11.2.min.js"></script>
<script src="<?php echo base_url();?>js/common_scripts_min.js"></script>
<script src="<?php echo base_url();?>js/functions.js"></script>
<script src="<?php echo base_url();?>assets/validate.js"></script>
<!-- Specific scripts -->
<script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>js/jquery.validate.min.js"></script>
</body>
</html>