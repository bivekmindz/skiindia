<style>
canvas{
-moz-user-select: none;
-webkit-user-select: none;
-ms-user-select: none;
}
</style>
 <script src="<?php echo base_url(); ?>js/Chart.bundle.js"></script>
  <script src="<?php echo base_url(); ?>js/utils.js"></script>
<!-- <div class="wrapper"> -->
<h1>Dashboard</h1>
    <div class="col-lg-12">
    <div class="row">
    <div class="page_name">
        
    </div>
    <div class="all-invoeic b1">
        <div class="col-md-12">
            <div class="left_bar">
                <div class="col-md-4">
                    <div class="box1 b_radius">
                        <p>Total Order</p>
                        <h1><?php echo ($orderdata->ordercnt) ? $orderdata->ordercnt : '0' ?></h1>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box2 b_radius">
                        <p>Last Transaction Amount</p>
                        <h1><?php echo ($lastamount->totalamount) ? $lastamount->totalamount : '0' ?></h1>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box1 b_radius">
                        <p>Last Transaction Date</p>
                        <h1><?php echo ($lastamount->addedon) ? $lastamount->addedon : '0' ?></h1>
                    </div>
                </div>
            </div>
            <div class="left_bar">
                <div class="col-md-4">
                    <div class="box1 b_radius">
                        <p>Current Ticket Sold</p>
                        <h1><?php echo ($lastamount->ticketid) ? $lastamount->ticketid : '0' ?></h1>
                    </div>
                </div>
                <div class="col-md-4 b_radius">
                    <div class="box2">
                        <p>User Total Amount</p>
                        <h1><?php echo ($totaluseramount->totalamount) ? $totaluseramount->totalamount : '0' ?></h1>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box1 b_radius">
                        <p>Total Credit Amount</p>
                        <h1><?php echo ($creditamount->total_amount) ? $creditamount->total_amount : '0' ?></h1>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12"><hr /></div>

 <div class="date_form">
    <form id="" method="post" action="partnerdashboard">
    
        <div class="box12 form_con">
         <div class="col-md-3">
           <input type="text" placeholder="From-date" required value ="<?php echo ($_POST['datepicker1']) ? $_POST['datepicker1'] : '' ?>" name="datepicker1" required id="datepicker1">
           </div>
           <div class="col-md-3">
           <input type="text" placeholder="To-date" value ="<?php echo ($_POST['datepicker2']) ? $_POST['datepicker2'] : '' ?>" name="datepicker2" id="datepicker2">
            </div>
          <div class="col-md-3"> <input type="submit" value ="Search" name="search" id="search" class="bnt_s"></div>
        
    </div>
    </form>
    </div>
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12 col-xs-12">
                    
                    <div class="page_box">
                        <div style="width:100%; float: left;">
                            <canvas id="canvas"></canvas>
                        </div>
                       <!--  <div class="count">
                        <div class="total-order">
                            Total Order : <span><?php echo $orderdata->ordercnt ?></span>
                        </div>
                         <div class="total-order">
                           Last Transaction Amount: <span><?php echo $lastamount->totalamount ?></span>
                          
                        </div>
                         <div class="total-order">
                            Last Transaction Date : <span><?php echo $lastamount->addedon ?></span>
                        </div>
                        <div class="total-order">
                            User Total Amount : <span>18</span>
                        </div>
                        <div class="total-order">
                            Last Added Amount : <span><?php echo $lastamount->totalamount ?></span>
                        </div>
                        <div class="total-order">
                            Current Ticket Sold : <span><?php echo $lastamount->ticketid ?></span>
                        </div>
                    </div> -->
                    </div>
                </div>                
            </div>
        </div>
    </div>
    





 
<script>
        var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var config = {
        type: 'line',
        data: {
        labels: [
        <?php foreach($transamount as $value){ ?>
        "<?php echo $value->monthname ?>",
        <?php } ?>
        ],
        datasets: [{
         label: "Total Amount",
        backgroundColor: window.chartColors.red,
        borderColor: window.chartColors.red,
        data: [
         <?php foreach($transamount as $value){ 
            echo $value->amnt.',';
          } ?>
        ],
        fill: false,
        }, {
        label: "Total Order Count",
        fill: false,
        backgroundColor: window.chartColors.blue,
        borderColor: window.chartColors.blue,
        data: [
       <?php foreach($transamountcount as $value){ 
          echo $value->ordercnt.',';
          }  
        ?>
        ],
        }]
        },
        options: {
        responsive: true,
        title:{
        display:true,
        // text:'Chart.js Line Chart'
        },
        tooltips: {
        mode: 'index',
        intersect: false,
        },
        hover: {
        mode: 'nearest',
        intersect: true
        },
        scales: {
        xAxes: [{
        display: true,
        scaleLabel: {
        display: true,
        labelString: 'Month'
        }
        }],
        yAxes: [{
        display: true,
        scaleLabel: {
        display: true,
        labelString: 'Value'
        }
        }]
        }
        }
        };
        window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx, config);
        };
        document.getElementById('randomizeData').addEventListener('click', function() {
        config.data.datasets.forEach(function(dataset) {
        dataset.data = dataset.data.map(function() {
        return randomScalingFactor();
        });
        });
        window.myLine.update();
        });
        var colorNames = Object.keys(window.chartColors);
        document.getElementById('addDataset').addEventListener('click', function() {
        var colorName = colorNames[config.data.datasets.length % colorNames.length];
        var newColor = window.chartColors[colorName];
        var newDataset = {
        label: 'Dataset ' + config.data.datasets.length,
        backgroundColor: newColor,
        borderColor: newColor,
        data: [],
        fill: false
        };
        for (var index = 0; index < config.data.labels.length; ++index) {
        newDataset.data.push(randomScalingFactor());
        }
        config.data.datasets.push(newDataset);
        window.myLine.update();
        });
        document.getElementById('addData').addEventListener('click', function() {
        if (config.data.datasets.length > 0) {
        var month = MONTHS[config.data.labels.length % MONTHS.length];
        config.data.labels.push(month);
        config.data.datasets.forEach(function(dataset) {
        dataset.data.push(randomScalingFactor());
        });
        window.myLine.update();
        }
        });
        document.getElementById('removeDataset').addEventListener('click', function() {
        config.data.datasets.splice(0, 1);
        window.myLine.update();
        });
        document.getElementById('removeData').addEventListener('click', function() {
        config.data.labels.splice(-1, 1); // remove the label first
        config.data.datasets.forEach(function(dataset, datasetIndex) {
        dataset.data.pop();
        });
        window.myLine.update();
        });
        </script>

        <style>
            .count{
                width: 25%;
                float: left;
                padding: 0 15px;
                box-sizing: border-box;
                }
                .total-order{
                    font-size: 18px;
                    line-height: 24px;
                    color: #393939;
                }
                .total-order{
                    padding: 10px 0;
                    border-bottom: 1px solid #c1c1c1;
                }
                .total-order span{
                    color: #1B78C7;
                    font-size: 22px;
                    line-height: :24px;
                    font-weight: 600;
                }
                .all-invoeic .left_bar .box2 p, .all-invoeic .left_bar .box1 p {
    padding: 5px 20px;
    font-size: large;
}

</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

     //$.noConflict();
     $("#datepicker1").datepicker({
         dateFormat: 'dd-mm-yy',
        // minDate: 0,
         maxDate: "-1D "
     });

     $("#datepicker2").datepicker({
         dateFormat: 'dd-mm-yy',
        // minDate: 0,
         //maxDate: "-1M "
     });

</script>