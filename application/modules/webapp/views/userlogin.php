<style>
.or {
    width:100%;
    float:left;
    height:100px;
    margin-top:20px;
}
.or_line {
    width:100%;
    float:left;
    position:relative;
    height:1px;
    background:#CCC;
}
.or_line .or_text {
position: absolute;
top: 0;
right: 0;
left: 0;
bottom: 0;
background: #fff;
width: 40px;
padding: 10px;
margin: auto;
display: table;
border-radius: 50%;
}
.facenbook_link {
width: 100%;
float: left;
margin-top: 35px;
}
.face_div {
    width:100%;
    float:left;
    text-align:center;
}
.face_div a {
    background:#3b5998;
    text-align:left;
    padding:10px;
    color:#fff;
    border-radius:3px;
    display: inline-block;
}
.f_color {
    background:#3b5998 !important;
}
.g_color {
    background:#fff !important;
    color:#333 !important;
    border: solid 1px #dedede !important;
}
.t_color {
    background:#39a0ce !important;
}
.div_height {
    height:410px;
}
.f_color i {
    font-size:16px;
}
.g_color i {
    font-size:16px;
    color:#d62020;
}
.t_color i {
    font-size:16px;
}
</style>

<section class="login_page">
    <div class="container">
        
        <div class="row">
            
            
            <div>
                <div class="col-md-12">
                    <form class="form-signin_2 div_height" method="post">
                        <div class='flashmsg'>
                            
                            <?php echo validation_errors(); ?>
                            <?php
                            echo $emsg;
                            if($this->session->flashdata('message')){
                            echo $this->session->flashdata('message');
                            }
                            ?>
                        </div>
                        <h2 class="form-signin-heading to_p"> <i class="icon-user"></i> Please login</h2>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" name="emailmob" class="form-control-1" placeholder="Enter Your Email" required title="The input is not a valid email address" maxlength="50">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="pwd" class="form-control-1" placeholder="Enter Your Password" required title="The input is not a valid Password" maxlength="35" min="6">
                        </div>
                        
                        
                        <button class="btn btn-lg btn-primary " type="submit" name="submit" value="Login">Login</button>
                        <label style="margin-left:15px;">
                            <a href="forgetpassword" class="foge"><i class="icon-right-2"></i>Forgot Your Password?</a>
                        </label>
                        
                        
                        <label class="checkbox">
                            <a href="<?php echo base_url();?>userregister" class="foge">User Registration Link</a>
                        </label>
                        
                        
                        
                        
                                                   
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    
    
</section>