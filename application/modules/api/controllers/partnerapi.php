<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class partnerapi extends REST_controller
{
    public function chmobileotp_post(){
        $parameter =array('act_mode'=>$this->input->post('act_mode'),
            'row_id'=>$this->input->post('row_id'),
            'p_userid'=>$this->input->post('p_userid'),
            'p_email'=>$this->input->post('p_email'),
            'p_mobilenum'=>$this->input->post('p_mobilenum'),
            'p_mas'=>$this->input->post('p_mas'));
        //$this->response($parameter, 202);exit();
        $data = $this->model_api->call_procedureRow('proc_enquiry',$parameter);
        if(!empty($data)){
            $this->response($data, 202);
        }else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
    }






    public function partnerlogin_post()
    {
        if ($this->input->post('type') == 'web') {
            $parameter = array(
                'act_mode' => $this->input->post('act_mode'),
                'pusername' => $this->input->post('pusername'),
                'ppassword' => ($this->input->post('ppassword')),

            );
        } else {
            $parameter = array(
                'act_mode' => $postdata('act_mode'),
                'pusername' => $postdata('pusername'),
                'ppassword' => ($postdata('ppassword')),

            );
        }
//$this->response($parameter, 202); exit();
        $data = $this->model_api->call_procedurerow('proc_partnerlogin_v', $parameter);
        if (!empty($data)) {
            $data = $this->send_json($data);
            $this->response($data, 202);
        } else {
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }


    }


    public function partnerregister_post()
    {
        if ($this->input->post('type') == 'web') {
            $parameter = array(
                'act_mode' => $this->input->post('act_mode'),
                'title' => $this->input->post('title'),
                'firstname' =>$this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'row_id' => $this->input->post('row_id'),

                'logintype' => $this->input->post('logintype'),
                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => $this->input->post('mobile'),
                'fax' => $this->input->post('fax'),
                'website' => $this->input->post('website'),
                'email' => $this->input->post('email'),
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone  ' => $this->input->post('management_executives_telephone '),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $this->input->post('copypancart'),
                'copyservicetax' => $this->input->post('copyservicetax'),
                'copytan' => $this->input->post('copytan'),
                'copyaddressproof' => $this->input->post('copyaddressproof'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'countryid' => $this->input->post('countryid'),
                'stateid' => $this->input->post('stateid'),
                'cityid' => $this->input->post('cityid'),
                'branch_id' => $this->input->post('branch_id'),
                'locationid' => $this->input->post('locationid'),
                'bank_creditamount' => $this->input->post('bank_creditamount'),
                'bank_agentcommision' => $this->input->post('bank_agentcommision'),
                 'aadhaar_number' => $this->input->post('aadhaar_number'),
                'ppid' => $this->input->post('ppid'),
                'pppermission' => $this->input->post('pppermission'),
            );
        } else {
            $parameter = array(
                'act_mode' => $postdata('act_mode'),
                'title' => $this->$postdata('title'),
                'firstname' =>$this->$postdata('firstname'),
                'lastname' => $this->$postdata('lastname'),
                'row_id' => $this->$postdata('row_id'),
                'logintype' => $this->$postdata('logintype'),
                'agencyname' => $this->$postdata('agencyname'),
                'office_address' => $this->$postdata('office_address'),
                'city' => $this->$postdata('city'),
                'state' => $this->$postdata('state'),
                'pincode' => $this->$postdata('pincode'),
                'telephone' => $this->$postdata('telephone'),
                'mobile' => $this->$postdata('mobile'),
                'fax' => $this->$postdata('fax'),
                'website' => $this->$postdata('website'),
                'email' => $this->$postdata('email'),
                'primarycontact_name' => $this->$postdata('primarycontact_name'),
                'primarycontact_email_mobile' => $this->$postdata('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->$postdata('primarycontact_telephone'),
                'secondrycontact_name' => $this->$postdata('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->$postdata('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->$postdata('secondrycontact_telephone'),
                'management_executives_name' => $this->$postdata('management_executives_name'),
                'management_executives_email_phone' => $this->$postdata('management_executives_email_phone'),
                'management_executives_telephone  ' => $this->$postdata('management_executives_telephone '),
                'branches' => $this->$postdata('branches'),

                'yearofestablism' => $this->$postdata('yearofestablism'),
                'organisationtype' => $this->$postdata('organisationtype'),
                'lstno' => $this->$postdata('lstno'),
                'cstno' => $this->$postdata('cstno'),
                'registrationno' => $this->$postdata('registrationno'),
                'vatno' => $this->$postdata('vatno'),
                'panno' => $this->$postdata('panno'),
                'servicetaxno' => $this->$postdata('servicetaxno'),
                'tanno' => $this->$postdata('tanno'),
                'pfno' => $this->$postdata('pfno'),
                'esisno' => $this->$postdata('esisno'),
                'officeregistrationno' => $this->$postdata('officeregistrationno'),
                'exceptiontax' => $this->$postdata('exceptiontax'),
                'otherexceptiontax' => $this->$postdata('otherexceptiontax'),
                'bank_benificialname' => $this->$postdata('bank_benificialname'),
                'bank_benificialaccno' => $this->$postdata('bank_benificialaccno'),
                'bank_benificialbankname' => $this->$postdata('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->$postdata('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->$postdata('bank_benificialaddress'),
                'bank_benificialifsc' => $this->$postdata('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->$postdata('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->$postdata('bank_benificialibanno'),
                'intermidiatebankname' => $this->$postdata('intermidiatebankname'),
                'intermidiatebankaddress' => $this->$postdata('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->$postdata('intermidiatebankswiftcode'),
                'bank_ecs' => $this->$postdata('bank_ecs'),
                'copypancart' => $this->$postdata('copypancart'),
                'copyservicetax' => $this->$postdata('copyservicetax'),
                'copytan' => $this->$postdata('copytan'),
                'copyaddressproof' => $this->$postdata('copyaddressproof'),
                'emailstatus' => $this->$postdata('emailstatus'),
                'emailcurrency' => $this->$postdata('emailcurrency'),
                'emailcountry' => $this->$postdata('emailcountry'),
                'username' => $this->$postdata('username'),
                'password' => $this->$postdata('password'),
                'countryid' => $this->$postdata('countryid'),
                'stateid' => $this->$postdata('stateid'),
                'cityid' => $this->$postdata('cityid'),
                'branch_id' => $this->$postdata('branch_id'),
                'locationid' => $this->$postdata('locationid'),
                  'aadhaar_number' =>$this->$postdata('aadhaar_number'),
                'ppid' => $this->$postdata('ppid'),
                'pppermission' => $this->$postdata('pppermission'),

            );
        }
//$this->response($parameter, 202);
        
        $data = $this->model_api->call_procedurerow('proc_partnerregister_v', $parameter);
        //$this->response($data, 202); exit();
        if (!empty($data)) {
            $data = $this->send_json($data);
            $this->response($data, 202);
        } else {
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }


    }

    public function partnerregisterpartner_post()
    {
        if ($this->input->post('type') == 'web') {
            $parameter = array(
                'act_mode' => $this->input->post('act_mode'),
                'title' => $this->input->post('title'),
                'firstname' =>$this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'row_id' => $this->input->post('row_id'),

                'logintype' => $this->input->post('logintype'),
                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => $this->input->post('mobile'),
                'fax' => $this->input->post('fax'),
                'website' => $this->input->post('website'),
                'email' => $this->input->post('email'),
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone  ' => $this->input->post('management_executives_telephone '),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $this->input->post('copypancart'),
                'copyservicetax' => $this->input->post('copyservicetax'),
                'copytan' => $this->input->post('copytan'),
                'copyaddressproof' => $this->input->post('copyaddressproof'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'countryid' => $this->input->post('countryid'),
                'stateid' => $this->input->post('stateid'),
                'cityid' => $this->input->post('cityid'),
                'branch_id' => $this->input->post('branch_id'),
                'locationid' => $this->input->post('locationid'),
                'bank_creditamount' => $this->input->post('bank_creditamount'),
                'bank_agentcommision' => $this->input->post('bank_agentcommision'),
                'aadhaar_number' => $this->input->post('aadhaar_number'),
                'ppid' => $this->input->post('ppid'),
                'pppermission' => $this->input->post('pppermission'),
            );
        } else {
            $parameter = array(
                'act_mode' => $postdata('act_mode'),
                'title' => $this->$postdata('title'),
                'firstname' =>$this->$postdata('firstname'),
                'lastname' => $this->$postdata('lastname'),
                'row_id' => $this->$postdata('row_id'),
                'logintype' => $this->$postdata('logintype'),
                'agencyname' => $this->$postdata('agencyname'),
                'office_address' => $this->$postdata('office_address'),
                'city' => $this->$postdata('city'),
                'state' => $this->$postdata('state'),
                'pincode' => $this->$postdata('pincode'),
                'telephone' => $this->$postdata('telephone'),
                'mobile' => $this->$postdata('mobile'),
                'fax' => $this->$postdata('fax'),
                'website' => $this->$postdata('website'),
                'email' => $this->$postdata('email'),
                'primarycontact_name' => $this->$postdata('primarycontact_name'),
                'primarycontact_email_mobile' => $this->$postdata('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->$postdata('primarycontact_telephone'),
                'secondrycontact_name' => $this->$postdata('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->$postdata('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->$postdata('secondrycontact_telephone'),
                'management_executives_name' => $this->$postdata('management_executives_name'),
                'management_executives_email_phone' => $this->$postdata('management_executives_email_phone'),
                'management_executives_telephone  ' => $this->$postdata('management_executives_telephone '),
                'branches' => $this->$postdata('branches'),

                'yearofestablism' => $this->$postdata('yearofestablism'),
                'organisationtype' => $this->$postdata('organisationtype'),
                'lstno' => $this->$postdata('lstno'),
                'cstno' => $this->$postdata('cstno'),
                'registrationno' => $this->$postdata('registrationno'),
                'vatno' => $this->$postdata('vatno'),
                'panno' => $this->$postdata('panno'),
                'servicetaxno' => $this->$postdata('servicetaxno'),
                'tanno' => $this->$postdata('tanno'),
                'pfno' => $this->$postdata('pfno'),
                'esisno' => $this->$postdata('esisno'),
                'officeregistrationno' => $this->$postdata('officeregistrationno'),
                'exceptiontax' => $this->$postdata('exceptiontax'),
                'otherexceptiontax' => $this->$postdata('otherexceptiontax'),
                'bank_benificialname' => $this->$postdata('bank_benificialname'),
                'bank_benificialaccno' => $this->$postdata('bank_benificialaccno'),
                'bank_benificialbankname' => $this->$postdata('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->$postdata('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->$postdata('bank_benificialaddress'),
                'bank_benificialifsc' => $this->$postdata('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->$postdata('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->$postdata('bank_benificialibanno'),
                'intermidiatebankname' => $this->$postdata('intermidiatebankname'),
                'intermidiatebankaddress' => $this->$postdata('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->$postdata('intermidiatebankswiftcode'),
                'bank_ecs' => $this->$postdata('bank_ecs'),
                'copypancart' => $this->$postdata('copypancart'),
                'copyservicetax' => $this->$postdata('copyservicetax'),
                'copytan' => $this->$postdata('copytan'),
                'copyaddressproof' => $this->$postdata('copyaddressproof'),
                'emailstatus' => $this->$postdata('emailstatus'),
                'emailcurrency' => $this->$postdata('emailcurrency'),
                'emailcountry' => $this->$postdata('emailcountry'),
                'username' => $this->$postdata('username'),
                'password' => $this->$postdata('password'),
                'countryid' => $this->$postdata('countryid'),
                'stateid' => $this->$postdata('stateid'),
                'cityid' => $this->$postdata('cityid'),
                'branch_id' => $this->$postdata('branch_id'),
                'locationid' => $this->$postdata('locationid'),
                'aadhaar_number' =>$this->$postdata('aadhaar_number'),
                'ppid' => $this->$postdata('ppid'),
                'pppermission' => $this->$postdata('pppermission'),

            );
        }
//$this->response($parameter, 202);

        $data = $this->model_api->call_procedure('proc_partnerregister_v', $parameter);
        //$this->response($data, 202); exit();
        if (!empty($data)) {
            $data = $this->send_json($data);
            $this->response($data, 202);
        } else {
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }


    }
}//end class
?>